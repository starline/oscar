#!/usr/bin/env bash

BASE_SAVE_DIR=/apollo/data/bag
SEGMENT_SIZE=10240
SEGMENT_DURATION=60

INFO="\
Скрипт по более удобному запуску cyber_recorder\n\
По умолчанию для записей создается папка:\n\
$BASE_SAVE_DIR/{Год}/{Месяц}/{День}/{Время}\n\
Подав первым аргументом название папки, можно заменить {Время} на что-то более осмысленное.\n\
\n\
Скрипт не записывает большие каналы:\n\
Лидары: Fused, Compensator, Scan;\n\
Камеры: Несжатые каналы с камер.\n\
\n\
Максимальный размер одного файла увеличен с 2048 MB (дефолтный в Apollo) до $SEGMENT_SIZE MB.\n\
Максимальная длительность одного файла изменена с 60 сек до $SEGMENT_DURATION сек\
"

for arg in "$@"; do
    ([ "$arg" == "-h" ] || [ "$arg" == "--help" ]) && echo -e $INFO && exit
done

SAVE_DIR=$(date +%Y/%m/%d | tr '[:upper:]' '[:lower:]')
[ $# -eq 0 ] && RECORD_NAME=lite_$(date +%H_%M_%S) || RECORD_NAME=$1

OUTPUT_SAVE_DIR=$BASE_SAVE_DIR/$SAVE_DIR/$RECORD_NAME

OUTPUT_FILENAME=$OUTPUT_SAVE_DIR/$RECORD_NAME.record
echo Saving to $OUTPUT_FILENAME
mkdir -p $OUTPUT_SAVE_DIR

cyber_recorder record \
    --output $OUTPUT_FILENAME \
    --segment-interval $SEGMENT_DURATION \
    --segment-size $SEGMENT_SIZE \
    --white-channel \
    /apollo/audio_detection \
    /apollo/audio_event \
    /apollo/canbus/chassis \
    /apollo/common/latency_records \
    /apollo/common/latency_reports \
    /apollo/control \
    /apollo/control/pad \
    /apollo/dreamview/progress \
    /apollo/drive_event \
    /apollo/hmi/status \
    /apollo/localization/msf_status \
    /apollo/localization/pose \
    /apollo/monitor \
    /apollo/monitor/system_status \
    /apollo/navigation \
    /apollo/perception/obstacles \
    /apollo/perception/traffic_light \
    /apollo/planning \
    /apollo/planning/learning_data \
    /apollo/prediction \
    /apollo/relative_map \
    /apollo/routing_request \
    /apollo/routing_response \
    /apollo/routing_response_history \
    /apollo/sensor/gnss/best_pose \
    /apollo/sensor/gnss/corrected_imu \
    /apollo/sensor/gnss/gnss_status \
    /apollo/sensor/gnss/heading \
    /apollo/sensor/gnss/imu \
    /apollo/sensor/gnss/ins_stat \
    /apollo/sensor/gnss/ins_status \
    /apollo/sensor/gnss/odometry \
    /apollo/sensor/gnss/rtcm_data \
    /apollo/sensor/gnss/rtk_eph \
    /apollo/sensor/gnss/rtk_obs \
    /apollo/sensor/gnss/stream_status \
    /apollo/storytelling \
    /apollo/task_manager \
    /cyberout \
    /tf \
    /tf_static \
    /local_map \
    /oscar/monitor
