#!/usr/bin/env bash

BASE_SAVE_DIR=/apollo/data/bag
SEGMENT_SIZE=10240
SEGMENT_DURATION=60

INFO="\
Скрипт по более удобному запуску cyber_recorder\n\
По умолчанию для записей создается папка:\n\
$BASE_SAVE_DIR/{Год}/{Месяц}/{День}/{Время}\n\
Подав первым аргументом название папки, можно заменить {Время} на что-то более осмысленное.\n\
\n\
Скрипт не записывает большие каналы:\n\
Лидары: Fused, Compensator, Scan;\n\
Камеры: Несжатые каналы с камер.\n\
\n\
Максимальный размер одного файла увеличен с 2048 MB (дефолтный в Apollo) до $SEGMENT_SIZE MB.\n\
Максимальная длительность одного файла изменена с 60 сек до $SEGMENT_DURATION сек\
"

for arg in "$@"; do
    ([ "$arg" == "-h" ] || [ "$arg" == "--help" ]) && echo -e $INFO && exit
done

SAVE_DIR=$(date +%Y/%m/%d | tr '[:upper:]' '[:lower:]')
[ $# -eq 0 ] && RECORD_NAME=all_$(date +%H_%M_%S) || RECORD_NAME=$1

OUTPUT_SAVE_DIR=$BASE_SAVE_DIR/$SAVE_DIR/$RECORD_NAME

OUTPUT_FILENAME=$OUTPUT_SAVE_DIR/$RECORD_NAME.record
echo Saving to $OUTPUT_FILENAME
mkdir -p $OUTPUT_SAVE_DIR

cyber_recorder record \
    --output $OUTPUT_FILENAME \
    --all \
    --segment-interval $SEGMENT_DURATION \
    --segment-size $SEGMENT_SIZE
