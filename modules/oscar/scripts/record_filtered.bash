#!/usr/bin/env bash

BASE_SAVE_DIR=/apollo/data/bag
SEGMENT_SIZE=10240
SEGMENT_DURATION=60

for arg in "$@"; do
    ([ "$arg" == "-h" ] || [ "$arg" == "--help" ]) && echo -e $INFO && exit
done

SAVE_DIR=$(date +%Y/%m/%d | tr '[:upper:]' '[:lower:]')
[ $# -eq 0 ] && RECORD_NAME=filtered_$(date +%H_%M_%S) || RECORD_NAME=$1

OUTPUT_SAVE_DIR=$BASE_SAVE_DIR/$SAVE_DIR/$RECORD_NAME

OUTPUT_FILENAME=$OUTPUT_SAVE_DIR/$RECORD_NAME.record
echo Saving to $OUTPUT_FILENAME
mkdir -p $OUTPUT_SAVE_DIR

# For lidars only /downsampled topics are kept, for cameras - only Compressed

cyber_recorder record \
    --output $OUTPUT_FILENAME \
    --all \
    --segment-interval $SEGMENT_DURATION \
    --segment-size $SEGMENT_SIZE \
    --black-channel \
    /apollo/sensor/lidar/ruby_left/PointCloud2 \
    /apollo/sensor/lidar/ruby_right/PointCloud2 \
    /apollo/sensor/lidar/bpearl_left/PointCloud2 \
    /apollo/sensor/lidar/bpearl_right/PointCloud2 \
    /apollo/sensor/lidar/ruby_left/compensator/PointCloud2 \
    /apollo/sensor/lidar/ruby_right/compensator/PointCloud2 \
    /apollo/sensor/lidar/bpearl_left/compensator/PointCloud2 \
    /apollo/sensor/lidar/bpearl_right/compensator/PointCloud2 \
    /apollo/sensor/camera/left_lower/image \
    /apollo/sensor/camera/left_upper/image \
    /apollo/sensor/camera/right_lower/image \
    /apollo/sensor/camera/right_upper/image

# /apollo/sensor/lidar/ruby_left/PointCloud2/downsampled \
# /apollo/sensor/lidar/ruby_right/PointCloud2/downsampled \
# /apollo/sensor/lidar/bpearl_left/PointCloud2/downsampled \
# /apollo/sensor/lidar/bpearl_right/PointCloud2/downsampled \
