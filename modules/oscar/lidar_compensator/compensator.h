/******************************************************************************
 * Copyright 2017 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include <memory>
#include <string>

#include "Eigen/Eigen"

// Eigen 3.3.7: #define ALIVE (0)
// fastrtps: enum ChangeKind_t { ALIVE, ... };
#if defined(ALIVE)
#undef ALIVE
#endif

#include "cyber/base/thread_pool.h"
#include "modules/common_msgs/sensor_msgs/pointcloud.pb.h"
#include "modules/oscar/lidar_compensator/proto/compensator_config.pb.h"
#include "modules/oscar/utils/transform_buffer/oscar_tf_buffer.h"
namespace apollo {
namespace oscar {

using apollo::cyber::base::ThreadPool;
using apollo::drivers::PointCloud;

class Compensator {
 public:
  explicit Compensator(const apollo::oscar::drivers::CompensatorConfig& config)
      : oscar_tf_buffer_(std::make_shared<OscarTFBuffer>()), config_(config) {
    thread_pool_ =
        std::make_shared<ThreadPool>(std::thread::hardware_concurrency());
  }

  virtual ~Compensator() {}

  bool MotionCompensation(const std::shared_ptr<PointCloud>& msg);

 private:
  /**
   * @brief motion compensation for point cloud
   */
  void MotionCompensation(const std::shared_ptr<PointCloud>& msg,
                          const uint64_t timestamp_min,
                          const uint64_t timestamp_max,
                          const Eigen::Affine3d& pose_min_time,
                          const Eigen::Affine3d& pose_max_time);

  std::shared_ptr<OscarTFBuffer> oscar_tf_buffer_;

  apollo::oscar::drivers::CompensatorConfig config_;
  std::shared_ptr<ThreadPool> thread_pool_ = nullptr;
};
}  // namespace oscar
}  // namespace apollo
