/******************************************************************************
 * Copyright 2017 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#include "modules/oscar/lidar_compensator/compensator_component.h"

#include <memory>

#include "cyber/time/time.h"
#include "modules/common/adapters/adapter_gflags.h"
#include "modules/oscar/lidar_compensator/proto/compensator_config.pb.h"
#include "modules/oscar/utils/point_cloud_pool.h"

using apollo::cyber::Time;

namespace apollo {
namespace oscar {

bool CompensatorComponent::Init() {
  apollo::oscar::drivers::CompensatorConfig config;
  if (!GetProtoConfig(&config)) {
    AWARN << "Load config failed, config file" << ConfigFilePath();
    return false;
  }

  writer_ = node_->CreateWriter<PointCloud>(config.output_channel());
  compensator_.reset(new Compensator(config));
  return true;
}

bool CompensatorComponent::Proc(const std::shared_ptr<PointCloud>& msg) {
  ZoneScopedN("Compensator Proc");

  auto info = "Compensator thread " + node_->Name();
  ZoneText(info.c_str(), info.size());

  auto point_cloud = cloud_pool_.get();
  if (point_cloud == nullptr) point_cloud = std::make_shared<PointCloud>();

  point_cloud->CopyFrom(*msg);

  const auto start_time = Time::Now();
  AINFO << node_->Name() << ": Compensator transport diff: "
        << Time::Now().ToSecond() - point_cloud->header().timestamp_sec();
  AINFO << node_->Name() << ": Compensator transport latency: "
        << apollo::cyber::Time::Now().ToSecond() -
               point_cloud->header().lidar_timestamp() / 1e9;

  if (!compensator_->MotionCompensation(point_cloud)) {
    AERROR << node_->Name() << ": Motion compensation failed";
    return false;
  }

  const auto end_time = Time::Now();
  const auto diff = end_time - start_time;
  const auto meta_diff =
      end_time - Time(point_cloud->header().lidar_timestamp());
  AINFO << "compenstator diff (ms):" << (diff.ToNanosecond() / 1e6)
        << ";meta (ns):" << point_cloud->header().lidar_timestamp()
        << ";meta diff (ms): " << (meta_diff.ToNanosecond() / 1e6);

  point_cloud->mutable_header()->set_sequence_num(seq_);
  point_cloud->mutable_header()->set_timestamp_sec(Time::Now().ToSecond());
  {
    ZoneScopedN("Compensator Write");
    std::string info = "Points: " + std::to_string(point_cloud->point().size());
    ZoneText(info.c_str(), info.size());
    writer_->Write(point_cloud);
  }
  seq_++;

  return true;
}

}  // namespace oscar
}  // namespace apollo
