/******************************************************************************
 * Copyright 2017 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/oscar/lidar_compensator/compensator.h"

#include <omp.h>

#include <limits>
#include <memory>
#include <string>

#include "cyber/cyber.h"
#include "modules/oscar/utils/parallel_for.h"
namespace apollo {
namespace oscar {
using namespace utils;

constexpr static auto kNumThreads = 16;

bool Compensator::MotionCompensation(const std::shared_ptr<PointCloud>& msg) {
  ZoneScoped;
  if (msg->height() == 0 || msg->width() == 0) {
    AERROR << "PointCloud width & height should not be 0";
    return false;
  }
  uint64_t start = cyber::Time::Now().ToNanosecond();

  uint64_t timestamp_min =
      msg->header().lidar_timestamp() - (config_.period() * 1e9);
  uint64_t timestamp_max = msg->header().lidar_timestamp();
  std::string frame_id = msg->header().frame_id();

  msg->mutable_header()->set_timestamp_sec(cyber::Time::Now().ToSecond());
  msg->mutable_header()->set_frame_id(msg->header().frame_id());
  msg->mutable_header()->set_lidar_timestamp(msg->header().lidar_timestamp());
  msg->set_measurement_time(msg->measurement_time());
  msg->set_height(msg->height());
  msg->set_is_dense(msg->is_dense());

  uint64_t new_time = cyber::Time().Now().ToNanosecond();
  AINFO << "compenstator new msg diff:" << new_time - start
        << ";meta:" << msg->header().lidar_timestamp();

  Eigen::Affine3d pose_min_time;
  Eigen::Affine3d pose_max_time;
  try {
    pose_min_time =
        oscar_tf_buffer_->get("world", msg->header().frame_id(),
                              static_cast<double>(timestamp_min) / 1e9);
    pose_max_time =
        oscar_tf_buffer_->get("world", msg->header().frame_id(),
                              static_cast<double>(timestamp_max) / 1e9);
  } catch (tf2::TransformException& e) {
    AERROR << "tf2 error: " << e.what()
           << ", frame id:" << msg->header().frame_id()
           << ", timestamp min:" << timestamp_min / 1e9
           << ", timestamp max:" << timestamp_max / 1e9;
    return false;
  }
  OscarTracyPeriodPlot("compensator period");
  uint64_t tf_time = cyber::Time().Now().ToNanosecond();
  AINFO << "compenstator tf msg diff:" << tf_time - new_time
        << ";meta:" << msg->header().lidar_timestamp();

  MotionCompensation(msg, timestamp_min, timestamp_max, pose_min_time,
                     pose_max_time);
  uint64_t com_time = cyber::Time().Now().ToNanosecond();

  AINFO << "compenstator com msg diff:" << com_time - tf_time
        << ";meta:" << msg->header().lidar_timestamp();
  return true;
}

void normalize_timestamps(const std::shared_ptr<PointCloud>& msg,
                          const uint64_t timestamp_min,
                          const uint64_t timestamp_max) {
  auto time_diff = timestamp_max - timestamp_min;

  for (int i = 0; i < msg->point_size(); ++i) {
    auto& point = (*msg->mutable_point())[i];

    auto frac = static_cast<double>(i) / msg->point_size();
    point.set_timestamp(timestamp_min + time_diff * frac);
  }
}

void Compensator::MotionCompensation(const std::shared_ptr<PointCloud>& msg,
                                     const uint64_t timestamp_min,
                                     const uint64_t timestamp_max,
                                     const Eigen::Affine3d& pose_min_time,
                                     const Eigen::Affine3d& pose_max_time) {
  ZoneScopedN("Motion compensation inner");
  using std::abs;
  using std::acos;
  using std::sin;

  // TODO: do something more sophisticated here.
  // normalize_timestamps(msg, timestamp_min, timestamp_max);

  Eigen::Vector3d translation =
      pose_min_time.translation() - pose_max_time.translation();
  Eigen::Quaterniond q_max(pose_max_time.linear());
  Eigen::Quaterniond q_min(pose_min_time.linear());
  Eigen::Quaterniond q1(q_max.conjugate() * q_min);
  Eigen::Quaterniond q0(Eigen::Quaterniond::Identity());
  q1.normalize();
  translation = q_max.conjugate() * translation;

  double d = q0.dot(q1);
  double abs_d = abs(d);
  double f = 1.0 / static_cast<double>(timestamp_max - timestamp_min);

  auto sz = msg->point_size();

  double theta = acos(abs_d);
  double sin_theta = sin(theta);
  double c1_sign = (d > 0) ? 1 : -1;

  parallel_for(
      sz, kNumThreads,
      [&](int i) {
        auto& point = (*msg->mutable_point())[i];
        if (std::isnan(point.x())) {
          return;
        }

        Eigen::Vector3d p(point.x(), point.y(), point.z());

        uint64_t tp = point.timestamp();
        double t = static_cast<double>(timestamp_max - tp) * f;

        Eigen::Translation3d ti(t * translation);

        double c0 = sin((1 - t) * theta) / sin_theta;
        double c1 = sin(t * theta) / sin_theta * c1_sign;
        Eigen::Quaterniond qi(c0 * q0.coeffs() + c1 * q1.coeffs());

        Eigen::Affine3d trans = ti * qi;
        p = trans * p;

        point.set_x(static_cast<float>(p.x()));
        point.set_y(static_cast<float>(p.y()));
        point.set_z(static_cast<float>(p.z()));
      },
      thread_pool_);
}

}  // namespace oscar
}  // namespace apollo
