///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "modules/oscar/drivers/robosense/proto/oscar_robosense_config.pb.h"
#include "modules/oscar/drivers/robosense/proto/robosense_status_message.pb.h"

#include "cyber/cyber.h"
#include "modules/oscar/drivers/robosense/driver.h"

namespace apollo {
namespace oscar {
namespace drivers {
namespace robosense {

using apollo::cyber::Component;

class RobosenseComponent : public Component<> {
 public:
  ~RobosenseComponent() {}
  bool Init() override;

 private:
  bool checkConfig();

  bool initWriters();

  bool initDriver();

  std::shared_ptr<apollo::cyber::Writer<PointCloud>> pc_writer_;
  std::shared_ptr<apollo::cyber::Writer<RobosenseLidarStatus>> status_writer_;

  std::unique_ptr<RobosenseDriver> driver_;
  RobosenseLidarConfig conf_;
};

CYBER_REGISTER_COMPONENT(RobosenseComponent)

}  // namespace robosense
}  // namespace drivers
}  // namespace oscar
}  // namespace apollo
