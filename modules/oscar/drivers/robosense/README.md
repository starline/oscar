
# Robosense driver

Модуль содержит драйвер лидаров Robosense. 

Функционал:
 - Для RS16, RS Bpearl, RS32, RS128, RS Ruby:
   - получение MSOP пакетов, конструирование облака точек и отправка сообщений в канал Cyber;
   - получение DIFOP пакетов, содержащих калибровочную информацию и состояние лидара;
 - Для RS Bpearl, RS Ruby:
   - изменение настроек на стороне драйвера.

![](imgs/lidars.png)

## Добавление нового лидара для изменения настроек

1. В директории param_setter  по аналогии с ruby и bpearl создать класс, наследующийся от ```BaseParamSetter``` и реализовать функции ```updateParamsImpl```, ```checkParamsImpl```:

```cpp
class RSBpearlParamSetter : public BaseParamSetter<RSBpearlParamSetter> {
 public:
  explicit RSBpearlParamSetter(const LidarSideParams& params);

  friend class BaseParamSetter<RSBpearlParamSetter>;

 private:
  bool updateParamsImpl(const EthPacket& difop_packet);

  bool checkParamsImpl(const EthPacket& difop_packet);

 private:
  LidarParams config_;
};
```

2. Добавить созданный класс к параметрам ParamSetterVariant в файле [param_setter.h](param_setter/param_setter.h):
```cpp
// Extend variant types to use with other Lidar Models
using ParamSetterVariant =
    SetterVariantT<EmptyParamSetter, RSRubyParamSetter, RSBpearlParamSetter>;
```

3. В фабрике по созданию объекта ParamSetter в блоке switch-case добавить опцию для нового лидара:
```cpp
ParamSetterVariant create_param_setter(Model model,
                                       const LidarSideParams& params) {
  switch (model) {
    case Model::MODEL_RSBP:
      return RSBpearlParamSetter(params);
    case Model::MODEL_RS128:
      return RSRubyParamSetter(params);
    case Model::MODEL_RS16:
    case Model::MODEL_RS32:
      std::cerr << "Unsupported lidar model for changing parameters. Supported "
                   "only RSBP and RS128\n";
  }
  return EmptyParamSetter();
}
```
