
pointcloud_channel: "/apollo/sensor/lidar/rs_ruby/PointCloud"
status_channel: "/apollo/sensor/lidar/rs_ruby/status"
msop_port: 6699
# difop_port: 7777
difop_port: 7788
calibration_file: "modules/oscar/drivers/robosense/param/rs128_calib_angles.csv"

model: MODEL_RS128

frame_id: "lidar_ruby"

lidar_params: {
  enable_param_update: false
  rpm: RPM_0
  phase_lock_angle: 3
  return_mode: RM_STRONGEST
  time_sync_mode: TSM_GPS
}
