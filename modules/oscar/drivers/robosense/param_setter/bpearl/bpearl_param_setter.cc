///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/drivers/robosense/param_setter/bpearl/bpearl_param_setter.h"

#include "cyber/cyber.h"
#include "modules/oscar/drivers/robosense/decoder/decoder_bp.hpp"
#include "modules/oscar/drivers/robosense/param_setter/bpearl/bpearl_utils.h"
#include "modules/oscar/drivers/robosense/utils/connection.h"

namespace apollo {
namespace oscar {
namespace drivers {
namespace robosense {

using apollo::drivers::robosense::RSBP_DifopPkt;

RSBpearlParamSetter::RSBpearlParamSetter(const LidarSideParams& params)
    : config_{
          .phase_angle = static_cast<uint16_t>(params.phase_lock_angle()),
          .rotation_speed = static_cast<uint16_t>(params.rpm()),
          .return_mode = params.return_mode(),
          .synchronization_mode = static_cast<uint8_t>(params.time_sync_mode()),
          .fov_start_angle = 0,
          .fov_end_angle = 360} {}

bool RSBpearlParamSetter::updateParamsImpl(const EthPacket& difop_packet) {
  if (recognizePacket(difop_packet) != PacketType::DIFOP) {
    AERROR << "EthPacket is not recognized as DIFOP";
    return false;
  }

  RSBP_DifopPkt difop_parsed = parse_difop<RSBP_DifopPkt>(difop_packet);

  // print_difop(difop_parsed);

  const auto lidar_address = get_ip(difop_parsed.eth.lidar_ip);
  const auto dest_port = to_le(difop_parsed.eth.local_port);

  UDPConnection sock;

  auto new_settings_packet = change_bpearl_settings(difop_packet, config_);

  sock.sendPacket(new_settings_packet, lidar_address, dest_port);

  return true;
}

bool RSBpearlParamSetter::checkParamsImpl(const EthPacket& difop_packet) {
  RSBP_DifopPkt difop_parsed = parse_difop<RSBP_DifopPkt>(difop_packet);

  // print_difop(difop_parsed);

  return params_equal(difop_parsed, config_);
}

}  // namespace robosense
}  // namespace drivers
}  // namespace oscar
}  // namespace apollo
