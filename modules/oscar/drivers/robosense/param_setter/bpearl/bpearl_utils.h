///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <algorithm>

#include "modules/oscar/drivers/robosense/decoder/decoder_bp.hpp"
#include "modules/oscar/drivers/robosense/param_setter/utils.h"
#include "modules/oscar/drivers/robosense/utils/common_types.h"

namespace apollo {
namespace oscar {
namespace drivers {
namespace robosense {

static const std::map<ReturnMode, uint8_t> BpearlReturnModeFlags{
    {ReturnMode::RM_DUAL, 0},
    {ReturnMode::RM_STRONGEST, 1},
    {ReturnMode::RM_LAST, 2},
};

bool params_equal(const RSBP_DifopPkt& difop, const LidarParams& config,
                  bool show_difference = false) {
  std::array comp_results{
      field_equal(to_le(difop.rpm), config.rotation_speed, "RPM",
                  show_difference),
      field_equal(to_le(difop.fov.start_angle) / 100, config.fov_start_angle,
                  "fov_start_angle", show_difference),
      field_equal(to_le(difop.fov.end_angle) / 100, config.fov_end_angle,
                  "fov_end_angle", show_difference),
      field_equal(to_le(difop.lock_phase_angle), config.phase_angle,
                  "phase_angle", show_difference),
      field_equal(static_cast<uint32_t>(difop.return_mode),
                  static_cast<uint32_t>(config.return_mode), "return_mode",
                  show_difference)};

  ADEBUG << "Sync mode not checked for Bpearl lidar";
  return std::all_of(comp_results.begin(), comp_results.end(),
                     [](const auto& el) { return el; });
}

void print_difop(const RSBP_DifopPkt& difop) {
  const auto& ip = difop.eth.lidar_ip;

  bool pps_status = difop.diagno.gps_status & 0x1;

  std::stringstream ss;
  ss << "lock_phase_angle: " << to_le(difop.lock_phase_angle) << "\n";
  ss << "RPM: " << to_le(difop.rpm) << "\n";
  ss << "Lidar IP: " << get_ip(ip) << "\n";
  ss << "MSOP port: " << to_le(difop.eth.local_port) << "\n";
  ss << "DIFOP port: " << to_le(difop.eth.dest_port) << "\n";

  ss << "Time sync status: " << std::boolalpha << pps_status << "\n";

  ss << "Return mode: " << std::to_string(difop.return_mode) << "\n";

  std::cout << ss.str() << "\n";
}

EthPacket change_bpearl_settings(const EthPacket& buffer, LidarParams params) {
  EthPacket new_settings{buffer};

  replace_bytes(new_settings, 0, kUCWPHeader);
  replace_bytes(new_settings, 8, params.rotation_speed);
  replace_bytes(new_settings, 38, params.phase_angle);
  replace_bytes(new_settings, 300,
                BpearlReturnModeFlags.at(params.return_mode));
  replace_bytes(new_settings, 1215, params.synchronization_mode);

  return new_settings;
}

}  // namespace robosense
}  // namespace drivers
}  // namespace oscar
}  // namespace apollo
