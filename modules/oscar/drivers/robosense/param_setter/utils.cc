///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/drivers/robosense/param_setter/utils.h"

#include <algorithm>

#include "modules/oscar/drivers/robosense/decoder/decoder_128.hpp"
#include "modules/oscar/drivers/robosense/decoder/decoder_bp.hpp"

namespace apollo {
namespace oscar {
namespace drivers {
namespace robosense {

using apollo::drivers::robosense::RS128_DifopPkt;
using apollo::drivers::robosense::RSBP_DifopPkt;

std::string get_ip(const uint8_t* buffer) {
  return {std::to_string(buffer[0]) + "." + std::to_string(buffer[1]) + "." +
          std::to_string(buffer[2]) + "." + std::to_string(buffer[3])};
}

void print_meta(const EthPacket& packet) {
  std::cout << "Header: " << std::hex
            << *reinterpret_cast<const uint64_t*>(packet.data());
  std::cout << " Tail: " << std::hex
            << *reinterpret_cast<const uint64_t*>(packet.data() +
                                                  packet.size() - 2);
  std::cout << " Size: " << std::dec << packet.size() << "\n";
}

PacketType recognizePacket(const EthPacket& packet) {
  if (check_packet(packet, kMSOPHeader, kLidarPacketLength))
    return PacketType::MSOP;
  else if (check_packet(packet, kDIFOPHeader, kLidarPacketLength))
    return PacketType::DIFOP;
  else if (check_packet(packet, kUCWPHeader, kLidarPacketLength))
    return PacketType::PseudoUCWP;
  else
    return PacketType::UNKNOWN;
}

}  // namespace robosense
}  // namespace drivers
}  // namespace oscar
}  // namespace apollo
