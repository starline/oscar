///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "modules/oscar/drivers/robosense/decoder/decoder_128.hpp"
#include "modules/oscar/drivers/robosense/decoder/decoder_bp.hpp"
#include "modules/oscar/drivers/robosense/utils/common_types.h"

namespace apollo {
namespace oscar {
namespace drivers {
namespace robosense {

using apollo::drivers::robosense::RS128_DifopPkt;
using apollo::drivers::robosense::RSBP_DifopPkt;

// convert to big endian
template <typename T>
T to_be(T value) {
  if constexpr (sizeof(T) == 2) {
    value = htobe16(value);
  } else if constexpr (sizeof(T) == 4) {
    value = htobe32(value);
  } else if constexpr (sizeof(T) == 8) {
    value = htobe64(value);
  } else if constexpr (sizeof(T) != 1) {
    std::cout << "size of T is " << sizeof(T) << "\n";
    throw std::runtime_error("Size of T is not 16/32/64");
  }

  return value;
}

// convert to little endian
template <typename T>
T to_le(T value) {
  if constexpr (sizeof(T) == 1) {
    return value;
  } else if constexpr (sizeof(T) == 2) {
    value = be16toh(value);
  } else if constexpr (sizeof(T) == 4) {
    value = be32toh(value);
  } else if constexpr (sizeof(T) == 8) {
    value = be64toh(value);
  } else {
    std::cout << "size of T is " << sizeof(T) << "\n";
    throw std::runtime_error("Size of T is not 16/32/64");
  }
  return value;
}

template <typename HeaderT>
bool check_packet(const EthPacket& packet, HeaderT header, size_t length) {
  return packet.size() == length &&
         *reinterpret_cast<const HeaderT*>(packet.data()) == to_be(header);
}

template <typename HeaderT, typename TailT>
bool check_packet(const EthPacket& packet, HeaderT header, TailT tail,
                  size_t length) {
  return check_packet(packet, header, length) &&
         *reinterpret_cast<const TailT*>(packet.data() + packet.size() - 2) ==
             to_be(tail);
}

template <typename T>
T parse_difop(const EthPacket& difop_packet) {
  T difop;
  memcpy(&difop, difop_packet.data(), sizeof(T));

  return difop;
}

template <typename T, typename U>
bool field_equal(const T& lhs, const U& rhs, const std::string& field_name,
                 bool show_difference = false) {
  if (lhs != rhs) {
    AWARN << "Config param " << field_name << " with value " << lhs
          << " != " << rhs;
    return false;
  }
  return true;
};

template <typename T, typename ContainerT>
void replace_bytes(ContainerT& buffer, size_t position, T new_value) {
  new_value = to_be(new_value);

  if (buffer.size() - position < sizeof(T))
    throw std::runtime_error(
        "Buffer overflow");  // TODO: replace with Cyber ACHECK

  std::memcpy(&buffer[position], &new_value, sizeof(T));
}

PacketType recognizePacket(const EthPacket& packet);

void print_meta(const EthPacket& packet);

std::string get_ip(const uint8_t* buffer);

}  // namespace robosense
}  // namespace drivers
}  // namespace oscar
}  // namespace apollo
