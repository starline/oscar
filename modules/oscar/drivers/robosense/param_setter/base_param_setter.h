///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <variant>

#include "modules/oscar/drivers/robosense/utils/common_types.h"

namespace apollo {
namespace oscar {
namespace drivers {
namespace robosense {

template <typename T>
class BaseParamSetter {
 public:
  BaseParamSetter() {}

  bool updateParams(const EthPacket& difop_packet) {
    return static_cast<T*>(this)->updateParamsImpl(difop_packet);
  }

  bool checkParams(const EthPacket& difop_packet) {
    return static_cast<T*>(this)->checkParamsImpl(difop_packet);
  }

 protected:
  ~BaseParamSetter() {}
};

class EmptyParamSetter : public BaseParamSetter<EmptyParamSetter> {
 public:
  EmptyParamSetter() {}
  bool updateParamsImpl(const EthPacket&) { return false; }
  bool checkParamsImpl(const EthPacket&) { return false; }
};

template <typename... Ts>
using SetterVariantT = std::variant<
    std::enable_if_t<std::is_base_of<BaseParamSetter<Ts>, Ts>::value, Ts>...>;

}  // namespace robosense
}  // namespace drivers
}  // namespace oscar
}  // namespace apollo
