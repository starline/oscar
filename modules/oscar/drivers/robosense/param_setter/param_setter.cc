///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/drivers/robosense/param_setter/param_setter.h"

#include <variant>

namespace apollo {
namespace oscar {
namespace drivers {
namespace robosense {
namespace {

ParamSetterVariant create_param_setter(Model model,
                                       const LidarSideParams& params) {
  switch (model) {
    case Model::MODEL_RSBP:
      return RSBpearlParamSetter(params);
    case Model::MODEL_RS128:
      return RSRubyParamSetter(params);
    case Model::MODEL_RS16:
    case Model::MODEL_RS32:
      std::cerr << "Unsupported lidar model for changing parameters. Supported "
                   "only RSBP and RS128\n";
  }
  return EmptyParamSetter();
}
}  // namespace

ParamSetter::ParamSetter(Model model, const LidarSideParams& params)
    : setter_(create_param_setter(model, params)) {}

bool ParamSetter::checkParams(const EthPacket& difop_packet) {
  return std::visit(
      [&](auto&& setter) { return setter.checkParams(difop_packet); }, setter_);
}

bool ParamSetter::updateParams(const EthPacket& difop_packet) {
  return std::visit(
      [&](auto&& setter) { return setter.updateParams(difop_packet); },
      setter_);
}

}  // namespace robosense
}  // namespace drivers
}  // namespace oscar
}  // namespace apollo
