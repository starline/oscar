///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "modules/oscar/drivers/robosense/proto/oscar_robosense_config.pb.h"

#include "modules/oscar/drivers/robosense/param_setter/base_param_setter.h"
#include "modules/oscar/drivers/robosense/param_setter/ruby/ruby_utils.h"
#include "modules/oscar/drivers/robosense/utils/connection.h"

namespace apollo {
namespace oscar {
namespace drivers {
namespace robosense {

using apollo::drivers::robosense::RS128_DifopPkt;

class RSRubyParamSetter : public BaseParamSetter<RSRubyParamSetter> {
  friend class BaseParamSetter<RSRubyParamSetter>;

 public:
  explicit RSRubyParamSetter(const LidarSideParams& params);

 private:
  bool updateParamsImpl(const EthPacket& difop_packet);

  bool checkParamsImpl(const EthPacket& difop_packet);

 private:
  bool checkParamsDifop(const RS128_DifopPkt& difop);

 private:
  LidarParams config_;
};

}  // namespace robosense
}  // namespace drivers
}  // namespace oscar
}  // namespace apollo
