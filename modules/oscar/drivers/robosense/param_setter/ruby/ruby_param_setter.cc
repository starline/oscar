///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/drivers/robosense/param_setter/ruby/ruby_param_setter.h"

#include "modules/oscar/drivers/robosense/proto/oscar_robosense_config.pb.h"

#include "cyber/cyber.h"
#include "modules/oscar/drivers/robosense/param_setter/base_param_setter.h"
#include "modules/oscar/drivers/robosense/param_setter/ruby/ruby_utils.h"
#include "modules/oscar/drivers/robosense/utils/connection.h"

namespace apollo {
namespace oscar {
namespace drivers {
namespace robosense {

namespace HTTPResponse {
static constexpr auto OK = 200;
static constexpr auto TIMEOUT = 504;
}  // namespace HTTPResponse

static constexpr auto kDefaultHTTPPort = 80;

static constexpr auto kHTTPConnectTimeoutSec = 5;

RSRubyParamSetter::RSRubyParamSetter(const LidarSideParams& params)
    : config_{
          .phase_angle = static_cast<uint16_t>(params.phase_lock_angle()),
          .rotation_speed = static_cast<uint16_t>(params.rpm()),
          // .return_mode = static_cast<uint8_t>(params.return_mode()),
          .return_mode = params.return_mode(),
          .synchronization_mode = static_cast<uint8_t>(params.time_sync_mode()),
          .fov_start_angle = 0,
          .fov_end_angle = 360} {}

bool RSRubyParamSetter::updateParamsImpl(const EthPacket& difop_packet) {
  if (recognizePacket(difop_packet) != PacketType::DIFOP) {
    AERROR << "EthPacket is not recognized as DIFOP";
    return false;
  }

  RS128_DifopPkt difop_parsed = parse_difop<RS128_DifopPkt>(difop_packet);

  if (!check_default_firmware_version(difop_parsed)) {
    AWARN << "Version of Lidar firmware differs from default one. Some fields "
             "can be parsed incorrectly.";
  }

  auto lidar_address = get_ip(difop_parsed.eth.lidar_ip);

  auto current_params = get_current_params(difop_parsed);

  if (checkParamsDifop(difop_parsed)) {
    ADEBUG << "Params already up to date";
    return true;
  }

  auto new_params =
      update_params(current_params, config_, ParamRemapper(difop_parsed));

  auto http_packet = HTTPPacketBuilder::build_packet(new_params);

  HTTPClientConnection sock(lidar_address, kDefaultHTTPPort);
  if (!sock.waitForConnection(kHTTPConnectTimeoutSec)) return false;

  auto response = sock.send(http_packet);

  auto status = check_http_answer(response);
  if (status != HTTPResponse::OK)
    AERROR << "Response code returned from lidar: " << status;

  return status == HTTPResponse::OK;
}

bool RSRubyParamSetter::checkParamsImpl(const EthPacket& difop_packet) {
  if (recognizePacket(difop_packet) != PacketType::DIFOP) {
    AERROR << "EthPacket is not recognized as DIFOP";
    return false;
  }

  RS128_DifopPkt difop_parsed = parse_difop<RS128_DifopPkt>(difop_packet);

  return checkParamsDifop(difop_parsed);
}

bool RSRubyParamSetter::checkParamsDifop(const RS128_DifopPkt& difop) {
  return params_equal(difop, config_);
}

}  // namespace robosense
}  // namespace drivers
}  // namespace oscar
}  // namespace apollo
