///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once
#include <cstdint>
#include <string>
#include <unordered_map>

#include "modules/oscar/drivers/robosense/decoder/decoder_128.hpp"
#include "modules/oscar/drivers/robosense/param_setter/utils.h"

namespace apollo {
namespace oscar {
namespace drivers {
namespace robosense {

using apollo::drivers::robosense::RS128_DifopPkt;

using RubyParameters = std::unordered_map<std::string, std::string>;

RubyParameters get_current_params(const RS128_DifopPkt& difop);

template <typename T>
using is_string =
    std::is_same<std::remove_const_t<std::remove_reference_t<T>>, std::string>;

using EnumMap = std::map<uint8_t, std::string>;

class ParamRemapper {
 public:
  ParamRemapper(const RS128_DifopPkt& difop);

  std::string remapSyncMode(uint8_t sync_mode) const;

  std::string remapConfigSyncMode(uint8_t sync_mode) const;

  std::string remapReturnMode(uint8_t return_mode) const;

 private:
  const std::string version_;

  const EnumMap time_sync_modes_;
  const EnumMap return_modes_;
  const EnumMap config_sync_modes_remap_;
};

RubyParameters update_params(const RubyParameters& current_params,
                             const LidarParams& config,
                             const ParamRemapper& param_remapper);

uint32_t check_http_answer(const std::string& response);

void print_difop(const RS128_DifopPkt& difop);

bool params_equal(const RS128_DifopPkt& difop, const LidarParams& config,
                  bool show_difference = false);

bool check_default_firmware_version(const RS128_DifopPkt& difop);

namespace HTTPPacketBuilder {

std::string build_content(const RubyParameters& params,
                          const std::string& boundary_val);

std::string build_header(const RubyParameters& params, size_t content_length,
                         const std::string& boundary_val);

std::string build_packet(const RubyParameters& params);

};  // namespace HTTPPacketBuilder

}  // namespace robosense
}  // namespace drivers
}  // namespace oscar
}  // namespace apollo
