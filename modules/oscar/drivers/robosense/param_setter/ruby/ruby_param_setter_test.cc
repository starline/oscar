///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include <regex>

#include "gtest/gtest.h"

#include "modules/oscar/drivers/robosense/param_setter/ruby/ruby_utils.h"

namespace apollo {
namespace oscar {
namespace drivers {
namespace robosense {

TEST(RubyParamSetterTest, PacketBuilderTest) {
  std::regex http_pattern(
      "POST [a-zA-Z\\/\\-\\._]+ HTTP\\/1.1\r\n"
      "Host: [0-9]{0,3}\\.[0-9]{0,3}\\.[0-9]{0,3}\\.[0-9]{0,3}\r\n"
      "Origin: [0-9]{0,3}\\.[0-9]{0,3}\\.[0-9]{0,3}\\.[0-9]{0,3}\r\n"
      "Content-Type: multipart\\/form-data; boundary=[0-9a-zA-Z]+\r\n"
      "Content-Length: [0-9]+\r\n"
      "Connection: close\r\n\r\n"
      "(--[0-9]+\r\n"
      "Content-Disposition: form-data; name=\"[a-zA-Z\\_]+\"\r\n"
      "\r\n"
      "[0-9a-zA-Z\\_\\ \\.]+"
      "\r\n)+"
      "--[0-9]+\r\n");

  RubyParameters params{
      {"device_ip_addr", "1.1.1.1"},
      {"device_ip_mask", "1.1.1.1"},
      {"device_ip_gateway", "1.1.1.1"},
      {"des_ip_addr", "1.1.1.1"},
      {"lidar_msop_port", "0000"},
      {"lidar_difop_port", "0000"},
      {"RetModeSel", "Strong"},
      {"lidar_fov_start", "0"},
      {"lidar_fov_end", "360"},
      {"phase_lock", "180"},
      {"rotation_speed", "300"},
      {"TimeSyncSrc", "1"},
      {"OperateMode", "2"},
      {"save_param", "Save"},
  };

  auto http_packet = HTTPPacketBuilder::build_packet(params);

  EXPECT_TRUE(std::regex_match(http_packet, http_pattern));
}

}  // namespace robosense
}  // namespace drivers
}  // namespace oscar
}  // namespace apollo
