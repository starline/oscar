///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/drivers/robosense/param_setter/ruby/ruby_utils.h"

#include <algorithm>
#include <cstdint>
#include <map>
#include <regex>
#include <string>
#include <type_traits>
#include <unordered_map>

using apollo::drivers::robosense::RS_CorAngle;
using apollo::drivers::robosense::RS_Diagno;
using apollo::drivers::robosense::RS_FOV;
using apollo::drivers::robosense::RS_SN;
using apollo::drivers::robosense::RS_Status;
using apollo::drivers::robosense::RS_Timestamp;

namespace apollo {
namespace oscar {
namespace drivers {
namespace robosense {

using apollo::drivers::robosense::RS128_DifopPkt;

namespace FirmwareVersionDefault {

static constexpr auto kVersion = "67437056";

static const EnumMap time_sync_modes{
    {0, "NULL"},
    {2, "GPS"},
    {4, "PTP"},
};

static const EnumMap return_modes{
    {1, "Strong"},
    {2, "Last"},
    {3, "Dual"},
};

static const EnumMap config_sync_remap =
    time_sync_modes;  // no remap for default version

}  // namespace FirmwareVersionDefault

namespace FirmwareVersion67437056 {

static constexpr auto kVersion = "67437056";
static const EnumMap time_sync_modes{
    {0, "GPS"},
    {1, "PTP-E2E"},
    {2, "PTP-P2P"},
    {3, "PTP-GPTP"},
};

static const EnumMap config_sync_remap{
    {0, "GPS"},  // For newer versions of Ruby there is no option for NULL time
                 // sync.
    {2, "GPS"},
    {4, "PTP-E2E"}};

}  // namespace FirmwareVersion67437056

std::string get_version(const RS128_DifopPkt& difop) {
  uint64_t tmp = 0;
  const auto& field = difop.version.bot_firmware_ver;
  std::memcpy(&tmp, &field, sizeof(field));

  return std::to_string(tmp);
}

EnumMap choose_time_sync_modes(const std::string& version) {
  if (version == FirmwareVersion67437056::kVersion)
    return FirmwareVersion67437056::time_sync_modes;

  return FirmwareVersionDefault::time_sync_modes;
}

EnumMap choose_return_modes(const std::string& version) {
  // Empty function, for possibible extensions

  return FirmwareVersionDefault::return_modes;
}

EnumMap choose_config_sync_remap(const std::string& version) {
  if (version == FirmwareVersion67437056::kVersion)
    return FirmwareVersion67437056::config_sync_remap;

  return FirmwareVersionDefault::config_sync_remap;
}

ParamRemapper::ParamRemapper(const RS128_DifopPkt& difop)
    : version_(get_version(difop)),
      time_sync_modes_(choose_time_sync_modes(version_)),
      return_modes_(choose_return_modes(version_)),
      config_sync_modes_remap_(choose_config_sync_remap(version_)) {}

std::string ParamRemapper::remapSyncMode(uint8_t sync_mode) const {
  return time_sync_modes_.at(sync_mode);
}

std::string ParamRemapper::remapConfigSyncMode(uint8_t sync_mode) const {
  return config_sync_modes_remap_.at(sync_mode);
}

std::string ParamRemapper::remapReturnMode(uint8_t return_mode) const {
  return return_modes_.at(return_mode);
}

static constexpr auto kDefaultMask = "255.255.255.0";
static constexpr auto kDefaultGateway = "192.168.1.1";
static constexpr auto kDefaultOperateMode = "High Performance";

using RubyParameters = std::unordered_map<std::string, std::string>;

RubyParameters get_current_params(const RS128_DifopPkt& difop) {
  ParamRemapper param_remapper(difop);

  return {
      {"device_ip_addr", get_ip(difop.eth.lidar_ip)},
      // {"device_ip_mask", kDefaultMask},
      // {"device_ip_gateway", kDefaultGateway},
      {"des_ip_addr", get_ip(difop.eth.dest_ip)},
      {"lidar_msop_port", std::to_string(to_le(difop.eth.msop_port))},
      {"lidar_difop_port", std::to_string(to_le(difop.eth.difop_port))},
      {"RetModeSel", param_remapper.remapReturnMode(difop.return_mode)},
      {"lidar_fov_start", std::to_string(to_le(difop.fov.start_angle) / 100)},
      {"lidar_fov_end", std::to_string(to_le(difop.fov.end_angle) / 100)},
      {"phase_lock", std::to_string(to_le(difop.lock_phase_angle))},
      {"rotation_speed", std::to_string(to_le(difop.rpm))},
      {"TimeSyncSrc", param_remapper.remapSyncMode(difop.time_info.sync_mode)},
      // {"TimeSyncSrc", time_sync_modes.at(difop.time_info.sync_mode)},
      // {"OperateMode", kDefaultOperateMode},
      {"save_param", "Save"},
      // {"tabs-two", "on"},
      {"noise_filter_switch", "on"},
      // {"mode1", "Mode1(+25%)"},
  };
}

RubyParameters update_params(const RubyParameters& current_params,
                             const LidarParams& config,
                             const ParamRemapper& param_remapper) {
  RubyParameters new_params(current_params);

  auto replace = [&](const auto& name, const auto& value) {
    auto& it = new_params[name];

    if constexpr (is_string<decltype(value)>())
      it = value;
    else
      it = std::to_string(value);
  };

  replace("phase_lock", config.phase_angle);
  replace("rotation_speed", config.rotation_speed);
  replace("RetModeSel", param_remapper.remapReturnMode(config.return_mode));
  replace("TimeSyncSrc",
          param_remapper.remapConfigSyncMode(config.synchronization_mode));
  // replace("TimeSyncSrc", time_sync_modes.at(config.synchronization_mode));
  replace("lidar_fov_start", config.fov_start_angle);
  replace("lidar_fov_end", config.fov_end_angle);

  return new_params;
}

static const std::regex http_reponse_regex(
    "HTTP/([0-9]\\.[0-9]) ([0-9]{3}) ([a-zA-Z]+)((.|\n|\r)+)");

uint32_t check_http_answer(const std::string& response) {
  std::smatch pieces_match;

  // std::cout << response << "\n";

  if (std::regex_match(response, pieces_match, http_reponse_regex)) {
    auto response_code = std::stoi(pieces_match[2].str());

    return response_code;
  } else {
    std::cerr << "Cannot parse response: " << response << "\n";
    return 0;
  }
}

void print_difop(const RS128_DifopPkt& difop) {
  const auto& ip = difop.eth.lidar_ip;

  ParamRemapper param_remapper(difop);

  std::stringstream ss;
  ss << "lock_phase_angle: " << to_le(difop.lock_phase_angle) << "\n";
  ss << "RPM: " << to_le(difop.rpm) << "\n";
  ss << "Lidar IP: " << get_ip(ip) << "\n";
  ss << "MSOP port: " << to_le(difop.eth.msop_port) << "\n";
  ss << "DIFOP port: " << to_le(difop.eth.difop_port) << "\n";

  ss << "Time sync mode: "
     << param_remapper.remapSyncMode(difop.time_info.sync_mode)
     // ss << "Time sync mode: " <<
     // time_sync_modes.at(difop.time_info.sync_mode)
     << "\n";
  ss << "Time sync status: " << std::boolalpha
     << static_cast<bool>(difop.time_info.sync_sts) << "\n";

  ss << "GPS Status: " << std::boolalpha
     << static_cast<bool>(to_le(difop.diagno.gps_status)) << "\n";

  ss << "Return mode: " << std::to_string(difop.return_mode) << "\n";

  std::cout << ss.str() << "\n";
}

bool params_equal(const RS128_DifopPkt& difop, const LidarParams& config,
                  bool show_difference) {
  ParamRemapper param_remapper(difop);

  std::array comp_results{
      field_equal(to_le(difop.rpm), config.rotation_speed, "RPM",
                  show_difference),
      field_equal(to_le(difop.fov.start_angle) / 100, config.fov_start_angle,
                  "fov_start_angle", show_difference),
      field_equal(to_le(difop.fov.end_angle) / 100, config.fov_end_angle,
                  "fov_end_angle", show_difference),
      field_equal(to_le(difop.lock_phase_angle), config.phase_angle,
                  "phase_angle", show_difference),
      field_equal(param_remapper.remapReturnMode(difop.return_mode),
                  param_remapper.remapReturnMode(config.return_mode),
                  "return_mode", show_difference),
      field_equal(
          param_remapper.remapSyncMode(difop.time_info.sync_mode),
          param_remapper.remapConfigSyncMode(config.synchronization_mode),
          // field_equal(time_sync_modes.at(difop.time_info.sync_mode),
          //             time_sync_modes.at(config.synchronization_mode),
          "TimeSyncSrc", show_difference)};
  return std::all_of(comp_results.begin(), comp_results.end(),
                     [](const auto& el) { return el; });
}

bool check_default_firmware_version(const RS128_DifopPkt& difop) {
  return get_version(difop) == FirmwareVersionDefault::kVersion;
}

namespace HTTPPacketBuilder {

static constexpr auto kDefaultBoundaryVal = "0123456789";

std::string build_content(const RubyParameters& params,
                          const std::string& boundary_val) {
  std::stringstream ss;

  ss << "--" << boundary_val << "\r\n";

  for (const auto& [key, val] : params) {
    ss << "Content-Disposition: form-data; name=\"" << key << "\"\r\n\r\n";
    ss << val << "\r\n";
    ss << "--" << boundary_val << "\r\n";
  }
  return ss.str();
}

std::string build_header(const RubyParameters& params, size_t content_length,
                         const std::string& boundary_val) {
  const auto src_ip = params.at("des_ip_addr");
  const auto dest_ip = params.at("device_ip_addr");

  std::stringstream ss;

  ss << "POST /cgi-bin/param_setting.cgi HTTP/1.1\r\n";
  ss << "Host: " << dest_ip << "\r\n";
  ss << "Origin: " << src_ip << "\r\n";
  ss << "Content-Type: multipart/form-data; boundary=" << boundary_val
     << "\r\n";

  ss << "Content-Length: " << content_length << "\r\n";

  ss << "Connection: keep-alive\r\n";  // check connection type:
                                       // close/keep-alive
  ss << "\r\n";

  return ss.str();
}

std::string build_packet(const RubyParameters& params) {
  const auto content = build_content(params, kDefaultBoundaryVal);
  const auto header = build_header(params, content.size(), kDefaultBoundaryVal);

  return header + content;
}

};  // namespace HTTPPacketBuilder

}  // namespace robosense
}  // namespace drivers
}  // namespace oscar
}  // namespace apollo
