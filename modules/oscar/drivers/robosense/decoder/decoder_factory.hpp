/******************************************************************************
 * Copyright 2020 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#pragma once
#include <memory>
#include <string>

#include "modules/oscar/drivers/robosense/decoder/decoder_128.hpp"
#include "modules/oscar/drivers/robosense/proto/oscar_robosense_config.pb.h"
// #include "modules/oscar/drivers/robosense/decoder/decoder_16.hpp"
// #include "modules/oscar/drivers/robosense/decoder/decoder_32.hpp"
#include "modules/oscar/drivers/robosense/decoder/decoder_bp.hpp"

namespace apollo {
namespace drivers {
namespace robosense {

struct PointXYZIT {};

template <typename vpoint>
class DecoderFactory {
 public:
  static std::unique_ptr<DecoderBase<vpoint>> createDecoder(
      apollo::oscar::drivers::robosense::Model lidar_type,
      const RS_Param& param, const std::string& calibration_file) {
    using apollo::oscar::drivers::robosense::Model;

    std::unique_ptr<DecoderBase<vpoint>> decoder;

    switch (lidar_type) {
      // case Model::MODEL_RS16:
      //   decoder = std::make_unique<Decoder16<vpoint>>(param);
      //   break;
      // case Model::MODEL_RS32:
      //   decoder = std::make_unique<Decoder32<vpoint>>(param);
      //   break;
      case Model::MODEL_RSBP:
        decoder = std::make_unique<DecoderBP<vpoint>>(param);
        break;
      case Model::MODEL_RS128:
        decoder = std::make_unique<Decoder128<vpoint>>(param);
        break;
      default:
        decoder = nullptr;
    }

    if (decoder != nullptr) decoder->loadCalibrationFile(calibration_file);
    return decoder;
  }
};

}  // namespace robosense
}  // namespace drivers
}  // namespace apollo
