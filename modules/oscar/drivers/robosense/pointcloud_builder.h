///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include <optional>

#include "modules/common_msgs/sensor_msgs/pointcloud.pb.h"
#include "modules/oscar/drivers/robosense/decoder/decoder_factory.hpp"
#include "modules/oscar/drivers/robosense/proto/oscar_robosense_config.pb.h"
#include "modules/oscar/drivers/robosense/proto/robosense_status_message.pb.h"

namespace apollo {
namespace oscar {
namespace drivers {
namespace robosense {

using apollo::drivers::PointCloud;
using apollo::drivers::PointXYZIT;

using apollo::drivers::robosense::DecoderBase;
using apollo::drivers::robosense::DecoderFactory;
using apollo::drivers::robosense::E_DECODER_RESULT;
using apollo::drivers::robosense::LidarStatus;

class PointCloudBuilder {
 public:
  explicit PointCloudBuilder(const RobosenseLidarConfig& config)
      : accumulated_msg_(new PointCloud),
        params_{.echo = config.lidar_params().return_mode(),
                .cut_angle = config.driver_params().cut_angle(),
                .max_distance = config.driver_params().max_distance(),
                .min_distance = config.driver_params().min_distance(),
                .start_angle = config.driver_params().start_angle(),
                .end_angle = config.driver_params().end_angle(),
                .use_lidar_clock = config.driver_params().use_lidar_clock()},
        packet_decoder_(DecoderFactory<PointXYZIT>::createDecoder(
            config.model(), params_, config.calibration_file())),
        frame_id_(config.frame_id()),
        use_lidar_clock_(config.driver_params().use_lidar_clock()),
        remove_nan_points_(config.driver_params().remove_nan_points()) {}

  std::optional<std::shared_ptr<PointCloud>> processMsopPkt(
      const EthPacket& packet) {
    ZoneScoped;
    auto point_vec_ptr = std::make_shared<std::vector<PointXYZIT>>();
    auto height_ptr = std::make_shared<int>();

    E_DECODER_RESULT ret;
    {
      std::lock_guard lg(process_mutex_);
      ret = packet_decoder_->processMsopPkt(packet.data(), point_vec_ptr,
                                            height_ptr);
    }
    if (!(ret == E_DECODER_RESULT::E_DECODE_OK ||
          ret == E_DECODER_RESULT::E_FRAME_SPLIT))
      return std::nullopt;

    for (auto& iter : *point_vec_ptr) {
      if (remove_nan_points_ && (std::isnan(iter.x()) || std::isnan(iter.y()) ||
                                 std::isnan(iter.z())))
        continue;

      PointXYZIT* point = accumulated_msg_->add_point();
      point->CopyFrom(iter);  // todo: check if we can do move here
    }

    if (ret != E_DECODER_RESULT::E_FRAME_SPLIT) return std::nullopt;

    accumulated_msg_->set_height(*height_ptr);
    preparePointsMsg(accumulated_msg_);

    if (use_lidar_clock_) {
      const auto timestamp =
          accumulated_msg_
              ->point(static_cast<int>(accumulated_msg_->point_size()) - 1)
              .timestamp();
      accumulated_msg_->set_measurement_time(static_cast<double>(timestamp) /
                                             1e9);

      accumulated_msg_->mutable_header()->set_lidar_timestamp(timestamp);
    }

    auto ret_msg =
        std::make_optional<std::shared_ptr<PointCloud>>(accumulated_msg_);

    auto arena = std::make_shared<google::protobuf::Arena>();

    accumulated_msg_.reset(
        google::protobuf::Arena::CreateMessage<PointCloud>(arena.get()),
        [arena](PointCloud* msg) {});

    return ret_msg;
  }

  std::optional<RobosenseLidarStatus> processDifopPkt(const EthPacket& packet) {
    LidarStatus lidar_status;
    {
      std::lock_guard lg(process_mutex_);
      if (packet_decoder_->processDifopPkt(packet.data(), &lidar_status) < 0)
        return std::nullopt;
    }

    RobosenseLidarStatus msg_status;

    msg_status.set_temperature(lidar_status.temperature);
    msg_status.set_is_synced(lidar_status.synced);

    return std::make_optional<RobosenseLidarStatus>(msg_status);
  }

 private:
  void preparePointsMsg(std::shared_ptr<PointCloud> msg) {
    msg->set_width(msg->point_size() / msg->height());
    msg->set_is_dense(false);
    msg->mutable_header()->set_sequence_num(++points_seq_);
    msg->mutable_header()->set_frame_id(frame_id_);
    msg->mutable_header()->set_timestamp_sec(cyber::Time().Now().ToSecond());
    msg->mutable_header()->set_lidar_timestamp(
        cyber::Time().Now().ToNanosecond());

    if (msg->point_size() > 0) {
      const auto timestamp =
          msg->point(static_cast<int>(msg->point_size()) - 1).timestamp();
      msg->set_measurement_time(static_cast<double>(timestamp) / 1e9);
    }
  }
  std::shared_ptr<PointCloud> accumulated_msg_;

  apollo::drivers::robosense::RS_Param params_;
  std::unique_ptr<DecoderBase<PointXYZIT>> packet_decoder_;

  uint32_t points_seq_ = 0;
  std::string frame_id_;
  bool use_lidar_clock_;
  bool remove_nan_points_;

  TracyLockable(std::mutex, process_mutex_);
};

}  // namespace robosense
}  // namespace drivers
}  // namespace oscar
}  // namespace apollo
