///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <chrono>
#include <thread>

#include "cyber/base/concurrent_object_pool.h"
#include "cyber/base/thread_safe_queue.h"
#include "cyber/cyber.h"
#include "modules/common_msgs/sensor_msgs/pointcloud.pb.h"
#include "modules/oscar/drivers/robosense/param_setter/param_setter.h"
#include "modules/oscar/drivers/robosense/pointcloud_builder.h"
#include "modules/oscar/drivers/robosense/proto/oscar_robosense_config.pb.h"
#include "modules/oscar/drivers/robosense/utils/connection.h"
#include "modules/oscar/drivers/robosense/utils/expected.h"

namespace apollo {
namespace oscar {
namespace drivers {
namespace robosense {

using ObjectPool = apollo::cyber::base::CCObjectPool<EthPacket>;
using apollo::drivers::robosense::DecoderBase;
using apollo::drivers::robosense::DecoderFactory;

using apollo::drivers::PointCloud;
using apollo::drivers::PointXYZIT;

using apollo::drivers::robosense::E_DECODER_RESULT;

using namespace std::chrono_literals;

static constexpr size_t kObjectPoolSize = 10'010;
static constexpr size_t kMaxPacketQueueSize = 20'000;

static constexpr auto kParamUpdateTimeout = 20s;
static constexpr auto kLoopSleepTime = 100ms;
static constexpr auto kDecoderQueueSleepTime = 100us;

using CloudCallbackFn = std::function<void(std::shared_ptr<PointCloud>)>;
using StatusCallbackFn =
    std::function<void(std::shared_ptr<RobosenseLidarStatus>)>;
class RobosenseDriver {
 public:
  RobosenseDriver(const RobosenseLidarConfig& config,
                  CloudCallbackFn pc_callback, StatusCallbackFn status_callback)
      : frame_id_(config.frame_id()),
        packet_pool_(new ObjectPool(kObjectPoolSize)),
        pc_publish_callback_(pc_callback),
        status_publish_callback_(status_callback),
        param_setter_(config.model(), config.lidar_params()),
        pc_builder_(std::make_unique<PointCloudBuilder>(config)),
        is_running_(true),
        difop_received_(false) {
    std::thread msop_socket_thread(
        [&]() {
          tracy::SetThreadName(("msop_socket_thread_" + frame_id_).c_str());

          AINFO << "Starting MSOP listening thread on port: "
                << config.msop_port();
          UDPConnection msop_connection(config.msop_port());

          while (is_running_) {
            auto packet = packet_pool_->GetObject();

            if (packet == nullptr) {
              AWARN_EVERY(5)
                  << " Object pool size is full. Falling back to default "
                     "allocation";
              packet = std::make_shared<EthPacket>();
            }
            if (msop_connection.getPacket(*packet) != ReadStatus::OK) continue;

            if (input_packets_queue_.Size() < kMaxPacketQueueSize)
              input_packets_queue_.Enqueue(packet);
            else {
              AWARN_EVERY(5) << "EthPacket queue is full. Dropping messages ";
            }
          }

          AINFO << "MSOP capture thread stopped";
        });

    std::thread difop_socket_thread([&]() {
      tracy::SetThreadName(("difop_socket_thread_" + frame_id_).c_str());

      AINFO << "Starting DIFOP listening thread on port: "
            << config.difop_port();
      UDPConnection difop_connection_(config.difop_port());

      EthPacket packet;

      while (is_running_) {
        if (difop_connection_.getPacket(packet) != ReadStatus::OK) continue;

        auto status = pc_builder_->processDifopPkt(packet);

        if (!status.has_value()) continue;
        status_publish_callback_(
            std::make_shared<RobosenseLidarStatus>(status.value()));

        if (difop_received_) continue;

        std::lock_guard<std::mutex> lg(difop_mutex_);
        param_difop_packet_ = packet;
        difop_received_ = true;
      }

      AINFO << "DIFOP capture thread stopped";
    });

    std::thread msop_decoder_thread([&]() {
      tracy::SetThreadName(("msop_decoder_thread_" + frame_id_).c_str());

      while (is_running_) {
        std::shared_ptr<EthPacket> packet;

        if (!input_packets_queue_.WaitDequeue(&packet)) break;

        auto pc_msg = pc_builder_->processMsopPkt(*packet);

        if (pc_msg.has_value()) pc_publish_callback_(*pc_msg);
      }

      AINFO << "MSOP decoder thread stopped";
    });

    std::thread param_update_thread([&]() {
      // 1. wait for difop
      // 2. parse current settings
      // 3. check if settings are up-to-date
      // 3. create new settings
      // 4. send new settings

      tracy::SetThreadName((("param_update_thread_" + frame_id_)).c_str());

      auto wait_difop_message =
          [&](const auto&&) -> tl::expected<bool, std::string> {
        difop_received_ = false;
        while (!difop_received_ && is_running_) {
          std::this_thread::sleep_for(kLoopSleepTime);
        }

        if (difop_received_)
          return true;
        else
          return tl::make_unexpected("DIFOP message not received");
      };

      auto make_bool_expected = [](auto&& pred, const auto& err_msg) {
        return [&, f = std::move(pred)](
                   const auto&&) -> tl::expected<bool, std::string> {
          if (f())
            return true;
          else
            return tl::make_unexpected(err_msg);
        };
      };
      auto check_if_params_not_synced = make_bool_expected(
          [&]() { return !param_setter_.checkParams(param_difop_packet_); },
          "Lidar parameters are up-to-date");

      auto check_if_update_enabled = make_bool_expected(
          [&]() { return config.lidar_params().enable_param_update(); },
          "Lidar parameters update turned off in config. Skipping sending new "
          "settings.");

      auto update_params = make_bool_expected(
          [&]() { return param_setter_.updateParams(param_difop_packet_); },
          "Send updated settings failed");

      auto wait_update = make_bool_expected(
          [&]() {
            auto start_time = apollo::cyber::Time::Now().ToSecond();
            double current_time = start_time;

            double timeout = kParamUpdateTimeout.count();

            bool params_synced = false;

            while (is_running_ && !params_synced &&
                   (current_time - start_time) < timeout) {
              difop_received_ = false;
              current_time = apollo::cyber::Time::Now().ToSecond();
              {
                std::lock_guard<std::mutex> lg(difop_mutex_);
                params_synced = param_setter_.checkParams(param_difop_packet_);
              }
              std::this_thread::sleep_for(1s);
            }
            return params_synced;
          },
          "Lidar parameters not updated");
      auto print_success = [](const auto&&) {
        AINFO << "Lidar parameters successfully updated";
      };
      auto print_error = [](const auto&& what) { AERROR << what; };

      tl::expected<bool, std::string>{true}
          .and_then(wait_difop_message)
          .and_then(check_if_params_not_synced)
          .and_then(check_if_update_enabled)
          .and_then(update_params)
          .and_then(wait_update)
          .map(print_success)
          .map_error(print_error);

      AINFO << "param update thread stopped";
    });

    threads_.push_back(std::move(msop_socket_thread));
    threads_.push_back(std::move(difop_socket_thread));
    threads_.push_back(std::move(msop_decoder_thread));
    threads_.push_back(std::move(param_update_thread));

    std::array thread_names = {
        "msop_socket_thread_",
        "difop_socket_thread_",
        "msop_decoder_thread_",
        "param_update_thread_",
    };

    for (size_t i = 0; i < threads_.size(); i++) {
      apollo::cyber::scheduler::Instance()->SetInnerThreadAttr(
          thread_names[i] + frame_id_, &threads_[i]);
    }
  }

  ~RobosenseDriver() {
    is_running_ = false;

    input_packets_queue_.BreakAllWait();

    for (auto& thread : threads_)
      if (thread.joinable()) thread.join();
  }

 private:
  std::string frame_id_;

  std::vector<std::thread> threads_;

  std::shared_ptr<ObjectPool> packet_pool_;

  CloudCallbackFn pc_publish_callback_;
  StatusCallbackFn status_publish_callback_;

  apollo::cyber::base::ThreadSafeQueue<std::shared_ptr<EthPacket>>
      input_packets_queue_;

  ParamSetter param_setter_;
  std::unique_ptr<PointCloudBuilder> pc_builder_;

  std::atomic_bool is_running_;

  std::mutex difop_mutex_;
  EthPacket param_difop_packet_;
  std::atomic_bool difop_received_;
};

}  // namespace robosense
}  // namespace drivers
}  // namespace oscar
}  // namespace apollo
