///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <cstdint>
#include <map>
#include <vector>

#include "modules/oscar/drivers/robosense/proto/oscar_robosense_config.pb.h"

namespace apollo {
namespace oscar {
namespace drivers {
namespace robosense {

using EthPacket = std::vector<uint8_t>;

struct LidarParams {
  uint16_t phase_angle;
  uint16_t rotation_speed;
  ReturnMode return_mode;
  uint8_t synchronization_mode;
  uint16_t fov_start_angle;
  uint16_t fov_end_angle;
};

static constexpr size_t kLidarPacketLength = 1248;

static constexpr uint64_t kUCWPHeader = 0xaa00ff552222aaaa;
static constexpr uint64_t kDIFOPHeader = 0xA5FF005A11115555;
static constexpr uint32_t kMSOPHeader = 0x55AA055A;

static constexpr uint16_t kDIFOPTail = 0x0FF0;

enum class PacketType {
  MSOP,
  DIFOP,
  PseudoUCWP,  // "Pseudo" is used because the actual packet does not follow
               // official docs
  UNKNOWN
};

static const std::map<PacketType, std::string> PacketTypeNames{
    {PacketType::MSOP, "MSOP"},
    {PacketType::DIFOP, "DIFOP"},
    {PacketType::PseudoUCWP, "PseudoUCWP"},
    {PacketType::UNKNOWN, "UNKNOWN"}};

}  // namespace robosense
}  // namespace drivers
}  // namespace oscar
}  // namespace apollo
