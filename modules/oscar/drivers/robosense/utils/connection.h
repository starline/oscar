///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>

#include <array>
#include <string>

#include "common_types.h"
#include "cyber/common/log.h"

namespace apollo {
namespace oscar {
namespace drivers {
namespace robosense {

static constexpr auto kBufferSize = 2048;

using Buffer = std::array<uint8_t, kBufferSize>;

enum class ReadStatus { OK = 0, TIMEOUT = 1, ERROR = 2 };

class HTTPClientConnection {
 public:
  HTTPClientConnection(const std::string& dest_address, uint16_t port);

  bool connectToDestination();

  bool waitForConnection(uint32_t timeout_sec);

  std::string send(const std::string& data, bool wait_response = true);

  ~HTTPClientConnection();

 private:
  sockaddr_in internet_addr_;
  int connection_socket_;
};

class UDPConnection {
 public:
  explicit UDPConnection(uint32_t port = 0);

  void sendPacket(const EthPacket& buffer, const std::string& ip_address,
                  uint16_t port);

  ReadStatus getPacket(EthPacket& packet);

  ReadStatus waitForPacket(EthPacket& packet);

  ~UDPConnection();

 private:
  Buffer buffer_;
  int connection_socket_;
  sockaddr_in server_addr_;
};

}  // namespace robosense
}  // namespace drivers
}  // namespace oscar
}  // namespace apollo
