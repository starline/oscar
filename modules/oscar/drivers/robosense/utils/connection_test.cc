///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "connection.h"

#include <regex>

#include "gtest/gtest.h"

namespace apollo {
namespace oscar {
namespace drivers {
namespace robosense {

TEST(ConnectionTest, InvalidAddressTest) {
  EXPECT_THROW({ HTTPClientConnection sock("aaaaaa", 80); },
               std::runtime_error);
}

TEST(ConnectionTest, ValidAddressTest) {
  EXPECT_NO_THROW({ HTTPClientConnection sock("127.0.0.1", 80); });
}

TEST(ConnectionTest, ConnectToUnexistingDestinationTest) {
  HTTPClientConnection sock("127.0.0.1", 0);

  EXPECT_FALSE(sock.connectToDestination());
}

}  // namespace robosense
}  // namespace drivers
}  // namespace oscar
}  // namespace apollo
