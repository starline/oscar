///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/drivers/robosense/utils/connection.h"

#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <array>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

#include "cyber/common/log.h"
#include "cyber/time/time.h"

namespace apollo {
namespace oscar {
namespace drivers {
namespace robosense {

namespace {
std::string prefix_errstr(const std::string& prefix_str) {
  return prefix_str + ". " + std::string(std::strerror(errno));
}

void set_socket_timeout(int sock, uint32_t seconds, uint32_t microseconds = 0) {
  timeval timeout;
  timeout.tv_sec = seconds;
  timeout.tv_usec = microseconds;
  if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) ==
          -1 ||
      setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &timeout, sizeof(timeout)) ==
          -1)
    throw std::runtime_error(prefix_errstr("Socket timeout set error"));
}

static constexpr size_t kReconnectPeriodMicroseconds = 100'000;
}  // namespace

HTTPClientConnection::HTTPClientConnection(const std::string& dest_address,
                                           uint16_t port) {
  internet_addr_.sin_family = AF_INET;
  internet_addr_.sin_port = htons(port);

  if (!inet_aton(dest_address.c_str(), &internet_addr_.sin_addr))
    throw std::runtime_error("Cannot convert address for " + dest_address);

  connection_socket_ = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

  if (connection_socket_ == -1)
    throw std::runtime_error(prefix_errstr(
        "HTTP socket not created. destination IP: " + dest_address +
        ", port: " + std::to_string(port)));

  set_socket_timeout(connection_socket_, 1);
}

bool HTTPClientConnection::connectToDestination() {
  if (connect(connection_socket_, (struct sockaddr*)&internet_addr_,
              sizeof(sockaddr_in)) == -1) {
    if (errno != ECONNREFUSED)
      std::cerr << std::strerror(errno)
                << ". Destination IP: " << inet_ntoa(internet_addr_.sin_addr)
                << ", port: " << std::to_string(ntohs(internet_addr_.sin_port))
                << "\n";
    return false;
  }

  std::cout << strerror(errno) << "\n";

  return true;
}

bool HTTPClientConnection::waitForConnection(uint32_t timeout_sec) {
  auto start_time = apollo::cyber::Time::Now().ToSecond();
  auto current_time = apollo::cyber::Time::Now().ToSecond();

  bool connected = false;

  while (!connected && (current_time - start_time < timeout_sec)) {
    current_time = apollo::cyber::Time::Now().ToSecond();
    connected = connectToDestination();
    if (errno != ECONNREFUSED && errno != 0)
      throw std::runtime_error(
          prefix_errstr("Cannot connect to destination address: "));

    usleep(kReconnectPeriodMicroseconds);
  }

  return connected;
}

std::string HTTPClientConnection::send(const std::string& data,
                                       bool wait_response) {
  auto write_sz = write(connection_socket_, data.c_str(), data.size());
  if (write_sz == -1)
    throw std::runtime_error(prefix_errstr("Cannot write message to socket"));

  if (!wait_response) return {};

  char buffer[kBufferSize];
  bzero(buffer, kBufferSize);
  auto read_sz = read(connection_socket_, buffer, kBufferSize - 1);

  while (read_sz < 0 && errno == EAGAIN) {
    sleep(1);
    read_sz = read(connection_socket_, buffer, kBufferSize - 1);
  }
  if (read_sz < 0)
    throw std::runtime_error(prefix_errstr("Response read error"));

  return std::string(buffer, read_sz);
}

HTTPClientConnection::~HTTPClientConnection() {
  if (shutdown(connection_socket_, SHUT_RDWR) == -1)
    AERROR << prefix_errstr("Error on socket shutdown");
  if (close(connection_socket_) == -1)
    AERROR << prefix_errstr("Error on socket closing");
}

UDPConnection::UDPConnection(uint32_t port) {
  connection_socket_ = socket(AF_INET, SOCK_DGRAM, 0);

  if (connection_socket_ == -1)
    throw std::runtime_error(
        prefix_errstr("UDP socket not created, port: " + std::to_string(port)));

  server_addr_.sin_family = AF_INET;
  server_addr_.sin_port = htons(port);
  server_addr_.sin_addr.s_addr = INADDR_ANY;

  if (bind(connection_socket_, (struct sockaddr*)&server_addr_,
           sizeof(server_addr_)) == -1)
    throw std::runtime_error(prefix_errstr("UDP socket bind error"));

  set_socket_timeout(connection_socket_, 1);
}

void UDPConnection::sendPacket(const EthPacket& buffer,
                               const std::string& ip_address, uint16_t port) {
  sockaddr_in client_addr;
  client_addr.sin_family = AF_INET;
  client_addr.sin_port = htons(port);

  if (!inet_aton(ip_address.c_str(), &client_addr.sin_addr))
    throw std::runtime_error("Cannot convert address for " + ip_address);

  // We do not throw exception here
  if (sendto(connection_socket_, buffer.data(), buffer.size(), 0,
             (const struct sockaddr*)&client_addr, sizeof(client_addr)) == -1)
    perror("send error");
}

ReadStatus UDPConnection::getPacket(EthPacket& packet) {
  buffer_.fill(0);

  ssize_t read_sz = 0;

  read_sz = read(connection_socket_, buffer_.data(), kBufferSize - 1);

  if (read_sz > 0) {
    packet = {buffer_.begin(), buffer_.begin() + read_sz};
    return ReadStatus::OK;
  }

  return ReadStatus::ERROR;
}

ReadStatus UDPConnection::waitForPacket(EthPacket& packet) {
  buffer_.fill(0);

  ssize_t read_sz = 0;
  do {
    read_sz = read(connection_socket_, buffer_.data(), kBufferSize - 1);
  } while (read_sz <= 0 && errno == EAGAIN);

  if (read_sz < 0) {
    perror("read error");
    return ReadStatus::ERROR;
  }
  packet = {buffer_.begin(), buffer_.begin() + read_sz};

  return ReadStatus::OK;
}

UDPConnection::~UDPConnection() {
  if (close(connection_socket_) == -1)
    AERROR << prefix_errstr("Error on socket closing");
}

}  // namespace robosense
}  // namespace drivers
}  // namespace oscar
}  // namespace apollo
