import json
import sys
import time
import os
import argparse
from typing import Iterable

import numpy as np
from tqdm import tqdm

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import cyber, cyber_time
from modules.common_msgs.sensor_msgs import pointcloud_pb2

np.set_printoptions(precision=4, suppress=True)

class ChannelTranslator:
    def __init__(self, node, channel) -> None:

        self.channel = channel

        self.reader = node.create_reader(channel, pointcloud_pb2.PointCloud, self.cb)

    def cb(self, msg):

        msg_time = msg.header.timestamp_sec

        measurement_time = msg.measurement_time

      
        print(f'{self.channel} Diff: {(msg_time - measurement_time):.6f}')


class TranslatorManager:

    def __init__(self):

        cyber.init()
        self.node = cyber.Node("LatencyChecker")

        channels = [
          # "/apollo/sensor/lidar/ruby_left/PointCloud2",
          # "/apollo/sensor/lidar/ruby_left/compensator/PointCloud2",
          "/apollo/sensor/lidar/fused/compensator/PointCloud2"
        ]

        self.translators = []

        for channel in channels:
            self.translators.append(ChannelTranslator(
                self.node, channel))


if __name__ == "__main__":
    tr = TranslatorManager()
    tr.node.spin()
