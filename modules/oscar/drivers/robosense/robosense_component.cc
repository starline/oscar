///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/drivers/robosense/robosense_component.h"

namespace apollo {
namespace oscar {
namespace drivers {
namespace robosense {

bool RobosenseComponent::Init() {
  if (!GetProtoConfig(&conf_)) {
    AERROR << "load config error, file:" << config_file_path_;
    return false;
  }

  return checkConfig() && initWriters() && initDriver();
}

bool RobosenseComponent::checkConfig() {
  std::vector<std::pair<bool, std::string>> checks{
      {!conf_.has_pointcloud_channel(), "No pointcloud channel in config"},
      {!conf_.has_status_channel(), "No status channel in config"},
      {!conf_.has_calibration_file(), "No calibration file in config"},
      {conf_.msop_port() < 1025 || conf_.msop_port() > 65535,
       "MSOP port should be in range [1025; 65535]. Current value: " +
           std::to_string(conf_.msop_port())},
      {conf_.difop_port() < 1025 || conf_.difop_port() > 65535,
       "DIFOP port should be in range [1025; 65535]. Current value: " +
           std::to_string(conf_.difop_port())}};

  bool config_valid = true;
  for (auto& [condition, msg] : checks)
    if (condition) {
      AERROR << msg;
      config_valid = false;
    }
  return config_valid;
}

bool RobosenseComponent::initWriters() {
  pc_writer_ = node_->CreateWriter<PointCloud>(conf_.pointcloud_channel());
  status_writer_ =
      node_->CreateWriter<RobosenseLidarStatus>(conf_.status_channel());

  if (pc_writer_ == nullptr || status_writer_ == nullptr) return false;

  return true;
}

bool RobosenseComponent::initDriver() {
  driver_.reset(new RobosenseDriver(
      conf_,
      [&](std::shared_ptr<PointCloud> msg) {
        ZoneScopedN("Cloud publishing");
        ZoneText(conf_.pointcloud_channel().c_str(),
                 conf_.pointcloud_channel().size());
        this->pc_writer_->Write(msg);
      },
      [&](std::shared_ptr<RobosenseLidarStatus> msg) {
        this->status_writer_->Write(msg);
      }));

  return true;
}

}  // namespace robosense
}  // namespace drivers
}  // namespace oscar
}  // namespace apollo
