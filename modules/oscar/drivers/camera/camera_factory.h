///////////////////////////////////////////////////////////////////////////////////
// Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "modules/oscar/drivers/camera/proto/config.pb.h"

#include "modules/oscar/drivers/camera/camera.h"
#include "modules/oscar/drivers/camera/lucid/lucid_camera.h"

// Factory to create instances of objects that are subclasses of Camera.
class CameraFactory {
 public:
  static std::unique_ptr<Camera> createCamera(
      std::shared_ptr<apollo::drivers::oscar::camera::Config> cfg) {
    if (cfg->has_lucid_config()) {
      return std::make_unique<LucidCamera>();
    } else if (cfg->has_usbv4l_config()) {
      throw std::runtime_error(
          "V4L is not implemented. Please, use default Apollo driver\n");
    } else {
      return nullptr;
    }
  }
};
