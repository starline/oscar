///////////////////////////////////////////////////////////////////////////////////
// Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/drivers/camera/oscar_camera_component.h"

#include <iostream>
#include <string>

#define SLEEP_TIME 2

namespace apollo {
namespace drivers {
namespace camera {

bool OscarCameraComponent::Init() {
  // load Config
  camera_config_ = std::make_shared<Config>();
  if (!apollo::cyber::common::GetProtoFromFile(config_file_path_,
                                               camera_config_.get())) {
    return false;
  }
  AERROR << "Oscar camera config: " << camera_config_->DebugString();

  // connect to camera
  camera_device_ = CameraFactory::createCamera(camera_config_);
  bool isConnected = false;

  auto max_attempts = camera_config_->max_connect_attempts() > 0
                          ? camera_config_->max_connect_attempts()
                          : UINT32_MAX;
  for (uint32_t i = 1; i <= max_attempts; i++) {
    if (camera_device_->init(camera_config_,
                             [this](const std::shared_ptr<Image>& pbImagePtr) {
                               this->writer_->Write(pbImagePtr);
                             })) {
      isConnected = true;
      break;
    }
    sleep(SLEEP_TIME);
    AERROR << "Connecting Attempt " << i + 1 << "/"
           << camera_config_->max_connect_attempts();

    // handle keyboard interrupt
    if (!cyber::OK()) {
      return false;
    }
  }

  if (!isConnected) {
    AERROR << "Camera is not connected. Exiting";
    return false;
  }

  // set Node and publisher
  writer_ = node_->CreateWriter<Image>(camera_config_->channel_name());
  pb_image_ptr_ = std::make_shared<Image>();

  AERROR << "Init complete.";

  return true;
}

void OscarCameraComponent::Shutdown() { camera_device_->shutdown(); }

}  // namespace camera
}  // namespace drivers
}  // namespace apollo
