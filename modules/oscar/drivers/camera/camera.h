///////////////////////////////////////////////////////////////////////////////////
// Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "modules/common_msgs/sensor_msgs/sensor_image.pb.h"
#include "modules/oscar/drivers/camera/proto/config.pb.h"

using apollo::drivers::Image;
using apollo::drivers::oscar::camera::Config;

// Factory method pattern is used
// http://cpp-reference.ru/patterns/creational-patterns/factory-method/

// Abstract camera class providing interface to Cyber RT
class Camera {
 public:
  // create device, establish connections
  virtual bool init(
      const std::shared_ptr<Config>& config,
      std::function<void(const std::shared_ptr<Image>&)> callback_fn) = 0;

  // capture single image and store result to imagePtr
  virtual bool capture(std::shared_ptr<Image> imagePtr) = 0;

  // performs clearing operations
  virtual void shutdown() = 0;

  // check if camera device is active
  virtual bool isConnected() = 0;
  virtual ~Camera(){};
};
