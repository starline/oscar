///////////////////////////////////////////////////////////////////////////////////
// Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <unistd.h>

#include <iostream>

#include <GenApi/IEnumEntry.h>
#include <GenApi/INode.h>
#include <google/protobuf/descriptor.h>

#include "ArenaApi.h"
#include "opencv2/opencv.hpp"

#include "modules/common_msgs/sensor_msgs/sensor_image.pb.h"
#include "modules/oscar/drivers/camera/proto/config.pb.h"

#include "modules/oscar/drivers/camera/camera.h"
#include "modules/oscar/drivers/camera/lucid/parameter_setter.h"
#include "modules/oscar/drivers/camera/lucid/utils.h"

using apollo::drivers::Image;
using apollo::drivers::oscar::camera::Config;
using apollo::drivers::oscar::camera::LucidConfig;

// Main class for Lucid camera operations.
class LucidCamera : public Camera {
 public:
  bool init(const std::shared_ptr<Config> &config,
            std::function<void(const std::shared_ptr<Image> &)> callback_fn =
                0) override;

  bool capture(std::shared_ptr<Image> imagePtr) override;
  bool isConnected();
  void shutdown();

 private:
  void onImage(Arena::IImage *pImage);
  void setSettings(const LucidConfig *in_config);
  bool searchForDevices();
  bool connectDevice();
  void disconnectDevice();
  void setupDevice();

  LucidConfig lucid_config_;

  Arena::ISystem *pSystem_ = nullptr;
  Arena::IDevice *pDevice_ = nullptr;

  GenApi::INodeMap *pNodeMap_ = nullptr;
  GenApi::INodeMap *pTLStreamNodeMap_ = nullptr;
  bool acq_single_ = false;
  bool use_receive_time_ = false;

  int64_t target_pixel_format_;
  uint32_t target_width_;
  uint32_t target_height_;
  double driver_gain_;
  double desired_average_channel_intensity_;
  std::unique_ptr<ParameterSetter> param_setter_;

  double timestamp_offset_ = 0;
  double start_camera_time_ = 0;

  int64_t get_image_timeout_ms_ = 0;

  std::unique_ptr<ImageCallbackWrapper> image_callback_;
  std::unique_ptr<DeviceDisconnectCallbackWrapper> device_disconnect_callback_;
  std::function<void(const std::shared_ptr<Image> &)> writer_callback_fn_ =
      nullptr;
};
