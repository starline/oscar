#pragma once

#include <algorithm>
#include <chrono>
#include <future>

#include "ArenaApi.h"
#include "opencv2/opencv.hpp"

#include "modules/common_msgs/sensor_msgs/sensor_image.pb.h"

// taken from https://stackoverflow.com/a/56268886
template <typename TF, typename TDuration, class... TArgs>
std::result_of_t<TF && (TArgs && ...)> run_with_timeout(TF&& f,
                                                        TDuration timeout,
                                                        TArgs&&... args) {
  using R = std::result_of_t<TF && (TArgs && ...)>;
  std::packaged_task<R(TArgs...)> task(f);
  auto future = task.get_future();
  std::thread thr(std::move(task), std::forward<TArgs>(args)...);
  if (future.wait_for(timeout) != std::future_status::timeout) {
    thr.join();
    return future.get();
  } else {
    thr.detach();  // we leave the thread still running
    throw std::runtime_error("Function call timeout");
  }
}

double current_time();

// taken from https://stackoverflow.com/a/9324086
template <typename T>
T clip(const T& n, const T& lower, const T& upper) {
  return std::max(lower, std::min(n, upper));
}

void white_balance_correction(cv::Mat* dst_im, double grey_world_color = 128);

void equalize_averages_per_channel(cv::Mat* dst_im,
                                   double desired_average = 128);

void processImage(const cv::Mat& src_im, cv::Mat* dst_im, uint32_t target_width,
                  uint32_t target_height,
                  double desired_average_channel_intensity);

class ImageCallbackWrapper final : public Arena::IImageCallback {
 public:
  ImageCallbackWrapper(std::function<void(Arena::IImage*)> cb_function)
      : cb_function_(cb_function) {}
  ~ImageCallbackWrapper() {}

  void OnImage(Arena::IImage* pImage) { cb_function_(pImage); };

 private:
  std::function<void(Arena::IImage*)> cb_function_;
};

class DeviceDisconnectCallbackWrapper final
    : public Arena::IDisconnectCallback {
  std::function<void(void)> reconnect_function_;

 public:
  DeviceDisconnectCallbackWrapper(std::function<void(void)> reconnect_function)
      : reconnect_function_(reconnect_function) {}
  ~DeviceDisconnectCallbackWrapper(){};

  void OnDeviceDisconnected(Arena::IDevice* pDevice) { reconnect_function_(); }
};
