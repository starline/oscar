///////////////////////////////////////////////////////////////////////////////////
// Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <algorithm>
#include <map>

#include "ArenaApi.h"

inline void extractNodeNamesMap(
    std::map<GenApi::INodeMap*, std::vector<GenICam::gcstring>>& NodeMapMap) {
  for (auto& nodeMapEl : NodeMapMap) {
    auto nodeMap = nodeMapEl.first;
    auto& nodeMapVector = nodeMapEl.second;

    GenApi::NodeList_t nodeList;
    nodeMap->GetNodes(nodeList);

    nodeMapVector.reserve(nodeMap->GetNumNodes());

    std::for_each(nodeList.begin(), nodeList.end(), [&](GenApi::INode* node) {
      nodeMapVector.push_back(node->GetName());
    });
  }
}

template <typename T, class Enable = void>
class ArenaNodeAdapter {};

// specialization for BOOLEANS
template <typename T>
struct ArenaNodeAdapter<
    T, typename std::enable_if<std::is_same<T, bool>::value>::type> {
  using NodeTypePtr = GenApi::CBooleanPtr;
  using DataType = bool;

  explicit ArenaNodeAdapter(NodeTypePtr node) : node_(node) {}

  void setValue(DataType value) { node_->SetValue(value); }
  DataType getValue() { return node_->GetValue(); }

  DataType processValue(DataType value) { return value; }
  bool checkValue(DataType value) { return true; }

 private:
  NodeTypePtr node_;
};

// specialization for INTEGER types
template <typename T>
struct ArenaNodeAdapter<
    T, typename std::enable_if<std::is_integral<T>::value &&
                               !std::is_same<T, bool>::value &&
                               !std::is_same<T, char>::value>::type> {
  using NodeTypePtr = GenApi::CIntegerPtr;
  using DataType = int;

  explicit ArenaNodeAdapter(NodeTypePtr node) : node_(node) {}

  void setValue(DataType value) { node_->SetValue(value); }
  DataType getValue() { return node_->GetValue(); }

  DataType processValue(DataType value) {
    DataType ret_value = value;
    auto min = node_->GetMin();
    auto max = node_->GetMax();
    if (ret_value < min) {
      ret_value = min;
      std::cout << "(min)";
    } else if (ret_value > max) {
      ret_value = max;
      std::cout << "(max)";
    }

    return ret_value;
  }

  bool checkValue(DataType value) { return true; }

 private:
  NodeTypePtr node_;
};

// specialization for FLOATING POINT types
template <typename T>
struct ArenaNodeAdapter<
    T, typename std::enable_if<std::is_floating_point<T>::value>::type> {
  using NodeTypePtr = GenApi::CFloatPtr;
  using DataType = double;

  explicit ArenaNodeAdapter(NodeTypePtr node) : node_(node) {}

  void setValue(DataType value) { node_->SetValue(value); }
  DataType getValue() { return node_->GetValue(); }

  DataType processValue(DataType value) {
    DataType ret_value = value;
    auto min = node_->GetMin();
    auto max = node_->GetMax();
    if (ret_value < min) {
      ret_value = min;
    } else if (ret_value > max) {
      ret_value = max;
    }

    return ret_value;
  }

  bool checkValue(DataType value) { return true; }

 private:
  NodeTypePtr node_;
};

// specialization for STRINGS
// for simplicity we use string values instead of creating all Enums
template <typename T>
struct ArenaNodeAdapter<
    T, typename std::enable_if<std::is_same<T, std::string>::value ||
                               std::is_same<T, const char*>::value>::type> {
  using NodeTypePtr = GenApi::CEnumerationPtr;
  using DataType = std::string;

  explicit ArenaNodeAdapter(NodeTypePtr node) : node_(node) {}

  DataType getValue() {
    return std::string(node_->GetCurrentEntry()->GetSymbolic());
  }

  void setValue(DataType value) {
    auto enumEntry = get_enum_entry(value);
    if (!enumEntry.IsValid()) {
      return;
    }
    node_->SetIntValue(enumEntry->GetNumericValue());
  }

  DataType processValue(DataType value) { return value; }
  bool checkValue(DataType value) {
    auto enumEntry = get_enum_entry(value);
    return enumEntry.IsValid();
  }

 private:
  GenApi::CEnumEntryPtr get_enum_entry(DataType value) {
    GenApi::NodeList_t entries;
    node_->GetEntries(entries);

    auto enumEntry =
        std::find_if(entries.begin(), entries.end(), [&](GenApi::INode* entry) {
          return static_cast<GenApi::CEnumEntryPtr>(entry)->GetSymbolic() ==
                 value.c_str();
        });

    if (enumEntry != entries.end()) {
      return static_cast<GenApi::CEnumEntryPtr>(*enumEntry);
    } else {
      return nullptr;
    }
  }
  NodeTypePtr node_;
};

class ParameterSetter final {
 public:
  explicit ParameterSetter(Arena::IDevice* pDevice) {
    std::array<GenApi::INodeMap*, 4> nodeMaps{
        pDevice->GetNodeMap(), pDevice->GetTLStreamNodeMap(),
        pDevice->GetTLDeviceNodeMap(), pDevice->GetTLInterfaceNodeMap()};

    for (auto m : nodeMaps) {
      NodeMapMap_[m] = std::vector<GenICam::gcstring>();
    }

    extractNodeNamesMap(NodeMapMap_);
  }

  template <typename T>
  bool setValue(const std::string& nodeName, T value) {
    GenApi::INodeMap* currentNodeMap = getNodeMapByNodeName(nodeName.c_str());
    if (currentNodeMap == nullptr) {
      std::cerr << "Node " << nodeName << " not found\n";
      return false;
    }

    typename ArenaNodeAdapter<T>::NodeTypePtr node =
        currentNodeMap->GetNode(nodeName.c_str());
    ArenaNodeAdapter<T> nodeAdapter(node);

    // maybe there is more proper way to check it?
    if (node->GetAccessMode() != GENAPI_NAMESPACE::_EAccessMode::RW) {
      std::cout << "Bad access mode for node " << nodeName << "\n";
    }

    // check validity for value
    if (!nodeAdapter.checkValue(value)) return false;

    // round to possible values
    auto updated_value = nodeAdapter.processValue(value);

    // set value
    auto prev_value = nodeAdapter.getValue();
    nodeAdapter.setValue(updated_value);

    // check new value
    auto new_value = nodeAdapter.getValue();

    // print info
    std::cout << "[" << nodeName << "]: ";
    std::cout << prev_value << " ";
    if (value != updated_value) {
      std::cout << "(" << value << " modified to " << updated_value << ") ";
    }
    std::cout << "-> " << new_value << "\n";

    return true;
  }

  bool execute(const std::string& nodeName) {
    GenApi::INodeMap* currentNodeMap = getNodeMapByNodeName(nodeName.c_str());
    if (currentNodeMap == nullptr) {
      std::cerr << "Node " << nodeName.c_str() << " not found\n";
      return false;
    }

    Arena::ExecuteNode(currentNodeMap, nodeName.c_str());
    return true;
  }

 private:
  GenApi::INodeMap* getNodeMapByNodeName(const GenICam::gcstring& name) {
    auto ret_node_it = std::find_if(
        NodeMapMap_.begin(), NodeMapMap_.end(),
        [&](const std::pair<GenApi::INodeMap*, std::vector<GenICam::gcstring>>
                nodeMapVal) {
          return std::find(nodeMapVal.second.begin(), nodeMapVal.second.end(),
                           name) != nodeMapVal.second.end();
        });
    if (ret_node_it == NodeMapMap_.end()) {
      return nullptr;
    } else {
      return ret_node_it->first;
    }
  }

  std::map<GenApi::INodeMap*, std::vector<GenICam::gcstring>> NodeMapMap_;
};
