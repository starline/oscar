#include "modules/oscar/drivers/camera/lucid/utils.h"

#include <algorithm>
#include <chrono>
#include <future>

#include "ArenaApi.h"
#include "opencv2/opencv.hpp"

#include "modules/common_msgs/sensor_msgs/sensor_image.pb.h"

double current_time() {
  double ret_time = static_cast<double>(
                        std::chrono::duration_cast<std::chrono::nanoseconds>(
                            std::chrono::system_clock::now().time_since_epoch())
                            .count()) /
                    1'000'000'000UL;
  return ret_time;
}

void white_balance_correction(cv::Mat* dst_im, double grey_world_color) {
  cv::cvtColor(*dst_im, *dst_im, cv::COLOR_BGR2Lab);

  std::vector<cv::Mat> splitted_channels(3);
  cv::split(*dst_im, splitted_channels);

  auto avg_a = cv::mean(splitted_channels[1])[0];
  auto avg_b = cv::mean(splitted_channels[2])[0];

  auto a_diff_mult = (avg_a - grey_world_color) / 255.0 * 1.1;
  auto b_diff_mult = (avg_b - grey_world_color) / 255.0 * 1.1;

  // Can it be done faster with fully parallel execution???
  std::for_each(dst_im->begin<cv::Vec3b>(), dst_im->end<cv::Vec3b>(),
                [&](cv::Vec3b& pixel) {
                  pixel[1] = static_cast<uint8_t>(
                      clip(pixel[1] - a_diff_mult * pixel[0], 0.0, 255.0));
                  pixel[2] = static_cast<uint8_t>(
                      clip(pixel[2] - b_diff_mult * pixel[0], 0.0, 255.0));
                });

  cv::cvtColor(*dst_im, *dst_im, cv::COLOR_Lab2BGR);
}

void equalize_averages_per_channel(cv::Mat* dst_im, double desired_average) {
  std::vector<cv::Mat> splitted_channels(3);
  cv::split(*dst_im, splitted_channels);

  auto avg_r = cv::mean(splitted_channels[0])[0];
  auto avg_g = cv::mean(splitted_channels[1])[0];
  auto avg_b = cv::mean(splitted_channels[2])[0];

  // desired_average = (avg_r + avg_g + avg_b) / 3;

  auto coeff_r = desired_average / avg_r;
  auto coeff_g = desired_average / avg_g;
  auto coeff_b = desired_average / avg_b;

  // Can it be done faster with fully parallel execution???
  std::for_each(
      dst_im->begin<cv::Vec3b>(), dst_im->end<cv::Vec3b>(),
      [&](cv::Vec3b& pixel) {
        pixel[0] = static_cast<uchar>(clip((pixel[0] * coeff_r), 0.0, 255.0));
        pixel[1] = static_cast<uchar>(clip((pixel[1] * coeff_g), 0.0, 255.0));
        pixel[2] = static_cast<uchar>(clip((pixel[2] * coeff_b), 0.0, 255.0));
      });
}

void processImage(const cv::Mat& src_im, cv::Mat* dst_im, uint32_t target_width,
                  uint32_t target_height,
                  double desired_average_channel_intensity) {
  // auto st = current_time();
  cv::cvtColor(src_im, *dst_im, cv::COLOR_BayerRG2BGR);
  cv::resize(*dst_im, *dst_im, cv::Size(target_width, target_height));

  white_balance_correction(dst_im, 128);
  equalize_averages_per_channel(dst_im, desired_average_channel_intensity);
  // std::cout << current_time() - st << "\n";
}
