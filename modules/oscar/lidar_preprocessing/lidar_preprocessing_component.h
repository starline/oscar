///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>

#include "Eigen/Geometry"
#include "cyber/base/concurrent_object_pool.h"
#include "cyber/base/thread_pool.h"
#include "cyber/component/component.h"
#include "cyber/cyber.h"
#include "modules/common_msgs/sensor_msgs/pointcloud.pb.h"
#include "modules/oscar/lidar_preprocessing/util_types.h"
#include "modules/oscar/utils/parallel_for.h"
#include "modules/oscar/utils/point_cloud_pool.h"
#include "modules/perception/onboard/transform_wrapper/transform_wrapper.h"

using apollo::cyber::Component;

using ApolloPointCloud = apollo::drivers::PointCloud;
using apollo::cyber::base::CCObjectPool;

using namespace apollo::oscar::utils;

namespace apollo {
namespace oscar {
class LidarPreprocessingComponent : public Component<ApolloPointCloud> {
 public:
  bool Init() override;

  bool Proc(const std::shared_ptr<ApolloPointCloud>& msg) override;

 private:
  void PrepareOutputMessage();
  std::shared_ptr<apollo::cyber::Writer<ApolloPointCloud>> writer_ = nullptr;

  apollo::oscar::preprocessing::LidarPreprocessingConfig config_;

  apollo::perception::onboard::TransformWrapper tf_provider_;
  Eigen::Affine3d bbox_filter_transform_ = Eigen::Affine3d::Identity();
  bool tf_uploaded_ = false;

  constexpr static auto kInitCloudSize = 50'000;
  apollo::oscar::utils::PointCloudPool cloud_pool_{kInitCloudSize};
  std::shared_ptr<ApolloPointCloud> next_msg_ = nullptr;
  uint32_t prev_cloud_size_ = 140'000;
  std::array<uint32_t, 600'000> indices_array_;

  std::shared_ptr<CCThreadPool> thread_pool_ = nullptr;

  std::unique_ptr<std::future<void>> prepare_output_msg_result_ = nullptr;
  std::vector<apollo::drivers::PointXYZIT*> elements_to_remove_{};
  IndicesMask indices_mask_;
};
CYBER_REGISTER_COMPONENT(LidarPreprocessingComponent)

}  // namespace oscar
}  // namespace apollo
