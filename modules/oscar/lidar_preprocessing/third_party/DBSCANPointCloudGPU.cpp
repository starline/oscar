/**
 * @file    DBSCANPointCloudGPU.cpp
 *
 * @author  btran
 *
 */
// mostly taken from https://github.com/xmba15/generic_dbscan

#include <pcl/common/time.h>

#include <algorithm>
#include <cstdio>
#include <iostream>
#include <memory>

#include "modules/oscar/lidar_preprocessing/third_party/dbscan_cuda/CudaUtils.cuh"
#include "modules/oscar/lidar_preprocessing/third_party/dbscan_cuda/DBSCAN.cuh"

namespace {
using PointT = pcl::PointXYZI;
using PointCloud = pcl::PointCloud<PointT>;
using PointCloudPtr = PointCloud::Ptr;
using DBSCAN = clustering::cuda::DBSCAN<pcl::PointXYZ>;

bool GPUInit(int gpu_id) {
  int devices;
  HANDLE_ERROR(cudaGetDeviceCount(&devices));

  if (devices <= gpu_id) {

    std::cerr << "request GPU id " + std::to_string(gpu_id) +
                             " but found only " + std::to_string(devices) +
                             " GPU(s). Falling back to first device\n";
    gpu_id = 0;
  }
  
  HANDLE_ERROR(cudaSetDevice(gpu_id));
  HANDLE_ERROR(cuda::utils::warmUpGPU());
  return true;
};

void verify_device(int target_gpu) {
  int current_gpu;
  HANDLE_ERROR(cudaGetDevice(&current_gpu));
  if (current_gpu != target_gpu)
    std::cerr << "request GPU id is " + std::to_string(target_gpu) +
                             " . Current GPU id is " + std::to_string(current_gpu) + "\n";
}

}  // namespace

std::vector<std::vector<int>> DBSCANClustering(PointCloudPtr& input_cloud,
                                               double eps, int minPoints, int gpu_id = 0) {
  [[maybe_unused]] thread_local bool gpu_init_complete = GPUInit(gpu_id);
  verify_device(gpu_id);

  PointCloudPtr filtered_cloud(new PointCloud);
  filtered_cloud->reserve(input_cloud->size());
  // std::cout << "number of points: " << input_cloud->size() << "\n";

  // static pcl::StopWatch timer;

  static DBSCAN::Param param{
      .pointDimension = 3, .eps = eps, .minPoints = minPoints};
  thread_local DBSCAN dbscanHandler(param);

  std::vector<std::vector<int>> clusterIndices;

  pcl::PointCloud<pcl::PointXYZ> cloudXYZ;
  pcl::copyPointCloud(*input_cloud, cloudXYZ);

  // timer.reset();
  clusterIndices = dbscanHandler.run(cloudXYZ.points.data(), cloudXYZ.size());
  // std::cout << "number of clusters: " << clusterIndices.size() << "\n";
  // std::cout << "processing time (gpu): " << timer.getTime() << "[ms]\n";

  return clusterIndices;
}
