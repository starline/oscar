/**
 * @file    CudaUtils.cu
 *
 * @author  btran
 *
 */

// taken from https://github.com/xmba15/generic_dbscan

#include "CudaUtils.cuh"

namespace cuda {
namespace utils {
namespace {
__global__ void warmUpGPUKernel() {
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  ++idx;
}
}  // namespace

cudaError_t warmUpGPU() {
  warmUpGPUKernel<<<1, 1>>>();
  cudaDeviceSynchronize();
  return cudaGetLastError();
}
}  // namespace utils
}  // namespace cuda
