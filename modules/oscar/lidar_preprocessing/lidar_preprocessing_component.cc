///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/lidar_preprocessing/lidar_preprocessing_component.h"

#include "modules/oscar/lidar_preprocessing/filters.h"

namespace apollo {
namespace oscar {

static auto kThreadPoolSize = std::thread::hardware_concurrency() / 4;

bool LidarPreprocessingComponent::Init() {
  if (!GetProtoConfig(&config_)) {
    AERROR << "Parse conf file failed, " << ConfigFilePath();
    return false;
  }

  thread_pool_ = std::make_shared<CCThreadPool>(kThreadPoolSize);

  prepare_output_msg_result_ = std::make_unique<std::future<void>>(
      thread_pool_->Enqueue([this] { this->PrepareOutputMessage(); }));

  return true;
}

void LidarPreprocessingComponent::PrepareOutputMessage() {
  ZoneScoped;

  next_msg_ = cloud_pool_.get();

  {
    ZoneScopedN("Reserve");
    next_msg_->mutable_point()->Reserve(prev_cloud_size_);
  }

  {
    ZoneScopedN("Creating points");
    ZoneValue(next_msg_->point_size());
    ZoneValue(prev_cloud_size_);
    for (size_t i = 0; i < prev_cloud_size_; i++) next_msg_->add_point();
  }

  {
    ZoneScopedN("Resetting mask");
    indices_mask_.set();
    for (std::size_t i = prev_cloud_size_; i < indices_mask_.size(); i++)
      indices_mask_.reset(i);
  }
}

bool LidarPreprocessingComponent::Proc(
    const std::shared_ptr<ApolloPointCloud>& msg) {
  ZoneScopedN("Lidar preprocessing");

  auto info = node_->Name();
  ZoneText(info.c_str(), info.size());

  if (writer_ == nullptr) {
    auto channel = readers_[0]->GetChannelName();

    writer_ = node_->CreateWriter<ApolloPointCloud>(channel +
                                                    config_.channel_postfix());
  }

  if (!tf_uploaded_ && config_.bbox_filter().enabled() &&
      config_.bbox_filter().frame_id().size() != 0) {
    tf_uploaded_ = tf_provider_.GetTrans(0, &bbox_filter_transform_,
                                         config_.bbox_filter().frame_id(),
                                         msg->header().frame_id());

    if (!tf_uploaded_) {
      AERROR << "Cannot get transform between frames "
             << msg->header().frame_id() << " and "
             << config_.bbox_filter().frame_id() << ".Skipping frame";
      return true;
    }
  }

  {
    ZoneScopedN("Resetting mask");
    indices_mask_.set();
    for (std::size_t i = msg->point_size(); i < prev_cloud_size_; i++)
      indices_mask_.reset(i);
  }

  std::vector<IndicesMask> filter_results;
  std::vector<Predicate> predicates;

  predicates.emplace_back(build_nan_predicate());

  if (config_.distance_filter().enabled()) {
    predicates.emplace_back(build_distance_predicate(config_));
  }

  if (config_.bbox_filter().enabled()) {
    predicates.emplace_back(
        build_bbox_filter_predicate(config_, &bbox_filter_transform_));
  }

  if (config_.intensity_filter().enabled()) {
    predicates.emplace_back(build_intensity_predicate(config_));
  }

  filter_results.emplace_back(ComplexFilter(msg, predicates, thread_pool_));

  if (config_.uniform_downsampling().enabled())
    filter_results.emplace_back(
        UniformFilter(msg, config_.uniform_downsampling().factor()));

  for (auto& res : filter_results) {
    indices_mask_ &= res;
  }

  for (int i = 0, out_i = 0; i < msg->point_size(); i++) {
    if (indices_mask_[i]) indices_array_[out_i++] = i;
  }

  auto keep_size = indices_mask_.count();

  prepare_output_msg_result_->wait();
  auto out_msg = next_msg_;
  int size_diff = static_cast<int>(keep_size) - out_msg->point_size();
  if (size_diff > 0) {
    ZoneScopedN("Resizing");
    out_msg->mutable_point()->Reserve(keep_size);
    for (int i = 0; i < size_diff; i++) out_msg->add_point();
  }

  constexpr static auto kNumThreads = 4;

  auto& points = *out_msg->mutable_point();

  {
    ZoneScopedN("Copy");
    parallel_for(
        keep_size, kNumThreads,
        [&](int i) { points[i].CopyFrom(msg->point()[indices_array_[i]]); },
        thread_pool_);
  }

  {
    ZoneScopedN("Delete redundant points");
    out_msg->mutable_point()->DeleteSubrange(keep_size,
                                             out_msg->point_size() - keep_size);
  }

  out_msg->mutable_header()->set_timestamp_sec(cyber::Time::Now().ToSecond());
  out_msg->mutable_header()->set_frame_id(msg->header().frame_id());
  out_msg->mutable_header()->set_lidar_timestamp(
      msg->header().lidar_timestamp());
  out_msg->set_measurement_time(msg->measurement_time());
  out_msg->set_height(msg->height());
  out_msg->set_is_dense(msg->is_dense());

  out_msg->set_width(out_msg->point_size() / msg->height());

  writer_->Write(out_msg);

  if (out_msg->point_size() > static_cast<int>(prev_cloud_size_)) {
    AWARN << "Increasing cloud size: " << prev_cloud_size_ << " -> "
          << out_msg->point_size();
    prev_cloud_size_ = out_msg->point_size();
  }

  prepare_output_msg_result_ = std::make_unique<std::future<void>>(
      thread_pool_->Enqueue([this] { this->PrepareOutputMessage(); }));

  return true;
}

}  // namespace oscar
}  // namespace apollo
