///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <algorithm>
#include <functional>
#include <iostream>

#include "Eigen/Core"
#include "cyber/cyber.h"
#include "modules/common_msgs/sensor_msgs/pointcloud.pb.h"
#include "modules/oscar/lidar_preprocessing/util_types.h"
#include "modules/oscar/utils/parallel_for.h"
#include "unsupported/Eigen/CXX11/Tensor"

using namespace apollo::oscar::utils;

using apollo::drivers::PointXYZIT;

namespace {

IndicesMask UniformFilter(
    const std::shared_ptr<apollo::drivers::PointCloud>& input_cloud,
    uint32_t factor) {
  ZoneScoped;
  IndicesMask keep_indices;

  uint32_t cloud_size = input_cloud->point_size();

  // beam filtering
  if (input_cloud->width() == 1) {
    for (uint32_t i = 0; i < cloud_size; i += factor) {
      keep_indices[i] = true;
    }
  } else {
    // column filtering
    auto height = input_cloud->height();
    auto width = input_cloud->width();
    for (uint32_t row = 0; row < width; row += factor)
      for (uint32_t col = 0; col < height; col++)
        keep_indices[height * row + col] = true;
  }

  return keep_indices;
}

static constexpr auto kNumThreads = 4;
using Predicate = std::function<bool(const PointXYZIT&)>;

IndicesMask ComplexFilter(
    const std::shared_ptr<apollo::drivers::PointCloud>& input_cloud,
    const std::vector<Predicate>& predicates,
    std::shared_ptr<CCThreadPool> thread_pool = nullptr) {
  ZoneScoped;
  IndicesMask keep_indices;

  std::mutex mask_mutex;

  parallel_for(
      input_cloud->point_size(), kNumThreads,
      [&](int start, int end) {
        IndicesMask tmp_mask;
        for (int i = start; i < end; i++) {
          tmp_mask[i] = std::all_of(
              predicates.begin(), predicates.end(),
              [&](auto& comp) { return comp(input_cloud->point(i)); });
        }

        {
          std::lock_guard lg(mask_mutex);
          keep_indices |= tmp_mask;
        }
      },
      thread_pool);

  return keep_indices;
}

Predicate build_distance_predicate(
    const apollo::oscar::preprocessing::LidarPreprocessingConfig& config) {
  const auto min_distance = config.distance_filter().min_distance();
  const auto max_distance = config.distance_filter().max_distance();

  if (config.distance_filter().use_z())
    return [=](const PointXYZIT& el) {
      const auto distance = std::hypot(el.x(), el.y(), el.z());
      return min_distance <= distance && distance < max_distance;
    };
  else
    return [=](const PointXYZIT& el) {
      const auto distance = std::hypot(el.x(), el.y());
      return min_distance <= distance && distance < max_distance;
    };
}

Predicate build_bbox_filter_predicate(
    const apollo::oscar::preprocessing::LidarPreprocessingConfig& config,
    Eigen::Affine3d* transform) {
  const auto& inner_bbox = config.bbox_filter().inner_bbox();
  const auto& outer_bbox = config.bbox_filter().outer_bbox();

  return [=](const auto& el) {
    const auto transformed_point =
        *transform * Eigen::Vector3d{el.x(), el.y(), el.z()};
    return transformed_point[0] > outer_bbox.x_min() &&
           transformed_point[0] < outer_bbox.x_max() &&
           transformed_point[1] > outer_bbox.y_min() &&
           transformed_point[1] < outer_bbox.y_max() &&
           transformed_point[2] > outer_bbox.z_min() &&
           transformed_point[2] < outer_bbox.z_max() &&
           !(transformed_point[0] > inner_bbox.x_min() &&
             transformed_point[0] < inner_bbox.x_max() &&
             transformed_point[1] > inner_bbox.y_min() &&
             transformed_point[1] < inner_bbox.y_max() &&
             transformed_point[2] > inner_bbox.z_min() &&
             transformed_point[2] < inner_bbox.z_max());
  };
}

Predicate build_intensity_predicate(
    const apollo::oscar::preprocessing::LidarPreprocessingConfig& config) {
  const auto min_intensity = config.intensity_filter().min_intensity();
  const auto max_intensity = config.intensity_filter().max_intensity();

  return [=](const auto& el) {
    return el.intensity() >= min_intensity && el.intensity() <= max_intensity;
  };
}

Predicate build_nan_predicate() {
  return [](const auto& point) {
    return !(std::isnan(point.x()) || std::isnan(point.y()) || std::isnan(point.z()));
  };
}

}  // namespace
