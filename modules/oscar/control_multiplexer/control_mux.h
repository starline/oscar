#pragma once

#include <memory>
#include <string>
#include "modules/common_msgs/control_msgs/control_cmd.pb.h"
#include "modules/control/proto/control_conf.pb.h"
#include "cyber/cyber.h"
#include "cyber/component/timer_component.h"
#include <vector>
#include "boost/bind.hpp"
#include "yaml-cpp/yaml.h"
#include "cyber/timer/timer.h"

namespace apollo {
namespace control {

struct control_channel {
  std::shared_ptr<cyber::Reader<apollo::control::ControlCommand>> reader;
  int priority;
  double timeout_ms;
  bool is_active;
};

class ControlMux : public apollo::cyber::TimerComponent {
  public:
    bool Init() override;
    bool Proc() override;
  private:
    void ControlCallback(const std::shared_ptr<apollo::control::ControlCommand>& msg, unsigned int idx);
    void OnLocalTimer(unsigned int);
    std::shared_ptr<apollo::cyber::Node> control_mux_node_;
    std::shared_ptr<cyber::Reader<apollo::control::ControlCommand>> reader_;
    control_channel reader_item_;
    std::vector <control_channel> ctl_readers_;
    std::vector <std::shared_ptr<apollo::cyber::Timer>> timers_;
    YAML::Node mux_parameters_;
    unsigned int allowed_source_id_;
    std::shared_ptr<cyber::Writer<apollo::control::ControlCommand>> control_writer_;
};

CYBER_REGISTER_COMPONENT(ControlMux)

}
}