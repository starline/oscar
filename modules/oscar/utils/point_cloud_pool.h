
#pragma once

#include <memory>

#include "cyber/base/concurrent_object_pool.h"
#include "modules/common_msgs/sensor_msgs/pointcloud.pb.h"
#include "modules/oscar/utils/object_provider.h"

namespace apollo {
namespace oscar {
namespace utils {

using apollo::drivers::PointCloud;
using apollo::drivers::PointXYZIT;

struct PointCloudPool {
  using CloudProvider = apollo::oscar::ObjectParallelProvider<PointCloud>;
  using ArenaInitBlockPool =
      apollo::cyber::base::CCObjectPool<std::vector<char>>;

  using CloudPtr = std::shared_ptr<PointCloud>;

  PointCloudPool(std::size_t init_cloud_size, std::size_t pool_size = 10) {
    auto init_block_size =
        init_cloud_size * sizeof(PointXYZIT) + sizeof(PointCloud);

    arena_init_block_pool_.reset(new ArenaInitBlockPool(pool_size));
    arena_init_block_pool_->ConstructAll();

    for (std::size_t i = 0; i < pool_size; ++i) {
      auto block = arena_init_block_pool_->GetObject();
      if (block == nullptr) {
        AERROR << "fail to get object:" << i;
      }
      block->reserve(init_block_size);
    }

    object_provider_ = std::make_unique<CloudProvider>([&] {
      auto arena_init_block = arena_init_block_pool_->GetObject();
      std::shared_ptr<google::protobuf::Arena> arena;

      google::protobuf::ArenaOptions arena_options;

      static constexpr char* kNullInitBlock = nullptr;

      arena_options.initial_block =
          arena_init_block ? arena_init_block->data() : kNullInitBlock;

      arena_options.initial_block_size = init_block_size;
      arena_options.max_block_size = 1024 * 1024;

      arena = std::make_shared<google::protobuf::Arena>(arena_options);

      return CloudPtr{
          google::protobuf::Arena::CreateMessage<PointCloud>(arena.get()),
          [arena_init_block, arena](auto ptr) {}};
    });
  }

  CloudPtr get() { return object_provider_->Get(); }

 private:
  std::shared_ptr<ArenaInitBlockPool> arena_init_block_pool_ = nullptr;
  std::unique_ptr<CloudProvider> object_provider_ = nullptr;
};
}  // namespace utils
}  // namespace oscar
}  // namespace apollo