///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/utils/transform_buffer/oscar_tf_buffer.h"

#include "boost/format.hpp"

namespace {
constexpr auto kWorldFrame = "world";

Eigen::Affine3d matrixFromTransQuat(
    const apollo::transform::TransformStamped& tf) {
  const Eigen::Translation3d trans{tf.transform().translation().x(),
                                   tf.transform().translation().y(),
                                   tf.transform().translation().z()};

  const Eigen::Quaterniond q{
      tf.transform().rotation().qw(), tf.transform().rotation().qx(),
      tf.transform().rotation().qy(), tf.transform().rotation().qz()};

  return trans * q;
}

Eigen::Affine3d IntepolateAffine(const Eigen::Affine3d& tf1,
                                 const Eigen::Affine3d& tf2, double ts1,
                                 double ts2, double target_ts) {
  Eigen::Affine3d res_tf;

  const auto ts_diff = ts2 - ts1;

  if (ts_diff < 0) {
    throw tf2::TransformException(
        (boost::format("tf2 timestamp is before tf1: %f vs %f") % ts1 % ts2)
            .str());
  }

  const auto tf2_diff = ts2 - target_ts;
  const double tf2_weight = tf2_diff / ts_diff;

  const auto trans_diff = tf1.translation() - tf2.translation();
  res_tf.translation() = tf2.translation() + trans_diff * tf2_weight;

  res_tf.linear() = Eigen::Quaterniond{tf2.rotation()}
                        .slerp(tf2_weight, Eigen::Quaterniond{tf1.rotation()})
                        .toRotationMatrix();

  return res_tf;
}

apollo::transform::TransformStamped calculateTF(
    const apollo::transform::TransformStamped& tf1,
    const apollo::transform::TransformStamped& tf2, const double msg_ts) {
  ZoneScoped;
  apollo::transform::TransformStamped res_msg;

  const Eigen::Affine3d tf1_affine =
      Eigen::Translation3d{tf1.transform().translation().x(),
                           tf1.transform().translation().y(),
                           tf1.transform().translation().z()} *
      Eigen::Quaterniond{
          tf1.transform().rotation().qw(), tf1.transform().rotation().qx(),
          tf1.transform().rotation().qy(), tf1.transform().rotation().qz()};

  const Eigen::Affine3d tf2_affine =
      Eigen::Translation3d{tf2.transform().translation().x(),
                           tf2.transform().translation().y(),
                           tf2.transform().translation().z()} *
      Eigen::Quaterniond{
          tf2.transform().rotation().qw(), tf2.transform().rotation().qx(),
          tf2.transform().rotation().qy(), tf2.transform().rotation().qz()};

  const auto res_tf =
      IntepolateAffine(tf1_affine, tf2_affine, tf1.header().timestamp_sec(),
                       tf2.header().timestamp_sec(), msg_ts);

  auto res_trans = res_msg.mutable_transform()->mutable_translation();
  res_trans->set_x(res_tf.translation()[0]);
  res_trans->set_y(res_tf.translation()[1]);
  res_trans->set_z(res_tf.translation()[2]);

  const auto quat = Eigen::Quaterniond{res_tf.linear()};
  auto res_rot = res_msg.mutable_transform()->mutable_rotation();
  res_rot->set_qx(quat.x());
  res_rot->set_qy(quat.y());
  res_rot->set_qz(quat.z());
  res_rot->set_qw(quat.w());

  return res_msg;
}
}  // namespace

namespace apollo {
namespace oscar {

OscarTFBuffer::OscarTFBuffer(const std::string& global2local_tf_topic)
    : global2local_tf_topic_(global2local_tf_topic) {
  node_ = apollo::cyber::CreateNode(
      "OscarTFBuffer_" + std::to_string(apollo::cyber::Time::Now().ToSecond()));
  tf_reader_ = node_->CreateReader<apollo::transform::TransformStampeds>(
      global2local_tf_topic_);
  tf_reader_->SetHistoryDepth(100);
}

std::pair<TransformStamped, TransformStamped> OscarTFBuffer::observeMessages(
    double target_timestamp) {
  std::pair<TransformStamped, TransformStamped> res;
  auto& tf_current = res.first;
  auto& tf_prev = res.second;

  auto it = tf_reader_->Begin();

  tf_current = (**it).transforms(0);
  it++;
  tf_prev = (**it).transforms(0);
  it++;

  for (; it != tf_reader_->End() and
         (**it).transforms(0).header().timestamp_sec() > target_timestamp;
       ++it) {
    tf_current = tf_prev;
    tf_prev = (**it).transforms(0);
  }

  if (tf_current.header().timestamp_sec() < tf_prev.header().timestamp_sec()) {
    AERROR << boost::format("Last ts in buffer is smaller than previous tf");
  }

  return res;
}

void warnExtrapolation(double target_timestamp, double current_timestamp) {
  const auto time_diff = target_timestamp - current_timestamp;
  AWARN << boost::format(
               "Last TF Message in buffer %f is older than target timestamp "
               "%f. Extrapolating transform by %f seconds") %
               current_timestamp % target_timestamp % time_diff;
}

Eigen::Affine3d OscarTFBuffer::get(const std::string& source_frame_id,
                                   const std::string& target_frame_id,
                                   double timestamp_sec,
                                   double max_extrapolation_sec) {
  if (source_frame_id == kWorldFrame) {
    return getInterpolatedTransform(target_frame_id, timestamp_sec,
                                    max_extrapolation_sec);
  }
  if (target_frame_id == kWorldFrame) {
    return getInterpolatedTransform(target_frame_id, timestamp_sec,
                                    max_extrapolation_sec)
        .inverse();
  }
  return getStaticTransform(source_frame_id, target_frame_id);
}

Eigen::Affine3d OscarTFBuffer::getInterpolatedTransform(
    const std::string& frame_id, double timestamp_sec,
    double max_extrapolation_sec) {
  ZoneScoped;

  if (timestamp_sec == 0) {
    timestamp_sec = apollo::cyber::Time::Now().ToSecond();
  }
  tf_reader_->Observe();

  if (std::distance(tf_reader_->Begin(), tf_reader_->End()) < 2) {
    throw tf2::TransformException("not enough tf data");
  }

  auto [tf_current, tf_prev] = observeMessages(timestamp_sec);

  const auto time_diff = timestamp_sec - tf_current.header().timestamp_sec();

  if (time_diff > 0) {
    warnExtrapolation(timestamp_sec, tf_current.header().timestamp_sec());
  }

  if (time_diff > max_extrapolation_sec) {
    throw tf2::ExtrapolationException(
        (boost::format("Extrapolation time %f is larger than max "
                       "extrapolation time %f") %
         time_diff % max_extrapolation_sec)
            .str());
  }

  const auto global2local_child_frame = tf_current.child_frame_id();

  const auto global2loc_tf = calculateTF(tf_prev, tf_current, timestamp_sec);
  const auto local2target_tf =
      getStaticTransform(global2local_child_frame, frame_id);

  return matrixFromTransQuat(global2loc_tf) * local2target_tf;
}

Eigen::Affine3d OscarTFBuffer::getStaticTransform(
    const std::string& source_frame, const std::string& target_frame) {
  ZoneScoped;

  const auto key = std::make_pair(source_frame, target_frame);
  const auto it = static_tf_cache_.find(key);
  if (it != static_tf_cache_.end()) {
    return matrixFromTransQuat(it->second);
  }

  const auto transform_stamped = tf2_buffer_->lookupTransform(
      source_frame, target_frame, apollo::cyber::Time(0));

  static_tf_cache_[key] = transform_stamped;

  return matrixFromTransQuat(transform_stamped);
}

void OscarTFBuffer::waitRecentTransform(double timestamp_sec) {
  bool tf_received = false;
  while (not tf_received) {
    tf_reader_->Observe();
    tf_received =
        (**tf_reader_->Begin()).transforms(0).header().timestamp_sec() -
            timestamp_sec >
        0;
  }
}

}  // namespace oscar
}  // namespace apollo