///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include <map>

#include "Eigen/Core"
#include "Eigen/Dense"
#include "cyber/cyber.h"
#include "modules/common_msgs/transform_msgs/transform.pb.h"
#include "modules/transform/buffer.h"

namespace apollo {
namespace oscar {

using apollo::transform::TransformStamped;

class OscarTFBuffer {
 public:
  explicit OscarTFBuffer(const std::string& global2local_tf_topic = "/tf");

  Eigen::Affine3d get(const std::string& source_frame_id,
                      const std::string& target_frame_id,
                      double timestamp_sec = 0,
                      double max_extrapolation_sec = 1.0);

 private:
  Eigen::Affine3d getInterpolatedTransform(const std::string& frame_id,
                                           double timestamp_sec,
                                           double max_extrapolation_sec = 1.0);

  void waitRecentTransform(double timestamp_sec);

  Eigen::Affine3d getStaticTransform(const std::string& source_frame,
                                     const std::string& target_frame);

  std::pair<TransformStamped, TransformStamped> observeMessages(
      double target_timestamp);

  std::shared_ptr<apollo::cyber::Reader<apollo::transform::TransformStampeds>>
      tf_reader_ = nullptr;
  std::shared_ptr<apollo::cyber::Node> node_ = nullptr;

  apollo::transform::Buffer* tf2_buffer_ =
      apollo::transform::Buffer::Instance();
  std::map<std::pair<std::string, std::string>, TransformStamped>
      static_tf_cache_;

  std::string global2local_tf_topic_;
};
}  // namespace oscar
}  // namespace apollo