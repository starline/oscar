///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>

#include "cyber.h"
#include "modules/oscar/utils/debug/proto/tracking_debug.pb.h"
#include "modules/perception/lidar/lib/tracker/common/mlf_track_data.h"
#include "modules/perception/lidar/lib/tracker/common/tracked_object.h"

namespace apollo {
namespace oscar {
namespace debug {

using apollo::perception::lidar::MlfTrackDataConstPtr;
using apollo::perception::lidar::TrackedObjectConstPtr;

class TrackingDebugPublisher {
 public:
  void add(TrackedObjectConstPtr object);
  void add(MlfTrackDataConstPtr object);
  void publish();

  static TrackingDebugPublisher* Instance() {
    static TrackingDebugPublisher instance;
    return &instance;
  }

 private:
  TrackingDebugPublisher();
  std::shared_ptr<apollo::cyber::Node> node_;
  std::shared_ptr<apollo::cyber::Writer<TrackingDebugMessage>> writer_;
  std::shared_ptr<TrackingDebugMessage> msg_;
  std::mutex mutex_;
};
}  // namespace debug
}  // namespace oscar
}  // namespace apollo