///////////////////////////////////////////////////////////////////////////////////
// Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/utils/debug/tracking_debug_publisher.h"

namespace apollo {
namespace oscar {
namespace debug {

TrackingDebugPublisher::TrackingDebugPublisher() {
  node_ = apollo::cyber::CreateNode("tracking_debug_node");
  writer_ = node_->CreateWriter<TrackingDebugMessage>("/oscar/tracking/debug");

  msg_ = std::make_shared<TrackingDebugMessage>();
}

template <typename T, typename U>
void setVectorToProto(T* proto_msg, U& vector) {
  for (int i = 0; i < vector.size(); ++i) {
    proto_msg->Add(vector.data()[i]);
  }
}

void setVelocity(Track& msg, TrackedObjectConstPtr object) {
  VelocityDebug velocity;

  setVectorToProto(velocity.mutable_measured_barycenter_velocity(),
                   object->measured_barycenter_velocity);
  setVectorToProto(velocity.mutable_measured_center_velocity(),
                   object->measured_center_velocity);
  setVectorToProto(velocity.mutable_measured_nearest_corner_velocity(),
                   object->measured_nearest_corner_velocity);
  setVectorToProto(velocity.mutable_measured_corners_velocity0(),
                   object->measured_corners_velocity[0]);
  setVectorToProto(velocity.mutable_measured_corners_velocity1(),
                   object->measured_corners_velocity[1]);
  setVectorToProto(velocity.mutable_measured_corners_velocity2(),
                   object->measured_corners_velocity[2]);
  setVectorToProto(velocity.mutable_measured_corners_velocity3(),
                   object->measured_corners_velocity[3]);

  setVectorToProto(velocity.mutable_output_velocity(), object->output_velocity);

  setVectorToProto(velocity.mutable_output_velocity_uncertainty(),
                   object->output_velocity_uncertainty);

  setVectorToProto(velocity.mutable_selected_measured_velocity(),
                   object->selected_measured_velocity);

  velocity.set_selected_measured_velocity_norm(
      object->selected_measured_velocity.col(0).norm());

  velocity.set_output_velocity_norm(object->output_velocity.col(0).norm());
  velocity.set_measured_barycenter_velocity_norm(
      object->measured_barycenter_velocity.col(0).norm());
  velocity.set_measured_center_velocity_norm(
      object->measured_center_velocity.col(0).norm());
  velocity.set_measured_nearest_corner_velocity_norm(
      object->measured_nearest_corner_velocity.col(0).norm());
  velocity.set_measured_corners_velocity0_norm(
      object->measured_corners_velocity[0].col(0).norm());
  velocity.set_measured_corners_velocity1_norm(
      object->measured_corners_velocity[1].col(0).norm());
  velocity.set_measured_corners_velocity2_norm(
      object->measured_corners_velocity[2].col(0).norm());
  velocity.set_measured_corners_velocity3_norm(
      object->measured_corners_velocity[3].col(0).norm());

  *msg.mutable_velocity() = velocity;
}

void setPosition(Track& msg, TrackedObjectConstPtr object) {
  Position position;

  auto tf = object->sensor_to_local_pose.inverse();

  setVectorToProto(position.mutable_corners0(), tf * object->corners[0]);
  setVectorToProto(position.mutable_corners1(), tf * object->corners[1]);
  setVectorToProto(position.mutable_corners2(), tf * object->corners[2]);
  setVectorToProto(position.mutable_corners3(), tf * object->corners[3]);

  setVectorToProto(position.mutable_center(), tf * object->center);
  setVectorToProto(position.mutable_barycenter(), tf * object->barycenter);
  setVectorToProto(position.mutable_anchor_point(), tf * object->anchor_point);

  setVectorToProto(position.mutable_direction(), tf * object->direction);
  setVectorToProto(position.mutable_lane_direction(),
                   tf * object->lane_direction);
  setVectorToProto(position.mutable_size(), object->size);

  *msg.mutable_position() = position;
}

// void setFilter(Track& msg, TrackedObjectConstPtr object) {
//   KalmanFilterDebug filter;

//   setVectorToProto(velocity.mutable_measured_barycenter_velocity(),
//                    object->measured_barycenter_velocity);

// }

void TrackingDebugPublisher::add(TrackedObjectConstPtr object) {
  std::lock_guard lg(mutex_);

  // Track track;
  auto track = msg_->add_tracks();

  setVelocity(*track, object);
  setPosition(*track, object);

  // track->set_id(object->object_ptr->id);
  track->set_id(object->track_id);

  track->set_text(object->ToString());
}

void TrackingDebugPublisher::add(MlfTrackDataConstPtr object) {
  if (object->history_objects_.empty()) {
    return;
  }
  auto latest_iter = object->history_objects_.rbegin();
  const auto& latest_object = latest_iter->second;

  add(latest_object);
}

void TrackingDebugPublisher::publish() {
  std::lock_guard lg(mutex_);
  writer_->Write(msg_);
  msg_ = std::make_shared<TrackingDebugMessage>();
}

}  // namespace debug
}  // namespace oscar
}  // namespace apollo