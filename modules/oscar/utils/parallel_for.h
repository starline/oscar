#pragma once
#include <algorithm>
#include <functional>
#include <thread>
#include <vector>

#include "cyber/base/thread_pool.h"
// #include "modules/oscar/profiling/profiling.h"

namespace apollo {
namespace oscar {
namespace utils {

using CCThreadPool = apollo::cyber::base::ThreadPool;

/// @param[in] nb_elements : size of your for loop
/// @param[in] functor(start, end) :
/// your function processing a sub chunk of the for loop.
/// "start" is the first index to process (included) until the index "end"
/// (excluded)
/// @code
///     for(int i = start; i < end; ++i)
///         computation(i);
/// @endcode
/// @param use_threads : enable / disable threads.
///
///
/// https://stackoverflow.com/questions/36246300/parallel-loops-in-c
void parallel_for(unsigned nb_elements, uint32_t nb_threads,
                  std::function<void(int start, int end)> functor,
                  std::shared_ptr<CCThreadPool> thread_pool = nullptr);

void parallel_for(unsigned nb_elements, uint32_t nb_threads,
                  std::function<void(int)> functor,
                  std::shared_ptr<CCThreadPool> thread_pool = nullptr);

}  // namespace utils
}  // namespace oscar
}  // namespace apollo
