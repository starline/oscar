#pragma once

#include <algorithm>
#include <functional>
#include <thread>
#include <vector>

#include "modules/oscar/profiling/profiling.h"
#include "modules/perception/lib/thread/thread_worker.h"

namespace apollo {
namespace oscar {

template <typename T>
class ObjectParallelProvider {
 public:
  using ConstructFn = std::function<void(std::shared_ptr<T>)>;
  using AllocateFn = std::function<std::shared_ptr<T>(void)>;
  ObjectParallelProvider(
      const ConstructFn&& construct_fn = [](const auto&) {},
      const AllocateFn&& allocate_fn =
          [] { return std::shared_ptr<T>(new T{}); })
      : construct_fn_(construct_fn), allocate_fn_(allocate_fn) {
    StartWorkers();
  }

  ObjectParallelProvider(const AllocateFn&& allocate_fn =
                             [] { return std::shared_ptr<T>(new T{}); })
      : construct_fn_([](const auto&) {}), allocate_fn_(allocate_fn) {
    StartWorkers();
  }

  std::shared_ptr<T> Get() {
    ZoneScoped;
    std::lock_guard<LockableBase(std::mutex)> lg(get_mutex_);
    worker_.Join();

    auto tmp = next_object_;
    next_object_ = nullptr;

    worker_.WakeUp();
    return tmp;
  }

  ~ObjectParallelProvider() { worker_.Join(); }

 private:
  void StartWorkers() {
    worker_.Bind([&] {
      PrepareObject();
      return true;
    });
    worker_.Start();
    worker_.WakeUp();
  }
  void PrepareObject() {
    if (next_object_ == nullptr) next_object_ = allocate_fn_();
    construct_fn_(next_object_);
  }

  TracyLockable(std::mutex, get_mutex_);

  std::shared_ptr<T> next_object_ = nullptr;

  apollo::perception::lib::ThreadWorker worker_;

  ConstructFn construct_fn_;
  AllocateFn allocate_fn_;
};

}  // namespace oscar
}  // namespace apollo