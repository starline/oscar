#include "modules/oscar/utils/parallel_for.h"

#include <algorithm>
#include <functional>
#include <thread>
#include <vector>

#include "cyber/base/thread_pool.h"
#include "cyber/task/task.h"
#include "modules/oscar/profiling/profiling.h"

namespace apollo {
namespace oscar {
namespace utils {

void parallel_for(unsigned nb_elements, uint32_t nb_threads,
                  std::function<void(int start, int end)> functor,
                  std::shared_ptr<CCThreadPool> thread_pool) {
  static std::shared_ptr<CCThreadPool> static_thread_pool =
      std::make_shared<CCThreadPool>(std::thread::hardware_concurrency());

  if (thread_pool == nullptr) {
    thread_pool = static_thread_pool;
  }

  unsigned batch_size = nb_elements / nb_threads;
  unsigned batch_remainder = nb_elements % nb_threads;

  std::vector<std::future<void>> results;
  results.reserve(nb_threads);

  for (unsigned int i = 0; i < nb_threads; ++i) {
    ZoneScopedN("Creating task");
    int start = i * batch_size;

    results.emplace_back(
        thread_pool->Enqueue(functor, start, start + batch_size));
    // apollo::cyber::Async(functor, start, start + batch_size));
  }

  // Deform the elements left
  int start = nb_threads * batch_size;
  functor(start, start + batch_remainder);

  for (auto& res : results) res.wait();
}

// overloading for lambdas with only index argument
void parallel_for(unsigned nb_elements, uint32_t nb_threads,
                  std::function<void(int)> functor,
                  std::shared_ptr<CCThreadPool> thread_pool) {
  parallel_for(
      nb_elements, nb_threads,
      [&, func = std::move(functor)](int start, int end) {
        for (int i = start; i < end; ++i) func(i);
      },
      thread_pool);
}

}  // namespace utils
}  // namespace oscar
}  // namespace apollo
