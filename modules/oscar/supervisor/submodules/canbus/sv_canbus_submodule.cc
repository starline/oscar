#include "modules/oscar/supervisor/submodules/canbus/sv_canbus_submodule.h"

DEFINE_double(canbus_check_period, 0.3,
            "Interval in seconds.");


namespace apollo {
namespace supervisor {

CANBUSSupervisor::CANBUSSupervisor()
    : SupervisorRunner("CANBUSSupervisor", FLAGS_canbus_check_period) {
  sv_canbus_node_ = apollo::cyber::CreateNode("sv_canbus_node");
  canbus_chassis_reader_ = sv_canbus_node_->CreateReader<apollo::canbus::Chassis>("/apollo/canbus/chassis", nullptr);
  ACHECK(canbus_chassis_reader_ != nullptr);
  canbus_chassis_detail_reader_ = sv_canbus_node_->CreateReader<apollo::canbus::ChassisDetail>("/apollo/canbus/chassis_detail", nullptr);
  ACHECK(canbus_chassis_detail_reader_ != nullptr);
  canbus_status_writer_ = sv_canbus_node_->CreateWriter<sv_canbus_msg>("/supervisor/canbus/status");
}

void CANBUSSupervisor::RunOnce(const double current_time) {
  status_ = 0;
  debug_msg_ = "";
  canbus_chassis_reader_->Observe();
  const auto &chassis_msg = canbus_chassis_reader_->GetLatestObserved();
  canbus_chassis_detail_reader_->Observe();
  const auto &chassis_detail_msg = canbus_chassis_detail_reader_->GetLatestObserved();
   if(chassis_msg == nullptr) {
    AERROR << "CANBUS message is not ready";
    status_ = 28;
    debug_msg_ = "CANBUS is not ready. Check that CANBUS module is on.";
    return void();
  }

  if(chassis_detail_msg == nullptr) {
    AERROR << "CANBUS DETAIL message is not ready";
    status_ = 27;
    debug_msg_ = "CANBUS  DETAIL is not ready. Check that CANBUS module is on.";
    return void();
  }
  // Check timeout
  if(current_time - chassis_msg->header().timestamp_sec() > 2){
    if(status_ < 28){
      status_ = 28;
      debug_msg_ = "CANBUS message timeout";
    }
  }

  bool has_data = chassis_detail_msg->has_vehicle_spd();
  if(!has_data){
    if(status_ < 29){
      status_ = 29;
      debug_msg_ = "CANBUS has no data";
    }
  }

  // Fill status message
  sv_canbus_msg msg;
  if(status_ < 10) {msg.set_overall_status("OK"); debug_msg_="canbus is OK";}
  if((status_ >= 10)&&(status_ < 20)) msg.set_overall_status("WARNING");
  if((status_ >= 20)&&(status_ < 30)) msg.set_overall_status("ERROR");
  if((status_ >= 30)&&(status_ < 40)) msg.set_overall_status("FATAL");
  msg.set_debug_message(debug_msg_);
  msg.set_canbus_has_data(has_data);
  canbus_status_writer_->Write(msg);
}

void CANBUSSupervisor::GetStatus(std::string* submodule_name, int* status, std::string* debug_msg) {
  *submodule_name = "canbus";
  *status = status_;
  *debug_msg = debug_msg_;
}

}
}
