#include "modules/oscar/supervisor/submodules/perception/sv_prc_submodule.h"

DEFINE_double(prc_check_period, 0.3,
            "Interval in seconds.");

DEFINE_double(timeout, 2, "Timeout in seconds.");


namespace apollo {
namespace supervisor {

PRCSupervisor::PRCSupervisor()
    : SupervisorRunner("PRCSupervisor", FLAGS_prc_check_period) {
  sv_prc_node_ = apollo::cyber::CreateNode("sv_prc_node");
  prc_reader_ = sv_prc_node_->CreateReader<apollo::perception::PerceptionObstacles>("/apollo/perception/obstacles", nullptr);
  ACHECK(prc_reader_ != nullptr);
  lidar_reader_ = sv_prc_node_->CreateReader<apollo::drivers::PointCloud>("/apollo/sensor/lidar128/compensator/PointCloud2", nullptr);
  ACHECK(lidar_reader_ != nullptr);
  prc_status_writer_ = sv_prc_node_->CreateWriter<sv_prc_msg>("/supervisor/perception/status");
  lidar_config_=YAML::LoadFile("/apollo/modules/drivers/ouster/config/config.yaml");
  lidar_rays_quantity_ = lidar_config_["point_cloud"]["rays_quantity"].as<int>();
}

void PRCSupervisor::RunOnce(const double current_time) {
  status_ = 0;
  debug_msg_ = "";
  prc_reader_->Observe();
  const auto &prc_msg = prc_reader_->GetLatestObserved();
  lidar_reader_->Observe();
  const auto &lidar_msg = lidar_reader_->GetLatestObserved();
  bool has_data_from_lidar=false;
  sv_prc_msg msg;

  if(prc_msg == nullptr) {
    AERROR << "PRC message is not ready";
    status_ = 27;
    debug_msg_ = "PRC message is not ready. Check that PRC module is on.";
    msg.set_obstacles_quantity(0);
  }else{
    msg.set_obstacles_quantity(prc_msg->perception_obstacle_size());
    if(current_time - prc_msg->header().timestamp_sec() > FLAGS_timeout){
    if(status_ < 29){
      status_ = 29;
      debug_msg_ = "PRC message timeout";
    }
  }
  }

  if(lidar_msg == nullptr) {
    AERROR << "PointCloud message is not ready";
    if(status_ < 28){
      status_ = 28;
      debug_msg_ = "PointCloud message is not ready. Check that Lidar is on.";
    }
  }
  else if(current_time - lidar_msg->measurement_time() > FLAGS_timeout){
    if(status_ < 29){
      status_ = 29;
      debug_msg_ = "Point Cloud message timeout";
    }
  }else if (lidar_msg->point_size()>0){
    has_data_from_lidar=true;
  }

  // Fill status message
  if(status_ < 10) {msg.set_overall_status("OK"); debug_msg_="Perception is OK";}
  if((status_ >= 10)&&(status_ < 20)) msg.set_overall_status("WARNING");
  if((status_ >= 20)&&(status_ < 30)) msg.set_overall_status("ERROR");
  if((status_ >= 30)&&(status_ < 40)) msg.set_overall_status("FATAL");
  msg.set_debug_message(debug_msg_);
  //msg.set_obstacles_quantity(prc_msg->perception_obstacle_size());
  msg.set_lidar_rays_quantity(lidar_rays_quantity_);
  msg.set_has_data_from_lidar(has_data_from_lidar);
  prc_status_writer_->Write(msg);
}

void PRCSupervisor::GetStatus(std::string* submodule_name, int* status, std::string* debug_msg) {
  *submodule_name = "perception";
  *status = status_;
  *debug_msg = debug_msg_;
}

}
}