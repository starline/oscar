#!/usr/bin/env bash

#############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Nikolay Dema <ndema2301@gmail.com>                             #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

HOOK_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

HOOK_CY_PATH="${HOOK_PATH}/hook_cyberrt"

OSC_DEVICE_PATH="${HOOK_PATH}/osc_device"

VENV_PATH="${HOOK_PATH}/hook_venv"


# ------------------------------------------------------------------------ #

set -e

if [ ! -d ${OSC_DEVICE_PATH} ]; then
  git clone https://gitlab.com/starline/oscar_utils/device.git ${OSC_DEVICE_PATH}
fi

if [ ! -d ${VENV_PATH} ]; then

    python3.8 -m venv ${VENV_PATH}

    source ${VENV_PATH}/bin/activate

    # worklog from OSCAR-1042 | 28.04.2023
    pip install protobuf==3.20.0

    pip install -r ${OSC_DEVICE_PATH}/requirements.txt
    pip install -e ${OSC_DEVICE_PATH}

fi
