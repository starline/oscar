#!/usr/bin/env python

#############################################################################
# Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Nikolay Dema <ndema2301@gmail.com>                             #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

import os
import sys
import json
import traceback

from threading import Event, Lock, Thread
from time      import sleep, time
from queue     import Queue, Empty

from argparse import ArgumentParser
from math     import isnan, pi

# cyber -------------------------------

sys.path.append('/apollo/cyber/python')

from cyber_py3 import cyber

sys.path.append('/apollo/bazel-bin')

from modules.common_msgs.localization_msgs import localization_pb2
from modules.common_msgs.sensor_msgs       import gnss_best_pose_pb2
from modules.common_msgs.chassis_msgs      import chassis_pb2
from modules.common_msgs.control_msgs      import control_cmd_pb2

from google.protobuf.json_format import MessageToDict

# osc -----------------------

import osc_device.log as log

from osc_device import ( Device,
                         Timer,
                         StoppableThread )


SEND_DATA_PERIOD = 5
SEND_BUF_Q_SIZE  = 5 * 60 / SEND_DATA_PERIOD

CYBER_TIMEOUT    = 30            # Подтверждение смены статуса узла


class HookCyberRT:

    def __init__(self, device, cfg = None):

        self._device = device
        self._cfg    = cfg

        self._aggregate_buf = {}

        self._aggregate_q = Queue()
        self._aggregate_l = Lock()

        # -------------------------------------------------

        self._send_buf = []
        self._send_tm  = Timer(self._send_loop, SEND_DATA_PERIOD)

        self._aggregate_thr = StoppableThread(self._aggregate_loop)
        self._aggregate_thr.start()

        self._vehicle_in_auto_mode = False    # TODO: Оптимизировать отправку
                                              # данных в зависимости от mode
        self._chassis_osc = 'chassis'
        self._control_osc = 'control'
        self._gnss_osc    = 'gnss'
        self._loc_osc     = 'loc'

        self._chassis_cy = '/apollo/canbus/chassis'
        self._control_cy = '/apollo/control'
        self._gnss_cy    = '/apollo/sensor/gnss/best_pose'
        self._loc_cy     = '/apollo/localization/pose'

        self._data_converter = { self._chassis_osc : self._chassis_convert,
                                 self._control_osc : self._control_convert,
                                 self._gnss_osc    : self._gnss_convert,
                                 self._loc_osc     : self._loc_convert }

        self._topic_converter = { self._chassis_osc : self._chassis_cy,
                                  self._control_osc : self._control_cy,
                                  self._gnss_osc    : self._gnss_cy,
                                  self._loc_osc     : self._loc_cy }

        # -------------------------------------------------

        cyber.init()
        self._node = cyber.Node('hook')

        # Проблема 1. Порядок работы cyber-а и других фреймворков (mqtt)
        # важен. Что-то внутри cyber-a может упасть c segfault и cb
        # на сообщения могут перестать обрабатываться или не начать вовсе.
        # Похоже, что порядок вызова не гарантирует успешный запуск
        # ввиду асинхронных процессов внутри.

        self._last_msg_time = { self._chassis_cy : 0.0,
                                self._gnss_cy    : 0.0,
                                self._loc_cy     : 0.0 }

                              # self._control_cy : 0.0,

        self._cy_check_tm = Timer(self._cy_check_loop, CYBER_TIMEOUT)

        # -------------------------------------------------

        self._device.connect()

        self._chassis_sub = self._node.create_reader(self._chassis_cy,
                                                     chassis_pb2.Chassis,
                                                     self._chassis_cb)

        # self._control_sub = self._node.create_reader(self._control_cy,
        #                                              control_cmd_pb2.ControlCommand,
        #                                              self._control_cb)

        self._gnss_sub = self._node.create_reader(self._gnss_cy,
                                                  gnss_best_pose_pb2.GnssBestPose,
                                                  self._gnss_cb)

        self._loc_sub = self._node.create_reader(self._loc_cy,
                                                 localization_pb2.LocalizationEstimate,
                                                 self._loc_cb)


    def _cy_check_loop(self):

        if cyber.ok():

            # Попытка обхода Проблема 1.

            # Если сообщения не приходят какое-то время, то проверяем, есть ли
            # узлы публикаторы в соответствующих топиках.
            #  - Если нет, то считаем, что узел выключен.
            #  - Если есть, то считаем, что мы сломались и перезапускаем себя.

            cur_time = time()

            for node in cyber.NodeUtils.get_nodes(sleep_s=0):

                for ch in cyber.NodeUtils.get_writersofnode(node, sleep_s=0):

                    # if ch in self._last_msg_time:
                    if ch == self._chassis_cy:

                        # Может совпасть время появления writer-a
                        # и cy_check, тогда сообщение может не успеть дойти
                        sleep(0.2)

                        dtime = cur_time - self._last_msg_time[ch]

                        if dtime >= CYBER_TIMEOUT:

                            log.warn('Cyber перестал вызывать callback-и '  +
                                     '(см. Проблема 1 в hook_node.py).\n\n' +
                                     'Перезапуск...', 'HOOK | w02')

                            os._exit(1)

            # -------------------------

            # cy_topo = {}  # { t: { node_name: {pub: [topic, ..],
            #                                    sub: [topic, ..] } } }

            #     info = strftime("%H:%M:%S %d-%m-%Y", localtime(last_time))
            #     info += ' for ' + str(round(dtime, 1)) + ' sec'

            #     timeouts.update({topic : info})

            # info = 'TIMEOUTS : ' + json.dumps(timeouts, indent=2)
            # log.warn(info, 'HOOK | w03')


    def _chassis_cb(self, msg):
        self._last_msg_time[self._chassis_cy] = time()
        self._aggregate_q.put((self._chassis_osc, msg))


    def _control_cb(self, msg):

        return

        if self._vehicle_in_auto_mode:

            try:

                data = {'t' : msg.header.timestamp_sec}
                data.update(MessageToDict(msg))

                self._aggregate_q.put((self._control_osc, data))

            except:
                log.err(traceback.format_exc() +
                    '\nHandling msg from ' + log.TEXT.STYLE.BOLD +
                    '/apollo/control' + log.TEXT.STYLE.RESET + ' topic ' +
                    'faild on: ' + json.dumps(MessageToDict(msg), indent=2),
                    'HOOK | e05')


    def _gnss_cb(self, msg):
        self._last_msg_time[self._gnss_cy] = time()
        self._aggregate_q.put((self._gnss_osc, msg))


    def _loc_cb(self, msg):
        self._last_msg_time[self._loc_cy] = time()
        self._aggregate_q.put((self._loc_osc, msg))


    def _chassis_convert(self, msg):

        data = {'t' : msg.header.timestamp_sec}

        if msg.HasField('speed_mps') and not isnan(msg.speed_mps):
            data.update({'speed_kph' : round(msg.speed_mps * 3.6, 1) })

        # if msg.HasField('throttle_percentage') and not isnan(msg.throttle_percentage):
        #     data.update({'throttle' : msg.throttle_percentage})

        # if msg.HasField('brake_percentage') and not isnan(msg.brake_percentage):
        #     data.update({'brake' : msg.brake_percentage})

        if msg.HasField('steering_percentage') and not isnan(msg.steering_percentage):
            data.update({'sw_prc' : round(msg.steering_percentage, 3) })

        if msg.HasField('driving_mode') and not isnan(msg.driving_mode):

            # mode_name = chassis_pb2.Chassis.DrivingMode.Name(msg.driving_mode)
            # data.update({'mode' : mode_name})

            if msg.driving_mode == chassis_pb2.Chassis.COMPLETE_AUTO_DRIVE:
                data.update({'mode' : 'AUTO'})
                self._vehicle_in_auto_mode = True
            else:
                data.update({'mode' : 'MANUAL'})
                self._vehicle_in_auto_mode = False

        return data


    def _control_convert(self, msg):
        pass


    def _gnss_convert(self, msg):

        data = {'t' : msg.header.timestamp_sec}

        if msg.HasField('longitude') and not isnan(msg.longitude):
            data.update({ 'lon' : msg.longitude })

        if msg.HasField('latitude') and not isnan(msg.latitude):
            data.update({ 'lat' : msg.latitude })

        if msg.HasField('height_msl') and not isnan(msg.height_msl):
            data.update({ 'msl' : msg.height_msl })       # mean sea level

        if msg.HasField('sol_status') and not isnan(msg.sol_status):

            status_name = gnss_best_pose_pb2.SolutionStatus.Name(msg.sol_status)

            data.update({ 'sol_status'     : msg.sol_status,
                          'sol_status_str' : status_name })

        if msg.HasField('sol_type') and not isnan(msg.sol_type):

            type_name = gnss_best_pose_pb2.SolutionType.Name(msg.sol_type)

            data.update({ 'sol_type'     : msg.sol_type,
                          'sol_type_str' : type_name })

        if msg.HasField('num_sats_tracked') and not isnan(msg.num_sats_tracked):
            data['sats_tracked'] = msg.num_sats_tracked

        if msg.HasField('num_sats_in_solution') and not isnan(msg.num_sats_in_solution):
            data['sats_in_solution'] = msg.num_sats_in_solution

        if msg.HasField('datum_id') and not isnan(msg.datum_id):
            data.update({ 'gs' : str(msg.datum_id) })        # Geodetic System

        return data

        # Проблема 2. При попытке собирать весь топик таким образом данные
        # передаются по mqtt, но через какое-то время воспроизводится Проблема 1.

        # Предположение: при больших посылках при QoS 2 где-то внутри
        # paho-mqtt слишком долго идет ожидание и сново каким-то чудесным
        # образом конфликтует с cyber.

        # return MessageToDict(msg)


    def _loc_convert(self, msg):

        data = {'t' : msg.header.timestamp_sec}

        if msg.HasField('pose'):

            data.update({ 'x' : round(msg.pose.position.x, 3) })
            data.update({ 'y' : round(msg.pose.position.y, 3) })
            data.update({ 'z' : round(msg.pose.position.z, 3) })

            data.update({ 'vx' : round(msg.pose.linear_velocity.x, 5) })
            data.update({ 'vy' : round(msg.pose.linear_velocity.y, 5) })
            # data.update({ 'vz' : round(msg.pose.linear_velocity.z, 5) })

            # data.update({ 'ax' : msg.pose.linear_acceleration.x })
            # data.update({ 'ay' : msg.pose.linear_acceleration.y })
            # data.update({ 'az' : msg.pose.linear_acceleration.z })

            data.update({ 'Z' : round(msg.pose.heading / pi * 180.0, 5) })

            # data.update({ 'vX' : msg.pose.angular_velocity.x })
            # data.update({ 'vY' : msg.pose.angular_velocity.y })
            # data.update({ 'vZ' : msg.pose.angular_velocity.z })

        return data


    def _aggregate_loop(self):

        while self._aggregate_thr.running():

            try:
                topic, msg = self._aggregate_q.get(block = True, timeout = 2)

            except Empty:
                continue

            self._aggregate_q.task_done()

            try:
                data = self._data_converter[topic](msg)

            except:
                log.err(traceback.format_exc() +
                        '\Converting ' + log.TEXT.STYLE.BOLD +
                        topic + log.TEXT.STYLE.RESET +
                        ' msg from apollo to OSC faild on: ' +
                         json.dumps(MessageToDict(msg), indent=2),
                        'HOOK | e03')

                continue

            try:
                if data:
                    self._aggregate(topic, data)

            except:
                log.err(traceback.format_exc(), 'HOOK | e04')


    def _aggregate(self, topic, data):

        with self._aggregate_l:

            if not topic in self._aggregate_buf:
                self._aggregate_buf.update({topic : []})

            self._aggregate_buf[topic].append(data)


    def _send_loop(self):

        # TODO: Завернуть в try, чтобы поток продолжал жить.

        with self._aggregate_l:

            if self._aggregate_buf:

                self._send_buf.append(self._aggregate_buf)
                self._aggregate_buf = {}

        if not self._send_buf:
            return

        # -------------------------------------------------

        combined = {}

        for chunk in self._send_buf:

            for topic in chunk:

                if not topic in combined:
                    combined.update({topic : []})

                combined[topic].extend(chunk[topic])

        topic_data = {}     # {'topic_1' : {'meta : {}, 'key_1' : [],
                            #                           'key_2' : [], ... } }

        for topic in combined:

            if combined[topic]:

                try:
                    topic_data.update({ topic : self._compress(combined[topic]) })

                except:
                    log.err(traceback.format_exc() +
                            '\nCompression from ' + log.TEXT.STYLE.BOLD +
                            topic + log.TEXT.STYLE.RESET + ' topic ' +
                            'faild on: ' + json.dumps(combined[topic], indent=2),
                            'HOOK | e06')

        # -------------------------------------------------

        if self._device.send(topic_data, 'apollo/topics', SEND_DATA_PERIOD * 0.8):
            self._send_buf = []

        else:
            send_buf_len = len(self._send_buf)

            if send_buf_len > SEND_BUF_Q_SIZE:
                self._send_buf.pop(send_buf_len // 2)


    def _compress(self, datas):

        # 'meta' : { 'reps' : { 'key_1' : [[pos_1, repeat_nums],
        #                                  [pos_2: repeat_nums], ... ],
        #                       'key_2' : [...], ... } }

        compressed = {}

        for key in datas[0]:

            if key == 'meta':
                continue

            compressed.update({ key : [ datas[0][key] ] })

        # -------------------------------------------------

        compressed_ends = { key : 0 for key in compressed}

        for data_idx in range(1, len(datas)):

            curr_data = datas[data_idx]

            for key in curr_data:

                curr_val = curr_data[key]

                if key in compressed_ends:

                    if curr_val == compressed[key][compressed_ends[key]]:

                        if not 'meta' in compressed:
                            compressed.update({ 'meta' : { 'reps' : {} } })

                        elif not 'reps' in compressed['meta']:
                            compressed['meta'].update({ 'reps' : {} })

                        reps = compressed['meta']['reps']

                        if not key in reps:
                            reps.update({ key : [] })

                        if reps[key] and compressed_ends[key] == reps[key][-1][0]:
                            reps[key][-1][1] += 1

                        else:
                            reps[key].append([compressed_ends[key], 1])

                    else:
                        compressed[key].append(curr_data[key])
                        compressed_ends[key] += 1

                else:
                    compressed_ends.update({ key : data_idx})
                    compressed.update({ key: [None, curr_data[key]] })

                    if not 'meta' in compressed:
                            compressed.update({ 'meta' : { 'reps' : {} } })

                    elif not 'reps' in compressed['meta']:
                        compressed['meta'].update({ 'reps' : {} })

                    compressed['meta']['reps'].update({ key : [0, data_idx - 1 ] })

        return compressed


    def spin(self):
        while cyber.ok():
            sleep(1)


# ------------------------------------------------------------------------ #

def connect_cb():
    info = 'Connected to osc_fleet ( ' + str(d_cfg['broker_host']) + ':'      \
                                       + str(d_cfg['broker_port']) + ' ) as ' \
                                       + str(d_cfg['uuid'])
    log.done(info, 'HOOK | d01')


def message_cb(msg, topic, mid):
    info = 'New message from osc_fleet ( '+ str(d_cfg['broker_host']) + ':' \
                                          + str(d_cfg['broker_port']) + ' )'
    info += '\n\nTopic  : ' + topic
    info +=   '\nMsg id : ' + mid

    info += '\n\n' + '\n'.join(json.dumps(msg, indent = 2).split('\n')[1:-1])

    log.info(info, 'HOOK | i01')


def disconnect_cb(rc):
    info = 'Disconnected from osc_fleet ( '         \
                + str(d_cfg['broker_host']) + ':'   \
                + str(d_cfg['broker_port']) + ' )'  \
                + '\n\npaho-mqtt reason: ' + str(rc)

    log.warn(info, 'HOOK | w01')


if __name__ == '__main__':

    cfg = {}

    if len(sys.argv) < 2:
        log.err('Setup path to config file as first param\n', 'HOOK | e08')
        exit(4)

    if os.path.exists(sys.argv[1]):

        try:
            with open(sys.argv[1]) as cfg_file:
                cfg.update(json.load(cfg_file))

        except:
            log.err('Config file cant be open or load:\n'   +
                    log.TEXT.STYLE.BOLD  + str(sys.argv[1]) +
                    log.TEXT.STYLE.RESET + '\n\n'           +
                    traceback.format_exc(),
                    'HOOK | e02')
            exit(2)

    else:
        log.err('Config file does not exist:\n' +
                log.TEXT.STYLE.BOLD + str(sys.argv[1]) +
                log.TEXT.STYLE.RESET,
                'HOOK | e01')

        exit(1)

    c_cfg = cfg.get('cyber', {})
    d_cfg = cfg.get('device', {})

    if not d_cfg:
        log.err('Config file does not have device part', 'HOOK | e07')
        exit(3)

    device = Device(d_cfg, connect_cb, message_cb, disconnect_cb, autocon = False)

    hook_cyber = HookCyberRT(device, c_cfg)
    hook_cyber.spin()
