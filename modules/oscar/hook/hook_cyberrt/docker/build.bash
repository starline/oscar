#!/usr/bin/env bash

#############################################################################
# Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Nikolay Dema <ndema2301@gmail.com>                             #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/setup.bash"


if [ -z "$(docker images -q ${BASE_DIMG} 2> /dev/null)" ]; then
    printf "\n\n Local image ${BASE_DIMG} not found. Do \"oscar docker start\" first!\n\n"
    exit 1
fi


printf "\n\n BUILDING ${HOOK_DIMG} DOCKER IMAGE: \n\n"

DOCKER_BUILDKIT=1                                                          \
docker build  -t ${HOOK_DIMG}                                              \
              -f ${DCTX_PATH}/hook_cyberrt/docker/Dockerfile ${DCTX_PATH}  \
             --network=host                                                \
             --build-arg base_image="${BASE_DIMG}"
