#!/usr/bin/env bash

#############################################################################
# Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Nikolay Dema <ndema2301@gmail.com>                             #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################


APOLLO_ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")/../../../../.." && pwd -P)"

APOLLO_CONFIG="${HOME}/.apollo"

BASE_DIMG="apolloauto/apollo:dev-x86_64-18.04-20221124_1708"
HOOK_DIMG="hook:dev-x86_64-18.04-20221124_1708"
HOOK_DCTR="hook"

DCTX_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"