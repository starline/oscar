#!/usr/bin/env bash

#############################################################################
# Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Nikolay Dema <ndema2301@gmail.com>                             #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/setup.bash"


if [ -z "$(docker images -q ${HOOK_DIMG} 2> /dev/null)" ]; then

    bash "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/build.bash"
    [ $? -ne 0 ] && exit 1

fi


if [ ! -d "${APOLLO_ROOT}/.cache" ]; then
    printf "\n\n Build oscar first!\n\n"
    exit 1
fi


if [ -n "$(which nvidia-smi)" ] && [ -n "$(nvidia-smi)" ]; then
    GPU_FLAG=(--gpus all)
    USE_GPU_HOST=1
else
    USE_GPU_HOST=0
fi

HOOK="/apollo/modules/oscar/hook/hook_cyberrt/scripts/run_within_env.bash"

# -ti --rm

docker run ${GPU_FLAG[@]} -d                                               \
           --privileged                                                    \
           --memory "600m"                                                 \
           --log-opt max-size=2G                                           \
           --env PYTHONUNBUFFERED=1                                        \
           --env DISPLAY="${DISPLAY:-:0}"                                  \
           --env USE_GPU_HOST="${USE_GPU_HOST}"                            \
           --env NVIDIA_VISIBLE_DEVICES=all                                \
           --env NVIDIA_DRIVER_CAPABILITIES=compute,video,graphics,utility \
           --net host                                                      \
           --pid=host                                                      \
           --ipc=host                                                      \
           --shm-size "2G"                                                 \
           --hostname "$(hostname)"                                        \
           --add-host "$(hostname):127.0.0.1"                              \
           --volume ${APOLLO_ROOT}:/apollo                                 \
           --volume ${APOLLO_CONFIG}:${APOLLO_CONFIG}                      \
           --volume /dev:/dev                                              \
           --volume /media:/media                                          \
           --volume /tmp/.X11-unix:/tmp/.X11-unix:rw                       \
           --volume /etc/localtime:/etc/localtime:ro                       \
           --volume /usr/src:/usr/src                                      \
           --volume /lib/modules:/lib/modules                              \
           --volume /dev/null:/dev/raw1394                                 \
           --workdir /apollo                                               \
           --restart unless-stopped                                        \
           --name "${HOOK_DCTR}"                                           \
           "${HOOK_DIMG}"                                                  \
           ${HOOK} ${1}       # 1-st agr - hook_cfg_path within docker