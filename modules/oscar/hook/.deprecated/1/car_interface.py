import os
from apollo_contact_layer import ApolloAutoHook
from modules.routing.proto.poi_pb2 import POI
from time import sleep
from pyproj import Proj
import common.proto_utils as proto_utils


class CarInterface:
    def __init__(self, hook: ApolloAutoHook):
        self.hook = hook
        self.map_trans = Proj("+proj=utm +zone=10 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs")

    def get_info(self, uuid=''):

        info = [(f'{uuid}/localisation/cartesian', self.localisation_cartesian()),
                (f'{uuid}/localisation/wgs', self.localization_wgs()),
                (f'{uuid}/auto/kinematic', self.auto_kinematic()),
                (f'{uuid}/hmi', self.hmi()),
                (f'{uuid}/map/poi', self.get_poi_list())]

        return info

    def localisation_cartesian(self):
        localization = self.hook.loc

        return {'data': {'position': {'x': localization['x'],
                                      'y': localization['y'],
                                      'z': localization['z']},
                         'heading': localization['heading']}}

    def localization_wgs(self):
        localization = self.hook.loc
        gps = self.hook.gps

        return {'data': {'position': {'latitude': gps['latitude'],
                                      'longitude': gps['longitude']},
                         'heading': localization['heading']}}

    def auto_kinematic(self):
        kinematic = self.hook.veh

        return {'data': {'speed': kinematic['speed'],
                         'steering': kinematic['steering'],
                         'throttle': kinematic['throttle'],
                         'brake': kinematic['brake']}}

    def hmi(self):
        hmi = self.hook.hmi

        return {'data': {'map': hmi['map'],
                         'car': hmi['car'],
                         'mode': hmi['mode']}}

    def routing_response(self):
        pass

    def start_auto(self):
        os.system('cyber_launch start /apollo/modules/control/launch/control.launch 1>&- 2>&-  &')
        os.system('cyber_launch start /apollo/modules/planning/launch/planning.launch 1>&- 2>&-  &')
        self.hook.start_time()

        return {'status': 0, 'statusText': 'OK'}

    def get_poi_list(self):
        poi = POI()

        try_count = 0
        hmi = self.hmi()
        while hmi['data']['map'] == '' and try_count < 10:
            sleep(0.2)
            try_count += 1

        if try_count == 10:
            return {'data': []}
        try:
            proto_utils.get_pb_from_text_file(
                '/apollo/modules/map/data/{}/default_end_way_point.txt'.format(hmi['data']['map']), poi)
        except Exception as e:
            print(e)
            return {"data": []}
        poi_list = []
        for landmarks in poi.landmark:
            for wp in landmarks.waypoint:
                wp_lon, wp_lat = self.map_trans(wp.pose.x, wp.pose.y, inverse=True)
                poi_list.append({"name": wp.id, "position": {"latitude": wp_lat, "longitude": wp_lon}})
        res = {"data": poi_list}
        return res

    @staticmethod
    def stop_auto():
        '''
        TO DO
        Soft stop to control
        '''
        os.system('kill $(pgrep -f "control.dag")')
        os.system('kill $(pgrep -f "planning.dag")')
        return {'status': 0, 'statusText': 'OK'}


if __name__ == '__main__':
    hook = ApolloAutoHook()
    interface = CarInterface(hook)
    interface.get_info()

