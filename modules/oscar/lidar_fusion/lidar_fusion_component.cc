///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/lidar_fusion/lidar_fusion_component.h"

#include <chrono>
#include <memory>
#include <thread>

#include "cyber/base/thread_safe_queue.h"
#include "cyber/scheduler/scheduler_factory.h"
#include "modules/oscar/utils/parallel_for.h"
#include "modules/oscar/utils/point_cloud_pool.h"

namespace apollo {
namespace oscar {

using apollo::cyber::Time;
using apollo::cyber::base::CCObjectPool;

using namespace std::chrono_literals;
static constexpr auto kDefaultReaderMsgDepth = 2;
static constexpr auto kOMPThreadsNum = 4;
static constexpr auto kDefaultCloudSize = 100'000;
static constexpr auto kSleepReadingChannelsCycle = 200us;
static constexpr auto kCloudPoolSize = 10;

namespace {

int copy_cloud(std::shared_ptr<PointCloud>& main_cloud,
               const std::shared_ptr<PointCloud>& add_cloud, int current_size,
               bool update_meta) {
  ZoneScopedN("Copy pointcloud");
  ZoneValue(current_size);
  if (update_meta) {
    main_cloud->mutable_header()->CopyFrom(add_cloud->header());

    main_cloud->set_frame_id(add_cloud->has_frame_id()
                                 ? add_cloud->frame_id()
                                 : add_cloud->header().frame_id());
    main_cloud->set_frame_id(add_cloud->frame_id());
    main_cloud->set_height(add_cloud->height());
    main_cloud->set_measurement_time(add_cloud->measurement_time());

    main_cloud->set_is_dense(false);
  }

  {
    ZoneScopedN("Expanding cloud");

    auto diff_sz =
        current_size + add_cloud->point_size() - main_cloud->point_size();
    ZoneValue(diff_sz);
    if (diff_sz > 0) {
      main_cloud->mutable_point()->Reserve(current_size +
                                           add_cloud->point_size());
    }
    for (int i = 0; i < diff_sz; ++i) {
      main_cloud->mutable_point()->Add();
    }
  }

  {
    ZoneScopedN("Copy cloud");
    auto sz = add_cloud->point_size();

    apollo::oscar::utils::parallel_for(sz, kOMPThreadsNum, [&](int idx) {
      auto& p = (*main_cloud->mutable_point())[current_size + idx];
      auto& add_p = add_cloud->point()[idx];
      p.MergeFrom(add_p);
    });
  }

  auto new_size = current_size + add_cloud->point_size();
  if (main_cloud->height() == add_cloud->height()) {
    int new_width = new_size / main_cloud->height();
    main_cloud->set_width(new_width);
  } else {
    main_cloud->set_height(1);
    main_cloud->set_width(new_size);
  }
  ZoneValue(new_size);
  return new_size;
}

/*
Class wraps memory efficient operations on point cloud:
1. Firstly, It takes a pointer to already allocated prototype cloud with
sufficient (probably) number of points
2. At each merge function call it assigns points to it's buffer, tracking
actual nubmer of stored points.
3. At getFinal() function it truncates redundant points from current cloud and
returns it.
4. In destructor, new prototype is created for fast assignment in next
message. Also, redundant points are deallocated.
*/
struct PointCloudMerger {
  PointCloudMerger(std::shared_ptr<PointCloud> cloud) : cloud_(cloud) {}

  void mergeCloud(const std::shared_ptr<PointCloud>& other,
                  bool update_meta = false) {
    current_size_ = copy_cloud(cloud_, other, current_size_, update_meta);
  }

  std::shared_ptr<PointCloud> getFinal() {
    ZoneScopedN("Cleanup output cloud");

    auto delete_sz = cloud_->point_size() - current_size_;

    if (delete_sz > 0)
      cloud_->mutable_point()->DeleteSubrange(current_size_, delete_sz);
    ZoneValue(delete_sz);

    return cloud_;
  }

  const std::shared_ptr<PointCloud>& get() const { return cloud_; }

  std::shared_ptr<PointCloud> cloud_;

  int current_size_ = 0;
};

}  // namespace

struct ChannelTransformer {
  ChannelTransformer(std::shared_ptr<apollo::cyber::Node> node,
                     const std::string& channel_name,
                     const std::string& main_frame_id,
                     PointCloudPool& cloud_pool)
      : buffer_ptr_(apollo::transform::Buffer::Instance()),
        main_frame_id_(main_frame_id),
        cloud_pool_(cloud_pool) {
    apollo::cyber::proto::RoleAttributes attrs;

    attrs.set_channel_name(channel_name);
    attrs.mutable_qos_profile()->set_depth(kDefaultReaderMsgDepth);

    reader_ = node->CreateReader<PointCloud>(
        attrs, [this](const auto& msg) { processMsg(msg); });
  }

  void processMsg(const std::shared_ptr<PointCloud>& msg) {
    // 1. get TF
    // 2. Save message to inner object
    // 3. transform message

    ZoneScopedN("Cloud channel transformer");

    AINFO << "Transforming " << msg->header().frame_id();

    if (transform_ == nullptr && !getTF(msg->header().frame_id())) return;

    auto save_msg = cloud_pool_.get();

    save_msg->mutable_point()->Reserve(msg->point_size());
    for (int i = 0; i < msg->point_size(); i++)
      save_msg->mutable_point()->Add();

    save_msg->mutable_header()->MergeFrom(msg->header());
    save_msg->set_measurement_time(msg->measurement_time());

    {
      ZoneScopedN("Transform");
      apollo::oscar::utils::parallel_for(
          msg->point_size(), kOMPThreadsNum, [&](int i) {
            auto& src_point = (*msg->mutable_point())[i];
            auto dest_point = save_msg->mutable_point(i);

            if (std::isnan(src_point.x())) return;

            Eigen::Matrix<float, 3, 1> pt(src_point.x(), src_point.y(),
                                          src_point.z());
            dest_point->set_x(static_cast<float>(
                (*transform_)(0, 0) * pt.coeffRef(0) +
                (*transform_)(0, 1) * pt.coeffRef(1) +
                (*transform_)(0, 2) * pt.coeffRef(2) + (*transform_)(0, 3)));
            dest_point->set_y(static_cast<float>(
                (*transform_)(1, 0) * pt.coeffRef(0) +
                (*transform_)(1, 1) * pt.coeffRef(1) +
                (*transform_)(1, 2) * pt.coeffRef(2) + (*transform_)(1, 3)));
            dest_point->set_z(static_cast<float>(
                (*transform_)(2, 0) * pt.coeffRef(0) +
                (*transform_)(2, 1) * pt.coeffRef(1) +
                (*transform_)(2, 2) * pt.coeffRef(2) + (*transform_)(2, 3)));

            dest_point->set_intensity(src_point.intensity());
            dest_point->set_timestamp(src_point.timestamp());
          });

      {
        std::lock_guard lg(msg_mutex_);
        last_msg_ = save_msg;
      }
    }
  }

  std::shared_ptr<PointCloud> getMsg() {
    std::lock_guard lg(msg_mutex_);
    return last_msg_;
  }

 private:
  std::shared_ptr<Eigen::Affine3d> getTF(const std::string& target_frame_id) {
    if (transform_ == nullptr) {
      transform_ = std::make_shared<Eigen::Affine3d>();
      if (!QueryTFAffine(target_frame_id, transform_.get()))
        transform_ = nullptr;
    }

    return transform_;
  }

  bool QueryTFAffine(const std::string& target_frame_id, Eigen::Affine3d* tf) {
    apollo::transform::TransformStamped stamped_transform;
    try {
      stamped_transform = buffer_ptr_->lookupTransform(
          main_frame_id_, target_frame_id, cyber::Time(0));
    } catch (tf2::TransformException& ex) {
      AERROR << ex.what();
      return false;
    }

    auto& translation = stamped_transform.transform().translation();
    auto& quat = stamped_transform.transform().rotation();
    *tf = Eigen::Translation3d(translation.x(), translation.y(),
                               translation.z()) *
          Eigen::Quaterniond(quat.qw(), quat.qx(), quat.qy(), quat.qz());
    return true;
  }

  std::mutex msg_mutex_;

  apollo::transform::Buffer* buffer_ptr_ = nullptr;
  std::shared_ptr<PointCloud> last_msg_ = nullptr;

  std::shared_ptr<Eigen::Affine3d> transform_ = nullptr;
  const std::string& main_frame_id_;

  std::shared_ptr<Reader<PointCloud>> reader_ = nullptr;
  PointCloudPool& cloud_pool_;
};

bool OscarLidarFusion::Init() {
  if (!GetProtoConfig(&conf_)) {
    AWARN << "Load config failed, config file" << ConfigFilePath();
    return false;
  }
  buffer_ptr_ = apollo::transform::Buffer::Instance();

  apollo::cyber::proto::RoleAttributes attrs;

  fusion_writer_ = node_->CreateWriter<PointCloud>(conf_.fusion_channel());

  for (const auto& channel : conf_.input_channel()) {
    channel_transformers_.emplace_back(std::make_shared<ChannelTransformer>(
        node_, channel, conf_.main_frame_id(), cloud_pool_));
  }

  return true;
}

bool OscarLidarFusion::Proc(const std::shared_ptr<PointCloud>& point_cloud) {
  AINFO << "Fusion transport diff: "
        << Time::Now().ToSecond() - point_cloud->header().timestamp_sec();

  AINFO << "Fusion transport latency: "
        << apollo::cyber::Time::Now().ToSecond() -
               point_cloud->header().lidar_timestamp() / 1e9;

  auto start_time = Time::Now().ToSecond();

  PointCloudMerger output_cloud_wrapper(cloud_pool_.get());

  ZoneScopedN("Fusion main");
  output_cloud_wrapper.mergeCloud(point_cloud, true);

  auto fusion_transformers = channel_transformers_;

  do {
    ZoneScopedN("Fusion reader Cycle");

    auto previous_remaining_channels = fusion_transformers.size();

    for (auto itr = fusion_transformers.begin();
         itr != fusion_transformers.end();) {
      {
        auto& transformer = **itr;

        auto last_msg = transformer.getMsg();

        if (last_msg == nullptr ||
            (conf_.drop_expired_data() &&
             IsExpired(output_cloud_wrapper.get(), last_msg))) {
          ZoneScopedN("Skipping");
          ++itr;
          continue;
        }
        
        output_cloud_wrapper.mergeCloud(last_msg);
      }

      itr = fusion_transformers.erase(itr);
    }

    if (fusion_transformers.size() > 0 &&
        (previous_remaining_channels == fusion_transformers.size()))
      std::this_thread::sleep_for(kSleepReadingChannelsCycle);

  } while ((Time::Now().ToSecond() - start_time) < conf_.wait_time_s() &&
           fusion_transformers.size() > 0);

  double end_time = cyber::Time::Now().ToSecond();
  double module_latency = (end_time - start_time) * 1e3;

  AINFO << std::setprecision(16) << "FRAME_STATISTICS: process time(msec)["
        << module_latency << "], cur_latency: "
        << (end_time - point_cloud->measurement_time()) * 1000;

  auto target = output_cloud_wrapper.getFinal();

  target->mutable_header()->set_timestamp_sec(end_time);

  TracyPlot("Fusion latency, msec",
            static_cast<int64_t>((end_time - point_cloud->measurement_time()) *
                                 1000));

  if (fusion_transformers.size() > 0) {
    AERROR << "Not all clouds merged";
    if (!conf_.allow_missing_channels()) return true;
  }

  {
    ZoneScopedN("Fusion write");

    std::string info = "Points: " + std::to_string(target->point().size());
    ZoneText(info.c_str(), info.size());

    fusion_writer_->Write(target);
  }

  TracyPlot("Fusion points", static_cast<int64_t>(target->point().size()));

  return true;
}

bool OscarLidarFusion::IsExpired(const std::shared_ptr<PointCloud>& target,
                                 const std::shared_ptr<PointCloud>& source) {
  auto diff = target->measurement_time() - source->measurement_time();

  bool expired = diff * 1000 > conf_.max_interval_ms();
  if (expired) {
    AWARN << "Diff: [" << target->header().frame_id() << " -> "
          << source->header().frame_id() << "]: " << diff;
  } else {
    AINFO << "Merging " << source->header().frame_id() << ". Diff: " << diff;
  }
  return expired;
}

}  // namespace oscar
}  // namespace apollo
