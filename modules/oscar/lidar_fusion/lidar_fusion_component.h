///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>
#include <string>
#include <vector>

#include "Eigen/Eigen"

// Eigen 3.3.7: #define ALIVE (0)
// fastrtps: enum ChangeKind_t { ALIVE, ... };
#if defined(ALIVE)
#undef ALIVE
#endif

#include "cyber/cyber.h"
#include "modules/common_msgs/sensor_msgs/pointcloud.pb.h"
#include "modules/oscar/lidar_fusion/proto/lidar_fusion_config.pb.h"
#include "modules/oscar/utils/point_cloud_pool.h"
#include "modules/transform/buffer.h"

namespace apollo {
namespace oscar {

using apollo::cyber::Component;
using apollo::cyber::Reader;
using apollo::cyber::Writer;
using apollo::drivers::PointCloud;
using apollo::drivers::PointXYZIT;

using apollo::oscar::utils::PointCloudPool;

struct ChannelTransformer;

class OscarLidarFusion : public Component<PointCloud> {
 public:
  bool Init() override;
  bool Proc(const std::shared_ptr<PointCloud>& point_cloud) override;

 private:
  bool IsExpired(const std::shared_ptr<PointCloud>& target,
                 const std::shared_ptr<PointCloud>& source);

  FusionConfig conf_;
  apollo::transform::Buffer* buffer_ptr_ = nullptr;
  std::shared_ptr<Writer<PointCloud>> fusion_writer_;
  constexpr static auto kInitCloudSize = 50'000;
  constexpr static auto kPoolSize = 20;
  PointCloudPool cloud_pool_{kInitCloudSize, kPoolSize};

  std::vector<std::shared_ptr<ChannelTransformer>> channel_transformers_;
};

CYBER_REGISTER_COMPONENT(OscarLidarFusion)
}  // namespace oscar
}  // namespace apollo
