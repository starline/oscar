///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/monitor/checkers/common.h"

#include <stdexcept>

namespace apollo {
namespace monitor {

int return_value(const std::string& cmd) {
  FILE* in = popen(cmd.c_str(), "r");
  if (in == nullptr)
    throw std::runtime_error("Cannot open process for command " + cmd);

  int exit_code = pclose(in);

  return exit_code;
}

}  // namespace monitor
}  // namespace apollo
