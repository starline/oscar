///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "cyber/cyber.h"
#include "modules/oscar/monitor/checkers/common.h"
#include "modules/oscar/monitor/proto/dump_message_with_header.pb.h"
#include "modules/oscar/monitor/proto/monitor_config.pb.h"
#include "modules/oscar/monitor/proto/monitor_message.pb.h"

namespace apollo {
namespace monitor {

using apollo::cyber::message::RawMessage;
using apollo::oscar::monitor::DumpMessageWithHeader;
using apollo::oscar::monitor::MonitorMessage;

using apollo::oscar::monitor::ChannelMonitorConfig;

class ReaderWrapper {
 public:
  ONLY_MOVABLE(ReaderWrapper)

  ReaderWrapper(const std::string& channel,
                std::weak_ptr<apollo::cyber::Node> node,
                bool skip_latency = false);

  [[nodiscard]] apollo::oscar::monitor::ChannelStatus get_info();

 private:
  bool skip_latency_ = false;
  std::string channel_;
  uint32_t msg_counter_ = 0;
  double last_ts_ = 0;
  double lidar_latency_ = 0;
  double camera_latency_ = 0;

  FPSTracker fps_tracker_;
  std::shared_ptr<apollo::cyber::Reader<RawMessage>> reader_;
  DumpMessageWithHeader header_struct_msg_;
};

class ChannelChecker {
 public:
  ONLY_MOVABLE(ChannelChecker)

  using StatusT = std::vector<apollo::oscar::monitor::ChannelStatus>;

  ChannelChecker(std::weak_ptr<apollo::cyber::Node> node,
                 const ChannelMonitorConfig& config);

  StatusT getStatus() noexcept;

  void handleResult(StatusT result, MonitorMessage& out_message) const noexcept;

  bool filterChannel(const std::string& channel) const;

 private:
  std::weak_ptr<apollo::cyber::Node> node_;
  std::unordered_map<std::string, ReaderWrapper> readers_;
  const ChannelMonitorConfig config_;
  bool skip_latency_ = false;
};

}  // namespace monitor
}  // namespace apollo
