///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <vector>

namespace apollo {
namespace monitor {

std::vector<std::string> GetStatsLines(const std::string& stat_file,
                                       const int line_count);

double GetMemoryUsage(const int pid, const std::string& process_name);

double GetCPUUsage(const int pid, const std::string& process_name);

uint64_t GetSystemMemoryValueFromLine(std::string stat_line);

double GetSystemMemoryUsage();

double GetSystemCPUUsage();

double GetSystemDiskload(const std::string& device_name);

std::string getDeviceName(const std::string& path);

double getDiskSpace(const std::string& path);

std::vector<std::string> sensorsInfo();

struct GpuData {
  double memory_usage_mb;
  double memory_total_mb;
  std::string name;
};

std::vector<GpuData> getGPUInfo();

}  // namespace monitor
}  // namespace apollo
