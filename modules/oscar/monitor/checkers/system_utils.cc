///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/monitor/checkers/system_utils.h"

#include <blkid/blkid.h> /* Linux, requires libblkid-dev on Debian/Ubuntu */

#if USE_GPU == 1
#include <cuda_runtime.h>
#endif

#include <stdlib.h>
#include <sys/stat.h>

#include <boost/filesystem.hpp>
#include <memory>
#include <unordered_map>

#include "absl/strings/match.h"
#include "absl/strings/str_cat.h"
#include "absl/strings/str_split.h"
#include "cyber/cyber.h"

constexpr auto kNanoToSecondsFactor = 1e9;

namespace apollo {
namespace monitor {

// Docs used for obtaining info from /proc directory
// https://man7.org/linux/man-pages/man5/proc.5.html
// https://www.linuxhowtos.org/System/procstat.htm

// Almost copy from modules/monitor
bool GetPIDByCmdLine(const std::string& process_dag_path, int* pid) {
  const std::string system_proc_path = "/proc";
  const std::string proc_cmdline_path = "/cmdline";
  const auto dirs = apollo::cyber::common::ListSubPaths(system_proc_path);
  std::string cmd_line;
  for (const auto& dir_name : dirs) {
    if (!std::all_of(dir_name.begin(), dir_name.end(), isdigit)) {
      continue;
    }
    *pid = std::stoi(dir_name);
    std::ifstream cmdline_file(
        absl::StrCat(system_proc_path, "/", dir_name, proc_cmdline_path));
    std::getline(cmdline_file, cmd_line);
    if (absl::StrContains(cmd_line, process_dag_path)) {
      return true;
    }
  }
  return false;
}

std::vector<std::string> GetStatsLines(const std::string& stat_file,
                                       const int line_count) {
  std::vector<std::string> stats_lines;
  std::ifstream buffer(stat_file);
  for (int line_num = 0; line_num < line_count; ++line_num) {
    std::string line;
    std::getline(buffer, line);
    if (line.empty()) {
      break;
    }
    stats_lines.push_back(line);
  }
  return stats_lines;
}

double GetMemoryUsage(const int pid, const std::string& process_name) {
  const std::string memory_stat_file = absl::StrCat("/proc/", pid, "/statm");
  const uint32_t page_size_kb = (sysconf(_SC_PAGE_SIZE) >> 10);
  const int resident_idx = 1, gb_2_kb = (1 << 20);
  constexpr static int kMemoryInfo = 0;

  const auto stat_lines = GetStatsLines(memory_stat_file, kMemoryInfo + 1);
  if (stat_lines.size() <= kMemoryInfo) {
    AERROR << "failed to load contents from " << memory_stat_file;
    return 0.f;
  }
  const std::vector<std::string> stats =
      absl::StrSplit(stat_lines[kMemoryInfo], ' ', absl::SkipWhitespace());
  if (stats.size() <= resident_idx) {
    AERROR << "failed to get memory info for process " << process_name;
    return 0.f;
  }
  return static_cast<double>(std::stoll(stats[resident_idx]) * page_size_kb) /
         gb_2_kb;
}

double GetCPUUsage(const int pid, const std::string& process_name) {
  static std::unordered_map<std::string, uint64_t> prev_jiffies_map;
  static std::unordered_map<std::string, uint64_t> prev_timestamps_nanos_map;

  if (prev_jiffies_map.find(process_name) == prev_jiffies_map.end()) {
    prev_jiffies_map.emplace(process_name, 0);
    prev_timestamps_nanos_map.emplace(
        process_name, apollo::cyber::Time::Now().ToNanosecond());
  }

  const std::string cpu_stat_file = absl::StrCat("/proc/", pid, "/stat");
  const int hertz = sysconf(_SC_CLK_TCK);
  const int utime = 13, stime = 14, cutime = 15, cstime = 16;
  constexpr static int kCpuInfo = 0;

  const auto stat_lines = GetStatsLines(cpu_stat_file, kCpuInfo + 1);
  if (stat_lines.size() <= kCpuInfo) {
    AERROR << "failed to load contents from " << cpu_stat_file;
    return 0.f;
  }
  const std::vector<std::string> stats =
      absl::StrSplit(stat_lines[kCpuInfo], ' ', absl::SkipWhitespace());
  if (stats.size() <= cstime) {
    AERROR << "failed to get CPU info for process " << process_name;
    return 0.f;
  }
  const uint64_t jiffies = std::stoll(stats[utime]) + std::stoll(stats[stime]) +
                           std::stoll(stats[cutime]) +
                           std::stoll(stats[cstime]);

  const uint64_t prev_jiffies = prev_jiffies_map[process_name];
  prev_jiffies_map[process_name] = jiffies;

  const uint64_t prev_ts = prev_timestamps_nanos_map[process_name];
  prev_timestamps_nanos_map[process_name] =
      apollo::cyber::Time::Now().ToNanosecond();

  const double interval =
      static_cast<double>(prev_timestamps_nanos_map[process_name] - prev_ts) /
      kNanoToSecondsFactor;

  if (prev_jiffies == 0) {
    return 0.f;
  }

  return 100.f *
         (static_cast<double>(jiffies - prev_jiffies) / hertz / interval);
}

uint64_t GetSystemMemoryValueFromLine(std::string stat_line) {
  constexpr static int kMemoryValueIdx = 1;
  const std::vector<std::string> stats =
      absl::StrSplit(stat_line, ' ', absl::SkipWhitespace());
  if (stats.size() <= kMemoryValueIdx) {
    AERROR << "failed to parse memory from line " << stat_line;
    return 0;
  }
  return std::stoll(stats[kMemoryValueIdx]);
}

double GetSystemMemoryUsage() {
  const std::string system_mem_stat_file = "/proc/meminfo";
  const int mem_total = 0, mem_free = 1, buffers = 3, cached = 4,
            swap_total = 14, swap_free = 15, slab = 21;
  const auto stat_lines = GetStatsLines(system_mem_stat_file, slab + 1);
  if (stat_lines.size() <= slab) {
    AERROR << "failed to load contents from " << system_mem_stat_file;
    return 0.f;
  }
  const auto total_memory =
      GetSystemMemoryValueFromLine(stat_lines[mem_total]) +
      GetSystemMemoryValueFromLine(stat_lines[swap_total]);
  int64_t used_memory = total_memory;
  for (int cur_line = mem_free; cur_line <= slab; ++cur_line) {
    if (cur_line == mem_free || cur_line == buffers || cur_line == cached ||
        cur_line == swap_free || cur_line == slab) {
      used_memory -= GetSystemMemoryValueFromLine(stat_lines[cur_line]);
    }
  }
  return 100.f * (static_cast<double>(used_memory) / total_memory);
}

double GetSystemCPUUsage() {
  const std::string system_cpu_stat_file = "/proc/stat";
  const int users = 1, system = 3, total = 7;
  constexpr static int kSystemCpuInfo = 0;
  static uint64_t prev_jiffies = 0, prev_work_jiffies = 0;
  const auto stat_lines =
      GetStatsLines(system_cpu_stat_file, kSystemCpuInfo + 1);
  if (stat_lines.size() <= kSystemCpuInfo) {
    AERROR << "failed to load contents from " << system_cpu_stat_file;
    return 0.f;
  }
  const std::vector<std::string> jiffies_stats =
      absl::StrSplit(stat_lines[kSystemCpuInfo], ' ', absl::SkipWhitespace());
  if (jiffies_stats.size() <= total) {
    AERROR << "failed to get system CPU info from " << system_cpu_stat_file;
    return 0.f;
  }
  uint64_t jiffies = 0, work_jiffies = 0;
  for (int cur_stat = users; cur_stat <= total; ++cur_stat) {
    const auto cur_stat_value = std::stoll(jiffies_stats[cur_stat]);
    jiffies += cur_stat_value;
    if (cur_stat <= system) {
      work_jiffies += cur_stat_value;
    }
  }
  const uint64_t tmp_prev_jiffies = prev_jiffies;
  const uint64_t tmp_prev_work_jiffies = prev_work_jiffies;
  prev_jiffies = jiffies;
  prev_work_jiffies = work_jiffies;
  if (tmp_prev_jiffies == 0) {
    return 0.f;
  }
  return 100.f * (static_cast<double>(work_jiffies - tmp_prev_work_jiffies) /
                  (jiffies - tmp_prev_jiffies));
}

double GetSystemDiskload(const std::string& device_name) {
  static std::unordered_map<std::string, uint64_t> prev_timestamps_nanos_map;
  static std::unordered_map<std::string, uint64_t> prev_disk_stats_map;

  if (prev_timestamps_nanos_map.find(device_name) ==
      prev_timestamps_nanos_map.end()) {
    prev_timestamps_nanos_map.emplace(
        device_name, apollo::cyber::Time::Now().ToNanosecond());
    prev_disk_stats_map.emplace(device_name, 0);
  }

  const std::string disks_stat_file = "/proc/diskstats";
  const int device = 2, in_out_ms = 12;
  const int seconds_to_ms = 1000;
  constexpr static int kDiskInfo = 128;

  const auto stat_lines = GetStatsLines(disks_stat_file, kDiskInfo);
  uint64_t disk_stats = 0;

  for (const auto& line : stat_lines) {
    const std::vector<std::string> stats =
        absl::StrSplit(line, ' ', absl::SkipWhitespace());
    // if (stats[device] == device_name) {
    if (device_name.find(stats[device]) != std::string::npos) {
      disk_stats = std::stoll(stats[in_out_ms]);
      break;
    }
  }
  const uint64_t tmp_prev_disk_stats = prev_disk_stats_map[device_name];
  prev_disk_stats_map[device_name] = disk_stats;

  const uint64_t prev_ts = prev_timestamps_nanos_map[device_name];
  prev_timestamps_nanos_map[device_name] =
      apollo::cyber::Time::Now().ToNanosecond();

  const double interval =
      static_cast<double>(prev_timestamps_nanos_map[device_name] - prev_ts) /
      kNanoToSecondsFactor;

  if (tmp_prev_disk_stats == 0) {
    return 0.f;
  }
  return 100.f * (static_cast<double>(disk_stats - tmp_prev_disk_stats) /
                  (interval * seconds_to_ms));
}

// mostly taken from
// https://www.linuxquestions.org/questions/programming-9/api-to-get-partition-name-based-on-file-path-in-c-on-llinux-os-892461/#post4566454
// and prettified.
std::string getDeviceName(const std::string& path) {
  struct stat s;

  char* pName;

  ACHECK(lstat(path.c_str(), &s) == 0);

  pName = blkid_devno_to_devname(s.st_dev);
  ACHECK(pName != nullptr);

  std::string name{pName};
  free(pName);

  return name;
}

constexpr uint64_t kByteToMB = 1'000'000;
constexpr uint64_t kByteToGB = 1'000'000'000;

double getDiskSpace(const std::string& path) {
  const auto space = boost::filesystem::space(path);
  return static_cast<double>(space.available) / kByteToGB;
}

// taken from https://stackoverflow.com/a/478960
std::string exec(const char* cmd) {
  std::array<char, 128> buffer;
  std::string result;
  std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
  if (!pipe) {
    throw std::runtime_error("popen() failed!");
  }
  while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
    result += buffer.data();
  }
  return result;
}

std::vector<std::string> sensorsInfo() {
  return absl::StrSplit(exec("sensors"), "\n");
}

#if USE_GPU == 1

std::vector<GpuData> getGPUInfo() {
  static const std::vector<cudaDeviceProp> devices = []() {
    int device_count = 0;
    if (cudaGetDeviceCount(&device_count) != cudaSuccess) {
      AERROR << "Fail to get GPU devices";
    }
    std::vector<cudaDeviceProp> devices;
    devices.reserve(device_count);

    for (int i = 0; i < device_count; i++) {
      devices.emplace_back();
      cudaGetDeviceProperties(&devices.back(), i);
    }

    return devices;
  }();

  std::vector<GpuData> result;

  for (size_t i = 0; i < devices.size(); i++) {
    if (cudaSetDevice(i) != cudaSuccess) {
      AERROR << "Failed to set GPU device: " << i;
      continue;
    }

    size_t free, total;
    if (cudaMemGetInfo(&free, &total) != cudaSuccess) {
      AERROR << "Failed to get info for GPU device: " << i;
      continue;
    }
    result.emplace_back();
    auto& el = result.back();
    el.memory_usage_mb = static_cast<double>(total - free) / kByteToMB;
    el.memory_total_mb = static_cast<double>(total) / kByteToMB;
    el.name = devices[i].name;
  }

  return result;
}

#else
std::vector<GpuData> getGPUInfo() {
  return {};
}
#endif

}  // namespace monitor
}  // namespace apollo
