///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "modules/oscar/monitor/checkers/channel_checker.h"
#include "modules/oscar/monitor/checkers/nodes_checker.h"
#include "modules/oscar/monitor/checkers/periphery_checker.h"
#include "modules/oscar/monitor/checkers/system_checker.h"

/*
In this module Variant mechanism is used instead of inheritance with dynamic
polymorhism.

Each concrete Checker class should be added in std::variant types list and
satisfy the following prototype:

class NewChecker {
 public:
  ONLY_MOVABLE(NewChecker)

  NewChecker(...); // any arguments

  using StatusT = ...; // any output type

  [[nodiscard]] StatusT getStatus() noexcept; // function that performs
checking. Will be executed asyncronously

  void handleResult(const StatusT& result, MonitorMessage& out_message) const;
// function that writes result to ouptu message
}

*/

namespace apollo {
namespace monitor {

using variants_t = std::variant<PeripheryDeviceChecker, NodesChecker,
                                ChannelChecker, SystemChecker>;

template <typename... Ts>
struct CheckerVariantTraits {};

template <typename... Ts>
struct CheckerVariantTraits<std::variant<Ts...>> {
  using checker_variant = std::variant<Ts...>;
  using result_variant =
      std::variant<decltype(std::declval<Ts>().getStatus())...>;
  using future_result_variant =
      std::variant<std::future<decltype(std::declval<Ts>().getStatus())>...>;
};

using checker_variant = CheckerVariantTraits<variants_t>::checker_variant;
using future_result_variant =
    CheckerVariantTraits<variants_t>::future_result_variant;

template <typename T>
using variant_future_result_t =
    std::future<typename std::remove_reference_t<T>::StatusT>;

}  // namespace monitor
}  // namespace apollo
