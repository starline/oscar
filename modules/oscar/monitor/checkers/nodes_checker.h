///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>
#include <string>
#include <vector>

#include "modules/oscar/monitor/proto/monitor_message.pb.h"

#include "cyber/cyber.h"
#include "modules/oscar/monitor/checkers/common.h"

using apollo::oscar::monitor::MonitorMessage;

namespace apollo {
namespace monitor {

class NodesChecker {
 public:
  explicit NodesChecker(std::weak_ptr<apollo::cyber::Node> node)
      : node_(node) {}
  ONLY_MOVABLE(NodesChecker)

  using StatusT = std::vector<apollo::oscar::monitor::NodeStatus>;

  StatusT getStatus();

  void handleResult(const StatusT& result, MonitorMessage& out_message);

 private:
  const std::weak_ptr<apollo::cyber::Node> node_;
  const std::string name_ = "node_checker";

  StatusT nodes_;
};

}  // namespace monitor
}  // namespace apollo
