///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>
#include <variant>
#include <vector>

#include "modules/oscar/monitor/proto/monitor_config.pb.h"
#include "modules/oscar/monitor/proto/monitor_message.pb.h"

#include "cyber/component/timer_component.h"
#include "cyber/cyber.h"

#include "modules/oscar/monitor/checkers/checker_variant.h"

namespace apollo {
namespace monitor {


class OscarMonitor : public apollo::cyber::TimerComponent {
 public:
  bool Init() override;
  bool Proc() override;

 private:
  std::vector<checker_variant> checkers_;
  std::vector<future_result_variant> results_;
  apollo::oscar::monitor::MonitorConfig config_;
  std::shared_ptr<cyber::Writer<MonitorMessage>> writer_ = nullptr;
};

CYBER_REGISTER_COMPONENT(OscarMonitor)

}  // namespace monitor
}  // namespace apollo
