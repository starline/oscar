/******************************************************************************
 * Copyright 2018 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include <atomic>
#include <condition_variable>
#include <cstdint>
#include <ctime>
#include <deque>
#include <iostream>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <unordered_map>
#include <utility>
#include <vector>

#include "cyber/base/concurrent_object_pool.h"
#include "cyber/common/macros.h"
#include "cyber/node/node.h"
#include "glog/logging.h"
#include "modules/oscar/foxglove/schemas/Log.pb.h"

namespace apollo {
namespace cyber {
namespace logger {

using NodePtr = apollo::cyber::Node*;
// using NodePtr = std::unique_ptr<apollo::cyber::Node>;
// mostly copied from AsyncLogger in cyber modules
class AsyncPublishLogger : public google::base::Logger {
 public:
  explicit AsyncPublishLogger(NodePtr node, google::base::Logger* wrapped);

  ~AsyncPublishLogger();

  /**
   * @brief start the async logger
   */
  void Start();

  /**
   * @brief Stop the thread. Flush() and Write() must not be called after this.
   * NOTE: this is currently only used in tests: in real life, we enable async
   * logging once when the program starts and then never disable it.
   * REQUIRES: Start() must have been called.
   */
  void Stop();

  /**
   * @brief Write a message to the log. Start() must have been called.
   *
   * @param force_flush is set by the GLog library based on the configured
   * '--logbuflevel' flag.
   * Any messages logged at the configured level or higher result in
   * 'force_flush' being set to true, indicating that the message should be
   * immediately written to the log rather than buffered in memory.
   * @param timestamp is the time of write a message
   * @param message is the info to be written
   * @param message_len is the length of message
   */
  void Write(bool force_flush, time_t timestamp, const char* message,
             int message_len) override;

  /**
   * @brief Flush any buffered messages.
   */
  void Flush() override;

  /**
   * @brief Get the current LOG file size.
   * The return value is an approximate value since some
   * logged data may not have been flushed to disk yet.
   *
   * @return the log file size
   */
  uint32_t LogSize() override;

  /**
   * @brief get the log thead
   *
   * @return the pointer of log thread
   */
  std::thread* LogThread() { return &log_thread_; }

 private:
  void RunThread();
  void FlushBuffer(const std::unique_ptr<std::deque<std::string>>& msg);

  NodePtr node_;
  std::shared_ptr<apollo::cyber::Writer<foxglove::Log>> writer_ = nullptr;

  google::base::Logger* const wrapped_;
  std::thread log_thread_;

  // Count of how many times the writer thread has flushed the buffers.
  // 64 bits should be enough to never worry about overflow.
  std::atomic<uint64_t> flush_count_ = {0};

  // Count of how many times the writer thread has dropped the log messages.
  // 64 bits should be enough to never worry about overflow.
  uint64_t drop_count_ = 0;

  // The buffer to which application threads append new log messages.
  std::unique_ptr<std::deque<std::string>> active_buf_;

  // The buffer currently being flushed by the logger thread, cleared
  // after a successful flush.
  std::unique_ptr<std::deque<std::string>> flushing_buf_;

  // Trigger for the logger thread to stop.
  enum State { INITTED, RUNNING, STOPPED };
  std::atomic<State> state_ = {INITTED};
  std::atomic_flag flag_ = ATOMIC_FLAG_INIT;

  std::string log_channel_ = "/cyberout";  // TODO: replace with GFLAG?

  std::shared_ptr<base::CCObjectPool<foxglove::Log>> out_message_pool_ =
      nullptr;

  DISALLOW_COPY_AND_ASSIGN(AsyncPublishLogger);
};

}  // namespace logger
}  // namespace cyber
}  // namespace apollo
