///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <algorithm>
#include <vector>
#include <memory>

#include "glog/logging.h"

namespace apollo {
namespace cyber {
namespace logger {

class LoggerAccumulator : public google::base::Logger {
 public:
  LoggerAccumulator() {}
  
  void AddLogger(std::unique_ptr<google::base::Logger>&& logger) { 
    loggers_.emplace_back(std::move(logger)); 
    }

  void Write(bool force_flush, time_t timestamp, const char* message,
             int message_len) override;
  void Flush() override;
  uint32_t LogSize() override;

  void shutdown() { is_alive_ = false; }

 private:

  std::vector<std::unique_ptr<google::base::Logger>> loggers_;
  bool is_alive_ = true;
};
}  // namespace logger
}  // namespace cyber
}  // namespace apollo
