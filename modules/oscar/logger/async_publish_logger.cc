/******************************************************************************
 * Copyright 2018 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/oscar/logger/async_publish_logger.h"

#include <cstdlib>
#include <regex>
#include <string>
#include <thread>
#include <unordered_map>

#include "cyber/base/macros.h"

namespace apollo {
namespace cyber {
namespace logger {

namespace {
const std::unordered_map<char, foxglove::Log_Level> log_levels_mapping{
    {'I', foxglove::Log_Level::Log_Level_INFO},
    {'D', foxglove::Log_Level::Log_Level_DEBUG},
    {'W', foxglove::Log_Level::Log_Level_WARNING},
    {'E', foxglove::Log_Level::Log_Level_ERROR},
    {'F', foxglove::Log_Level::Log_Level_FATAL}};

const std::regex pieces_regex(
    "([IWDEF]*)([0-9]{2}[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2})\\.([0-9]{6}) +"
    "([0-9]+) ([a-zA-Z0-9\\_\\.]+):([0-9]+)] ((.|\n)+)");

void parseLogMessage(const std::string& msg,
                     std::shared_ptr<foxglove::Log> parsed_msg) {
  /*
  RegEx for parsing messages like these:
  E0919 13:11:19.116185 1275925 foxglove_bridge.cc:56] [mainboard]invalid state
  E0919 13:11:19.116293 1275925 foxglove_bridge.cc:56] [mainboard]Unsubscribing
  from connection graph updates.
  */

  std::smatch pieces_match;

  if (!std::regex_match(msg, pieces_match, pieces_regex) ||
      pieces_match.size() != 9) {
    std::cerr << "Failed to parse string for logging: (match size: "
              << pieces_match.size() << " ) " << msg << "\n";
    return;
  }

  std::tm tm{};
  std::stringstream ss(pieces_match[2].str());
  ss >> std::get_time(&tm, "%m%d %H:%M:%S");

  auto t = std::time(nullptr);

  // set current year because there is no such information in log message
  tm.tm_year = std::localtime(&t)->tm_year;
  auto tp = std::chrono::system_clock::from_time_t(std::mktime(&tm));
  auto seconds =
      std::chrono::duration_cast<std::chrono::seconds>(tp.time_since_epoch())
          .count();

  parsed_msg->set_level(log_levels_mapping.at(pieces_match[1].str()[0]));
  parsed_msg->set_name(pieces_match[4].str() + " " + pieces_match[5].str() +
                       " " + pieces_match[6].str());
  parsed_msg->set_file(pieces_match[5].str());
  parsed_msg->set_line(std::stoi(pieces_match[6].str()));
  parsed_msg->set_message(pieces_match[7].str());

  auto ts = parsed_msg->mutable_timestamp();
  ts->set_seconds(seconds);
  ts->set_nanos(std::stoi(pieces_match[3].str()) * 1000);
}
}  // namespace

AsyncPublishLogger::AsyncPublishLogger(NodePtr node,
                                       google::base::Logger* wrapped)
    // : node_(std::move(node)), wrapped_(wrapped) {
    : node_(node), wrapped_(wrapped) {
  auto logtostderr = std::getenv("GLOG_logtostderr");
  if (logtostderr != nullptr && strcmp(logtostderr, "1") == 0)
    throw std::runtime_error(
        "Logging is disabled because GLOG_logtostderr=1. Consider "
        "using GLOG_alsologtostderr=1 instead\n");

  constexpr auto message_pool_size = 1000;
  constexpr auto qos_depth = 990;

  out_message_pool_.reset(
      new base::CCObjectPool<foxglove::Log>(message_pool_size));
  out_message_pool_->ConstructAll();

  apollo::cyber::proto::RoleAttributes role_attr;
  role_attr.set_channel_name(log_channel_);
  auto qos_profile = role_attr.mutable_qos_profile();
  qos_profile->set_history(
      apollo::cyber::proto::QosHistoryPolicy::HISTORY_KEEP_LAST);
  qos_profile->set_depth(qos_depth);
  qos_profile->set_mps(1000);
  qos_profile->set_reliability(
      apollo::cyber::proto::QosReliabilityPolicy::RELIABILITY_RELIABLE);
  qos_profile->set_durability(
      apollo::cyber::proto::QosDurabilityPolicy::DURABILITY_TRANSIENT_LOCAL);

  writer_ = node_->CreateWriter<foxglove::Log>(role_attr);
  active_buf_.reset(new std::deque<std::string>());
  flushing_buf_.reset(new std::deque<std::string>());
}

AsyncPublishLogger::~AsyncPublishLogger() { Stop(); }

void AsyncPublishLogger::Start() {
  CHECK_EQ(state_.load(std::memory_order_acquire), INITTED);
  state_.store(RUNNING, std::memory_order_release);
  log_thread_ = std::thread(&AsyncPublishLogger::RunThread, this);
}

void AsyncPublishLogger::Stop() {
  state_.store(STOPPED, std::memory_order_release);
  if (log_thread_.joinable()) {
    log_thread_.join();
  }

  FlushBuffer(active_buf_);
  ACHECK(active_buf_->empty());
  ACHECK(flushing_buf_->empty());
}

void AsyncPublishLogger::Write(bool force_flush, time_t timestamp,
                               const char* message, int message_len) {
  if (cyber_unlikely(state_.load(std::memory_order_acquire) != RUNNING)) {
    std::cerr << "Publish Logger is not running!" << std::endl;
    return;
  }
  if (message_len > 0) {
    while (flag_.test_and_set(std::memory_order_acquire)) {
      cpu_relax();
    }
    active_buf_->emplace_back(message, message_len);
    flag_.clear(std::memory_order_release);
  }

  if (force_flush && timestamp == 0 && message && message_len == 0) {
    Stop();
  }
}

void AsyncPublishLogger::Flush() {}

uint32_t AsyncPublishLogger::LogSize() { return wrapped_->LogSize(); }

void AsyncPublishLogger::RunThread() {
  tracy::SetThreadName("Async Publish logger");
  while (state_ == RUNNING) {
    while (flag_.test_and_set(std::memory_order_acquire)) {
      cpu_relax();
    }
    active_buf_.swap(flushing_buf_);
    flag_.clear(std::memory_order_release);
    FlushBuffer(flushing_buf_);
    if (active_buf_->size() < 800) {
      std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
  }
}

void AsyncPublishLogger::FlushBuffer(
    const std::unique_ptr<std::deque<std::string>>& buffer) {
  while (!buffer->empty()) {
    auto& msg = buffer->front();
    // auto out_msg = out_message_pool_->GetObject();
    auto out_msg = std::make_shared<foxglove::Log>();
    ACHECK(out_msg != nullptr);

    parseLogMessage(msg, out_msg);

    writer_->Write(out_msg);

    buffer->pop_front();
  }
  Flush();
}

}  // namespace logger
}  // namespace cyber
}  // namespace apollo
