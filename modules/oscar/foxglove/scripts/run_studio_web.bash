#!/usr/bin/env bash

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

CONTAINER_NAME=lichtblick

if [ ! -f /.dockerenv ]; then

    running_containers="$(docker container list --format '{{.Names}}' | grep $CONTAINER_NAME)"
    [ "$running_containers" == $CONTAINER_NAME ] && echo "FoxGlove Studio (Lichtblick) web already has running container. You can access it via http://localhost:8080" && exit

    echo "FoxGlove Studio (Lichtblick) is running at http://localhost:8080 in container named $CONTAINER_NAME"

    if [ "$1" == "--debug" ]; then
        REDIRECT_OUT="/dev/stdout"
        DETACH=""
    else
        REDIRECT_OUT="/dev/null"
        DETACH="-d"
    fi

    docker run \
        --name $CONTAINER_NAME \
        --rm \
        -p "8080:8080" $DETACH oscsl/lichtblick >"$REDIRECT_OUT"

else
    echo "This command should be used outside Apollo docker container"
fi
