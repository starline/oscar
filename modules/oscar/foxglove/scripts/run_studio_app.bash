#!/usr/bin/env bash

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

set -e

OSCAR_ROOT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../../../../" &> /dev/null && pwd )

# Install FoxGlove if it does not exist
if [ ! "$(command -v lichtblick)" ]; then
    echo "FoxGlove studio (Lichtblick) was not found locally. Downloading..."
    if [ ! -f $OSCAR_ROOT_DIR/modules/oscar/tools/data/lichtblick.deb ]; then
        mkdir -p $OSCAR_ROOT_DIR/modules/oscar/tools/data
        wget -O $OSCAR_ROOT_DIR/modules/oscar/tools/data/lichtblick.deb \
        https://github.com/Lichtblick-Suite/lichtblick/releases/download/v1.6.0/lichtblick-1.6.0-linux-amd64.deb
    fi
    sudo apt install $OSCAR_ROOT_DIR/modules/oscar/tools/data/lichtblick.deb -y
fi

# install extensions
INSTALL_EXTENSIONS_SCRIPT=$OSCAR_ROOT_DIR/modules/calibration/data/mb_actros/foxglove_conf/download_extensions.bash
MESSAGE_CONVERTERS_ZIP=/tmp/message_converters.foxe

if [ -f "$INSTALL_EXTENSIONS_SCRIPT" ]; then
    $INSTALL_EXTENSIONS_SCRIPT
else
    echo "Downloading extensions..."
    wget https://gitlab.com/starline/foxglove/message_converters/-/releases/permalink/latest/downloads/starline.message_converters.foxe \
    -O $MESSAGE_CONVERTERS_ZIP -q
fi

mkdir -p ~/.lichtblick/extensions
unzip -qo $MESSAGE_CONVERTERS_ZIP -d ~/.lichtblick/extensions/message_converters

echo "Extensions have been installed."

[ "$(pgrep -c -f lichtblick)" -gt 0 ] && echo "FoxGlove Studio (Lichtblick) is already running" && exit 1

[ -f /.dockerenv ] && ENVIRONMENT=Docker || ENVIRONMENT=Host

if [ "$1" == "--debug" ]; then
    lichtblick
else
    nohup lichtblick >/dev/null 2>&1 &
    echo "$ENVIRONMENT: FoxGlove studio (Lichtblick) is running in background"
fi
