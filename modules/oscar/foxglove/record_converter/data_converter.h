///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////
#pragma once

#include <string>

#include "mcap/writer.hpp"

#include "modules/oscar/foxglove/record_converter/mcap_writer_manager.h"
#include "modules/oscar/foxglove/record_converter/record_reader_manager.h"
#include "modules/oscar/foxglove/record_converter/utils/tqdm.h"

namespace apollo {

class Converter {
 public:
  Converter(const std::string& src_path, const std::string& dest_path,
            const std::string& prefix, const std::string& compression_method,
            const bool compress_images, const uint32_t chunk_size,
            uint32_t max_file_size_mb);
  void run();

 private:
  RecordReaderManager reader_manager_;
  McapWriterManager writer_manager_;

  std::string dest_path_;
  tqdm pbar_;

  bool compress_images_;
};

}  // namespace apollo
