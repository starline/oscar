///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////
#include "modules/oscar/foxglove/record_converter/mcap_writer_manager.h"

#include <sys/stat.h>

#include <cstdio>
#include <experimental/filesystem>
#include <future>
#include <queue>
#include <unordered_set>

#include "cyber/common/file.h"
#include "modules/oscar/foxglove/utils/types_conversion.h"

namespace fs = std::experimental::filesystem;

namespace apollo {

McapWriterManager::McapWriterManager(const std::string& dest_path,
                                     const std::string& file_prefix,
                                     const std::string& compression,
                                     const uint32_t chunksize,
                                     uint32_t max_file_size_MB)
    : writer_opts_("protobuf"),
      file_prefix_(file_prefix),
      max_file_size_mb_(max_file_size_MB) {
  const std::string cur_dir = apollo::cyber::common::GetCurrentPath();

  auto abs_path = apollo::cyber::common::GetAbsolutePath(cur_dir, dest_path);

  save_in_one_file_ = !fs::is_directory(abs_path);

  dest_path_ = abs_path;

  AINFO << "Saving result to " << dest_path_;
  if (!save_in_one_file_ && mkdir(dest_path_.c_str(), S_IRWXU) != 0) {
    ACHECK(errno == EEXIST);
  }

  if (compression.compare("lz4") == 0) {
    writer_opts_.compression = mcap::Compression::Lz4;
  } else {
    writer_opts_.compression = mcap::Compression::Zstd;
  }

  writer_opts_.chunkSize = chunksize;  // default: 786432UL

  writer_opts_.noChunkCRC = false;
  writer_opts_.noAttachmentCRC = false;
  writer_opts_.enableDataCRC = false;
  writer_opts_.noSummaryCRC = false;
  writer_opts_.noChunking = false;
  writer_opts_.noMessageIndex = false;
  writer_opts_.noSummary = false;
  writer_opts_.compressionLevel = mcap::CompressionLevel::Default;
  writer_opts_.forceCompression = false;
}

McapWriterManager::~McapWriterManager() {
  writer_.close();

  // remove counter postfix in filename if there is only one output file
  if (!save_in_one_file_ && mcap_file_counter_ == 1) {
    std::string old_name = dest_path_ + "/" + file_prefix_ + "_" +
                           std::to_string(mcap_file_counter_ - 1) + ".mcap";
    std::string new_name = dest_path_ + "/" + file_prefix_ + ".mcap";

    if (std::rename(old_name.data(), new_name.data()) != 0) {
      std::cout << "rename error\n";
    }
  }
}

void McapWriterManager::update_writer() {
  uint32_t f_size;
  if (!curr_out_filename_.empty()) {
    f_size = std::experimental::filesystem::file_size(curr_out_filename_) >>
             20;  // convert to MBs
    if (save_in_one_file_ || f_size <= max_file_size_mb_) {
      return;
    }
  }

  writer_.close();

  curr_out_filename_ = save_in_one_file_
                           ? dest_path_
                           : dest_path_ + "/" + file_prefix_ + "_" +
                                 std::to_string(mcap_file_counter_) + ".mcap";
  mcap_file_counter_++;
  auto status = writer_.open(curr_out_filename_, writer_opts_);
  if (status.code != mcap::StatusCode::Success) {
    AERROR << status.message;
  }
  add_schemas_to_writer();
}

void McapWriterManager::write_message(const RecordMessage& in_msg) {
  if (channel2type_map_.find(in_msg.channel_name) == channel2type_map_.end()) {
    return;
  }
  auto type_name = channel2type_map_[in_msg.channel_name];

  mcap::Message msg;
  msg.channelId = channel2Id_[in_msg.channel_name];
  msg.logTime = in_msg.time;
  msg.publishTime = in_msg.time;
  msg.data = reinterpret_cast<const std::byte*>(in_msg.content.data());
  msg.dataSize = in_msg.content.size();

  auto status = writer_.write(msg);
  if (status.code != mcap::StatusCode::Success) {
    AERROR << status.message;
    throw std::runtime_error(status.message);
  }

  update_writer();

  message_counter_ += 1;
}

void McapWriterManager::set_channel2type_map(
    const std::map<std::string, std::string>& channel2type_map) {
  channel2type_map_ = channel2type_map;
}

void McapWriterManager::add_schemas_to_writer() {
  std::vector<std::string> channels_to_remove;
  for (auto const& el : channel2type_map_) {
    auto channel_name = el.first;
    auto type_name = el.second;

    type_name = convertTypeName(type_name);
    auto desc = getApolloDescriptor(type_name);

    if (desc == nullptr) {
      AWARN << "Skipping channel " << channel_name << " of type " << type_name
            << ". Descriptor not found";
      channels_to_remove.push_back(channel_name);
      continue;
    }

    mcap::Schema schema(type_name, "protobuf",
                        BuildFileDescriptorSet(desc).SerializeAsString());
    writer_.addSchema(schema);

    mcap::Channel channel(channel_name, "protobuf", schema.id);
    writer_.addChannel(channel);

    channel2Id_[channel_name] = channel.id;
  }

  for (auto& ch : channels_to_remove) channel2type_map_.erase(ch);
}

}  // namespace apollo
