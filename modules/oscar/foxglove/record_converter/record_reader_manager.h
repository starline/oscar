///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////
#pragma once

#include <map>
#include <memory>
#include <queue>
#include <string>
#include <vector>

#include <google/protobuf/descriptor.pb.h>

#include "cyber/record/record_message.h"
#include "cyber/record/record_reader.h"

using apollo::cyber::record::RecordReader;

namespace apollo {

class RecordReaderManager final {
 public:
  explicit RecordReaderManager(const std::string& src_path);

  bool get_message(apollo::cyber::record::RecordMessage* out_msg);

  std::map<std::string, std::string> get_channel2type_map() {
    return channel2type_map_;
  }
  std::string get_type_name(const std::string& channel_name) {
    return channel2type_map_[channel_name];
  }
  uint32_t get_total_messages() { return total_messages_; }
  uint32_t get_total_convert_messages() { return total_convert_messages_; }
  uint32_t get_readed_messages() { return current_reader_message_counter_; }

  bool is_finished() { return finish_.load(); }

 private:
  bool update_reader();

  std::queue<std::string> in_files_;

  std::unique_ptr<RecordReader> reader_ = nullptr;
  uint32_t total_messages_ = 0;
  uint32_t total_convert_messages_ = 0;
  uint32_t total_drop_messages_ = 0;

  uint32_t current_reader_message_counter_ = 0;
  uint32_t total_reader_message_counter_ = 0;
  uint32_t current_reader_message_total_ = 0;

  std::string restricted_list_types_file_ =
      "/apollo/modules/oscar/foxglove/record_converter/"
      "restricted_types.txt";
  std::atomic_bool finish_ = false;

  std::vector<std::string> record_types_;

  std::map<std::string, std::string> channel2type_map_;
  std::vector<std::string> restricted_types_;
};
}  // namespace apollo
