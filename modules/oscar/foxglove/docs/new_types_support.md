# Добавление новых типов сообщений

Для конвертации сообщений из формата Apollo в Foxglove необходимы protobuf дескрипторы. Поиск дескрипторов осуществляется в два этапа:
1. Автоматический поиск в apollo protobuf factory;
2. Если дескриптор не найден, то происходит поиск декспритора среди вручную добавленных в словаре descriptors_map.

Если ни в одном из этапов дескриптор не получен, то канал пропускается. При этом выводится сообщение при конвертации записей:

```
Skipping channel {channgel_name} of type {type_name}. Descriptor not found

```

В таком случае требуется добавить необходимый тип вручную.
## Добавление отсутствующих типов
### 1. header файл

В файл [utils.cc](../utils/utils.cc) добавить путь до сгенерированного header файла сообщения. Обычно путь совпадает с protobuf описанием сообщения, за исключением расширения: ```.proto``` -> ```.pb.h```.

Например, protobuf файл **modules/oscar/monitor/proto/monitor_message.proto** следует включать следующим образом:
```cpp
#include "modules/oscar/monitor/proto/monitor_message.pb.h"
```
### 2. пара значений [название типа, дексриптора]

В файл [utils.cc](../utils/utils.cc) в статичный словарь descriptors_map добавить пару [**название типа**, **функция, возвращающая указатель на дескриптор**].

Например, для типа ```apollo.oscar.monitor.MonitorMessage``` путь до дескриптора будет ```&apollo::oscar::monitor::MonitorMessage::descriptor```, обернутый в ```DescriptorFn```:
```cpp
  {"apollo.oscar.monitor.MonitorMessage", DescriptorFn(&apollo::oscar::monitor::MonitorMessage::descriptor)},

```

### 3. Зависимость при сборке

В [BUILD](../utils/BUILD) файле для таргета utils добавить путь до таргета с C++ сгенерированным сообщением.

```python
cc_library(
    name = "utils",
    srcs = [
        "utils.cc",
    ],
    hdrs = [
        "utils.h",
    ],
    deps = [
        ...
        "//modules/oscar/monitor/proto:monitor_message_cc_proto"
    ]
)
```

### 4. Сборка

Сборка модуля foxglove:

```
oscarcd
sh apollo.sh build_opt_gpu oscar/foxglove
```


