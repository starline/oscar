///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once
#include <unistd.h>

#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <vector>

#include "foxglove/websocket/websocket_notls.hpp"
#include "foxglove/websocket/websocket_server.hpp"

#include "modules/oscar/foxglove/bridge/proto/foxglove_bridge_config.pb.h"

#include "cyber/component/component.h"
#include "cyber/cyber.h"
#include "cyber/node/node.h"
#include "modules/oscar/foxglove/bridge/parameter_manager/parameter_manager.h"
#include "modules/oscar/foxglove/bridge/topology_observer/topology_observer.h"

namespace apollo {

namespace oscar {

using apollo::cyber::Component;
using apollo::oscar::ParameterManager;

class FoxGloveBridge final {
 public:
  FoxGloveBridge(apollo::cyber::Node& node,
                 std::shared_ptr<apollo::oscar::FoxGloveBridgeConfig> config);
  ~FoxGloveBridge();

 private:
  bool loadConfig();
  void setHandlers();

  foxglove::Server<foxglove::WebSocketNoTls> server_;

  TopologyObserver topology_observer_;
  ParameterManager parameter_manager_;
};

}  // namespace oscar
}  // namespace apollo
