///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/foxglove/bridge/topology_observer/channel_handler.h"

#include <foxglove/websocket/websocket_notls.hpp>
#include <foxglove/websocket/websocket_server.hpp>
#include <memory>

#include "cyber/cyber.h"
#include "modules/oscar/foxglove/schemas/CompressedImage.pb.h"
#include "modules/oscar/foxglove/schemas/FrameTransforms.pb.h"
#include "modules/oscar/foxglove/schemas/Log.pb.h"
#include "modules/oscar/foxglove/schemas/PointCloud.pb.h"
#include "modules/oscar/foxglove/schemas/RawImage.pb.h"
#include "modules/oscar/foxglove/utils/types_conversion.h"
#include "modules/oscar/foxglove/utils/utils.h"

namespace {

inline double current_ts() { return apollo::cyber::Time::Now().ToSecond(); }

}  // namespace

ChannelHandler::ChannelHandler(
    foxglove::Server<foxglove::WebSocketNoTls>& server,
    apollo::cyber::Node& node, const std::string& channel_name,
    const std::string& channel_type, Conversion conversion_type)
    : server_(server),
      node_(node),
      channel_name_(channel_name),
      message_transformer_(conversion_type) {
  auto descriptor = message_transformer_.get_out_descriptor(channel_type);

  if (descriptor == nullptr) {
    AERROR << "Descriptor for channel " << channel_name << "(" << channel_type
           << ") not found. Skipping...";
    return;
  }

  channel_id_ = server_.addChannels({{
      .topic = channel_name_,
      .encoding = "protobuf",
      .schemaName = descriptor->full_name(),
      .schema = Base64Encode(SerializeFdSet(descriptor)),
  }})[0];

  AINFO << "Adding reader: " << channel_name_
        << " with channel id: " << channel_id_;

  apollo::cyber::proto::RoleAttributes role_attrs;
  role_attrs.set_channel_name(channel_name_);
  auto qos = role_attrs.mutable_qos_profile();

  qos->set_durability(
      apollo::cyber::proto::QosDurabilityPolicy::DURABILITY_TRANSIENT_LOCAL);

  qos->set_reliability(
      apollo::cyber::proto::QosReliabilityPolicy::RELIABILITY_RELIABLE);

  reader_ = node_.CreateReader<RawMessage>(role_attrs, [this](auto&&... ts) {
    callback(std::forward<decltype(ts)>(ts)...);
  });
  waitFirstMessage();
  // by default we shutdown reader and Init it if any subscriber appears
  disable();
}

bool has_transient_writers(const std::string& channel_name) {
  static auto nm = apollo::cyber::service_discovery::TopologyManager::Instance()
                       ->channel_manager();

  std::vector<apollo::cyber::RoleAttributes> writers;
  nm->GetWritersOfChannel(channel_name, &writers);

  return std::any_of(writers.begin(), writers.end(), [](auto&& node) {
    return node.qos_profile().durability() ==
           apollo::cyber::transport::QosDurabilityPolicy::
               DURABILITY_TRANSIENT_LOCAL;
  });
}

void ChannelHandler::callback(const std::shared_ptr<RawMessage>& msg) {
  out_msg_.clear();
  message_transformer_.transform(msg->message, out_msg_);
  send_message();
}

void ChannelHandler::waitFirstMessage() {
  if (not has_transient_writers(channel_name_)) {
    AWARN << "no transient writers found for channel " << channel_name_
          << ". Disabling without waiting message.";
    return;
  }

  auto start_time = current_ts();

  using namespace std::chrono_literals;

  while (current_ts() - start_time < max_wait_time_ &&
         !reader_->HasReceived()) {
    reader_->Observe();
    std::this_thread::sleep_for(10ms);
  }

  if (!reader_->HasReceived()) {
    AERROR << channel_name_ << ": Reader not received first message at "
           << max_wait_time_ << " sec";
  } else {
    AINFO << "Waiting for first message for channel " << channel_name_
          << " took " << current_ts() - start_time;
  }
}

void ChannelHandler::send_message() {
  server_.broadcastMessage(channel_id_, nanosecondsSinceEpoch(),
                           reinterpret_cast<const uint8_t*>(out_msg_.data()),
                           out_msg_.size());
}
