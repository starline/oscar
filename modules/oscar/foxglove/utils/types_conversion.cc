///////////////////////////////////////////////////////////////////////////////////
// Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/foxglove/utils/types_conversion.h"

#include <memory>
#include <opencv2/imgcodecs.hpp>
#include <unordered_map>

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"

void convertTimestamp(double src_ts, google::protobuf::Timestamp* out_ts) {
  out_ts->set_seconds(static_cast<int>(src_ts));
  double intpart;
  out_ts->set_nanos(modf(src_ts, &intpart) * 1'000'000'000);
}

void convertCyberMessageToFoxGlove(
    const apollo::transform::TransformStampeds& src_msg, std::string& out_msg) {
  foxglove::FrameTransforms out_fg_ransforms;
  for (auto src_transform : src_msg.transforms()) {
    auto fg_transform = out_fg_ransforms.add_transforms();

    convertTimestamp(src_transform.header().timestamp_sec(),
                     fg_transform->mutable_timestamp());

    fg_transform->set_parent_frame_id(src_transform.header().frame_id());
    fg_transform->set_child_frame_id(src_transform.child_frame_id());

    auto src_translation = src_transform.transform().translation();
    auto out_translation = fg_transform->mutable_translation();
    out_translation->set_x(src_translation.x());
    out_translation->set_y(src_translation.y());
    out_translation->set_z(src_translation.z());

    auto src_quaternion = src_transform.transform().rotation();
    auto out_quaternion = fg_transform->mutable_rotation();
    out_quaternion->set_x(src_quaternion.qx());
    out_quaternion->set_y(src_quaternion.qy());
    out_quaternion->set_z(src_quaternion.qz());
    out_quaternion->set_w(src_quaternion.qw());
  }

  out_msg = out_fg_ransforms.SerializeAsString();
}

void convertCyberMessageToFoxGlove(
    const apollo::cyber::message::RawMessage& src_msg, std::string& out_msg) {
  src_msg.SerializeToString(&out_msg);
}

struct PointXYZI {
  float x;
  float y;
  float z;
  uint32_t i;
};

void convertCyberMessageToFoxGlove(const apollo::drivers::PointCloud& src_pc,
                                   std::string& out_serialized_msg,
                                   bool keep_intensity, bool keep_ts) {
  // create and prepare foxglove message
  foxglove::PointCloud out_pc;

  uint32_t point_stride =
      sizeof(float) * (3 + (int)keep_intensity) + sizeof(double) * (int)keep_ts;
  out_pc.set_point_stride(point_stride);
  const char* const fieldNames[] = {"x", "y", "z"};

  int fieldOffset = 0;
  for (const auto& name : fieldNames) {
    auto field = out_pc.add_fields();
    field->set_name(name);
    field->set_offset(fieldOffset);
    field->set_type(foxglove::PackedElementField_NumericType_FLOAT32);
    fieldOffset += sizeof(float);
  }

  if (keep_intensity) {
    auto field = out_pc.add_fields();
    field->set_name("intensity");
    field->set_offset(fieldOffset);
    field->set_type(foxglove::PackedElementField_NumericType_UINT32);
    fieldOffset += sizeof(float);
  }
  if (keep_ts) {
    auto field = out_pc.add_fields();
    field->set_name("timestamp");
    field->set_offset(fieldOffset);
    field->set_type(
        foxglove::
            PackedElementField_NumericType_FLOAT64);  // It should be uint64 but
                                                      // there is not such type
                                                      // in FoxGlove schema
    fieldOffset += sizeof(double);
  }

  auto* pose = out_pc.mutable_pose();
  auto* position = pose->mutable_position();
  position->set_x(0);
  position->set_y(0);
  position->set_z(0);
  auto* orientation = pose->mutable_orientation();
  orientation->set_x(0);
  orientation->set_y(0);
  orientation->set_z(0);
  orientation->set_w(1);

  // fill fields
  auto num_points = src_pc.point().size();

  out_pc.mutable_data()->append(num_points * point_stride, '\0');
  out_pc.set_frame_id(src_pc.header().frame_id());

  convertTimestamp(src_pc.header().timestamp_sec(), out_pc.mutable_timestamp());

  size_t offset = 0;
  char* data = const_cast<char*>(out_pc.mutable_data()->data());

  PointXYZI point;
  size_t pointXYZI_size = sizeof(PointXYZI);

  for (auto p : src_pc.point()) {
    point.x = p.x();
    point.y = p.y();
    point.z = p.z();
    point.i = p.intensity();
    std::memcpy(&data[offset], reinterpret_cast<const char*>(&point),
                pointXYZI_size);

    offset += pointXYZI_size;

    if (keep_ts) {
      auto ts = static_cast<double>(p.timestamp());
      std::memcpy(&data[offset], reinterpret_cast<const char*>(&ts),
                  sizeof(ts));
      offset += sizeof(ts);
    }
  }

  out_pc.SerializeToString(&out_serialized_msg);
}

void convertCyberMessageToFoxGlove(
    const apollo::drivers::CompressedImage& src_image, std::string& out_msg) {
  foxglove::CompressedImage fg_image;

  convertTimestamp(src_image.header().timestamp_sec(),
                   fg_image.mutable_timestamp());

  auto frame_id = src_image.frame_id();

  if (frame_id.empty()) {
    fg_image.set_frame_id("");
  } else {
    fg_image.set_frame_id(src_image.frame_id());
  }

  fg_image.set_data(src_image.data());
  fg_image.set_format(src_image.format());

  out_msg = fg_image.SerializeAsString();
}

void convertCyberImageToFoxGloveImage(const apollo::drivers::Image& src_image,
                                      std::string& out_msg) {
  foxglove::RawImage fg_image;

  convertTimestamp(src_image.header().timestamp_sec(),
                   fg_image.mutable_timestamp());

  auto frame_id = src_image.frame_id();
  if (frame_id.empty()) {
    fg_image.set_frame_id("");
  } else {
    fg_image.set_frame_id(src_image.frame_id());
  }

  fg_image.set_width(src_image.width());
  fg_image.set_height(src_image.height());
  fg_image.set_encoding(src_image.encoding());
  fg_image.set_step(src_image.step());
  fg_image.set_data(src_image.data());
  out_msg = fg_image.SerializeAsString();
}

void convertCyberMessageToFoxGlove(const std::string& src_msg,
                                   const std::string& type_name,
                                   std::string& out_msg,
                                   const bool compress_if_possible) {
  if (type_name.find("apollo.drivers.CompressedImage") != std::string::npos) {
    convertCyberMessageToFoxGlove(
        parse_pb_message<apollo::drivers::CompressedImage>(src_msg), out_msg);
  } else if (type_name.find("apollo.drivers.Image") != std::string::npos) {
    convertCyberMessageToFoxGlove(
        parse_pb_message<apollo::drivers::Image>(src_msg), out_msg,
        compress_if_possible);
  } else if (type_name.find("apollo.drivers.PointCloud") != std::string::npos) {
    convertCyberMessageToFoxGlove(
        parse_pb_message<apollo::drivers::PointCloud>(src_msg), out_msg);
    // } else if (type_name.find("apollo.transform.TransformStampeds") !=
    //            std::string::npos) {
    //   convertCyberMessageToFoxGlove(
    //       parse_pb_message<apollo::transform::TransformStampeds>(src_msg),
    //       out_msg);
  } else {
    out_msg = src_msg;
  }
}

void convertCyberMessageToFoxGlove(const RecordMessage& src_msg,
                                   const std::string& type_name,
                                   RecordMessage& out_msg,
                                   const bool compress_if_possible) {
  convertCyberMessageToFoxGlove(src_msg.content, type_name, out_msg.content,
                                compress_if_possible);
}

void convertCyberImageToFoxGloveCompressedImage(
    const apollo::drivers::Image& src_image, std::string& out_msg) {
  std::vector<int> params;
  params.resize(3, 0);
  params[0] = cv::IMWRITE_JPEG_QUALITY;
  params[1] = 95;

  cv::Mat mat_image(src_image.height(), src_image.width(), CV_8UC3,
                    const_cast<char*>(src_image.data().data()),
                    src_image.step());
  cv::Mat tmp_mat;
  cv::cvtColor(mat_image, tmp_mat, cv::COLOR_RGB2BGR);
  std::vector<uint8_t> compress_buffer;
  if (!cv::imencode(".jpg", tmp_mat, compress_buffer, params)) {
    std::cerr << "cv::imencode (jpeg) failed on input image";
    return;
  }
  // compressed_image->set_data(compress_buffer.data(), compress_buffer.size());
  foxglove::CompressedImage fg_image;
  convertTimestamp(src_image.header().timestamp_sec(),
                   fg_image.mutable_timestamp());
  fg_image.set_data(compress_buffer.data(), compress_buffer.size());
  fg_image.set_format("jpeg");
  out_msg = fg_image.SerializeAsString();
}

void convertCyberMessageToFoxGlove(const apollo::drivers::Image& src_image,
                                   std::string& out_msg, const bool compress) {
  if (compress) {
    convertCyberImageToFoxGloveCompressedImage(src_image, out_msg);
  } else {
    convertCyberImageToFoxGloveImage(src_image, out_msg);
  }
}

const std::unordered_map<std::string, std::string> conversion_types_map{
    {"apollo.drivers.CompressedImage", "foxglove.CompressedImage"},
    {"apollo.drivers.Image", "foxglove.RawImage"},
    {"apollo.drivers.PointCloud", "foxglove.PointCloud"}};

const std::string convertTypeName(const std::string& type_name,
                                  const bool compress_images) {
  auto it = conversion_types_map.find(type_name);
  if (it == conversion_types_map.end()) return type_name;
  if (it->second.find("foxglove.RawImage") != std::string::npos &&
      compress_images)
    return "foxglove.CompressedImage";

  return it->second;
}

const std::unordered_map<std::string, Conversion> conversion_enums_map{
    {"apollo.drivers.CompressedImage",
     Conversion::COMPRESSEDIMAGE_APOLLO2FOXGLOVE},
    {"apollo.drivers.Image", Conversion::RAWIMAGE_APOLLO2FOXGLOVE},
    {"apollo.drivers.PointCloud", Conversion::POINTCLOUD_APOLLO2FOXGLOVE}};

Conversion get_conversion_type_by_channel(const std::string& type_name) {
  auto it = conversion_enums_map.find(type_name);
  return it != conversion_enums_map.end() ? it->second
                                          : Conversion::NO_CONVERSION;
}

google::protobuf::FileDescriptorSet BuildFileDescriptorSet(
    const google::protobuf::Descriptor* toplevelDescriptor) {
  google::protobuf::FileDescriptorSet fdSet;
  std::queue<const google::protobuf::FileDescriptor*> toAdd;
  toAdd.push(toplevelDescriptor->file());
  std::unordered_set<std::string> seenDependencies;
  while (!toAdd.empty()) {
    const google::protobuf::FileDescriptor* next = toAdd.front();
    toAdd.pop();
    next->CopyTo(fdSet.add_file());
    for (int i = 0; i < next->dependency_count(); ++i) {
      const auto& dep = next->dependency(i);
      if (seenDependencies.find(dep->name()) == seenDependencies.end()) {
        seenDependencies.insert(dep->name());
        toAdd.push(dep);
      }
    }
  }
  return fdSet;
}

void MessageTransformer::transform(const std::string& src_msg,
                                   std::string& out_msg) const noexcept {
  std::visit(
      [&, this](auto&& arg) {
        std::remove_reference_t<decltype(arg)>::transform(src_msg, out_msg);
      },
      transformer_);
}

[[nodiscard]] const google::protobuf::Descriptor*
MessageTransformer::get_out_descriptor(
    const std::string& read_msg_type) const noexcept {
  return std::visit(
      [&, this](auto&& arg) {
        return std::remove_reference_t<decltype(arg)>::get_out_descriptor(
            read_msg_type);
      },
      transformer_);
}

static const std::unordered_map<Conversion, transformer_variant>
    transformers_v_map = {
        {Conversion::NO_CONVERSION, MessageTransformerPass()},
        {Conversion::POINTCLOUD_APOLLO2FOXGLOVE,
         MessageTransformerPointCloud2FoxGlove()},
        {Conversion::RAWIMAGE_APOLLO2FOXGLOVE,
         MessageTransformerRawImage2Foxglove()},
        {Conversion::COMPRESSEDIMAGE_APOLLO2FOXGLOVE,
         MessageTransformerCompressedImage2Foxglove()},
};

MessageTransformer::MessageTransformer(Conversion conversion_type)
    : transformer_(transformers_v_map.at(conversion_type)) {}