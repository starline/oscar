###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import sys

sys.path.append("/apollo/bazel-bin")

from google.protobuf.timestamp_pb2 import Timestamp


from modules.common_msgs.localization_msgs import localization_pb2, imu_pb2 as l_imu_pb2, gps_pb2
from modules.common_msgs.sensor_msgs import gnss_best_pose_pb2
from modules.drivers.gnss.proto import gnss_status_pb2
from modules.common_msgs.chassis_msgs import chassis_pb2
from modules.common_msgs.control_msgs import control_cmd_pb2
from modules.common_msgs.sensor_msgs import pointcloud_pb2
from modules.common_msgs.sensor_msgs import imu_pb2
from modules.common_msgs.planning_msgs import planning_pb2
from modules.common_msgs.transform_msgs import transform_pb2
from modules.common_msgs.dreamview_msgs import hmi_status_pb2
from modules.common_msgs.sensor_msgs import sensor_image_pb2
from modules.common_msgs.perception_msgs import perception_obstacle_pb2
from modules.common_msgs.perception_msgs import perception_lane_pb2
from modules.common_msgs.prediction_msgs import prediction_obstacle_pb2

from foxglove.PointCloud_pb2 import PointCloud as FGPointCloud
from foxglove.Vector3_pb2 import Vector3
from foxglove.Quaternion_pb2 import Quaternion

from foxglove.RawImage_pb2 import RawImage as FGRawImage
from foxglove.CompressedImage_pb2 import CompressedImage as FGCompressedImage
from foxglove.FrameTransform_pb2 import FrameTransform as FGFrameTransform
from foxglove.ImageAnnotations_pb2 import ImageAnnotations


from cpp_bindings.build import cpp_bindings

import google.protobuf.message
from google.protobuf.descriptor_pb2 import FileDescriptorSet
from google.protobuf.descriptor import FileDescriptor

from base64 import b64encode
from typing import Set, Type

cyber_foxglove_types_mapping = {
    'apollo.drivers.PointCloud': FGPointCloud,
    'apollo.drivers.Image': FGRawImage,
    'apollo.drivers.CompressedImage': FGCompressedImage,
    'apollo.transform.TransformStampeds': FGFrameTransform,
}


channel_types_classes = {
    'apollo.canbus.Chassis': chassis_pb2.Chassis,
    'apollo.control.ControlCommand': control_cmd_pb2.ControlCommand,
    'apollo.drivers.gnss.GnssBestPose': gnss_best_pose_pb2.GnssBestPose,
    'apollo.drivers.gnss.GnssStatus': gnss_status_pb2.GnssStatus,
    'apollo.drivers.gnss.InsStatus': gnss_status_pb2.InsStatus,
    'apollo.drivers.gnss.Imu': imu_pb2.Imu,
    'apollo.drivers.Image': sensor_image_pb2.Image,
    'apollo.drivers.CompressedImage': sensor_image_pb2.CompressedImage,
    'apollo.drivers.PointCloud': pointcloud_pb2.PointCloud,
    'apollo.dreamview.HMIStatus': hmi_status_pb2.HMIStatus,
    'apollo.localization.LocalizationEstimate': localization_pb2.LocalizationEstimate,
    'apollo.localization.LocalizationStatus': localization_pb2.LocalizationStatus,
    'apollo.localization.CorrectedImu': l_imu_pb2.CorrectedImu,
    'apollo.localization.Gps': gps_pb2.Gps,
    'apollo.planning.ADCTrajectory': planning_pb2.ADCTrajectory,
    'apollo.perception.PerceptionObstacles': perception_obstacle_pb2.PerceptionObstacles,
    'apollo.prediction.PredictionObstacles': prediction_obstacle_pb2.PredictionObstacles,
    'apollo.transform.TransformStampeds': transform_pb2.TransformStampeds,
    'foxglove.ImageAnnotations': ImageAnnotations

}


def get_pb_timestamp_from_secs(t: float) -> Timestamp:
    nanos = int((t - int(t)) * 10**9)
    # timestamp = Timestamp()
    # timestamp.GetCurrentTime()
    # return timestamp
    return Timestamp(seconds=int(t), nanos=nanos)


def convertPointCloud(raw_msg: bytes, serialize=True):
    '''
    (Cyber) PointCloud2 -> (FoxGlove) PointCloud
    Converting from raw Cyber PointCloud2 protobuf message to FoxGlove protobuf message

    To correctly visualize 3D data FoxGlove expects to see it's own schema. So we convert Cyber PointCloud message to FoxGlove format.
    The biggest problem is that each point in Cyber PointCloud is represented as a struct. Just calling ParseFromString() method takes around 0.1 secs.
    And iterating in python is also slow. To avoid this, converting is written in C++ with python bindings. In realtime streaming Pointcloud converting 
    should be used with multiprocessing (see middleware.py for our solution) to avoid freezing in Foxglove studio.
    '''
    ret_msg = cpp_bindings.convertCyberPBToFoxGlove(
        raw_msg, keep_intensity=True, keep_ts=False)
    if serialize:
        return ret_msg
    else:
        pc = FGPointCloud()
        pc.ParseFromString(ret_msg)
        return pc


def convertTransforms(raw_msg: bytes, serialize=True) -> list:
    '''
    (Cyber) TransformStampeds -> (FoxGlove) FrameTransform
    Converting from raw Cyber transform message to rawFoxGlove transform message.

    FoxGlove requires one transform per message, but Cyber module publish single message containing all static transforms and one for all dynamic transforms.
    Each transform is packed to separate FoxGlove message and returned,
    '''
    src_tr = channel_types_classes['apollo.transform.TransformStampeds']()
    src_tr.ParseFromString(raw_msg)

    res_trs = []
    for cur_tr in src_tr.transforms:
        src_tr = cur_tr

        tr = src_tr.transform.translation
        rot = src_tr.transform.rotation

        pc = FGFrameTransform(timestamp=get_pb_timestamp_from_secs(src_tr.header.timestamp_sec),
                              parent_frame_id=src_tr.header.frame_id,
                              child_frame_id=src_tr.child_frame_id,
                              translation=Vector3(
            x=tr.x, y=tr.y, z=tr.z),
            rotation=Quaternion(
            x=rot.qx, y=rot.qy, z=rot.qz, w=rot.qw)
        )
        res_trs.append(pc.SerializeToString() if serialize else pc)

    return res_trs


def convertRawImage(image_msg: bytes, serialize=True):
    '''
    (Cyber) RawImage -> (FoxGlove) RawImage
    Converting between raw image formats. The only different parts are timestamp convertion and encoding mapping. 
    '''

    cyber_img = sensor_image_pb2.Image()
    cyber_img.ParseFromString(image_msg)

    fg_msg = FGRawImage(timestamp=get_pb_timestamp_from_secs(cyber_img.header.timestamp_sec),
                        frame_id=cyber_img.frame_id,
                        width=cyber_img.width,
                        height=cyber_img.height,
                        encoding=cyber_img.encoding,
                        data=cyber_img.data,
                        step=cyber_img.step
                        )
    return fg_msg.SerializeToString() if serialize else fg_msg


def convertCompressedImage(image_msg: bytes, serialize=True):
    '''
    (Cyber) CompressedImage -> (FoxGlove) CompressedImage

    Converting between compressed image formats. The only different part is timestamp convertion and frame_id. 
    '''
    cyber_img = sensor_image_pb2.CompressedImage()
    cyber_img.ParseFromString(image_msg)

    fg_msg = FGCompressedImage(timestamp=get_pb_timestamp_from_secs(cyber_img.header.timestamp_sec),
                               frame_id=cyber_img.frame_id if cyber_img.frame_id != '' else cyber_img.header.frame_id,
                               data=cyber_img.data,
                               format=cyber_img.format)
    return fg_msg.SerializeToString() if serialize else fg_msg


cyber_foxglove_convert_functions_dict = {
    'apollo.drivers.PointCloud': convertPointCloud,
    'apollo.drivers.Image': convertRawImage,
    'apollo.drivers.CompressedImage': convertCompressedImage,
    'apollo.transform.TransformStampeds': convertTransforms
}


def convertMessage(msg: bytes, message_type: str) -> google.protobuf.message.Message:
    '''
    Parsing Cyber raw message and converting to FoxGlove format (for PointClouds/Transforms/Images)
    '''
    if message_type in cyber_foxglove_convert_functions_dict:
        parsedMsg = cyber_foxglove_convert_functions_dict[message_type](
            msg, serialize=False)
    else:
        parsedMsg = channel_types_classes[message_type]()
        parsedMsg.ParseFromString(msg)
    return parsedMsg


# This was taken from examples in Foxglove websocket package
def build_file_descriptor_set(
    message_class: Type[google.protobuf.message.Message],
) -> FileDescriptorSet:
    """
    Build a FileDescriptorSet representing the message class and its dependencies.
    """
    file_descriptor_set = FileDescriptorSet()
    seen_dependencies: Set[str] = set()

    def append_file_descriptor(file_descriptor: FileDescriptor):
        for dep in file_descriptor.dependencies:
            if dep.name not in seen_dependencies:
                seen_dependencies.add(dep.name)
                append_file_descriptor(dep)
        file_descriptor.CopyToProto(
            file_descriptor_set.file.add())  # type: ignore

    append_file_descriptor(message_class.DESCRIPTOR.file)
    return file_descriptor_set


def getFGSchema(target_class: Type[google.protobuf.message.Message]):
    return b64encode(build_file_descriptor_set(target_class).SerializeToString()
                     ).decode("ascii")


def get_total_messages(freader):
    return sum(freader.get_messagenumber(channel) for channel in freader.get_channellist())
