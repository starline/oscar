###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import sys
import time
import struct

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from modules.common_msgs.sensor_msgs import pointcloud_pb2
from foxglove.PointCloud_pb2 import PointCloud as FGPointCloud

import cpp_bindings.build.cpp_bindings as cpp_bindings
import pytest

NUM_POINTS = 10


@pytest.fixture
def sample_pc():
    src_pc = pointcloud_pb2.PointCloud()
    src_pc.header.timestamp_sec = time.time()
    src_pc.measurement_time = time.time() - 1

    for i in range(NUM_POINTS):
        new_point = src_pc.point.add()
        new_point.x = i
        new_point.y = i + 1
        new_point.z = i + 2
        new_point.intensity = i + 3
        new_point.timestamp = i + 4

    return src_pc


class TestCppBindings:

    def test_docs(self):
        _ = cpp_bindings.__doc__

    def test_simple_convertion(self, sample_pc):

        dst_pc = cpp_bindings.convertCyberPBToFoxGlove(
            sample_pc.SerializeToString(), keep_intensity=False, keep_ts=False)

        fg_pc = FGPointCloud()
        fg_pc.ParseFromString(dst_pc)

        assert fg_pc.timestamp.seconds == int(sample_pc.header.timestamp_sec)
        assert len(fg_pc.fields) == 3

        offset = sum([field.offset for field in fg_pc.fields])
        assert NUM_POINTS * offset == len(fg_pc.data)

    def test_conversion_with_all_fields(self, sample_pc):
        dst_pc = cpp_bindings.convertCyberPBToFoxGlove(
            sample_pc.SerializeToString(), keep_intensity=True, keep_ts=True)

        fg_pc = FGPointCloud()
        fg_pc.ParseFromString(dst_pc)

        desired_fields = ['x', 'y', 'z', 'intensity', 'timestamp']
        
        for field in desired_fields:
            assert field in [f.name for f in fg_pc.fields], f'{field} not in Description PackElementField'

    def test_data_changes(self, sample_pc):
        dst_pc = cpp_bindings.convertCyberPBToFoxGlove(
            sample_pc.SerializeToString(), keep_intensity=True, keep_ts=True)

        fg_pc = FGPointCloud()
        fg_pc.ParseFromString(dst_pc)
 
        offset = 3 * 4  + 1 * 4 + 1 * 8 # 3 floats + 1 int + 1 double
        chunks = (fg_pc.data[i*offset: (i + 1)*offset] for i in range(NUM_POINTS))    
        
        def check_field(src_field, cur_field):
            assert src_field == cur_field, f"X - src: {src_field:.2f}; converted: {cur_field:.2f}"

        for point_buffer, src_point in zip(chunks, sample_pc.point):

            x, y, z, intensity, timestamp = struct.unpack('fffid', point_buffer)
            check_field(src_point.x, x)
            check_field(src_point.y, y)
            check_field(src_point.z, z)
            check_field(src_point.intensity, intensity)
            check_field(src_point.timestamp, timestamp)
            