///////////////////////////////////////////////////////////////////////////////////
// Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include <math.h>

#include <chrono>
#include <codecvt>
#include <iostream>
#include <locale>
#include <string>

#include <pybind11/pybind11.h>

#include "fg_proto_generated/PointCloud.pb.h"
#include "modules/common_msgs/sensor_msgs/pointcloud.pb.h"

namespace py = pybind11;

std::string convertCyberPBToFoxGlove(const std::string* src_msg,
                                     bool keep_intensity, bool keep_ts) {
  // unpack cyber message
  apollo::drivers::PointCloud src_pc;

  // uint64_t s =
  // std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
  // uint64_t ms =
  // std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count()
  // % 1000; std::cout<<s<<"."<<ms<<"\n";
  src_pc.ParseFromString(*src_msg);

  // uint64_t s2 =
  // std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
  // uint64_t ms2 =
  // std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count()
  // % 1000; std::cout<<s2<<"."<<ms2<<" "<<(ms2 - ms)<<"\n";

  // create and prepare foxglove message
  foxglove::PointCloud out_pc;

  uint32_t point_stride =
      sizeof(float) * (3 + (int)keep_intensity) + sizeof(double) * (int)keep_ts;
  out_pc.set_point_stride(point_stride);
  const char* const fieldNames[] = {"x", "y", "z"};

  int fieldOffset = 0;
  for (const auto& name : fieldNames) {
    auto field = out_pc.add_fields();
    field->set_name(name);
    field->set_offset(fieldOffset);
    field->set_type(foxglove::PackedElementField_NumericType_FLOAT32);
    fieldOffset += sizeof(float);
  }

  if (keep_intensity) {
    auto field = out_pc.add_fields();
    field->set_name("intensity");
    field->set_offset(fieldOffset);
    field->set_type(foxglove::PackedElementField_NumericType_UINT32);
    fieldOffset += sizeof(float);
  }
  if (keep_ts) {
    auto field = out_pc.add_fields();
    field->set_name("timestamp");
    field->set_offset(fieldOffset);
    field->set_type(
        foxglove::
            PackedElementField_NumericType_FLOAT64);  // It should be uint64 but
                                                      // there is not such type
                                                      // in FoxGlove schema
    fieldOffset += sizeof(double);
  }

  auto* pose = out_pc.mutable_pose();
  auto* position = pose->mutable_position();
  position->set_x(0);
  position->set_y(0);
  position->set_z(0);
  auto* orientation = pose->mutable_orientation();
  orientation->set_x(0);
  orientation->set_y(0);
  orientation->set_z(0);
  orientation->set_w(1);

  // fill fields
  auto num_points = src_pc.point().size();

  out_pc.mutable_data()->append(num_points * point_stride, '\0');

  out_pc.set_frame_id(src_pc.header().frame_id());

  auto timestamp = out_pc.mutable_timestamp();

  timestamp->set_seconds(static_cast<int>(src_pc.header().timestamp_sec()));
  double intpart;
  timestamp->set_nanos(modf(src_pc.header().timestamp_sec(), &intpart) *
                       1'000'000'000);

  // auto time = std::chrono::system_clock::now().time_since_epoch();
  // timestamp->set_seconds(
  //     std::chrono::duration_cast<std::chrono::seconds>(time).count());
  // timestamp->set_nanos(
  //     std::chrono::duration_cast<std::chrono::nanoseconds>(time).count() %
  //     1'000'000'000);

  size_t offset = 0;
  char* data = const_cast<char*>(out_pc.mutable_data()->data());

  for (auto p : src_pc.point()) {
    auto x = p.x();
    auto y = p.y();
    auto z = p.z();

    std::memcpy(&data[offset], reinterpret_cast<const char*>(&x), sizeof(x));
    offset += sizeof(x);
    std::memcpy(&data[offset], reinterpret_cast<const char*>(&y), sizeof(y));
    offset += sizeof(y);
    std::memcpy(&data[offset], reinterpret_cast<const char*>(&z), sizeof(z));
    offset += sizeof(z);

    if (keep_intensity) {
      auto intensity = p.intensity();
      std::memcpy(&data[offset], reinterpret_cast<const char*>(&intensity),
                  sizeof(intensity));
      offset += sizeof(intensity);
    }
    if (keep_ts) {
      auto ts = static_cast<double>(p.timestamp());
      std::memcpy(&data[offset], reinterpret_cast<const char*>(&ts),
                  sizeof(ts));
      offset += sizeof(ts);
    }
  }

  return out_pc.SerializeAsString();
}

PYBIND11_MODULE(cpp_bindings, m) {
  m.doc() = "Point cloud conversion from Cyber to FoxGlove schema";
  m.def(
      "convertCyberPBToFoxGlove",
      [](const std::string* src, bool keep_intensity, bool keep_ts) {
        // m.def("convertCyberPBToFoxGlove", [](const std::string *src,
        // py::bytes *out_array) { convertCyberPBToFoxGlove(src, out_array);
        return py::bytes(
            convertCyberPBToFoxGlove(src, keep_intensity, keep_ts));
        // return convertCyberPBToFoxGlove(src);
      },
      py::arg("src"), py::arg("keep_intensity") = true,
      py::arg("keep_ts") = true, py::return_value_policy::move);
}