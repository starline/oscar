#!/usr/bin/env bash

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd $SCRIPT_DIR

! [ -f /.dockerenv ] && echo "Starting server must be used inside docker container" && exit 1

if [ "$1" = "--debug" ]; then
    ./foxglove_server/multi_threaded_server.py ${@:2}
else
    echo "starting FoxGlove server in background"
    nohup ./foxglove_server/multi_threaded_server.py ${@:2} >/dev/null 2>&1 &
fi