#!/usr/bin/env bash

###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

set -e

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

pip3 install pipenv

if ! [ -d .venv ]; then
    export PIPENV_VENV_IN_PROJECT=1
    export LC_ALL=C.UTF-8
    export LANG=C.UTF-8
    cd $SCRIPT_DIR
    python -m pipenv sync
fi

# MCAP CLI to convert .record files to .mcap
sudo wget https://github.com/foxglove/mcap/releases/latest/download/mcap-linux-amd64 -O /usr/local/bin/mcap && \
sudo chmod +x /usr/local/bin/mcap