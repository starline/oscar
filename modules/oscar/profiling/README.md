# Profiling

Директория содержит код для подключения профилирования:
 - вывод стека вызовов при крашах программы
 - интеграцию профилировщика [Tracy](https://github.com/wolfpld/tracy) для мониторинга различных участков кода, ресурсов, выделения/освобождения памяти.

Файл [```profiling.h```](./profiling.h) включается в файл [`cyber/init.h`](../../../cyber/init.h) и добавляет функции и макросы Tracy. 

## Генерация стека вызовов

При вылетах программ (segfault, double free, необработанное исключение, и т.д.) создается файл в директории `/apollo/data/crash` с выводом стека вызовов и конкретной строкой кода, на которой произошла ошибка.

Файл имеет следующий формат: `stacktrace_{BINARY_NAME}_{YEAR}{MONTH}{DAY}-{HOUR}{MIN}{SEC}.txt`. Например, - `stacktrace_mainboard_20240528-181210.txt`. Также имеется symlink для последнего файла для каждого процесса в формате `last_stacktrace_{BINARY_NAME}.txt`.

### Когда стек не создастся

Для создания стека вызовов необходимы отладочные символы, доступные в режимах сборки с флагом компилятора `-g`.

Также стек не создастся, когда исключения возникают в нескольких потоках одновременно или при обработке текущего исключения - программа на С++ в таких случаях завершается сразу же без возможности перехвата управления.

## Профилировщик Tracy

Для измерения времени работы отдельных блоков кода, функций интегрирован профилировщик Tracy. По умолчанию, профилирование памяти отключено, так как в несколько раз замедляет как работу программ, так и время визуализации памяти.


Для включения необходимо в файле [`bazel.rc`](../../../tools/bazel.rc) выставить флаг компилятора `-DOSCAR_TRACY_MEMORY_ALLOCATIONS=1`.

Подключиться к профилируемой программе можно следующими способами:
 - запустив GUI: `oscar tracy gui` и подлючившись к 127.0.0.1:15001;
 - запустив утилиту для сохранения логов профилирования для последующего анализа в GUI: `oscar tracy capture`

Для профилирования используется сокет 15001.