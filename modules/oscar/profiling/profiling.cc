/***************************************************
 * Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
 *
 * Created by Ivan Nenakhov <nenakhov.id@starline.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************/

#include "modules/oscar/profiling/profiling.h"

#include <cstring>

#include "cyber/common/file.h"

// #define BACKWARD_HAS_DW 1
#define BACKWARD_HAS_BFD 1
// #define BACKWARD_HAS_DWARF 1
#include "modules/oscar/profiling/backward/backward.h"

#ifdef TRACY_ENABLE

namespace {
constexpr auto project_root_dir = "/apollo/bazel-apollo";
constexpr auto replace_prefix = "/proc/self/cwd";

char* sourceContentCallback(void* data, const char* filename, size_t& size) {
  std::string content;

  auto abs_filename =
      apollo::cyber::common::GetAbsolutePath(project_root_dir, filename);

  auto pos = abs_filename.find(replace_prefix);
  if (pos != std::string::npos) {
    abs_filename.replace(pos, std::strlen(replace_prefix), project_root_dir);
  }

  apollo::cyber::common::GetContent(abs_filename, &content);

  size = content.size();
  auto buffer = tracy::tracy_malloc_fast(size);
  std::memcpy(buffer, content.data(), size);

  return static_cast<char*>(buffer);
}
}  // namespace

#if OSCAR_TRACY_MEMORY_ALLOCATIONS

void* operator new(std ::size_t count) {
  auto ptr = malloc(count);
  TracyAlloc(ptr, count);
  return ptr;
}

void operator delete(void* ptr) noexcept {
  TracyFree(ptr);
  free(ptr);
}

#endif
#endif

namespace apollo {
namespace oscar {

void setStacktracePrint() { backward::SignalHandling sh; }

void initProfiling() {
  // static IIFE is used to ensure initialization performs only once
  __attribute__((unused)) static auto is_init = []() {
#ifdef TRACY_ENABLE
    // initialization stuff
    TracySourceCallbackRegister(&sourceContentCallback, nullptr);
#endif
    setStacktracePrint();
    return true;
  }();
}

}  // namespace oscar
}  // namespace apollo
