/***************************************************
 * Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
 *
 * Created by Ivan Nenakhov <nenakhov.id@starline.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************/

#pragma once

#include "tracy/Tracy.hpp"
#ifdef TRACY_ENABLE

#if OSCAR_TRACY_MEMORY_ALLOCATIONS

void* operator new(std ::size_t count);
void operator delete(void* ptr) noexcept;

#endif
#endif

namespace apollo {
namespace oscar {

void initProfiling();

}  // namespace oscar
}  // namespace apollo

#ifdef TRACY_ENABLE

#define OscarTracyPeriodPlot(name)                                       \
  static int64_t prev_time = apollo::cyber::Time::Now().ToMicrosecond(); \
  int64_t now = apollo::cyber::Time::Now().ToMicrosecond();              \
  int64_t latency = (now - prev_time) / 1000;                            \
  prev_time = now;                                                       \
  TracyPlot(name, latency);

#else
#define OscarTracyPeriodPlot(name)
#endif
