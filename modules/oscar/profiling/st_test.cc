// #include "modules/oscar/profiling/stacktrace.h"
#define BACKWARD_HAS_BFD 1
// #define BACKWARD_HAS_DW 1

#include "modules/oscar/profiling/backward/backward.h"

void ooops() { *(char*)(0x0) = 1; }

int main() {
  backward::SignalHandling sh;

  ooops();
  return 0;
}