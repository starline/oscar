#!/usr/bin/env bash

[ -f /.dockerenv ] && echo "Profiler should be run outside docker container. Exiting." && exit

CONTAINER_NAME=tracy-profiler
TRACY_VERSION=0.10

xhost +local:docker >/dev/null

docker container kill $CONTAINER_NAME >/dev/null 2>&1 && sleep 0.1

docker run -ti --rm \
    --ipc=host \
    --privileged \
    --network host \
    --name $CONTAINER_NAME \
    -e DISPLAY="${DISPLAY}" \
    -e XAUTHORITY=$XAUTHORITY \
    -e XDG_RUNTIME_DIR=/tmp \
    -v $XAUTHORITY:$XAUTHORITY \
    -v /tmp/.X11-unix \
    -v $(pwd):/workspace/save_dir \
    -u "$(id -u):$(id -g)" \
    registry.gitlab.com/starline/oscar_utils/tracy/tracy_docker/profiler_v$TRACY_VERSION:latest
