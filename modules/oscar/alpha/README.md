### Alpha

Набор утилит и пакетов для взаимодействия с ПАК Alpha используемого в рамках проекта
для взаимодействия с различными автомобилями.

Состав:

* **alpha_docs** - описание способов подключения к ПАК Alpha в составе атомобиля и соответствующих доступных протоколов для взаимодействия с ней.

  https://gitlab.com/starline/alpha/alpha_docs

* **alpha_py** - пакет на python, предоставляющий доступ к функционалу ПАК Alpha через последовательный порт и соответствующий протокол.

  https://gitlab.com/starline/alpha/alpha_py

* **alpha_cyberRT** - обертка над alpha_py для  интеграции в Apollo. Узел CyberRT, заменяющий функционал модуля canbus.

  Запускается в venv, которое собирается вместе с проектом oscar.

  https://gitlab.com/starline/oscar/-/tree/master/modules/oscar/alpha

* **alpha_utils** - Набор утилит для управления и отладки alpha_py.

  https://gitlab.com/starline/alpha/alpha_utils

* **alpha_emu** - Эмулятор ПАК Alpha (со стороны автомобиля) на основе симулятора Gazebo 11.

  https://gitlab.com/starline/alpha/alpha_gazebo