#!/usr/bin/env python

#############################################################################
# Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Nikolay Dema <ndema2301@gmail.com>                             #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

import os
import sys
import json
import alpha


params = {}

if len(sys.argv) > 1 and os.path.exists(sys.argv[1]):

    with open(sys.argv[1]) as param_file:
        params = alpha.VehicleParams(json.load(param_file))

vehicle = alpha.Vehicle(params, alpha.WSCfg())