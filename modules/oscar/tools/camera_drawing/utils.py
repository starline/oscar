###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import os
import sys
import time
from functools import wraps
from typing import List

import numpy as np
import yaml
import google.protobuf.text_format as text_format
from scipy.spatial.transform import Rotation as R
from copy import deepcopy as dc

import cv2 as cv

from matplotlib import cm

sys.path.append("/apollo/bazel-bin")  # nopep8
from modules.common_msgs.map_msgs import map_pb2

N_COLORS = 50
colormap = cm.get_cmap('jet', N_COLORS)


def timeit(func):
    @wraps(func)
    def timeit_wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        total_time = end_time - start_time
        print(
            f'Function {func.__name__} Took {total_time:.4f} seconds')
        return result
    return timeit_wrapper


def load_if_needed(x, func):
    return x if not isinstance(x, str) else func(x)


def get_pb_from_text_file(filename):
    """Get a proto from given text file."""
    assert os.path.exists(filename), f"Map file does not exist: {filename}"

    with open(filename, 'r') as file_in:
        return text_format.Merge(file_in.read(), map_pb2.Map())


def load_transformation(filename, return_frames=False):
    assert os.path.exists(
        filename), f"File with extrinsics parameters does not exist: {filename}"
    T = np.eye(4)
    with open(filename, 'r') as f:
        f_yaml = yaml.safe_load(f)
        quat = f_yaml['transform']['rotation']
        shift = f_yaml['transform']['translation']
        T[:-1, :-1] = R.from_quat([quat['x'],
                                          quat['y'], quat['z'], quat['w']]).as_matrix()
        T[:-1, -1] = np.array((shift['x'], shift['y'], shift['z']))

        parent_frame = f_yaml['header']['frame_id']
        child_frame = f_yaml["child_frame_id"]

        return (T, parent_frame, child_frame) if return_frames else T


def load_camera_matrix(filename):
    assert os.path.exists(
        filename), f"File with camera maxtrix parameters does not exist: {filename}"
    with open(filename, 'r') as f:
        f_yaml = yaml.safe_load(f)
        M = np.array(f_yaml['K']).reshape((3, 3))
        # M = np.array(f_yaml['P']).reshape((3, 4))[:,:-1]
    return M


def load_dimensions(filename):
    assert os.path.exists(
        filename), f"File with camera maxtrix parameters does not exist: {filename}"
    with open(filename, 'r') as f:
        f_yaml = yaml.safe_load(f)
        return f_yaml['width'], f_yaml['height']


def load_distortion(filename):
    assert os.path.exists(
        filename), f"File with camera parameters does not exist: {filename}"
    with open(filename, 'r') as f:
        f_yaml = yaml.safe_load(f)
        return np.array(f_yaml['D'])


def is_oob(x, y):
    return x > 1e5 or y > 1e5 or y < 0


def filter_points(line_3d):

    ids = np.where(line_3d[:, 2] > 0)[0]

    if len(ids) < 2:
        return []
    return ids


def transform_points(line_3d, transform_matrix):
    ones_col = np.ones(len(line_3d))[..., np.newaxis]
    line_3d = np.append(line_3d, ones_col, axis=1)

    line_3d = (transform_matrix @ line_3d.T).T

    # line_3d = line_3d[filter_points(line_3d), :-1]
    return line_3d[:, :-1]


def transform_points_to_image_plane(line_3d, transform_matrix, cam_matrix, distortion_coeffs):
    line_3d = transform_points(line_3d, transform_matrix)
    line_3d_inliers = filter_points(line_3d)
    line_3d = line_3d[line_3d_inliers]

    if len(line_3d) == 0:
        return []
    line_image, _ = cv.projectPoints(line_3d,
                                     cv.Rodrigues(
                                         np.eye(3))[0],
                                     np.zeros(3),
                                     cam_matrix,
                                     distortion_coeffs)

    return line_image.astype(np.int32)[:, 0, :]


def draw_line(image, line_3d, color, transform_matrix, cam_matrix, distortion_coeffs, line_width=5):

    line_image = transform_points_to_image_plane(
        line_3d, transform_matrix, cam_matrix, distortion_coeffs)

    draw_line_image(image, line_image, color, line_width)


def draw_line_image(image, line_image, color, line_width=5):
    if len(line_image) == 0:
        return
    for (x1, y1), (x2, y2) in zip(line_image[:-1, :], line_image[1:, :]):
        cv.line(image, (int(x1), int(y1)),
                (int(x2), int(y2)), color, line_width)

def rescale(ar):
    min_val = np.min(ar)
    return (ar - min_val) / np.max(ar - min_val)

def draw_points(image, points_3d, transform_matrix, cam_matrix, distortion_coeffs, radius=2, colors='height'):

    points_camera_frame = transform_points(
        points_3d, transform_matrix)
    points_camera_frame_inliers = filter_points(points_camera_frame)
    points_camera_frame = points_camera_frame[points_camera_frame_inliers]


    if len(points_camera_frame) == 0:
        return

    if colors == 'distance':
        distances = np.linalg.norm(points_3d[points_camera_frame_inliers], axis=1)
        colors = colormap(distances / np.quantile(distances, 0.999))
        colors = (colors[:, :-1] * 255).astype(np.uint8)
    elif colors == "height":
        inl_pts_z = points_3d[points_camera_frame_inliers, -1]
        # min_height = np.min(inl_pts_z)
        color_keys = rescale(inl_pts_z)
        colors = colormap(color_keys)
        colors = (colors[:, :-1] * 255).astype(np.uint8)
    else:
        # colors = np.repeat(color, len(points_camera_frame))
        rescaled_colors = rescale(colors)
        # min_height = np.min(color)
        # color_keys = (inl_pts_z - min_height) / np.max(inl_pts_z - min_height)
        colors = colormap(rescaled_colors)
        print(colors)
        colors = (colors[:, :-1] * 255).astype(np.uint8)

    line_image, _ = cv.projectPoints(points_camera_frame,
                                     cv.Rodrigues(
                                         np.eye(3))[0],
                                     np.zeros(3),
                                     cam_matrix,
                                     distortion_coeffs)

    for (x, y), color in zip(line_image.astype(np.int32)[:, 0, :], colors):
        cv.circle(image, (int(x), int(y)), radius, color.tolist(), -1)


def draw_marker(image,
                point_3d,
                text,
                text_color,
                transform,
                cam_matrix,
                distortion_coeffs,
                font=cv.FONT_HERSHEY_SIMPLEX,
                text_scale=1.5):

    max_scale = 1.5
    max_dist = 40
    min_dist = 5

    point_3d = np.append(point_3d, 1)
    point_3d = (transform @ point_3d)

    transform = np.eye(4)

    if point_3d[2] < 1 or point_3d[2] > max_dist:
        return

    point_image, _ = cv.projectPoints(point_3d[:-1, np.newaxis],
                                      cv.Rodrigues(
                                          transform[:-1, :-1])[0],
                                      transform[:-1, -1],
                                      cam_matrix,
                                      distortion_coeffs)

    point_image = point_image[0][0].astype(int)

    b = max_scale / (1 - min_dist / max_dist)
    k = -b / max_dist

    text_scale = k * point_3d[2] + b

    if text_scale < 0:
        return

    textsize = cv.getTextSize(text, font, text_scale, 4)[0]
    textX = point_image[0] - textsize[0] // 2
    textY = point_image[1] - textsize[1] // 2

    cv.putText(image, text, (textX, textY),
               font, text_scale, text_color, 4)


def filter_points_behind(line_3d, transform_matrix, threshold):
    ones_col = np.ones(len(line_3d)).reshape(-1, 1)
    line_3d_exp = np.append(line_3d, ones_col, axis=1)

    line_local = (transform_matrix @ line_3d_exp.T).T

    return line_local[:, 2] > threshold


def get_expanded_borders(line_3d, width):
    vectors = np.zeros_like(line_3d)
    vectors[:-1] = line_3d[1:] - line_3d[:-1]
    vectors[-1] = vectors[-2]

    perpendicular_vectors = np.zeros_like(vectors)
    perpendicular_vectors[:, 0] = 1
    perpendicular_vectors[:, 1] = -1 * vectors[:, 0] / vectors[:, 1]
    perpendicular_vectors[:, 2] = 0

    norms = np.linalg.norm(perpendicular_vectors, axis=1)
    perpendicular_vectors = np.divide(perpendicular_vectors.T, norms).T

    left_borders = line_3d + width / 2 * perpendicular_vectors
    right_borders = np.flip(line_3d - width / 2
                            * perpendicular_vectors, axis=0)

    polygon_points = np.concatenate((left_borders, right_borders))

    return polygon_points


def draw_wide_line(image, line_3d, color, transform_matrix, cam_matrix, distortion_coeffs, line_width=5, alpha=0.3):

    copy_image = image.copy()

    line_3d = line_3d[filter_points_behind(line_3d, transform_matrix, 1)]

    if len(line_3d) < 2:
        return

    line_width = 0.5

    polygon_points = get_expanded_borders(line_3d, line_width)

    line_image, _ = cv.projectPoints(polygon_points,
                                     cv.Rodrigues(
                                         transform_matrix[:-1, :-1])[0],
                                     transform_matrix[:-1, -1],
                                     cam_matrix,
                                     distortion_coeffs)
    line_image = line_image.astype(np.int32)

    cv.fillConvexPoly(copy_image, line_image, color)
    cv.addWeighted(image, 1 - alpha, copy_image, alpha, 0, image)


def append_z_col(in_arr, z_value):
    z_col = np.ones(len(in_arr))[..., np.newaxis] * \
        z_value
    return np.append(in_arr, z_col, axis=1)


def load_transformations(base_frame: str, extrinsic_files: List[str]):
    res_transform = np.eye(4)

    last_parent_frame = base_frame

    print(extrinsic_files)
    for f in extrinsic_files:

        T, parent_frame, child_frame = load_transformation(
            f, return_frames=True)

        if parent_frame == last_parent_frame:
            res_transform = res_transform @ T
            last_parent_frame = child_frame
        elif child_frame == last_parent_frame:
            res_transform = res_transform @ np.linalg.inv(T)
            last_parent_frame = parent_frame
        else:
            raise Exception(
                f"broken transform chain. Got frames: {parent_frame}, {child_frame} expected: {last_parent_frame}")

    return res_transform

# TODO: do smth with offset
def removeXYRotation(transform, offset=0.):
    transform = dc(transform)
    z_rotation = R.from_matrix(transform[:-1, :-1]).as_euler("YXZ")[-1]

    z_rotation_added = z_rotation + np.deg2rad(offset)
    z_rotation = z_rotation_added
    transform[:-1, :-1] = R.from_euler("Z", [z_rotation]).as_matrix()
    return transform

def quat2mat(x): return R.from_quat(x).as_matrix()

def mat2quat(x): 
    if x.shape == (4, 4):
        x = x[:-1, :-1]
    assert x.shape == (3, 3)
    return R.from_matrix(x).as_quat()

def quat2euler(x): return R.from_quat(x).as_euler('xyz')

def v_quat2euler(x): return np.apply_along_axis(quat2euler, 1, x)
