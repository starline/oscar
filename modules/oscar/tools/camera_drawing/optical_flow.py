###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import sys

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")
from copy import deepcopy as dc

import cv2 as cv
import numpy as np

from utils import mat2quat
np.set_printoptions(precision=2, suppress=True)


# https://docs.opencv.org/3.4/d4/dee/tutorial_optical_flow.html

class MeanAccumulator:

    def __init__(self) -> None:
        self.values = {}
        self.counters = {}

    def add(self, **kwargs):
        for key, value in kwargs.items():
            if key not in self.values:
                self.values[key] = value
                self.counters[key] = 1
            else:
                self.values[key] = (self.values[key] * self.counters[key] + value) / (self.counters[key] + 1)
                self.counters[key] += 1

    def get(self, key):
        return self.values[key]


class OpticalFlowDetector:

    def __init__(self, camera_matrix, loc2novatel_tr, novatel2camera_tr) -> None:

        self.loc2novatel_tr = loc2novatel_tr
        self.novatel2camera_tr = novatel2camera_tr

        self.loc2camera_tr = self.loc2novatel_tr @ self.novatel2camera_tr

        self.M = camera_matrix
        # params for ShiTomasi corner detection
        self.feature_params = dict(maxCorners=20,
                                   qualityLevel=0.3,
                                   minDistance=7,
                                   blockSize=7)

        # Parameters for lucas kanade optical flow
        self.lk_params = dict(winSize=(15, 15),
                              maxLevel=2,
                              criteria=(cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 0.03))

        self.p0 = None
        self.old_gray = None

        # self.feature_detector = cv.FastFeatureDetector_create(
        #     threshold=100, nonmaxSuppression=True)

        self.n_features = 0


        self.good_new = None
        self.good_old = None

        self.prev_kpts = None
        self.prev_descriptors = None


        self.feature_detector = cv.ORB_create()
        self.matcher = cv.FlannBasedMatcher_create()

        # self.R = np.eye(3)
        # self.t = np.zeros(3)

        self.T = np.eye(4)

        self.prev_world2loc_tr = None

        self.diff_accumulator = MeanAccumulator()

        self.counter = 0

    def get_absolute_scale(self, current_tr):
        if self.prev_world2loc_tr is not None:
            print(np.linalg.norm(self.prev_world2loc_tr[:-1, -1] - current_tr[:-1, -1]))
            return np.linalg.norm(self.prev_world2loc_tr[:-1, -1] - current_tr[:-1, -1])
        else:
            return 1

    def update(self, image, world2loc_tr):

        self.counter +=1
        if self.counter < 5:
            return [], np.eye(4)

        frame_gray = cv.cvtColor(image, cv.COLOR_RGB2GRAY)

        kpts, descriptors = self.feature_detector.detectAndCompute(
            frame_gray, None)
        descriptors = descriptors.astype(np.float32)
        if self.old_gray is None:
            # if self.old_gray is None or len(self.p0) < 2:
            self.old_gray = frame_gray

            self.prev_kpts, self.prev_descriptors = kpts, descriptors
            self.prev_world2loc_tr = dc(world2loc_tr)

            return [], np.eye(4)

        matches = self.matcher.knnMatch(
            self.prev_descriptors, descriptors, k=2)

        # print(len(matches))
        # print(len(descriptors), len(self.prev_descriptors))

        # ratio test as per Lowe's paper
        good_old_pts = []
        good_old_descriptors = []
        pts1 = []
        best_matches = []
        for m, n in matches:
            # if m.distance < 100:
            if m.distance < 0.8 * n.distance:
                # print(m.imgIdx, m.queryIdx, m.trainIdx)
                # print(n.imgIdx, n.queryIdx, n.trainIdx)
                # print(self.prev_kpts[n.queryIdx].pt)
                # print(np.linalg.norm(
                #     np.array(self.prev_kpts[m.queryIdx].pt) - np.array(kpts[m.trainIdx].pt)))
                good_old_pts.append(self.prev_kpts[m.queryIdx].pt)
                good_old_descriptors.append(self.prev_descriptors[m.queryIdx])

                pts1.append(kpts[m.trainIdx].pt)

                best_matches.append(m)

        # print(len(good_old_pts))

        if len(good_old_pts) < 8:
            return [], np.eye(4)

        self.good_old = np.stack(good_old_pts).reshape((-1, 1, 2))
        self.prev_descriptors = np.stack(good_old_descriptors)
        self.good_new = np.stack(pts1).reshape((-1, 1, 2))

        E, _ = cv.findEssentialMat(
            self.good_new, self.good_old, self.M, cv.RANSAC, 0.999, 1.0)
        
        _, self.R, self.t, _ = cv.recoverPose(
            E, self.good_new, self.good_old, self.M)

        # E, self.good_new, self.good_old, self.M, R=self.R.copy(), t=self.t.copy())

        self.t *= self.get_absolute_scale(world2loc_tr)
        # if (absolute_scale > 0.1 and abs(t[2][0]) > abs(t[0][0]) and abs(t[2][0]) > abs(t[1][0])):
        #     self.t = self.t + absolute_scale * self.R.dot(t)
        #     self.R = R.dot(self.R)

        loc_tr_diff = np.linalg.inv(self.prev_world2loc_tr @ self.loc2camera_tr) @ world2loc_tr @ self.loc2camera_tr
        print(loc_tr_diff[:-1, -1])

        print(mat2quat(loc_tr_diff))

        self.diff_accumulator.add(translation_error=loc_tr_diff[:-1, -1]-self.t.squeeze(),
                                  rotation_error=mat2quat(loc_tr_diff)-mat2quat(self.R))

        # print(self.t)
        # print(self.R)

        self.prev_world2loc_tr = dc(world2loc_tr)

        self.old_gray = frame_gray.copy()
        self.prev_kpts, self.prev_descriptors = kpts, descriptors

        T = np.eye(4)
        T[:-1, :-1] = self.R
        T[:-1, -1] = self.t.squeeze()
        print(T[:-1, -1])
        print(mat2quat(T))
        print()

        print(self.diff_accumulator.get("translation_error"))
        print(self.diff_accumulator.get("rotation_error"))
        print()
        print()
        return self.good_new, T
