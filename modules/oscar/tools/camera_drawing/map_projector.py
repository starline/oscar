###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import numpy as np
from numpy.linalg import inv
from tqdm.auto import tqdm

from utils import draw_line, get_pb_from_text_file, draw_marker, transform_points_to_image_plane, \
    append_z_col, load_camera_matrix, load_distortion, load_transformation, load_if_needed, draw_line_image, removeXYRotation
np.set_printoptions(precision=3, suppress=True)


def get_left_lines(map):
    return [np.stack(
        [(p.x, p.y) for p in lane.left_boundary.curve.segment[0].line_segment.point]) for lane in map.lane]


def get_right_lines(map):
    return [np.stack(
        [(p.x, p.y) for p in lane.right_boundary.curve.segment[0].line_segment.point]) for lane in map.lane]


def get_central_lines(map):
    return [np.stack(
        [(p.x, p.y) for p in lane.central_curve.segment[0].line_segment.point]) for lane in map.lane]


def get_lane_ids(map):
    return np.array(
        [lane.id.id for lane in map.lane])


def dist(p1, p2):
    s = sum([(i1 - i2)**2 for i1, i2 in zip(p1, p2)])
    return np.sqrt(s)


class Lane:
    def __init__(self, pb_lane):

        self.left_lines = np.stack(
            [(p.x, p.y) for p in pb_lane.left_boundary.curve.segment[0].line_segment.point])
        self.right_lines = np.stack(
            [(p.x, p.y) for p in pb_lane.right_boundary.curve.segment[0].line_segment.point])
        self.central_lines = np.stack(
            [(p.x, p.y) for p in pb_lane.central_curve.segment[0].line_segment.point])

        self.lane_id = pb_lane.id.id

    def __str__(self):
        return f"Left lines: \n{self.left_lines}\nRight lines: \n{self.right_lines}\nCentral lines: \n{self.central_lines}\nId: {self.lane_id}"


def get_stop_lines(map_pb):

    return [np.stack([(p.x, p.y) for p in signal.stop_line[0].segment[0].line_segment.point]) for signal in map_pb.signal]


class TrafficLight:

    def __init__(self, signal) -> None:
        self.bbox = np.array([(p.x, p.y, p.z) for p in signal.boundary.point])
        self.id = signal.id.id


class MapProjector:

    def __init__(self,
                 map_file=None,
                 camera_matrix=np.eye(3),
                 localization2novatel_tr=np.eye(4),
                 novatel2camera_tr=np.eye(4),
                 distortion_coeffs=None,
                 localization_frame_road_offset=1,
                 left_line_color=(0, 255, 0),
                 central_line_color=(255, 0, 0),
                 right_line_color=(0, 0, 255),
                 distance_threshold=100,
                 stop_line_color=(255, 128, 0),
                 traffic_light_color=(0, 255, 0),
                 ) -> None:

        self.map = get_pb_from_text_file(map_file)

        self.camera_matrix = load_if_needed(camera_matrix, load_camera_matrix)
        self.distortion_coeffs = load_if_needed(
            distortion_coeffs, load_distortion)
        self.localization2novatel_tr = load_if_needed(
            localization2novatel_tr, load_transformation)
        self.novatel2camera_tr = load_if_needed(
            novatel2camera_tr, load_transformation)
        self.init_novatel2camera_tr = self.novatel2camera_tr

        self.localization_frame_road_offset = localization_frame_road_offset

        self.left_line_color = left_line_color
        self.central_line_color = central_line_color
        self.right_line_color = right_line_color
        self.stop_line_color = stop_line_color
        self.traffic_light_color = traffic_light_color

        self.distance_threshold = distance_threshold

        self.lanes = [Lane(pb_lane) for pb_lane in tqdm(self.map.lane)]

        self.stop_lines = get_stop_lines(self.map)

        self.traffic_lights = [TrafficLight(signal)
                               for signal in self.map.signal]

        print("MapProjector init complete")

    def update_camera_transform(self, new_camera_matrix):
        self.novatel2camera_tr = new_camera_matrix

    def filter_segments(self, current_position, dist_thresh=3000):

        near_lanes = list(filter(
            lambda lane: dist(current_position, lane.central_lines[0])
            < dist_thresh,
            self.lanes
        ))

        return near_lanes

    def draw_map_lineset(self, image, line, color, transform, height, line_width=2):
        line = append_z_col(line, height)
        draw_line(image, line, color, transform,
                  self.camera_matrix, self.distortion_coeffs, line_width=line_width)

    def draw_marker(self,
                    image,
                    point_3d,
                    text,
                    height,
                    transform,
                    text_color=(255, 0, 0)):
        point_3d = np.append(point_3d, height)

        draw_marker(image, point_3d, text, text_color, transform,
                    self.camera_matrix, self.distortion_coeffs)

    def draw_lane(self,
                  image: np.ndarray,
                  lane: Lane,
                  transform: np.ndarray,
                  height):
        # draw left, central and right lines
        self.draw_map_lineset(
            image, lane.left_lines, self.left_line_color, transform, height)

        self.draw_map_lineset(image, lane.right_lines,
                              self.right_line_color, transform, height)
        self.draw_map_lineset(image, lane.central_lines,
                              self.central_line_color, transform, height)
        # draw lane id
        id_coords = lane.central_lines[0]
        self.draw_marker(image, id_coords, lane.lane_id, height,
                         transform)

    def draw_stop_line(self,
                       image,
                       transform,
                       height,
                       color=(255, 128, 0),
                       dist_thresh=100):

        lines = list(filter(
            lambda line: dist(transform[:-2, -1], line[0])
            < dist_thresh,
            self.stop_lines
        ))

        for line in lines:
            self.draw_map_lineset(image, line, color, inv(
                transform @ self.novatel2camera_tr), height, line_width=5)

    def draw_traffic_light(self, image, world2novatel_tr, height, dist_thresh=300, color=(0, 255, 0)):

        traffic_lights = list(filter(
            lambda traffic_light: dist(
                world2novatel_tr[:-2, -1], traffic_light.bbox[0, :-1])
            < dist_thresh,
            self.traffic_lights
        ))

        transform = inv(world2novatel_tr @ self.novatel2camera_tr)

        for tl in traffic_lights:
            coords = tl.bbox
            pts = np.array([
                coords[0],
                [coords[0, 0], coords[2, 1], coords[0, 2]],
                coords[1],
                [coords[0, 0], coords[2, 1], coords[1, 2]],
                coords[2],
                [coords[2, 0], coords[1, 1], coords[0, 2]],
                coords[3],
                [coords[2, 0], coords[1, 1], coords[1, 2]],
            ])

            lines = np.array([
                [pts[0], pts[1]],
                [pts[0], pts[2]],
                [pts[0], pts[5]],
                [pts[1], pts[3]],
                [pts[1], pts[4]],
                [pts[2], pts[3]],
                [pts[2], pts[3]],
                [pts[2], pts[7]],
                [pts[3], pts[6]],
                [pts[4], pts[5]],
                [pts[4], pts[6]],
                [pts[5], pts[7]],
                [pts[6], pts[7]],
            ])

            lines[:, :, -1] += height

            for line in lines:
                draw_line(image, line, color, transform,
                          self.camera_matrix, self.distortion_coeffs, line_width=3)

            # draw traffic light marker
            min_z = np.min(pts[:, -1])
            marker_pos = np.mean(pts[pts[:, -1] == min_z, :], axis=0)
            marker_pos[2] = min_z + height - 1

            draw_marker(image, marker_pos, tl.id, color, transform,
                        self.camera_matrix, self.distortion_coeffs)

    def draw(self, image, world2loc_tr):

        # TODO:
        # use self.get_map_data to draw traffic lights and stop lines

        # self.draw_stop_line(image, world2novatel_tr, height,
        #                     dist_thresh=self.distance_threshold, color=self.stop_line_color)

        # self.draw_traffic_light(image, world2novatel_tr, height,
        # dist_thresh=self.distance_threshold, color=self.traffic_light_color)

        map_data = self.get_map_data(world2loc_tr)

        lines_image = map_data["lanes"]
        for line in lines_image:
            draw_line_image(
                image, line["left_borders"], self.left_line_color, 2)
            draw_line_image(
                image, line["right_borders"], self.right_line_color, 2)
            draw_line_image(
                image, line["central_lines"], self.central_line_color, 2)

        stop_lines = map_data["stop_lines"]

        for line in stop_lines:
            draw_line_image(image, line, self.stop_line_color, 2)

    def get_map_data(self, world2loc_tr):

        world2loc_tr = removeXYRotation(world2loc_tr)

        world2novatel_tr = world2loc_tr @ self.localization2novatel_tr

        lanes = self.filter_segments(
            world2novatel_tr[:-2, -1], dist_thresh=self.distance_threshold)

        height = world2loc_tr[2, -1] + self.localization_frame_road_offset
        transform = inv(world2novatel_tr @ self.novatel2camera_tr)

        out_map = {"lanes": [],
                   "traffic_lights": [],
                   "stop_lines": []
                   }

        for lane in lanes:

            left_borders = transform_points_to_image_plane(
                append_z_col(lane.left_lines, height), transform, self.camera_matrix, self.distortion_coeffs)

            right_borders = transform_points_to_image_plane(
                append_z_col(lane.right_lines, height), transform, self.camera_matrix, self.distortion_coeffs)

            central_line_image = transform_points_to_image_plane(append_z_col(
                lane.central_lines, height), transform, self.camera_matrix, self.distortion_coeffs)

            if any([len(left_borders) < 2, len(right_borders) < 2, len(central_line_image) < 2]):
                continue

            out_map["lanes"].append({
                "left_borders": left_borders,
                "right_borders": right_borders,
                "central_lines": central_line_image
            })

        stop_lines = list(filter(
            lambda line: dist(
                world2novatel_tr[:-2, -1], line[0]) < self.distance_threshold,
            self.stop_lines
        ))

        for stop_line in stop_lines:

            stop_line_image = transform_points_to_image_plane(append_z_col(
                stop_line, height), transform, self.camera_matrix, self.distortion_coeffs)
            out_map["stop_lines"].append(stop_line_image)

        for tl in self.traffic_lights:
            coords = tl.bbox
            pts = np.array([
                coords[0],
                [coords[0, 0], coords[2, 1], coords[0, 2]],
                coords[1],
                [coords[0, 0], coords[2, 1], coords[1, 2]],
                coords[2],
                [coords[2, 0], coords[1, 1], coords[0, 2]],
                coords[3],
                [coords[2, 0], coords[1, 1], coords[1, 2]],
            ])

            lines = np.array([
                [pts[0], pts[1]],
                [pts[0], pts[2]],
                [pts[0], pts[5]],
                [pts[1], pts[3]],
                [pts[1], pts[4]],
                [pts[2], pts[3]],
                [pts[2], pts[7]],
                [pts[3], pts[6]],
                [pts[4], pts[5]],
                [pts[4], pts[6]],
                [pts[5], pts[7]],
                [pts[6], pts[7]],
            ])

            lines[:, :, -1] += height

            tl_lines = transform_points_to_image_plane(lines.reshape(
                (-1, 3)), transform, self.camera_matrix, self.distortion_coeffs)

            if len(tl_lines) == 24:
                out_map["traffic_lights"].append(tl_lines)

        return out_map


if __name__ == "__main__":
    mp = MapProjector("/apollo/modules/map/data/M11_part_1/base_map.txt")
