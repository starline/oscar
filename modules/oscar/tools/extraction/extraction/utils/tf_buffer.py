###############################################################################
# Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import sys

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from modules.common_msgs.transform_msgs import transform_pb2

from collections import deque
from scipy.spatial.transform import Rotation
from copy import deepcopy as dc
import numpy as np


class TFBuffer:
    def __init__(self, maxsize=50, diff_thresh=0.1, node_for_callback=None) -> None:
        self.ts_buffer = deque(maxlen=maxsize)
        self.data_buffer = deque(maxlen=maxsize)
        self.diff_thresh = diff_thresh

        if node_for_callback is not None:
            self.tf_reader = node_for_callback.create_reader(
                "/tf", transform_pb2.TransformStampeds, self.transform_cb)

    def empty(self):
        return len(self.ts_buffer) == 0

    def add(self, element, timestamp):
        self.ts_buffer.append(timestamp)
        self.data_buffer.append(element)

    def get_nearest(self, timestamp, return_ts=False):

        ts_array = np.array(self.ts_buffer)
        nearest_idx = np.argmin(np.abs(ts_array - timestamp))
        nearest_element = self.data_buffer[nearest_idx]

        rets = dc(nearest_element) if not return_ts else (
            dc(nearest_element), self.ts_buffer[nearest_idx])
        return rets

    def transform_cb(self, msg):
        msg_tr = msg.transforms[0].transform.translation
        msg_quat = msg.transforms[0].transform.rotation

        tf_matrix = np.eye(4)
        tf_matrix[:-1, :-1] = Rotation.from_quat(
            [msg_quat.qx, msg_quat.qy, msg_quat.qz, msg_quat.qw]).as_matrix()
        tf_matrix[:-1, -1] = np.array([msg_tr.x, msg_tr.y, msg_tr.z])

        self.add(
            tf_matrix, msg.transforms[0].header.timestamp_sec)
