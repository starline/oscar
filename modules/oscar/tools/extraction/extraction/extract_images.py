#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
import sys
import time
import os
from datetime import datetime

from scipy.spatial.transform import Rotation as R
import open3d as o3d
import numpy as np
import cv2 as cv
from copy import deepcopy as dc

from tqdm.auto import tqdm

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")


from cyber.python.cyber_py3 import record
from modules.common_msgs.transform_msgs import transform_pb2
from modules.common_msgs.sensor_msgs import sensor_image_pb2

from extraction.utils import get_total_messages, TFBuffer


def parse_compressed_image(msg):
    msg_struct = sensor_image_pb2.CompressedImage.FromString(msg)

    buf = np.frombuffer(msg_struct.data, dtype=np.uint8)
    im = cv.imdecode(buf, cv.IMREAD_COLOR)

    return im, msg_struct.header.timestamp_sec


def parse_raw_image(msg):
    msg_struct = sensor_image_pb2.Image()
    msg_struct.ParseFromString(msg)

    im = np.fromstring(msg_struct.data, np.uint8).reshape(
        (msg_struct.height, msg_struct.width, 3))

    return im, msg_struct.header.timestamp_sec


class ImageSaver:

    def __init__(self, channelname, save_folder, time_diff=0) -> None:
        self.image_list = []
        self.time_diff = time_diff
        self.prev_image_ts = 0

        if "compressed" in channelname:
            self.parse_image = parse_compressed_image
        else:
            self.parse_image = parse_raw_image

        self.tf_buffer = TFBuffer(maxsize=100000)

        self.save_folder = save_folder

    def add_tf(self, tf_msg):

        msg_struct = transform_pb2.TransformStampeds.FromString(tf_msg)

        msg_tr = msg_struct.transforms[0].transform.translation
        msg_quat = msg_struct.transforms[0].transform.rotation

        tf_matrix = np.eye(4)
        tf_matrix[:-1, :-1] = R.from_quat(
            [msg_quat.qx, msg_quat.qy, msg_quat.qz, msg_quat.qw]).as_matrix()
        tf_matrix[:-1, -1] = np.array([msg_tr.x, msg_tr.y, msg_tr.z])

        self.tf_buffer.add(
            tf_matrix, msg_struct.transforms[0].header.timestamp_sec)

    def add_image(self, image_msg):
        image, image_ts = self.parse_image(image_msg)

        if image_ts - self.prev_image_ts < self.time_diff:
            return

        self.prev_image_ts = image_ts

        self.image_list.append((image, image_ts))

    def save_images(self):
        self.keep_images_list = []
        for image, image_ts in tqdm(self.image_list):
            msecs = int(image_ts % 1 * 1e3 % 1e3)
            cur_time = datetime.fromtimestamp(
                image_ts)

            str_time = cur_time.strftime("%Y-%m-%d-%H-%M-%S")
            str_time = f'{str_time}-{msecs:03d}'

            tf, tf_ts = self.tf_buffer.get_nearest(image_ts, return_ts=True)

            if abs(image_ts - tf_ts) > 0.05:
                print(
                    f'High diff:{image_ts - tf_ts:.3f}, {image_ts:.3f}, {tf_ts:.3f}')
                self.keep_images_list.append((image, image_ts))
                continue

            np.savetxt(f'{self.save_folder}/{str_time}_pose.txt', tf)
            cv.imwrite(f"{self.save_folder}/{str_time}.png", image)

        if len(self.keep_images_list) != 0:
            print(
                f"{len(self.keep_images_list)}/{len(self.image_list)} images were not saved because of high timestamp diff with /tf")

        # self.image_list = dc(self.keep_images_list)
        self.image_list = []


def main(src_folder, save_folder, image_channel):

    files = []
    for (r, ds, fs) in os.walk(src_folder):
        [files.append(f'{r}/{f}') for f in fs]

    os.makedirs(save_folder, exist_ok=True)

    assert len(files) > 0

    saver = ImageSaver(image_channel, save_folder)

    for idx, f in enumerate(sorted(files)):

        rfile = f

        freader = record.RecordReader(rfile)
        if image_channel == 'auto':
            image_channel = [a for a in freader.get_channellist()
                             if 'image' in a][0]
        print(f'Extracting channel: {image_channel}')

        for channelname, msg, datatype, timestamp in tqdm(freader.read_messages(), total=get_total_messages(freader)):

            if channelname == image_channel:

                saver.add_image(msg)

            elif channelname == '/tf':
                saver.add_tf(msg)

        saver.save_images()


if __name__ == '__main__':  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str, help='a path to folder containing .record files',
                        default='/apollo/data/bag/24_08_nissan/')
    parser.add_argument('-d', '--dest', type=str, help='a path to destination folder',
                        default='/apollo/scripts/oscar/cyber_scripts/extraction/images_infinity')
    parser.add_argument('-c', '--channel', type=str,
                        help='name of channel with pointclouds', default='auto')

    args = parser.parse_args()
    print(args)
    try:
        main(args.path, args.dest, args.channel)
    except KeyboardInterrupt:
        exit()
