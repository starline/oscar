#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import sys
from threading import Lock
import time
import argparse
import google.protobuf.text_format as text_format

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import cyber

from extraction.utils import NumpyPCProcessor, RawImageProcessor, CompressedImageProcessor


def get_message_processor(channelname):
    if "compressed" in channelname:
        return CompressedImageProcessor
    elif "image" in channelname:
        return RawImageProcessor
    elif "PointCloud2" in channelname:
        return NumpyPCProcessor
    else:
        raise RuntimeError("Unknown processing for channel " + channelname)


class Saver:
    def __init__(self, read_channel: str, save_file: str) -> None:

        self.save_file = save_file

        self.node = cyber.Node("SingleMessageSaver")

        self.lock = Lock()

        self.keep_running = True

        self.message_processor = get_message_processor(read_channel)
        self.reader = self.node.create_rawdata_reader(
            read_channel, self.callback)

    def callback(self, msg):
        with self.lock:
            if not self.keep_running:
                return
            print("Got message. Saving...")
            self.message_processor.save(msg, self.save_file)
            self.keep_running = False
            print("Saved")

    def spin(self):

        while self.keep_running and cyber.ok():
            time.sleep(0.1)

        # print("Exiting...")
        # cyber.shutdown()


if __name__ == "__main__":

    cyber.init()

    saver = Saver(
        "/apollo/sensor/lidar/rs_ruby_left/PointCloud2", "ruby_l.npy")

    saver.spin()
