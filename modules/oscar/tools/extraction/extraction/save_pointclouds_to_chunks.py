###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################


import argparse
from multiprocessing import Event, Process, Queue
import os
import time

import open3d as o3d
import numpy as np
from scipy.spatial.transform import Rotation as R
import google.protobuf.message
from dataclasses import dataclass

import sys

from tqdm import tqdm

sys.path.append("/apollo/bazel-bin")

from modules.common_msgs.transform_msgs import transform_pb2
from modules.common_msgs.sensor_msgs import pointcloud_pb2

from extraction.utils import ReaderWrapper, TransformsManager, voxel_downsample


N_CONSUMERS = 10


def extract_transform(msg):
    msg_struct = transform_pb2.TransformStampeds()
    msg_struct.ParseFromString(msg)

    tl = msg_struct.transforms[0].transform.translation
    rot = msg_struct.transforms[0].transform.rotation

    transform = np.eye(4)
    transform[:-1, -1] = [tl.x, tl.y, tl.z]
    transform[:-1, :-
              1] = R.from_quat([rot.qx, rot.qy, rot.qz, rot.qw]).as_matrix()

    return transform


def extract_np_points(msg, transform, voxel_size):

    try:
        msg_struct = pointcloud_pb2.PointCloud.FromString(msg)
    except google.protobuf.message.DecodeError:
        return np.empty((0, 3))

    points = np.array(
        [[i.x, i.y, i.z, i.intensity] for i in msg_struct.point], dtype=np.float64)

    points = voxel_downsample(points, voxel_size)

    rotation = transform[:-1, :-1]
    translation = transform[:-1, -1]

    points[:, :3] = points[:, :3] @ rotation.T + translation

    return points


def produce_messages(src_path, pc_channel, produce_queue, producer_done_event, msg_step):

    reader = ReaderWrapper(src_path)

    last_transform = None
    counter = 0

    it = 0

    for channelname, msg, datatype, timestamp in tqdm(reader, total=len(reader), position=0):

        if channelname == "/tf":
            last_transform = extract_transform(msg)
            continue

        if channelname != pc_channel or last_transform is None:
            continue

        it += 1
        if it != msg_step:
            continue
        it = 0

        produce_queue.put((msg, last_transform))
        counter += 1

    produce_queue.put(None)

    producer_done_event.set()


def convert_message(static_tf, produce_queue, consume_queue, consumer_done_event, voxel_size):

    while True:
        if produce_queue.qsize() == 0:
            time.sleep(0.1)
            continue

        task = produce_queue.get()
        if task is None:
            produce_queue.put(None)
            break

        msg, tf = task

        consume_queue.put(extract_np_points(msg, tf @ static_tf, voxel_size))

    consumer_done_event.set()


def extract_chunks(src_path, dest_path, pc_channel, tf_config_file, chunk_size, min_points, msg_step, voxel_size, sensor_frame):

    produce_queue = Queue()
    consume_queue = Queue()

    transform_manager = TransformsManager(
        tf_config_file)

    static_tf = transform_manager.get_tf("localization", sensor_frame)

    producer_done = Event()
    consumers_done = [Event() for _ in range(N_CONSUMERS)]

    producer = Process(target=produce_messages, args=(
        src_path, pc_channel, produce_queue, producer_done, msg_step))
    consumers = [Process(target=convert_message, args=(
        static_tf, produce_queue, consume_queue, consumers_done[i], voxel_size)) for i in range(N_CONSUMERS)]

    [c.start() for c in consumers]
    producer.start()

    cloud_chunk_pool = PointCloudChunkPool(
        chunk_size, min_points, voxel_size)

    pbar = tqdm(position=1, bar_format='{desc}')
    counter = 0
    while consume_queue.qsize() > 0 or not producer_done.is_set() or not all(c.is_set() for c in consumers_done):

        pbar.set_description(
            f'produce queue: {produce_queue.qsize()}, consume queue: {consume_queue.qsize()}, added {counter}')
        if consume_queue.qsize() == 0:
            time.sleep(0.1)
            continue
        counter += 1
        r = consume_queue.get()
        cloud_chunk_pool.add_cloud(r)

    pbar.close()

    save_folder = dest_path
    os.makedirs(save_folder, exist_ok=True)
    cloud_chunk_pool.save(save_folder)

    producer.join()

    for c in consumers:
        c.join()


@dataclass
class HashablePoint:
    x: float
    y: float

    def __hash__(self) -> int:
        return hash((self.x, self.y))


def write_pointcloud(points, fname):
    np.savetxt(fname, points)


class PointCloudChunk:

    def __init__(self, x, y, size, voxel_size, max_points=500_000) -> None:
        self.origin = np.array([x, y])

        self.init_max_points = max_points

        self.center = self.origin + size / 2

        self.points = np.empty((self.init_max_points, 4), dtype=np.float64)
        self.n_points = 0

        self.downsample_size = voxel_size

    def downsample(self):

        if self.n_points <= 1:
            return

        points = voxel_downsample(
            self.points[:self.n_points], self.downsample_size)

        self.n_points = len(points)
        self.points[:self.n_points] = points

    def add_points(self, points):
        if self.n_points + len(points) > self.init_max_points:
            self.points = np.concatenate([self.points[:self.n_points], points])
            self.n_points = len(self.points)
            self.downsample()
        else:
            self.points[self.n_points: self.n_points + len(points)] = points
            self.n_points += len(points)


class PointCloudChunkPool:

    def __init__(self, chunk_size, min_points, voxel_size) -> None:
        self.chunks = {}
        self.chunk_size = chunk_size
        self.min_points = min_points
        self.voxel_size = voxel_size

    def add_cloud(self, pointcloud):

        xs, ys = pointcloud[:, :2].T
        x_min = xs.min()
        x_max = xs.max()

        y_min = ys.min()
        y_max = ys.max()

        for x in np.arange(x_min - x_min % self.chunk_size, (x_max + self.chunk_size) - x_max % self.chunk_size, self.chunk_size):
            for y in np.arange(y_min - y_min % self.chunk_size, (y_max + self.chunk_size) - y_max % self.chunk_size, self.chunk_size):
                grid_idx = HashablePoint(x, y)

                inl_indices = np.logical_and(
                    pointcloud[:, 0] >= x, pointcloud[:, 0] < x + self.chunk_size)
                inl_indices = np.logical_and(
                    inl_indices, pointcloud[:, 1] >= y)
                inl_indices = np.logical_and(
                    inl_indices, pointcloud[:, 1] < y + self.chunk_size)

                if sum(inl_indices) < self.min_points:
                    continue
                if grid_idx not in self.chunks:
                    self.chunks[grid_idx] = PointCloudChunk(
                        x, y, self.chunk_size, self.voxel_size)

                self.chunks[grid_idx].add_points(pointcloud[inl_indices])

    def save(self, folder):
        for key, chunk in tqdm(self.chunks.items(), desc="Saving chunks"):

            chunk.downsample()
            if chunk.n_points < self.min_points:
                print("Skipping chunk due to small size:",
                      chunk.n_points, chunk.origin)
                continue
            x, y = chunk.origin.astype(int)
            chunk_fname = f"chunk_x{x}_y{y}.npy"

            write_pointcloud(
                chunk.points[:chunk.n_points], f'{folder}/{chunk_fname}')


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--src_path', type=str,
                        help='path to records folder', default="/apollo/data/bag/ssd_internal/29_08_2023/m11_remapped")

    parser.add_argument('-d', '--dest_path', type=str,
                        help='path to save folder', default="/apollo/modules/oscar/tools/data/m11_bpearls")

    parser.add_argument('-c', '--channel', type=str, help='channelname with pointclouds',
                        default="/apollo/sensor/lidar/rs_ruby_left/PointCloud2")
    parser.add_argument('--tf_config', type=str, help='path to static transform module config',
                        default="/apollo/modules/calibration/data/mb_actros/transform_conf/static_transform_conf.pb.txt")

    parser.add_argument('-f', '--frame', type=str, help='frame for lidar',
                        default="lidar_bpearl_left")

    parser.add_argument('-s', '--size', type=int,
                        help='Size of chunk in metres', default=100)
    parser.add_argument('--min_points', type=int,
                        help='min points in chunk', default=500)
    parser.add_argument('--voxel_size', type=float,
                        help='size of voxel in downsampling', default=0.05)
    parser.add_argument('--step', type=int,
                        help='step in reading pointclouds', default=20)

    args = parser.parse_args()
    print(args)

    extract_chunks(args.src_path, args.dest_path, args.channel, args.tf_config, args.size, args.min_points,
                   args.step, args.voxel_size, args.frame)
