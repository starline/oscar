#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import time
import os
import argparse
import sys
from datetime import datetime

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from tqdm.auto import tqdm

from cyber.python.cyber_py3 import record

from extraction.utils import get_total_messages
from extraction.utils import NumpyPCProcessor, O3dPCDProcessor


def main(src_folder, save_folder,
         main_channel='/apollo/sensor/lidar/rs_ruby_left/compensator/PointCloud2',
         secondary_channel='/apollo/sensor/lidar/rs_bpearl_left/compensator/PointCloud2',
         save_numpy=False):

    os.makedirs(save_folder, exist_ok=True)

    MessageProcessor = NumpyPCProcessor if save_numpy else O3dPCDProcessor

    for idx, f in enumerate(sorted(os.listdir(src_folder))):

        rfile = src_folder + '/' + f

        freader = record.RecordReader(rfile)
        time.sleep(.1)

        last_main_pc = None
        last_main_ts = None

        for channelname, msg, datatype, timestamp in tqdm(freader.read_messages(), total=get_total_messages(freader)):

            if channelname == main_channel:
                last_main_ts, last_main_pc = MessageProcessor.parseMessage(msg)

            elif channelname == secondary_channel:

                if last_main_pc is not None:
                    ts, sec_pc = MessageProcessor.parseMessage(msg)

                    cur_time = datetime.fromtimestamp(last_main_ts)
                    msecs = int((last_main_ts % 1) * 1000)
                    str_time = cur_time.strftime("%Y-%m-%d-%H-%M-%S")
                    str_time = f'{str_time}-{msecs:03d}'

                    MessageProcessor.save(
                        last_main_pc, f"{save_folder}/{str_time}_main")
                    MessageProcessor.save(
                        sec_pc, f"{save_folder}/{str_time}_sec")


if __name__ == '__main__':  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str, help='a path to folder containing .record files',
                        default='/apollo/data/bag/24_08_nissan/24_08_nissan_1')
    parser.add_argument('-d', '--dest', type=str, help='a path to destination folder',
                        default='/apollo/scripts/oscar/cyber_scripts/extraction/pcds_infinity')
    parser.add_argument('-m', '--main', type=str, help='main channel name',
                        default='/apollo/sensor/lidar/rs_ruby_left/compensator/PointCloud2')
    parser.add_argument('-s', '--sec', type=str, help='secondary channel name',
                        default='/apollo/sensor/lidar/rs_ruby_right/compensator/PointCloud2')

    parser.add_argument("--npy", action="store_true",
                        help="to save in .npy file instead of .pcd")

    args = parser.parse_args()
    print(args)
    try:
        main(args.path,
             args.dest,
             main_channel=args.main,
             secondary_channel=args.sec,
             save_numpy=args.npy
             )
    except KeyboardInterrupt:
        exit()
