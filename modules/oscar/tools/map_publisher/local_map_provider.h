///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "modules/common_msgs/map_msgs/map.pb.h"
#include "modules/common_msgs/transform_msgs/transform.pb.h"
#include "modules/oscar/foxglove/schemas/SceneUpdate.pb.h"
#include "modules/oscar/tools/map_publisher/proto/map_publisher_conf.pb.h"

#include "modules/oscar/tools/map_publisher/utils/convertion_utils.h"

using apollo::hdmap::Map;
using apollo::transform::TransformStampeds;

using apollo::oscar::map::publisher::ColorOptions;

namespace apollo {
namespace oscar {

class LocalMapProvider final {
 public:
  LocalMapProvider(const std::string& map_file, double distance_threshold,
                   std::array<double, 2> local_map_offset,
                   double loc_frame_height,
                   const ColorOptions& color_options = default_options);

  foxglove::SceneUpdate provideLocalMap(const TransformStampeds& loc_tf_msg);

 private:
  template <typename T, typename = std::enable_if_t<
                            std::is_same<T, apollo::hdmap::Lane>::value ||
                            std::is_same<T, apollo::hdmap::Signal>::value>>
  void filter_objects(const google::protobuf::RepeatedPtrField<T>& map_objects,
                      foxglove::SceneUpdate* out_update_msg,
                      const std::array<double, 3>& current_coords,
                      double distance_threshold);

  template <typename T>
  void add_map_object(foxglove::SceneUpdate* out_update_msg, const T& object,
                      double height, const std::string& object_id);

 private:
  double loc_frame_height_;
  double dist_thresh_;
  Map map_data_;
  std::array<double, 2> local_map_offset_;

  ColorOptions color_options_;
};
}  // namespace oscar
}  // namespace apollo
