///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/tools/map_publisher/utils/convertion_utils.h"

#include <algorithm>
#include <cmath>
#include <stdexcept>
#include <vector>

#include <Eigen/Core>
#include <Eigen/Eigenvalues>
#include "google/protobuf/util/time_util.h"

namespace apollo {
namespace oscar {

namespace {

template <uint64_t Dim1, uint64_t Dim2>
double distance2D(const std::array<double, Dim1>& p1,
                  const std::array<double, Dim2>& p2) {
  return std::sqrt(std::pow(p1[0] - p2[0], 2) + std::pow(p1[1] - p2[1], 2));
}

}  // namespace

Eigen::Quaterniond get_orientation_around_z_axis(
    const Eigen::MatrixXd& boundary_coords) {
  Eigen::EigenSolver<Eigen::MatrixXd> eigensolver;

  auto coords_2d = boundary_coords.block<4, 2>(0, 0);
  Eigen::MatrixXd centered = coords_2d.rowwise() - coords_2d.colwise().mean();
  Eigen::MatrixXd cov = (centered.adjoint() * centered) /
                        static_cast<double>(coords_2d.rows() - 1);

  eigensolver.compute(cov);

  Eigen::Vector2d main_vector = eigensolver.eigenvectors().col(0).real();
  auto angle = std::atan2(main_vector.coeff(1), main_vector.coeff(0));

  return Eigen::AngleAxisd(angle, Eigen::Vector3d::UnitZ()) *
         Eigen::AngleAxisd(0, Eigen::Vector3d::UnitX()) *
         Eigen::AngleAxisd(0, Eigen::Vector3d::UnitY());
}

foxglove::LinePrimitive convert_line(const apollo::hdmap::CurveSegment& segment,
                                     const foxglove::Color& color,
                                     const std::array<double, 2>& map_offset,
                                     double fixed_height) {
  foxglove::LinePrimitive converted_line;
  converted_line.set_type(foxglove::LinePrimitive_Type_LINE_STRIP);

  converted_line.set_thickness(THICKNESS);
  converted_line.set_scale_invariant(true);

  auto fg_color = converted_line.mutable_color();
  fg_color->CopyFrom(color);

  for (auto point : segment.line_segment().point()) {
    auto converted_point = converted_line.add_points();
    converted_point->set_x(point.x() - map_offset[0]);
    converted_point->set_y(point.y() - map_offset[1]);
    // converted_point->set_z(point.has_z() ? point.z() : fixed_height);
    converted_point->set_z(fixed_height);
  }

  return converted_line;
}

void add_polygon(foxglove::SceneEntity* entity,
                 const apollo::hdmap::Polygon& boundary,
                 const std::array<double, 2>& map_offset, double fixed_height,
                 const foxglove::Color& color) {
  foxglove::CubePrimitive* fg_bbox = entity->add_cubes();

  auto fg_color = fg_bbox->mutable_color();
  fg_color->CopyFrom(color);

  Eigen::Matrix<double, 4, 3> boundary_coords;

  for (int i = 0, total = boundary.point().size(); i < total; i++) {
    auto point = boundary.point(i);
    boundary_coords.row(i) << point.x(), point.y(), point.z();
  }

  Eigen::Vector3d center_coords = boundary_coords.colwise().mean();

  auto quaternion = get_orientation_around_z_axis(boundary_coords);

  auto pose = fg_bbox->mutable_pose();
  auto position = pose->mutable_position();

  position->set_x(center_coords.coeff(0) - map_offset[0]);
  position->set_y(center_coords.coeff(1) - map_offset[1]);
  position->set_z(center_coords.coeff(2) + fixed_height);

  auto orientation = pose->mutable_orientation();

  orientation->set_x(quaternion.x());
  orientation->set_y(quaternion.y());
  orientation->set_z(quaternion.z());
  orientation->set_w(quaternion.w());

  auto sz = fg_bbox->mutable_size();

  // sligthly not correct if traffic light bbox is not aligned to axes
  sz->set_x(boundary_coords.col(0).maxCoeff() -
            boundary_coords.col(0).minCoeff());
  sz->set_y(boundary_coords.col(1).maxCoeff() -
            boundary_coords.col(1).minCoeff());
  sz->set_z(boundary_coords.col(2).maxCoeff() -
            boundary_coords.col(2).minCoeff());
}

void add_stop_line(foxglove::SceneEntity* entity,
                   const apollo::hdmap::Curve& line,
                   const std::array<double, 2>& map_offset, double fixed_height,
                   const foxglove::Color& color) {
  auto segment = line.segment();

  std::vector<foxglove::LinePrimitive> fg_lines(segment.size());

  std::transform(segment.begin(), segment.end(), fg_lines.begin(),
                 [&](const apollo::hdmap::CurveSegment& segment) {
                   return convert_line(segment, color, map_offset,
                                       fixed_height);
                 });
  entity->mutable_lines()->MergeFrom({fg_lines.begin(), fg_lines.end()});
}

void convert_map_object(foxglove::SceneEntity* entity,
                        const apollo::hdmap::Lane& lane,
                        const std::array<double, 2>& map_offset,
                        double fixed_height,
                        const map::publisher::ColorOptions& options) {
  auto central_segment = lane.central_curve().segment();
  std::vector<foxglove::LinePrimitive> fg_lines(central_segment.size());

  std::transform(central_segment.begin(), central_segment.end(),
                 fg_lines.begin(),
                 [&](const apollo::hdmap::CurveSegment& segment) {
                   return convert_line(segment, options.central_line_color(),
                                       map_offset, fixed_height);
                 });

  entity->mutable_lines()->MergeFrom({fg_lines.begin(), fg_lines.end()});

  auto left_segment = lane.left_boundary().curve().segment();
  std::vector<foxglove::LinePrimitive> left_lines(left_segment.size());

  std::transform(left_segment.begin(), left_segment.end(), left_lines.begin(),
                 [&](const apollo::hdmap::CurveSegment& segment) {
                   return convert_line(segment, options.left_line_color(),
                                       map_offset, fixed_height);
                 });

  entity->mutable_lines()->MergeFrom({left_lines.begin(), left_lines.end()});

  auto right_segment = lane.right_boundary().curve().segment();
  std::vector<foxglove::LinePrimitive> right_lines(right_segment.size());

  std::transform(right_segment.begin(), right_segment.end(),
                 right_lines.begin(),
                 [&](const apollo::hdmap::CurveSegment& segment) {
                   return convert_line(segment, options.right_line_color(),
                                       map_offset, fixed_height);
                 });

  entity->mutable_lines()->MergeFrom({right_lines.begin(), right_lines.end()});
}

void convert_map_object(foxglove::SceneEntity* entity,
                        const apollo::hdmap::Signal& signal,
                        const std::array<double, 2>& map_offset,
                        double fixed_height,
                        const map::publisher::ColorOptions& options) {
  // convert bbox
  add_polygon(entity, signal.boundary(), map_offset, fixed_height,
              options.traffic_light_color());

  // convert stop line
  for (auto& line : signal.stop_line()) {
    add_stop_line(entity, line, map_offset, fixed_height,
                  options.stop_line_color());
  }
}

bool is_near(const apollo::hdmap::Lane& object,
             const std::array<double, 3>& current_coords,
             double distance_threshold) {
  auto segment_sz = object.central_curve().segment().size();

  auto first_point = object.central_curve().segment(0).line_segment().point(0);
  auto end_point =
      object.central_curve().segment(segment_sz - 1).line_segment().point(0);

  std::array<double, 2> first_point_coords{first_point.x(), first_point.y()};
  std::array<double, 2> end_point_coords{end_point.x(), end_point.y()};

  bool lane_is_near =
      (distance2D(current_coords, first_point_coords) <= distance_threshold) ||
      (distance2D(current_coords, end_point_coords) <= distance_threshold);

  return lane_is_near;
}

bool is_near(const apollo::hdmap::Signal& object,
             const std::array<double, 3>& current_coords,
             double distance_threshold) {
  auto first_point = object.boundary().point(0);
  std::array<double, 2> signal_point{first_point.x(), first_point.y()};

  return distance2D(current_coords, signal_point) <= distance_threshold;
}

double distance(const Transform& tf1, const Transform tf2) {
  return std::sqrt(std::pow(tf1.translation().x() - tf2.translation().x(), 2) +
                   std::pow(tf1.translation().y() - tf2.translation().y(), 2) +
                   std::pow(tf1.translation().z() - tf2.translation().z(), 2));
}

void convert_timestamp(double src_ts, google::protobuf::Timestamp* out_ts) {
  out_ts->set_seconds(static_cast<int>(src_ts));
  double intpart;
  out_ts->set_nanos(modf(src_ts, &intpart) * 1'000'000'000);
}

}  // namespace oscar
}  // namespace apollo