///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "modules/oscar/tools/map_publisher/map_publisher_component.h"

#include <limits>

#include "modules/common/adapters/adapter_gflags.h"
#include "modules/common/configs/config_gflags.h"
#include "modules/common/util/message_util.h"

#if __GNUC__ < 8
#include <experimental/filesystem>
using std::experimental::filesystem::exists;
#else
#include <filesystem>
using std::filesystem::exists;
#endif

#define DIST_THRESH 0.3
namespace apollo {
namespace oscar {

bool MapPublisherComponent::Init() {
  if (!GetProtoConfig(&conf_)) {
    AERROR << "Parse conf file failed, " << ConfigFilePath();
    return false;
  }

  cyber::proto::RoleAttributes attr;
  attr.set_channel_name(conf_.channel_name());

  auto qos_profile = attr.mutable_qos_profile();
  qos_profile->set_history(
      apollo::cyber::transport::QosHistoryPolicy::HISTORY_KEEP_LAST);
  qos_profile->set_depth(10);
  qos_profile->set_mps(0);
  qos_profile->set_reliability(
      apollo::cyber::transport::QosReliabilityPolicy::RELIABILITY_RELIABLE);
  qos_profile->set_durability(apollo::cyber::transport::QosDurabilityPolicy::
                                  DURABILITY_TRANSIENT_LOCAL);

  map_writer_ = node_->CreateWriter<foxglove::SceneUpdate>(attr);

  attr.set_channel_name("/tf_local_map");

  tf_writer_ = node_->CreateWriter<TransformStampeds>(attr);

  loc_tf_reader_ = node_->CreateReader<TransformStampeds>(
      FLAGS_tf_topic, [this](auto& msg) { Callback(msg); });

  if (conf_.has_map_file()) {
    map_file_ = conf_.map_file();
    AINFO << "Using map file: " << map_file_;
  } else {
    map_file_ = FLAGS_map_dir + "/base_map.bin";
    AERROR << "No map file found in config file. Using map from config_glags: "
           << map_file_;
  }

  if (!exists(map_file_)) {
    AERROR << "Map file does not exist: " << map_file_;
    return false;
  }

  return true;
}

void MapPublisherComponent::Callback(
    std::shared_ptr<TransformStampeds> loc_tf_msg) {
  auto start = apollo::cyber::Time::Now().ToSecond();
  if (local_map_provider_ == nullptr) {
    local_frame_offset_[0] =
        loc_tf_msg->transforms(0).transform().translation().x();
    local_frame_offset_[1] =
        loc_tf_msg->transforms(0).transform().translation().y();

    local_map_provider_ =
        conf_.has_color_options()
            ? std::make_unique<apollo::oscar::LocalMapProvider>(
                  map_file_, conf_.max_map_distance(), local_frame_offset_,
                  conf_.localization_frame_height(), conf_.color_options())
            : std::make_unique<apollo::oscar::LocalMapProvider>(
                  map_file_, conf_.max_map_distance(), local_frame_offset_,
                  conf_.localization_frame_height());
  }

  double dist;

  if (last_update_transform_.has_translation() &&
      last_update_transform_.has_rotation())
    dist =
        distance(last_update_transform_, loc_tf_msg->transforms(0).transform());
  else
    dist = std::numeric_limits<double>::max();

  if (dist < conf_.max_map_distance() * DIST_THRESH) return;

  last_update_transform_.mutable_translation()->CopyFrom(
      loc_tf_msg->transforms(0).transform().translation());

  last_update_transform_.mutable_rotation()->CopyFrom(
      loc_tf_msg->transforms(0).transform().rotation());

  auto local_map = local_map_provider_->provideLocalMap(*loc_tf_msg);

  auto dur = apollo::cyber::Time::Now().ToSecond() - start;
  AERROR << "Publish objects: " << local_map.entities().size()
         << ". Time taken: " << dur;

  map_writer_->Write(local_map);
  PublishLocalTf(local_frame_offset_);
}

void MapPublisherComponent::PublishLocalTf(
    const std::array<double, 2>& local_frame_offset) {
  TransformStampeds transforms;
  auto tf = transforms.add_transforms();

  auto mutable_head = tf->mutable_header();
  mutable_head->set_timestamp_sec(cyber::Time::Time::Now().ToSecond());
  mutable_head->set_frame_id("world");
  tf->set_child_frame_id("local_map_frame");

  auto mutable_translation = tf->mutable_transform()->mutable_translation();
  mutable_translation->set_x(local_frame_offset[0]);
  mutable_translation->set_y(local_frame_offset[1]);
  mutable_translation->set_z(0);

  auto mutable_rotation = tf->mutable_transform()->mutable_rotation();
  mutable_rotation->set_qx(0);
  mutable_rotation->set_qy(0);
  mutable_rotation->set_qz(0);
  mutable_rotation->set_qw(1);

  tf_writer_->Write(transforms);
}

}  // namespace oscar
}  // namespace apollo
