///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <memory>
#include <string>
#include <vector>

#include "modules/common_msgs/map_msgs/map.pb.h"
#include "modules/common_msgs/transform_msgs/transform.pb.h"
#include "modules/oscar/foxglove/schemas/SceneUpdate.pb.h"
#include "modules/oscar/tools/map_publisher/proto/map_publisher_conf.pb.h"

#include "cyber/component/timer_component.h"
#include "cyber/cyber.h"
#include "modules/oscar/tools/map_publisher/local_map_provider.h"
#include "modules/transform/transform_broadcaster.h"

using apollo::hdmap::Map;
using apollo::transform::TransformStampeds;

namespace apollo {
namespace oscar {

class MapPublisherComponent final : public apollo::cyber::Component<> {
 public:
  bool Init() override;

 private:
  void Callback(std::shared_ptr<TransformStampeds> loc_tf_msg);
  void PublishLocalTf(const std::array<double, 2>& local_frame_offset);

  map::publisher::Conf conf_;
  std::shared_ptr<cyber::Writer<foxglove::SceneUpdate>> map_writer_;
  std::shared_ptr<cyber::Writer<TransformStampeds>> tf_writer_;

  std::shared_ptr<cyber::Reader<TransformStampeds>> loc_tf_reader_;
  std::unique_ptr<apollo::oscar::LocalMapProvider> local_map_provider_;

  Transform last_update_transform_;

  std::unique_ptr<apollo::transform::TransformBroadcaster> tf2_broadcaster_;
  std::array<double, 2> local_frame_offset_;

  std::string map_file_;
};

CYBER_REGISTER_COMPONENT(MapPublisherComponent)

}  // namespace oscar
}  // namespace apollo
