
#include "cyber/component/component.h"
#include "cyber/cyber.h"
#include "modules/common_msgs/sensor_msgs/pointcloud.pb.h"
#include "modules/common_msgs/transform_msgs/transform.pb.h"
#include "modules/perception/onboard/transform_wrapper/transform_wrapper.h"

apollo::transform::TransformStamped interpolateTf(
    const apollo::transform::TransformStamped& tf1,
    const apollo::transform::TransformStamped& tf2, const double msg_ts) {
  apollo::transform::TransformStamped res_tf;

  const auto ts_diff =
      tf2.header().timestamp_sec() - tf1.header().timestamp_sec();

  ACHECK(ts_diff > 0) << "tf1 timestamp is before tf2";

  // const auto tf1_diff = tf1.header().timestamp_sec() - msg_ts;
  const auto tf2_diff = tf2.header().timestamp_sec() - msg_ts;

  // double tf1_weight = (tf1_diff) / ts_diff;
  const double tf2_weight = (tf2_diff) / ts_diff;

  // implement interploation of transform

  const Eigen::Vector3d trans1{tf1.transform().translation().x(),
                               tf1.transform().translation().y(),
                               tf1.transform().translation().z()};
  const Eigen::Vector3d trans2{tf2.transform().translation().x(),
                               tf2.transform().translation().y(),
                               tf2.transform().translation().z()};

  // auto res_trans_vec = trans1 * tf1_weight + trans2 * tf2_weight;

  const auto trans_diff = trans1 - trans2;
  const auto res_trans_vec = trans2 + trans_diff * tf2_weight;

  Eigen::Quaterniond q1(
      tf1.transform().rotation().qw(), tf1.transform().rotation().qx(),
      tf1.transform().rotation().qy(), tf1.transform().rotation().qz());

  Eigen::Quaterniond q2(
      tf2.transform().rotation().qw(), tf2.transform().rotation().qx(),
      tf2.transform().rotation().qy(), tf2.transform().rotation().qz());

  auto res_quat = q2.slerp(tf2_weight, q1);

  auto res_trans = res_tf.mutable_transform()->mutable_translation();
  res_trans->set_x(res_trans_vec[0]);
  res_trans->set_y(res_trans_vec[1]);
  res_trans->set_z(res_trans_vec[2]);

  auto res_rot = res_tf.mutable_transform()->mutable_rotation();
  res_rot->set_qx(res_quat.x());
  res_rot->set_qy(res_quat.y());
  res_rot->set_qz(res_quat.z());
  res_rot->set_qw(res_quat.w());

  return res_tf;
}

apollo::transform::TransformStamped get_transform(
    const std::vector<double>& translation, const std::vector<double>& rotation,
    double time) {
  apollo::transform::TransformStamped res;
  res.mutable_header()->set_timestamp_sec(time);

  res.mutable_transform()->mutable_translation()->set_x(translation[0]);
  res.mutable_transform()->mutable_translation()->set_y(translation[1]);
  res.mutable_transform()->mutable_translation()->set_z(translation[2]);

  res.mutable_transform()->mutable_rotation()->set_qx(rotation[0]);
  res.mutable_transform()->mutable_rotation()->set_qy(rotation[1]);
  res.mutable_transform()->mutable_rotation()->set_qz(rotation[2]);
  res.mutable_transform()->mutable_rotation()->set_qw(rotation[3]);

  return res;
}

int main() {
  AINFO << interpolateTf(
               get_transform({0, 0, 0}, {0, 0, 0, 1}, 0),
               get_transform({1, 1, 1},
                             {0, 0, -0.70710678118654746, 0.70710678118654746},
                             1),
               -1)
               .DebugString();

  return 0;
}