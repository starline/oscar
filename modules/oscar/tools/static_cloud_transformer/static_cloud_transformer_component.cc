///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

#include "cyber/component/component.h"
#include "cyber/cyber.h"
#include "modules/common_msgs/sensor_msgs/pointcloud.pb.h"
#include "modules/common_msgs/transform_msgs/transform.pb.h"
#include "modules/oscar/utils/transform_buffer/oscar_tf_buffer.h"

namespace apollo {
namespace localization {

constexpr static double kOffsetX = -5e5;
constexpr static double kOffsetY = -6.4e6;
constexpr static double kOffsetZ = 0;

constexpr static auto kFrameName = "local_obstacle_frame";  // or "world"

class VoxelComponent
    : public apollo::cyber::Component<apollo::drivers::PointCloud> {
 public:
  bool Init() override {
    tf_buffer_ = std::make_shared<apollo::oscar::OscarTFBuffer>();
    return true;
  }

  bool Proc(const std::shared_ptr<apollo::drivers::PointCloud>& msg) override {
    if (msg_ == nullptr) {
      msg_ = std::make_shared<apollo::drivers::PointCloud>();
      msg_->CopyFrom(*msg);
    }

    if (out_writer_ == nullptr) {
      out_writer_ = node_->CreateWriter<apollo::drivers::PointCloud>(
          readers_[0]->GetChannelName() + "/world_interp");
    }

    auto lidar_timestamp = msg->header().lidar_timestamp() / 1e9;

    auto world2loc_tf = Eigen::Affine3d::Identity();

    try {
      world2loc_tf =
          tf_buffer_->get("world", "lidar_ruby_left", lidar_timestamp);
    } catch (const tf2::TransformException& e) {
      AERROR << e.what();
    }

    for (auto& point : *msg->mutable_point()) {
      const auto transformed_point =
          world2loc_tf * Eigen::Vector3d{point.x(), point.y(), point.z()};
      point.set_x(transformed_point[0] + kOffsetX);
      point.set_y(transformed_point[1] + kOffsetY);
      point.set_z(transformed_point[2] + kOffsetZ);
    }

    msg->mutable_header()->set_frame_id(kFrameName);

    out_writer_->Write(msg);

    return true;
  }

 private:
  std::shared_ptr<apollo::cyber::Writer<apollo::drivers::PointCloud>>
      out_writer_;

  std::shared_ptr<apollo::oscar::OscarTFBuffer> tf_buffer_;

  // apollo::perception::onboard::TransformWrapper tf_provider_;
  Eigen::Affine3d last_tf_ = Eigen::Affine3d::Identity();

  std::shared_ptr<apollo::drivers::PointCloud> msg_ = nullptr;
};
CYBER_REGISTER_COMPONENT(VoxelComponent);

}  // namespace localization
}  // namespace apollo
