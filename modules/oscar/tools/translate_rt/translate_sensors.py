import json
import sys
import time
import os
import argparse
from typing import Iterable

import numpy as np
from tqdm import tqdm

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import cyber, cyber_time

from modules.common_msgs.sensor_msgs import sensor_image_pb2
from modules.common_msgs.sensor_msgs import pointcloud_pb2
from modules.common_msgs.localization_msgs import localization_pb2
from modules.common_msgs.transform_msgs import transform_pb2

channel_types_mapper = {
    # 'apollo.canbus.Chassis': chassis_pb2.Chassis,
    # 'apollo.control.ControlCommand': control_cmd_pb2.ControlCommand,
    # 'apollo.drivers.gnss.GnssBestPose': gnss_best_pose_pb2.GnssBestPose,
    # 'apollo.drivers.gnss.GnssStatus': gnss_status_pb2.GnssStatus,
    # 'apollo.drivers.gnss.InsStatus': gnss_status_pb2.InsStatus,
    # 'apollo.drivers.gnss.Imu': imu_pb2.Imu,
    'apollo.drivers.Image': sensor_image_pb2.Image,
    # 'apollo.drivers.CompressedImage': sensor_image_pb2.CompressedImage,
    'apollo.drivers.PointCloud': pointcloud_pb2.PointCloud,
    # 'apollo.dreamview.HMIStatus': hmi_status_pb2.HMIStatus,
    'apollo.localization.LocalizationEstimate': localization_pb2.LocalizationEstimate,
    # 'apollo.localization.LocalizationStatus': localization_pb2.LocalizationStatus,
    # 'apollo.localization.CorrectedImu': l_imu_pb2.CorrectedImu,
    # 'apollo.localization.Gps': gps_pb2.Gps,
    # 'apollo.planning.ADCTrajectory': planning_pb2.ADCTrajectory,
    # 'apollo.perception.PerceptionObstacles': perception_obstacle_pb2.PerceptionObstacles,
    # 'apollo.prediction.PredictionObstacles': prediction_obstacle_pb2.PredictionObstacles,
    'apollo.transform.TransformStampeds': transform_pb2.TransformStampeds
}


class ChannelTranslator:
    def __init__(self, node, channel, frame_id, write_channel, update_ts=False) -> None:
        self.update_ts = update_ts
        self.channel = channel

        self.write_channel = write_channel

        msg_type = cyber.ChannelUtils.get_msgtype(
            channel, sleep_s=1).decode('utf-8')
        if "lidar" in channel:
            msg_type = "apollo.drivers.PointCloud"
        elif "camera" in channel and not "compressed" in channel:
            msg_type = "apollo.drivers.Image"

        if msg_type == "":
            return

        ch_t = channel_types_mapper[msg_type]
        self.reader = node.create_reader(channel, ch_t, self.cb)
        self.frame_id = frame_id

        self.writer = node.create_writer(self.write_channel, ch_t)

    def cb(self, msg):

        if self.update_ts:
            msg.header.timestamp_sec = cyber_time.Time.now().to_sec()
            if msg.header.HasField("lidar_timestamp"):
                msg.header.lidar_timestamp = int(
                    msg.header.timestamp_sec * 1e9)
            if msg.header.HasField("camera_timestamp"):
                msg.header.camera_timestamp = int(
                    msg.header.timestamp_sec * 1e9)

            msg.measurement_time = cyber_time.Time.now().to_sec()

        msg.frame_id = self.frame_id
        msg.header.frame_id = self.frame_id

        st = time.time()

        self.writer.write(msg)
        dur = time.time() - st

        print(f'Current time: {time.time():.3f}, write msg: {dur:.3f}')


class TranslatorManager:

    def __init__(self):

        cyber.init()
        self.node = cyber.Node("translator")

        config = (
            # ("/apollo/sensor/camera/far/image", 'camera_far',
            #  "/apollo/sensor/camera/far/image/rt"),
            # ("/apollo/sensor/camera/near/image", 'front_12mm'),
            # ("/apollo/sensor/camera/near/image", 'camera_near',
            #  "/apollo/sensor/camera/near/image/rt"),
            # ("/apollo/sensor/camera/left/image", 'front_6mm'),
            # ("/apollo/sensor/camera/left/image", 'camera_left',
            #  "/apollo/sensor/camera/left/image/rt"),
            # ("/apollo/sensor/camera/front_6mm/image/image", 'camera_left',
            #  "/apollo/sensor/camera/front_6mm/image/image/rt"),
            # ("/apollo/sensor/lidar/fused/compensator/PointCloud2", 'velodyne128',
            #  "/apollo/sensor/lidar/fused/compensator/PointCloud2/rt"),
            ("/apollo/sensor/lidar/rs_ruby_left/PointCloud2", 'lidar_ruby_left',
             "/apollo/sensor/lidar/rs_ruby_left/PointCloud2/rt"),
            ("/apollo/sensor/lidar/rs_ruby_right/PointCloud2", 'lidar_ruby_right',
             "/apollo/sensor/lidar/rs_ruby_right/PointCloud2/rt"),
            # ("/apollo/sensor/lidar/fused/compensator/PointCloud2", 'lidar_ruby_left',
            #  "/apollo/sensor/lidar/fused/compensator/PointCloud2/rt"),
            # ("/apollo/sensor/lidar/rs_bpearl_left/PointCloud2", 'lidar_bpearl_left', "/apollo/sensor/lidar/fused/compensator/PointCloud2/rt"),
            # ("/apollo/sensor/lidar/rs_ruby_left/compensator/PointCloud2", 'lidar_ruby_left', "/apollo/sensor/lidar/fused/compensator/PointCloud2/rt"),
        )

        self.translators = []

        for channel, frame_id, wr_channel in config:
            self.translators.append(ChannelTranslator(
                self.node, channel, frame_id, wr_channel, update_ts=True))

        self.translators.append(TFTranslator(self.node))


if __name__ == "__main__":
    tr = TranslatorManager()
    tr.node.spin()
