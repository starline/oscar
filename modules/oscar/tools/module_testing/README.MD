# Module Testing

Набор утилит предназначен для симуляции работы модулей при различных настройках и сохранения результатов в формат MCAP на основе предзаписанных данных.

## Tester

Утилита предназначена для проигрывания .record записей, включения всех необходимых модулей и сохранения результатов при фиксированном конфиге и состоит из следующих компонентов:

1. **Data source provider** - читает запись и публикует в realtime сообщения с записи в каналы;
2. **Process manager** - запускает все необходимые модули в фоновом режиме;
3. **Result saver** - Сохраняет сообщения с каналов в MCAP файл.

Утилита запускает все компоненты, ожидает инициализации выходного модуля (указанного в конфиге в поле ```process_manager/output_node```), ожидает проигрывания всех сообщений с ```Data source provider``` и завершает все запущенные в фоне процессы.

```
./tester.py [-c CONFIG]
```

### Data source provider

Компонент подготавливает данные и публикует сообщения в каналы. Из-за медленной скорости чтения записей при первой загрузке записи создается сериализованный кэш, ускоряющий последующие воспроизведения этой записи с данными параметрами.

Конфиг:

```yaml
source_data_provider:
  source_folder: /apollo/data/bag/path/to/records
  start_time_offset: 0
  end_time_offset: 100
  channels: 
    - /apollo/localization/pose
    - /tf
    - /apollo/sensor/lidar/rs_ruby_left/PointCloud2
```

### Process Manager

Компонент запускает все необходимые модули в разных процессах, сохраняя логи в отдельных файлах.

Конфиг:

```yaml
processes_manager:
  output_node: LidarInnerOutputComponent
  processes:
    - 
      name: Transform
      command: mainboard -d /path/to/dag.dag
    -
      name: Perception
      command: mainboard -d /path/to/dag.dag
    -
      name: LidarMapPublisher
      command: python some/python/script.py
  log_folder: /apollo/data/module_testing/logs/processes
```

### Result saver

Компонент сохраняет сообщения с каналов в MCAP файл. Сохраняются все сообщения, публикуемые ```data source provider```, и также сообщения, указанные в конфиге ```result_saver/additional_save_channels```.

Конфиг:

```yaml
result_saver:
  folder: /apollo/data/module_testing/results
  filename: result_m11_perception.mcap
  additional_save_channels: 
    - /apollo/perception/obstacles
    - /tf_static
```

## Test manager

Утилита, осуществляющая обновление конфигов модулей, запускающая тестирование с каждым конфигом и объединяющая результаты тестов.

Алгоритм работы утилиты:

1. Для всех вариантов конфигов:
   1. обновляется конфиг файл тестируемого модуля;
   2. Запускается утилита tester для выбранного варианта конфига.
2. Полученные MCAP файлы объединяются в один для сравнения, временные метки сообщений сдвигаются к одному начальному значению;
3. в полученном mcap файле происходит преобразование сообщения облаков точек и изображений из формата apollo в формат foxglove.

MCAP файл может быть открыт и исследован утилитой foxglove studio: 

```
oscar foxglove studio
```

Для использования утилиты необходимо создать несколько конфигурационных файлов:

1. базовый конфиг для экспериментов, содержащий неизменяемые между запусками параметры и заглушки (placeholders) для изменяемых параметров, задаваемые через {NAME}. [Пример конфига тестера](configs/test_config.yaml)
2. базовый конфиг для тестируемого модуля (поддерживается только изменение конфига одного модуля) также с загрушками по параметрам, как в предыдущем пункте. [Пример конфига модуля](configs/base_lidar_detection_pipeline.pb.txt)
3. конфиг с вариациями настройки модулей. [Пример конфига](configs/perception_tests_config.yaml)

Запуск утилиты:

```
./test_manager.py [-c CONFIG]
```

## FAQ

 - Если во время публикации сообщений с записи число полученных выходных сообщений равно 0 и не увеличивается, посмотрите логи модуля, расположенные по пути, указанному в конфиге на tester: ```process_manager/log_folder```.  
