
import asyncio
import copy
import logging
import os
import subprocess

import signal
import time

from config import ProcessesManagerConfig


class Process:

    def __init__(self, name: str, command: str, log_folder: str):
        self.name = name
        self.command = command
        self.log_folder = log_folder

        self.log_file = f"{self.log_folder}/{self.name}_log.txt"

        self.logger = open(self.log_file, "w")

        env_vars = copy.deepcopy(os.environ)
        env_vars["GLOG_logtostderr"] = str(1)
        self.bg_process = subprocess.Popen(
            self.command,
            stdout=self.logger,
            stderr=self.logger,
            shell=True,
            env=env_vars)

    def __del__(self):
        self.logger.close()
        print("Sending SIGINT to", self.name)
        self.bg_process.send_signal(signal.SIGINT)

    def shutdown(self):
        self.bg_process.send_signal(signal.SIGINT)

    def wait(self):
        if not self.bg_process.poll():
            self.bg_process.wait()

    def is_alive(self):
        return self.bg_process.poll() is None

    def print_output(self):
        print("reading log", self.log_file)
        with open(self.log_file, 'r') as f:
            print(f.read())


class ProcessManager:

    def __init__(self, cfg: ProcessesManagerConfig, init_process_checker) -> None:

        self.logger = logging.getLogger("test")
        self.cfg = copy.deepcopy(cfg)
        self.log_folder = self.cfg.log_folder
        self.init_process_checker = init_process_checker

        os.makedirs(self.log_folder, exist_ok=True)

        self.processes = {}

        for p in self.cfg.processes:
            self.processes[p.name] = Process(
                p.name, p.command, log_folder=self.log_folder)

            time.sleep(0.1)
            if not self.processes[p.name].is_alive():
                self.processes[p.name].print_output()
                raise RuntimeError(
                    f'{p.name} is not created. Check log files. Exiting')

            self.logger.debug(f"Created [{p.name}]: {p.command}")

    def __del__(self):
        self.shutdown()
        self.logger.debug(f"{self.__class__.__name__} destroyed")

    def add_process(self, p):
        self.processes[p.name] = Process(
            p.name, p.command, log_folder=self.log_folder)

        time.sleep(0.1)
        if not self.processes[p.name].is_alive():
            self.processes[p.name].print_output()
            raise RuntimeError(f'{p.name} is dead. Exiting')

        self.logger.debug(f"Created [{p.name}]: {p.command}")

    def shutdown(self, process=None):
        if process is None:
            [p.shutdown() for p in self.processes.values()]
            self.logger.debug("waiting shutdown")
            self.wait()
            self.logger.debug("Background processes exited")
        else:
            self.processes[process].shutdown()
            self.wait(process)

    def wait(self, process=None):
        if process is None:
            [p.wait() for p in self.processes.values()]
        else:
            self.processes[process].wait()

    def spin(self):
        try:
            while all([p.is_alive() for p in self.processes]):
                time.sleep(0.2)

            for p in self.processes:
                if not p.is_alive():
                    p.print_output()
                    self.logger.error(p.name, " dead. Exiting...")
                    return

        except KeyboardInterrupt:
            pass
        finally:
            self.shutdown()

    def check_processes(self):
        for p in self.processes.values():
            if not p.is_alive():
                p.print_output()
                self.logger.error(f'{p.name} is dead')
                return False

        if not self.init_process_checker.is_initialized():
            self.logger.error(
                f"{self.init_process_checker.nodename} is dead")
            return False

        return True

    async def wait_initialization(self):
        print(
            f"Waiting process {self.init_process_checker.nodename} to initialize", end="")
        while not self.init_process_checker.is_initialized():
            await asyncio.sleep(0.5)
            print(".", end="", flush=True)
        print(" Done!")

    def terminate(self):
        for p in self.processes.values():
            p.terminate()
