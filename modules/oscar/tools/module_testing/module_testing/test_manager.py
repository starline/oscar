#!/apollo/modules/oscar/tools/module_testing/.venv/bin/python3

###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
from dataclasses import dataclass
import os
import subprocess
import time
from typing import Dict, List
from utils import read_config
from mcap_merger import McapMerger
from config import TestingConfig

@dataclass
class TestParam:
    name: str
    value: str


@dataclass
class TestCasesConfig:
    src_file: str
    dest_file: str
    configurations: Dict[str, List[TestParam]]

    default_module_config: str
    tmp_module_config: str
    result_mcap_file: str



class TestManager:
    def __init__(self, cfg: TestCasesConfig) -> None:

        self.cfg = cfg

        self.default_param_config_file = cfg.default_module_config
        self.result_channel = None

        with open(self.cfg.src_file, 'r') as f:
            self.src_config = f.read()

        with open(self.cfg.default_module_config, 'r') as f:
            self.test_config = f.read()

    def update_config(self):

        save_src_channels = "true"

        for test_case_name, params in self.cfg.configurations.items():

            cfg_f = self.src_config
            test_cfg_f = self.test_config

            for param in params:

                cfg_f = cfg_f.replace(f'${param.name}', param.value)

            with open(self.cfg.dest_file, 'w') as f:
                f.write(cfg_f)

            test_cfg_f = test_cfg_f.replace("{TEST_NAME}", test_case_name)
            test_cfg_f = test_cfg_f.replace(
                "{SAVE_SRC_CHANNELS}", save_src_channels)

            save_src_channels = "false"

            with open(self.cfg.tmp_module_config, 'w') as f:
                f.write(test_cfg_f)

            self.result_channel = read_config(self.cfg.tmp_module_config, TestingConfig).result_channel

            yield test_case_name, params

    def run_experiments(self):
        for test_case_name, params in self.update_config():
            print(f"Experiment: {test_case_name}")

            p = subprocess.Popen(f"./tester.py -c {self.cfg.tmp_module_config}", shell=True)
            code = p.wait()
            if code != 0:
                print("Error occured")
                return

            time.sleep(5)

            p.terminate()

        print("Tests complete")

    def convert_file(self):

        # os.system(
        #     f"oscar foxglove convert /apollo/data/bag/ssd_internal/nissan_all_lidars_all_cameras {self.src_mcap_file}")

        # for test_case_name, params in self.update_config():
        #     print(, f"Processing data: {test_case_name}", )

        tmp_file = "/tmp/mcap_temp.mcap"

        commands = [
            f"/apollo/bazel-bin/modules/oscar/foxglove/record_converter/convert_mcap_types {self.cfg.result_mcap_file} {tmp_file}",
            f"mv -f {tmp_file} {self.cfg.result_mcap_file}"
            ]
        for c in commands:
            print(c)
            os.system(c)

    def merge_files(self):

        src_files = [
            f"/apollo/data/module_testing/results/{test_case_name}_result.mcap" for test_case_name in self.cfg.configurations.keys()]

        suffixes = list(self.cfg.configurations.keys())

        m = McapMerger(
            src_files,
            suffixes,
            self.cfg.result_mcap_file,
            self.result_channel
        )

        m.run()

        print("Result merged file:", self.cfg.result_mcap_file)

    def run_tests(self):

        stages = [
            self.run_experiments,
            self.merge_files,
            self.convert_file,
        ]

        n_stages = len(stages)

        for idx, func in enumerate(stages):

            print(f'{"*"*20} Stage {idx+1}/{n_stages}: {func.__name__:^20s} {"*"*20}')
            func()


if __name__ == "__main__":


    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--config', type=str, help='path to config file', 
                        default="/apollo/modules/oscar/tools/module_testing/configs/perception_tests_config.yaml"
                        )

    args = parser.parse_args()

    test_manager = TestManager(read_config(args.config, TestCasesConfig))

    test_manager.run_tests()
