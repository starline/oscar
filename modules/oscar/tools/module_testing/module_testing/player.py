

import time

from tqdm import tqdm
import logging


class DataPlayer:

    def __init__(self,
                 data_provider,
                 process_manager,
                 result_saver,
                 main_src_channel,
                 result_channel,
                 save_src_channels) -> None:

        self.data_provider = data_provider
        self.process_manager = process_manager
        self.result_saver = result_saver

        self.save_src_channels = save_src_channels

        self.main_src_channel = main_src_channel
        self.result_channel = result_channel
        self.duration = data_provider.get_duration()

        self.logger = logging.getLogger("test")

    def __del__(self):
        self.logger.debug(f"{self.__class__.__name__} destroyed")

    def run(self):

        prev_msg_ts = 0
        sleep_t = 0
        main_msg_counter = 0
        prev_publish_ts = time.time()

        pbar = tqdm(total=self.duration,
                    bar_format="{l_bar}{bar}{n:.2f}/{total:.2f} sec [{elapsed}<{remaining}, {rate_fmt}{postfix}]")

        for channelname, msg, datatype, timestamp in self.data_provider.get_message_pool():

            if not self.process_manager.check_processes():
                return False

            cur_msg_ts = timestamp / 1e9
            msg_ts_offset = cur_msg_ts - prev_msg_ts if prev_msg_ts > 0 else 0

            publish_ts_offset = time.time() - prev_publish_ts

            prev_msg_ts = cur_msg_ts
            prev_publish_ts = time.time()

            sleep_t += msg_ts_offset - publish_ts_offset
            time.sleep(max(0, sleep_t))

            if channelname == self.main_src_channel:
                main_msg_counter += 1

            self.data_provider.publish_msg(channelname, msg)

            if self.save_src_channels:
                self.result_saver.callback(
                    msg, channelname)


            pbar.set_description(
                f'Main msgs: {main_msg_counter}, output msgs: {self.result_saver.collected_msgs[self.result_channel]}')
            # pbar.set_description(
            #     f'Main msgs: {main_msg_counter}')
            pbar.update(min(msg_ts_offset, pbar.total - pbar.n))

        pbar.close()

        return True
