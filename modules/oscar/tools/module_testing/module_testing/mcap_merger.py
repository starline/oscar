#!/apollo/modules/oscar/tools/module_testing/.venv/bin/python3

from typing import List
import argparse

from tqdm import tqdm

from mcap_protobuf.writer import Writer
from mcap_protobuf.decoder import DecoderFactory

from mcap.reader import make_reader


def update_ts(proto_msg, ts_offset):
    if proto_msg.HasField("header") and proto_msg.header.HasField("timestamp_sec"):
        proto_msg.header.timestamp_sec -= ts_offset / 1e9


class McapMerger:
    def __init__(self, src_files: List[str], suffixes: List[str], dest_file: str, merge_channel: str) -> None:

        self.suffixes = suffixes
        self.src_files = src_files
        self.dest_file = dest_file
        self.merge_channel = merge_channel

        self.writer = Writer(dest_file)

        assert len(src_files) == len(suffixes)
        assert len(src_files) >= 2

    def run(self):

        with open(self.src_files[0], "rb") as f:
            reader = make_reader(f, decoder_factories=[DecoderFactory()])

            main_file_start_time = reader.get_summary().statistics.message_start_time
            total_messages = reader.get_summary().statistics.message_count

            main_file_channels = [
                ch.topic for ch in reader.get_summary().channels.values()]

            for schema, channel, message, proto_msg in tqdm(reader.iter_decoded_messages(), total=total_messages):
                topic = channel.topic

                if topic == self.merge_channel:
                    topic += "/" + self.suffixes[0]

                self.writer.write_message(
                    topic=topic,
                    message=proto_msg,
                    log_time=message.log_time,
                    publish_time=message.publish_time,
                )

        for f, suffix in zip(self.src_files[1:], self.suffixes[1:]):
            with open(f, "rb") as f:
                reader = make_reader(f, decoder_factories=[DecoderFactory()])

                ts_offset = reader.get_summary().statistics.message_start_time - \
                    main_file_start_time
                total_messages = reader.get_summary().statistics.message_count

                for schema, channel, message, proto_msg in tqdm(reader.iter_decoded_messages(), total=total_messages):

                    if channel.topic in main_file_channels and channel.topic != self.merge_channel:
                        continue

                    topic = channel.topic
                    if channel.topic == self.merge_channel:
                        topic += "/" + suffix


                    self.writer.write_message(
                        topic=topic,
                        message=proto_msg,
                        log_time=message.log_time - ts_offset,
                        publish_time=message.publish_time - ts_offset,
                    )

    def __del__(self):
        self.writer.finish()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-f', '--files', type=str, help='paths to src .mcap files. The first one if considered main',
                        nargs="+")
    parser.add_argument('-p', '--prefixes', type=str,
                        help='prefixes to add to merge channel', nargs='+')
    parser.add_argument('-o', '--output', type=str, help='Output filename')
    parser.add_argument('-c', '--channel', type=str, help='channel to merge')

    args = parser.parse_args()

    # m = McapMerger(
    #     [
    #         "/apollo/data/module_testing/results/CENTER_POINT_DETECTION_2_rubies_converted.mcap",
    #         "/apollo/data/module_testing/results/CENTER_POINT_DETECTION_conveted.mcap",
    #     ],
    #     [
    #         "BOTH_RUBIES",
    #         "LEFT_RUBY",
    #     ],
    #     "/apollo/data/module_testing/results/CENTER_POINT_DETECTION_rubies_merged.mcap",
    #     "/apollo/perception/obstacles"
    # )

    m = McapMerger(
        [
            "/apollo/data/module_testing/results/converted.mcap",
            "/apollo/data/module_testing/results/lidar_compensator_converted.mcap",
        ],
        [
            "base",
            "compensated",
        ],
        "/apollo/data/module_testing/results/compensator_merged.mcap",
        "/apollo/perception/obstacles"
    )

    m.run()
