from multiprocessing.pool import ThreadPool
import sys
import time
import os

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from tqdm import tqdm

from cyber.python.cyber_py3 import record


class Recoverer:
    def __init__(self, node, src_folder, channels, start_time_offset=0, end_time_offset=1e12) -> None:

        # self.writer = self.node.create_writer("/apolssslocaliza", localization_pb2.LocalizationEstimate)
        # print("Writer created", "/apolssslocaliza")

        self.src_folder = src_folder
        self.channels = channels

        self.start_time_offset = start_time_offset
        self.end_time_offset = end_time_offset

        self.src_files = []
        for (root, dirs, fs) in os.walk(self.src_folder):
            for f in fs:
                if "record" in f:
                    self.src_files.append(f'{root}/{f}')
        self.src_files = sorted(self.src_files)

        self.start_time = self.get_first_ts()
        self.end_time = None

        self.start_read_time = self.start_time + self.start_time_offset
        self.end_read_time = self.start_time + self.end_time_offset

        self.writers = {}

        # self.node = cyber.Node("DataProvider")
        for ch in self.channels:
            # self.writers[ch] = self.node.create_writer(ch, self.get_channel_type(ch))
            self.writers[ch] = node.create_writer(
                ch, self.get_channel_type(ch))

            print("writer created:", ch)
            # break

        # self.message_queue = Queue(maxsize=30000)
        # self.thread = Thread(target=self.store_messages_in_queue,
        #                      args=(self.message_queue,), daemon=True)

        # self.thread.start()

        self.pool = ThreadPool(processes=10)
        self.chunks = [self.pool.apply_async(self.read_record, (f,))
                       for f in self.src_files]

        self.loaded_data = []
        pbar = tqdm(self.chunks, desc="Preloading: ")
        for chunk in pbar:
            self.loaded_data += chunk.get()

    def get_first_ts(self, offset=False):
        reader = record.RecordReader(self.src_files[0])

        return next(reader.read_messages())[-1] / 1e9 if not offset else next(reader.read_messages())[-1] / 1e9 + self.start_time_offset

    def read_message(self):

        for f in self.src_files:
            freader = record.RecordReader(f)

            for data in freader.read_messages(int(self.start_read_time * 1e9), int(self.end_read_time * 1e9)):
                if len(self.channels) == 0 or data[0] in self.channels:
                    yield data

    def store_messages_in_queue(self, q):
        for data in self.read_message():
            q.put(data)

    def get_message_count(self):

        sum_n = 0

        for f in self.src_files:
            reader = record.RecordReader(f)
            sum_n += sum([reader.get_messagenumber(ch)
                         for ch in reader.get_channellist() if ch in self.channels])

        return sum_n

    def get_channel_type(self, ch):

        reader = record.RecordReader(self.src_files[0])
        # return channel_types_mapper[reader.get_messagetype(ch)]
        return find_message_type_by_str(reader.get_messagetype(ch))

    def get_msg(self):

        for channelname, msg, datatype, timestamp in self.read_message():
            self.end_time = timestamp / 1e9

            yield channelname, msg, datatype, timestamp

    def get_message_thread(self):

        while self.thread.is_alive() or self.message_queue.qsize() > 0:
            if self.message_queue.qsize() == 0:
                time.sleep(0.001)
                continue

            yield self.message_queue.get()

        self.thread.join()

    def publish_msg(self, channelname, msg):

        self.writers[channelname].write(RawMessageProxy(msg))

    def get_duration(self):
        return self.end_time - self.start_time - self.start_time_offset if self.end_time is not None else 0

    def read_record(self, f):

        # print("enter thread")

        result = []
        freader = record.RecordReader(f)

        for data in freader.read_messages(int(self.start_read_time * 1e9), int(self.end_read_time * 1e9)):
            if len(self.channels) == 0 or data[0] in self.channels:
                result.append(data)

        # print("exiting thread")
        return result

    def get_message_pool(self):

        for data in self.loaded_data:
            yield data
