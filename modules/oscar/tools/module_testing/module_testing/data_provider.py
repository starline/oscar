import copy
import logging
import sys

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import cyber
import os
from tqdm import tqdm

from cyber.python.cyber_py3 import record
from utils import find_message_type_by_str, save_cache, load_cache

from config import SourceDataProviderConfig


class RawMessageProxy:
    def __init__(self, msg) -> None:
        self.msg = msg

    def SerializeToString(self):
        return self.msg


def read_record(f, start_time, end_time, channels):

    result = []
    freader = record.RecordReader(f)

    for data in freader.read_messages(start_time, end_time):
        if len(channels) == 0 or data[0] in channels:
            result.append(data)
    return result


class DataProvider:
    def __init__(self, node: cyber.Node, cfg: SourceDataProviderConfig):

        self.logger = logging.getLogger("test")
        self.cfg = copy.deepcopy(cfg)
        self.source_folder = self.cfg.source_folder
        self.channels = self.cfg.channels

        self.start_time_offset = self.cfg.start_time_offset
        self.end_time_offset = self.cfg.end_time_offset

        assert os.path.exists(
            self.source_folder), f"Source folder not found: {self.source_folder}"

        self.src_files = []
        for (root, dirs, fs) in os.walk(self.source_folder):
            for f in fs:
                self.src_files.append(f'{root}/{f}')

        assert len(
            self.src_files) > 0, f"No files found under folder: {self.source_folder}"

        self.src_files = sorted(self.src_files)

        self.start_time = self.get_first_ts()
        self.end_time = None

        self.start_read_time = self.start_time + self.start_time_offset
        self.end_read_time = self.start_time + self.end_time_offset

        self.writers = {}

        # self.node = cyber.Node("DataProvider")
        for ch in self.channels:
            # self.writers[ch] = self.node.create_writer(ch, self.get_channel_type(ch))
            self.writers[ch] = node.create_writer(
                ch, self.get_channel_type(ch))

            self.logger.debug("writer created: " + ch)

        start_ts = int(self.start_read_time * 1e9)
        end_ts = int(self.end_read_time * 1e9)

        self.loaded_data = []
        self.cache_loaded = False

        cache_folder = "cache"
        cache_file = f"{cache_folder}/cache.pickle"
        cache_meta_file = f"{cache_folder}/cache_meta.pickle"

        self.logger.debug(f"Trying to load cache data from {cache_file}")

        self.loaded_data = load_cache(
            cache_file, cache_meta_file, self.cfg)

        if not self.loaded_data:
            self.loaded_data = []

            pbar = tqdm(self.src_files, desc="Preloading: ")
            for f in pbar:
                chunk = read_record(f, start_ts, end_ts, self.channels)
                self.loaded_data += chunk

            self.logger.debug("Saving cache data")
            save_cache(cache_file, cache_meta_file, self.cfg, self.loaded_data)
        else:
            self.logger.debug("Cache data load successful")

    def get_first_ts(self, offset=False):
        reader = record.RecordReader(self.src_files[0])

        ts = next(reader.read_messages())[-1] / 1e9
        off = self.start_time_offset if offset else 0
        return ts + off

    def read_message(self):

        for f in self.src_files:
            freader = record.RecordReader(f)

            for data in freader.read_messages(int(self.start_read_time * 1e9), int(self.end_read_time * 1e9)):
                if len(self.channels) == 0 or data[0] in self.channels:
                    yield data

    def store_messages_in_queue(self, q):
        for data in self.read_message():
            q.put(data)

    def get_message_count(self):

        sum_n = 0

        for f in self.src_files:
            reader = record.RecordReader(f)
            sum_n += sum([reader.get_messagenumber(ch)
                         for ch in reader.get_channellist() if ch in self.channels])

        return sum_n

    def get_channel_type(self, ch):

        reader = record.RecordReader(self.src_files[0])

        typename = reader.get_messagetype(ch)
        assert typename != "", f"type not found for channel {ch}"
        t = find_message_type_by_str(typename)
        assert t is not None, f'type class not found by name: {typename}'
        return t

    def get_msg(self):

        for channelname, msg, datatype, timestamp in self.read_message():
            self.end_time = timestamp / 1e9

            yield channelname, msg, datatype, timestamp

    def publish_msg(self, channelname, msg):

        self.writers[channelname].write(RawMessageProxy(msg))

    def get_duration(self):
        return self.end_time_offset - self.start_time_offset


    def get_message_pool(self):

        for data in self.loaded_data:
            yield data

    def __del__(self):
        self.logger.debug(f"{self.__class__.__name__} destroyed")
