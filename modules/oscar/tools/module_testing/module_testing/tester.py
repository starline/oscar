#!/apollo/modules/oscar/tools/module_testing/.venv/bin/python3

###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import asyncio
import copy
import logging
import signal
from typing import Union
import sys
import time
import argparse

from process_manager import ProcessManager
from data_provider import DataProvider
from result_saver import ResultSaver
from player import DataPlayer

from utils import read_config, ProcessChecker, init_cyber

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import cyber

import warnings

from config import TestingConfig


warnings.filterwarnings('ignore')


class ModuleTester:
    def __init__(self, cfg: Union[TestingConfig, str]) -> None:

        init_cyber()
        self.node = cyber.Node("ModuleTester")

        if isinstance(cfg, str):
            cfg = read_config(cfg, TestingConfig)

        self.cfg = copy.deepcopy(cfg)

        logging.basicConfig(level=logging.DEBUG)

        self.logger = logging.getLogger("test")
        self.logger.setLevel("DEBUG")

        self.process_manager = ProcessManager(self.cfg.processes_manager,
                                              ProcessChecker(
                                                  self.cfg.result_channel, self.cfg.processes_manager.output_node)
                                              )

        self.data_provider = DataProvider(
            self.node,
            self.cfg.source_data_provider)

        self.result_saver = ResultSaver(
            self.node,
            self.cfg.result_saver,
            first_record_ts=int(self.data_provider.get_first_ts() * 1e9)
        )

        self.data_player = DataPlayer(
            self.data_provider,
            self.process_manager,
            self.result_saver,
            self.cfg.main_src_channel,
            self.cfg.result_channel,
            self.cfg.save_src_channels
        )

        self.logger.debug("Waiting initialization")

        async def wait_all(self):
            await asyncio.gather(
                self.process_manager.wait_initialization(),
                self.result_saver.init_readers_async()
            )

        asyncio.run(wait_all(self))

        self.logger.debug("Initialization complete")

    def __del__(self):
        self.logger.debug(f"{self.__class__.__name__} destroyed")

    def run(self):
        if not self.data_player.run():
            sys.exit(1)

    def shutdown(self):
        time.sleep(5)
        self.logger.debug("Finishing writing")
        self.result_saver.writer.finish()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--config', type=str, help='paths to config file', 
                        default="/apollo/modules/oscar/tools/module_testing/configs/tester_config.yaml"
                        )

    cfg = parser.parse_args()


    tester = ModuleTester(cfg.config)
    tester.run()

    tester.shutdown()

    class TimeoutException(Exception):
        def __init__(self, *args: object) -> None:
            super().__init__(*args)

    def handler(a, b):
        raise TimeoutException("timeout")

    signal.signal(signal.SIGALRM, handler)
    signal.alarm(3)
