# Checking

- Checking
  - [vis_calibration_table.py](#vis_calibration_tablepy)
  - [check_gnss.py](#check_gnsspy)
  - [check_estop.py](#check_estoppy)
  - [vis_control.py](#vis_control_errorspy)
  - [vis_ts_diff.py](#vis_ts_diffpy)
  - [vis_lidar_ts_diff.py](#vis_lidar_ts_diffpy)
  - [vis_channel_data.py](#vis_channel_datapy)

Директория содержит скрипты для проверки и визуализации данных, записанных в .record файлы.

Запуск всех скриптов осуществляется следующим образом:

```
python any_script.py [CLI args]
oscar records checking any_script.py [CLI args]
```


## vis_calibration_table.py

Скрипт читает record файлы, извлекает из каналов **/apollo/localization/pose** и **/apollo/canbus/chassis** скорости, ускорения и процентное нажатие на педаль газа (положительное) или тормоза (отрицательное) и проецирует на 3D пространство. Также визуализирует записанную в конфиг файл калибровочную таблицу

Параметры:
 - ```-p``` - исходная папка с .record файлами ИЛИ .pb.txt конфиг на модуль Control.

Пример визуализации таблицы:
![](docs/imgs/calibration_table.png)

## check_gnss.py

Скрипт проверяет статусы GNSS с приемника и выводит в консоль сообщения, если были промежутки с неточным решением.

Статус максимально точного решения - **NARROW_INT**. При любом другом статусе выведется сообщение о времени начала этого статуса, время окончания и длительность. Если статус поменялся на другой неточный, то это будет отражено в терминале с указанием времени смены статуса.


### Пример вывода

```
python scripts/oscar/cyber_scripts/checking/check_gnss.py 
WARNING: Logging before InitGoogleLogging() is written to STDERR
I0805 13:00:10.281186  8788 py_record.cc:574] []init _cyber_record_wrapper
Checking folder: data/bag/2022-07-29-11-44-31

PSRDIFF  -  No corrections from server received
Start:  PSRDIFF 2012-07-24 12:44:50
End:  2012-07-24 12:44:54.100000
Duration: 4.10 sec
Checking complete

```

## check_estop.py
Скрипт смотрит, когда модуль Control послал команду emergency stop и вдавил педаль тормоза. Выводит время, когда послана команда, и статус.

Параметры:
 - ```-p``` - путь до папки с файлами;
 - ```-v``` - допустимые значения brake, соответствующие переходу в Emergency stop, можно указать несколько значений (если в процессе записи вносились изменения в это значение).

### Пример вывода

```
python estop_checker.py -p /apollo/data/bag/2022-08-09-11-54-16/
WARNING: Logging before InitGoogleLogging() is written to STDERR
I0810 13:22:57.461246  1539 py_record.cc:574] []init _cyber_record_wrapper
Checking folder: data/bag/2022-08-09-11-54-16/

Emergency stop at 2022-08-09 11:54:16.899647, brake value: 20.0
error_code: OK
msg: "estop for empty planning trajectory, planning headers: timestamp_sec: 1660035230.5629478 module_name: \"planning\" sequence_num: 1"


Emergency stop at 2022-08-09 11:55:01.897856, brake value: 20.0
error_code: CONTROL_COMPUTE_ERROR
msg: "planning has no trajectory point. planning_seq_num:710"

Record started 2022-08-09 13:14:34.591498
Record ended   2022-08-09 14:00:31.480698
```


## vis_control.py

Читает топик /control с .record файла и выводит на графиках изменения требуемых полей для каждого участка, когда был включен автопилот.

Параметры :
 - ```-p``` - путь до папки с файлами;
 - ```-f``` - путь до файла с названием полей для визуализации и группировкой;

### Пример графиков

В левом верхнем углу отображается траектория движения. Также для каждого графика указывается макисмальное по модулю значение.

![](docs/imgs/control_plots.png)


## vis_ts_diff.py

Скрипт выводит информацию о частоте вывода сообщений в каналы за записанный промежуток времени.

Параметры :
 - ```-p``` - путь до папки с файлами;
 - ```-с``` - префикс в названии каналов, для которых проводится визуализация.

### Пример графиков

![](docs/imgs/vis_freq.png)

## vis_lidar_ts_diff.py

Скрипт визуализирует задержку между снятием кадра с лидара и генерацией Cyber сообщения. Автоматически будут взяты только каналы с префиксом **/apollo/sensor/lidar**.

Параметры :
 - ```-p``` - путь до папки с файлами.

### Пример графика

![](docs/imgs/lidar_freq.png)

## vis_channel_data.py

Скрипт выводит на графиках данные с каналов в .record файле.

Параметры:
 - ```-p``` - путь до папки с файлами;
 - ```-c``` - путь до конфиг файла.


На данный момент поддерживаются следующие виды графиков:
 - маршрут (plot_coordinates);
 - Вывод значения полей за все время записи (plot_vals);
 - Вывод гистограмм значений полей (plot_hists);
 - Вывод разницы между соседними значениями полей (plot_differences). 

Для просмотра всех параметров и типов графиков обратитесь к функциям класса  [ChannelPlotter](checking/Plotter.py).

### Пример графика

![](docs/imgs/localization.png)

