#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import unittest
from unittest import mock

import yaml
import logging
import os

import matplotlib
from matplotlib import pyplot as plt

from checking.plotter import ChannelPlotter
from checking.vis_ts_diff import main as ts_diff_main
from checking.vis_calibration_table import read_config, read_records
from checking.vis_lidar_ts_diff import main as lidar_ts_diff_main
from checking.vis_control import main as vis_control

matplotlib.use('agg')

class TestVisualization(unittest.TestCase):

    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)
        script_dir = os.path.dirname(os.path.realpath(__file__))
        test_data_folder = f'../../test_data'
        self.src_folder = f'{script_dir}/{test_data_folder}/loc_control_records'
        self.cfg_fname = f'{script_dir}/{test_data_folder}/cfg_vis_channel_data_test.yaml'
        self.lidars_src_folder = f'{script_dir}/{test_data_folder}/aligned_lidars_records'
        self.control_vis_cfg = f'{script_dir}/{test_data_folder}/vis_fields_control_test.yaml'

        for i in [self.src_folder, self.cfg_fname, self.lidars_src_folder, self.control_vis_cfg]:
            assert os.path.exists(i), f'{i} is missing. Maybe test data is not downloaded'

    @mock.patch(f"checking.vis_ts_diff.plt.show")
    def testVisTsDiff(self, mock_plt):

        channels = [
            '/apollo/localization/pose',
            '/apollo/sensor/gnss/best_pose',
        ]
        ts_diff_main(self.src_folder, channels)

    @mock.patch(f"checking.vis_calibration_table.plt.show")
    def testVisCalibrationTableFromConfig(self, mock_plt):

        cfg_file = '/apollo/modules/calibration/data/Lincoln2017MKZ_LGSVL/control_conf.pb.txt'
        assert os.path.exists(cfg_file), f'{cfg_file} is missing. '

        read_config(cfg_file)

    @mock.patch(f"checking.vis_calibration_table.plt.show")
    def testVisCalibrationTableFromRecords(self, mock_plt):

        read_records(self.src_folder)

    @mock.patch(f"checking.vis_lidar_ts_diff.plt.show")
    def testVisLidarTimeDiff(self, mock_plt):

        lidar_ts_diff_main(self.lidars_src_folder)

    @mock.patch(f"checking.vis_control.plt.show")
    def testVisControl(self, mock_plt):

        with open(self.control_vis_cfg, 'r') as f:
            params = yaml.safe_load(f)

        vis_control(self.src_folder, params['required_fields'])


if __name__ == '__main__':  # pragma: no cover
    unittest.main()
