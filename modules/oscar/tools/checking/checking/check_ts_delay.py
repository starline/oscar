#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################
import argparse
import os

from tqdm import tqdm

from checking.utils import channel_types, get_total_messages

import sys

sys.path.append("/apollo")
from cyber.python.cyber_py3 import record


ESTOP_BRAKE_VALS = [20, 30, 50]


def main(src_folder):

    print(f'Checking folder: {src_folder}')

    previous_tss = {}

    skip_channels = [
        "/tf",
        "/tf_static"
    ]

    for f in tqdm(sorted(os.listdir(src_folder))):
        rfile = src_folder + '/' + f

        freader = record.RecordReader(rfile)

        for channelname, msg, datatype, timestamp in freader.read_messages():

            if datatype not in channel_types:
                continue

            if channelname in skip_channels:
                continue

            msg_struct = channel_types[datatype].FromString(msg)

            if msg_struct.HasField("header"):
                field = msg_struct.header.timestamp_sec
            else:
                field = msg_struct.transforms[0].header.timestamp_sec

            if channelname in previous_tss:
                diff_prev = field - previous_tss[channelname]

                if diff_prev < 0:
                    print("Jump bag in time", channelname,
                          field, diff_prev)
            previous_tss[channelname] = field

            # if previous_ts > field:
            diff = timestamp / 1e9 - field

            DIFF_THRESHOLD = 1e-1

            if diff > DIFF_THRESHOLD:
                print(f"{channelname}, {diff:.4f}, {field:.4f}")


if __name__ == '__main__':  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str, help='a path to folder containing .record files',
                        default='/apollo/data/bag/2022-08-11-13-16-59')
    args = parser.parse_args()

    try:
        main(args.path)
    except KeyboardInterrupt:
        exit()
