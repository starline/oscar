#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
import sys
import os

from google.protobuf import text_format as pbtf
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
from tqdm import tqdm
import open3d as o3d

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import record
from modules.common_msgs.localization_msgs import localization_pb2
from modules.control.proto import control_conf_pb2

from modules.common_msgs.chassis_msgs import chassis_pb2


matplotlib.use("tkagg")
plt.rcParams['axes.grid'] = True


def downsample(points):

    ps = np.array(points)
    pc = o3d.geometry.PointCloud()
    pc.points = o3d.utility.Vector3dVector(ps.T)
    pc = pc.voxel_down_sample(0.2)
    return np.asarray(pc.points)


def read_config(src_file):

    cfg = control_conf_pb2.ControlConf()

    with open(src_file, 'rb') as f:
        pbtf.Parse(f.read(), cfg)

    calibration_table = cfg.lon_controller_conf.calibration_table.calibration

    print(len(calibration_table))
    data = np.stack([(d.speed, d.acceleration, d.command)
                     for d in calibration_table])

    speed, accel, command = data.T

    print(data.shape)

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    ax.scatter(speed, accel, command)
    ax.set_xlabel('speed')
    ax.set_ylabel('accel')
    ax.set_zlabel('command')
    plt.show()


def get_vrf_val(val, heading):
    norm = np.linalg.norm([val.x, val.y, val.z])
    # if norm < 0.1:
    #     return
    vec_angle = np.arctan2(val.y, val.x)
    return -norm if abs(vec_angle - heading) > np.pi else norm


def getMaxDiff(A):

    diff = -sys.maxsize

    n = len(A)
    if n == 0:
        return diff

    for i in range(n - 1):
        for j in range(i + 1, n):
            if A[j] > A[i]:
                diff = max(diff, A[j] - A[i])

    return diff


class MessageSynchronizer:
    def __init__(self, channels, ts_thresh=0.1) -> None:
        self.thresh = ts_thresh
        self.channels = channels

        self.last_channels_ts = {key: 0 for key in channels}
        self.last_channels_msg = {key: None for key in channels}

        self.synchronized_messages = []

    def update(self, message, channel, ts):
        if channel not in self.channels:
            return

        if self.last_channels_ts[channel] == 0:
            self.last_channels_ts[channel] = ts
            self.last_channels_msg[channel] = message
        else:
            return

        if not any([ts == 0 for ts in self.last_channels_ts.values()]):
            if getMaxDiff(list(self.last_channels_ts.values())) < self.thresh:
                self.synchronized_messages.append(
                    list(self.last_channels_msg.values()))

            self.last_channels_ts = {key: 0 for key in self.channels}


def read_records(src_folder):

    files = sorted([f'{root}/{f}' for (root, _, files)
                    in os.walk(src_folder) for f in files])

    speed_list = []
    accel_list = []
    command_list = []

    is_automode = []

    ms = MessageSynchronizer(
        ['/apollo/localization/pose', '/apollo/canbus/chassis'])

    for idx, f in enumerate(tqdm(files)):

        rfile = f
        freader = record.RecordReader(rfile)

        for channelname, msg, datatype, timestamp in freader.read_messages():
            ms.update(msg, channelname, timestamp / 10**9)

    for loc_msg, chassis_msg in tqdm(ms.synchronized_messages):
        loc_msg_struct = localization_pb2.LocalizationEstimate()
        loc_msg_struct.ParseFromString(loc_msg)

        # print(loc_msg_struct)

        chassis_msg_struct = chassis_pb2.Chassis()
        chassis_msg_struct.ParseFromString(chassis_msg)

        command = chassis_msg_struct.throttle_percentage if chassis_msg_struct.throttle_percentage != 0 else - \
            chassis_msg_struct.brake_percentage

        # print(chassis_msg_struct.driving_mode)
        mode = chassis_pb2._CHASSIS_DRIVINGMODE.values_by_number[
            chassis_msg_struct.driving_mode].name

        if command in [-30.0, -20.0]:
            continue
        is_automode.append(mode == 'COMPLETE_AUTO_DRIVE')
        # print(loc_msg_struct.header.timestamp_sec - chassis_msg_struct.header.timestamp_sec)

        # speed_list.append(get_vrf_val(loc_msg_struct.pose.linear_velocity, loc_msg_struct.pose.heading))
        speed_list.append(chassis_msg_struct.speed_mps)
        # speed_list.append(loc_msg_struct.pose.linear_a)
        # accel_list.append(get_vrf_val(loc_msg_struct.pose.linear_acceleration, loc_msg_struct.pose.heading))
        # print(chassis_msg_struct.speed_mps, loc_msg_struct.pose.linear_acceleration_vrf.y)
        accel_list.append(loc_msg_struct.pose.linear_acceleration_vrf.y)

        command_list.append(command)

    speed_list = np.array(speed_list)
    accel_list = np.array(accel_list)
    command_list = np.array(command_list)

    ad_speed_list = speed_list[is_automode]
    ad_accel_list = accel_list[is_automode]
    ad_command_list = command_list[is_automode]

    # print(np.max(ad_accel_list))

    ad_points = downsample([ad_speed_list, ad_accel_list, ad_command_list])

    manual_mode = [not elem for elem in is_automode]
    m_speed_list = speed_list[manual_mode]
    m_accel_list = accel_list[manual_mode]
    m_command_list = command_list[manual_mode]

    m_points = downsample([m_speed_list, m_accel_list, m_command_list])

    fig = plt.figure(figsize=(19.2, 10.8))
    ax = fig.add_subplot(projection='3d')
    ax.scatter(ad_points[:, 2], ad_points[:, 0], ad_points[:, 1])
    ax.scatter(m_points[:, 2], m_points[:, 0], m_points[:, 1])
    ax.set_xlabel('Cmd')
    ax.set_ylabel('speed')
    ax.set_zlabel('Accel')
    ax.legend(["auto", "manual"])

    # fig, axs = plt.subplots(nrows=3, ncols=1)
    # axs[0].plot(np.arange(len(speed_list)), speed_list)
    # axs[1].plot(np.arange(len(accel_list)), accel_list)
    # axs[2].plot(np.arange(len(command_list)), command_list)
    # axs[0].plot(np.arange(len(m_speed_list)), m_speed_list, ad_speed_list)
    # axs[0].plot(np.arange(len(m_speed_list)), ad_speed_list)

    plt.show()


if __name__ == '__main__':  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str, help='a path to control config .pb.txt file or folder containing .record files',
                        default='/apollo/modules/calibration/data/mb_actros/control_conf/control_conf.pb.txt')

    args = parser.parse_args()
    print(args)

    try:
        if args.path.endswith('pb.txt'):
            read_config(args.path)
        else:
            read_records(args.path)
        # for f in os.listdir('/apollo/data/bag/calib_table_data'):
        #     read_records(f'/apollo/data/bag/calib_table_data/{f}')
        # read_records('/apollo/data/bag/calib_table_data/2022-08-11-14-15-53')
    except KeyboardInterrupt:
        exit()
