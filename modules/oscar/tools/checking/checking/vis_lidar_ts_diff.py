#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import json
import sys
import time
import os
import argparse
from typing import Iterable

from tqdm import tqdm
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import matplotlib.dates as mdates

matplotlib.use("tkagg")
plt.rcParams['axes.grid'] = True

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")


from cyber.python.cyber_py3 import record
from modules.common_msgs.sensor_msgs import pointcloud_pb2

from checking.utils import get_total_messages
from checking.plotter import vfrom_ts


class Checker:

    def __init__(self, channels, cache=True) -> None:

        self.cache = cache
        script_dir = os.path.dirname(os.path.realpath(__file__))
        self.cache_folder = f'{script_dir}/lidar_freq_cache'

        self.channels = channels
        self.data = {}

    def save_cache(self, src_file):
        save_dir = f'{self.cache_folder}/{src_file.split("/")[-2]}'
        os.makedirs(save_dir, exist_ok=True)

        # print(src_file, save_dir)
        # exit()
        with open(f'{save_dir}/{src_file.split("/")[-1]}_cached_freq.json', 'w') as f:
            json.dump(self.data, f)

        # print('Saving', f'{save_dir}/{src_file}_cached_freq.json')

    def load_cache(self, rfile):

        rfile_name = '/'.join(rfile.split('/')[-2:])
        # print(rfile_name)
        # print('Loading', rfile_name)
        if os.path.exists(f'{self.cache_folder}/{rfile_name}_cached_freq.json'):
            with open(f'{self.cache_folder}/{rfile_name}_cached_freq.json') as f:
                cached_data = json.load(f)

            for key in cached_data.keys():
                # if key not in self.data.keys():
                #     self.data[key] = []
                # self.data[key] += (cached_data[key])
                self.data[key] = cached_data[key]
            return True
        else:
            # print('File not found')
            return False

    def update(self, timestamp, channel, msg):

        if all(saved_channel not in channel for saved_channel in self.channels):
            return

        if channel not in self.data.keys():
            self.data[channel] = []

        msg_struct = pointcloud_pb2.PointCloud()
        msg_struct.ParseFromString(msg)

        self.data[channel].append(
            (timestamp / 10**9, msg_struct.measurement_time))
        # (timestamp / 10**9, msg_struct.point[-1].timestamp))

    def plot(self):

        plots = [
            self.plot_differences,
        ]

        fig, axs = plt.subplots(nrows=len(plots), figsize=(
            12.8, 7.2), constrained_layout=True)
        if not isinstance(axs, Iterable):
            axs = [axs]
        for ax, plot_func in zip(axs, plots):
            plot_func(ax)

        [a.legend(sorted(self.data.keys())) for a in axs]
        plt.show()

    def plot_differences(self, axs):
        print('plot differences')

        keys = sorted(list(self.data.keys()))

        for key in keys:
            val = self.data[key]
            val = np.array(val).T
            diff = val[0] - val[1]
            axs.plot(vfrom_ts(val[1]), diff)
        axs.set_title('Time differences')
        axs.set_ylabel('time, s')
        axs.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))

    def extract_ts(self, src_folder):
        for idx, f in enumerate(tqdm(sorted(os.listdir(src_folder)))):
            # if idx > 15:
            #     break
            rfile = src_folder + '/' + f

            if self.load_cache(rfile):
                continue

            freader = record.RecordReader(rfile)

            for idxs, (channelname, msg, datatype, timestamp) in tqdm(enumerate((freader.read_messages())), leave=False, total=get_total_messages(freader)):
                self.update(timestamp, channelname, msg)

            self.save_cache(rfile)


def main(path):

    checker = Checker(['/PointCloud2'], cache=True)

    checker.extract_ts(path)
    checker.plot()


if __name__ == '__main__':  # pragma: no cover
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str, help='a path to folder containing .record files',
                        default='/apollo/data/bag/2022-08-11-13-16-59')
    args = parser.parse_args()
    print(args)

    try:
        main(args.path)
    except KeyboardInterrupt:
        exit()
