#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import sys
import argparse
import yaml

from checking.plotter import ChannelPlotter


def main(src_folder, cfg):

    print(f'Checking folder: {src_folder}')
    plotter = ChannelPlotter(channels=cfg['channels'],
                             fill_between_field=cfg['fill_between_field'] if 'fill_between_field' in cfg.keys(
    ) else None,
        ts_field=cfg['ts_field'] if 'ts_field' in cfg.keys(
    ) else None,
        src_folder=src_folder)
    plotter.plot(cfg['plots'])


if __name__ == '__main__':  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-p', '--path', type=str,
                        help='a path to folder containing .record files', default='/apollo/data/bag/28.09/temp')
    parser.add_argument('-c', '--conf', type=str, help='a path to config .yaml file',
                        default=f'{sys.path[0]}/cfg_vis_channel_localization.yaml')

    args = parser.parse_args()
    print(args)

    with open(args.conf, 'r') as f:
        cfg = yaml.safe_load(f)

    try:
        main(args.path, cfg)
    except KeyboardInterrupt:
        exit()
