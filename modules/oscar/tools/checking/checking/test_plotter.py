#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import unittest
from unittest.mock import patch

import yaml
import os
import logging

import matplotlib
from matplotlib import pyplot as plt

from checking.plotter import ChannelPlotter
from checking.vis_channel_data import main as call_plotter

logging.basicConfig(level=logging.WARN)
matplotlib.use('agg')


class TestPlotter(unittest.TestCase):

    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)
        script_dir = os.path.dirname(os.path.realpath(__file__))
        self.src_folder = f'{script_dir}/../../test_data/loc_control_records'
        self.cfg_fname = f'{script_dir}/../../test_data/cfg_vis_channel_data_test.yaml'

        for i in [self.src_folder, self.cfg_fname]:
            assert os.path.exists(i), f'{i} is missing. Maybe test data is not downloaded'


    def testRecordReading(self):

        plotter = ChannelPlotter()
        plotter.load_data(self.src_folder)

    def testCacheLoading(self):

        plotter = ChannelPlotter(use_cached=True)
        plotter.load_data(self.src_folder)

    def testMsgTypes(self):

        channels = [
            '/apollo/localization/pose',
            '/apollo/localization/msf_status',
            '/apollo/hmi/status',
            '/apollo/sensor/gnss/best_pose',
            '/apollo/sensor/gnss/corrected_imu',
            '/apollo/sensor/gnss/gnss_status',
            '/apollo/sensor/gnss/imu',
            '/apollo/sensor/gnss/ins_status',
            '/apollo/sensor/gnss/odometry',
            '/tf'
        ]
        plotter = ChannelPlotter(use_cached=False, channels=channels)

        plotter.load_data(self.src_folder)

    @patch(f"checking.plotter.plt.show")
    def testPlotting(self, mock_plt):

        with open(self.cfg_fname, 'r') as f:
            cfg = yaml.safe_load(f)

        call_plotter(self.src_folder, cfg)


if __name__ == '__main__':  # pragma: no cover
    unittest.main()
