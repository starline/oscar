header {
  timestamp_sec: 1173545122.22
  module_name: "localization"
  sequence_num: 128
}
pose {
  position {
    x: 351783.740102756
    y: 6650149.145779221
    z: 24.426803332
  }
  orientation {
    qx: 0.011969667
    qy: 0.015395172
    qz: -0.780088946
    qw: -0.625364656
  }
  linear_velocity {
    x: -2.862270581
    y: -0.044665547
    z: 1.156052721
  }
  linear_acceleration {
    x: 1.309413658
    y: 2.139858478
    z: -1.610580069
  }
  angular_velocity {
    x: 0.055355823
    y: 0.015109944
    z: -0.046981907
  }
  heading: -2.922310029
}
