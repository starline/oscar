# Camera info parameter publisher

Утилита периодически публикует в каналы ```/apollo/sensor/camera/{camera_frame}/CameraCalibration``` сообщения с внутренними параметрами камер.

## Использование

```
./camera_info_publisher.py [-p PERIOD] [-c FILE1 FILE2 ...]
```

Параметры:
 - ```-p``` - период публикации сообщений;
 - ```-c``` - список yaml файлов с внутренними трансформациями.
