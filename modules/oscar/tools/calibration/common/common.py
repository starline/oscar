import numpy as np
import yaml
import open3d as o3d
from copy import deepcopy as dc


from kiss_icp.kiss_icp import KissICP
from kiss_icp.config import load_config


def quat2mat(quat):
    n = np.linalg.norm(quat, ord=2)
    x, y, z, w = quat / n

    m = np.array([
        [1.0 - 2 * (y**2 + z**2), 2 * (x * y - z * w), 2 * (x * z + y * w)],
        [2 * (x * y + z * w), 1.0 - 2 * (x**2 + z**2), 2 * (y * z - x * w)],
        [2 * (x * z - y * w), 2 * (y * z + x * w), 1.0 - 2 * (x**2 + y**2)]
    ])
    return m


def quat_pos_to_tf_mat(pose):
    # M = np.hstack([quat2mat(pose[:-3]), pose[-3:].reshape((3, 1))])
    M = np.hstack([quat2mat(pose[-4:]), pose[:-4].reshape((3, 1))])
    M = np.concatenate([M, np.array([0, 0, 0, 1]).reshape(1, 4)])
    return M


# Taken from https://github.com/ros/geometry/blob/noetic-devel/tf/src/tf/transformations.py
def tf_mat_to_quat_pos(matrix):
    """Return quaternion from rotation matrix.
    >>> R = rotation_matrix(0.123, (1, 2, 3))
    >>> q = quaternion_from_matrix(R)
    >>> numpy.allclose(q, [0.0164262, 0.0328524, 0.0492786, 0.9981095])
    True
    """
    q = np.empty((4, ), dtype=np.float64)
    M = np.array(matrix, dtype=np.float64, copy=False)[:4, :4]
    t = np.trace(M)
    if t > M[3, 3]:
        q[3] = t
        q[2] = M[1, 0] - M[0, 1]
        q[1] = M[0, 2] - M[2, 0]
        q[0] = M[2, 1] - M[1, 2]
    else:
        i, j, k = 0, 1, 2
        if M[1, 1] > M[0, 0]:
            i, j, k = 1, 2, 0
        if M[2, 2] > M[i, i]:
            i, j, k = 2, 0, 1
        t = M[i, i] - (M[j, j] + M[k, k]) + M[3, 3]
        q[i] = t
        q[j] = M[i, j] + M[j, i]
        q[k] = M[k, i] + M[i, k]
        q[3] = M[k, j] - M[j, k]
    q *= 0.5 / np.sqrt(t * M[3, 3])
    return np.concatenate([q, matrix[:-1, -1]])


def load_transform_from_yaml(f, quat=False):

    with open(f, 'r') as f:
        data = yaml.safe_load(f)['transform']

    rotation = [data['rotation']['x'], data['rotation']
                ['y'], data['rotation']['z'], data['rotation']['w']]
    translation = [data['translation']['x'],
                   data['translation']['y'], data['translation']['z']]

    # data = np.array(translation + rotation)
    data = np.array(rotation + translation)
    return quat_pos_to_tf_mat(data) if not quat else data


def save_transform_to_yaml(transform, save_file, replace_file_content=None):

    if transform.size == 16:
        transform = tf_mat_to_quat_pos(transform)
    transform = np.round(transform, decimals=4)

    res = {
        'translation':
            {k: val for k, val in zip(
                # ('x', 'y', 'z'), transform[-3:].astype(float).tolist())},
                ('x', 'y', 'z'), transform[:3].astype(float).tolist())},
        'rotation':
            {k: val for k, val in zip(
                # ('x', 'y', 'z', 'w'), transform[:-3].astype(float).tolist())}
                ('x', 'y', 'z', 'w'), transform[3:].astype(float).tolist())}
    }

    if replace_file_content is not None:
        with open(replace_file_content, "r") as f:
            data_original = yaml.safe_load(f)

        data_original["transform"] = dc(res)

        res = data_original

    with open(save_file, "w") as f:
        yaml.dump(res, f, default_flow_style=False)


class Vis:
    def __init__(self):
        self.pcs = []
        self.poses = []

        self.vis = o3d.visualization.Visualizer()
        self.vis.create_window()

    def update(self, pc, pose):
        if len(self.poses) == 0:
            self.poses.append(pose)
        else:
            self.poses.append(self.poses[-1] @ np.linalg.inv(pose))

        pc = dc(pc).transform(self.poses[-1])  # .voxel_down_sample(0.5)
        self.pcs.append(pc)

        # self.vis.update_geometry(geometry)
        self.vis.add_geometry(pc)
        view = self.vis.get_view_control()
        view.set_zoom(0.3)
        view.set_front(np.array([0., 0, 1]))
        view.set_lookat(np.array([0., 0, 10]))
        view.set_up(np.array([0, 1., 0]))

        self.vis.poll_events()
        self.vis.update_renderer()

    def loop(self):

        tmp_pc = o3d.geometry.PointCloud()
        tmp_pc.points = o3d.utility.Vector3dVector(
            np.concatenate([pc.points for pc in self.pcs]))
        print(len(tmp_pc.points))
        tmp_pc = tmp_pc.voxel_down_sample(0.5)
        print(len(tmp_pc.points))
        try:
            while True:
                self.vis.poll_events()
                self.vis.update_renderer()
        except KeyboardInterrupt:
            self.vis.close()
            self.vis.destroy_window()
            return


def merge_extrinsics(*files, out_f):

    transform = np.eye(4)

    # parent_frame =

    for f in files:
        with open(f, 'r') as f:
            data = yaml.safe_load(f)

        translation = data['transform']['translation']
        rotation = data['transform']['rotation']
        data = np.array([rotation['x'], rotation['y'], rotation['z'], rotation['w'],
                         translation['x'], translation['y'], translation['z']])

        transform = transform @ quat_pos_to_tf_mat(data)

    return save_transform_to_yaml(transform, out_f)


def load_transform(in_file):

    with open(in_file, 'r') as f:
        data = yaml.safe_load(f)

    translation = data['transform']['translation']
    rotation = data['transform']['rotation']
    data = np.array([rotation['x'], rotation['y'], rotation['z'], rotation['w'],
                     translation['x'], translation['y'], translation['z']])

    return quat_pos_to_tf_mat(data)


cfg = load_config(None, None, None)


def register_clouds(pc1, pc2, init_transform):

    reg = KissICP(cfg)

    pc1_tr = dc(pc1).transform(np.linalg.inv(init_transform))

    reg.register_frame(np.asarray(pc1_tr.points), 0)
    reg.register_frame(np.asarray(pc2.points), 0)

    return init_transform @ reg.poses[-1]


try:
    from pypatchworkpp import Parameters, patchworkpp
    params = Parameters()
    # params.verbose = True
    params.min_range = 1
    # params.uprightness_thr = 0.1
    PatchworkPLUSPLUS = patchworkpp(params)

    def remove_ground_points(pc: np.ndarray):

        PatchworkPLUSPLUS.estimateGround(pc)
        return PatchworkPLUSPLUS.getNonground()

except ImportError:
    def remove_ground_points(pc: np.ndarray):
    
        pcd = o3d.geometry.PointCloud()
        pcd.points = o3d.utility.Vector3dVector(pc)
        _, inliers = pcd.segment_plane(distance_threshold=0.1,
                                    ransac_n=3,
                                    num_iterations=2000)

        pcd = pcd.select_by_index(inliers, invert=True)
        return np.asarray(pcd.points)
