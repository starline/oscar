#!/usr/bin/env python

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import numpy as np
from scipy.spatial.distance import cdist
from scipy.stats import gaussian_kde as kde

class AngleDiffConverter:
    def __init__(self) -> None:
        
        # self.coordinate_history = np.empty((0, 2))
        self.angles = np.empty((0,1))
        self.angle_diffs = np.empty((0,1))
        self.corrected_angles = np.empty((0,1))
        self.periods = np.empty((0,1)) 

        self.current_period = 0

    def update(self, angle):

        # # store current point
        if len(self.angles) == 0:
            self.angles = np.vstack([self.angles, [0]])
            self.corrected_angles = np.vstack([self.corrected_angles, [0]])
            self.angle_diffs = np.vstack([self.angle_diffs, [0]])
            self.periods = np.vstack([self.periods, [0]])
            return

        if angle < 0:
            angle += np.pi*2

        if angle - self.angles[-1] > np.pi:
            self.current_period -= 1
        elif angle - self.angles[-1] < -np.pi:
            self.current_period += 1



        self.angles = np.vstack([self.angles, [angle]])
        self.periods = np.vstack([self.periods, [self.current_period]])
        self.corrected_angles = np.vstack([self.corrected_angles, [self.current_period * 2 * np.pi + angle]])


        # get angle speed
        cur_diff = self.corrected_angles[-1] - self.corrected_angles[-2]
        if cur_diff > 2 * np.pi:
            cur_diff -= 2 * np.pi
        if cur_diff < - 2 * np.pi:
            cur_diff += 2 * np.pi

        self.angle_diffs = np.vstack([self.angle_diffs, [cur_diff]])
        return
        

class EMA:
    def __init__(self, init_val=0, alpha=0.5) -> None:
        self.val_history = init_val
        self.alpha = alpha

    def update(self, value):
        self.val_history = value * self.alpha + (1 - self.alpha) * self.val_history
        return self.val_history


def get_kde(values):
    # print(values)
    if len(values) > 1:
        ret_val = kde(values)(values)
        # print(values[ret_val.argmax()])
        return values[ret_val.argmax()]
    else:
        return values.mean()


