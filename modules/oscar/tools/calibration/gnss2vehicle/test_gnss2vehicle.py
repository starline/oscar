#!/apollo/bazel-bin/modules/oscar/tools/calibration/.venv/bin/python3

###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import sys
import os

import matplotlib
matplotlib.use('agg')

from gnss2vehicle.calibrate_heading_angle import main as heading_main
from gnss2vehicle.calibrate_gnss_Y import main as gnss_Y_main


class TestGnss2Vehicle:

    def test_heading_angle(self):
        records_path = '/apollo/modules/oscar/tools/test_data/localization_records'

        assert os.path.exists(records_path)
        angle = heading_main(records_path, plot_result=False)
        gt_angle = -2.4870159231223923
        assert abs(angle - gt_angle) < 1e6

    def test_gnss_Y(self):

        records_path = '/apollo/modules/oscar/tools/test_data/calibration_gnss_Y_data_no_lidars'
        assert os.path.exists(records_path)

        gnss_Y_main(records_path, None, 0, 1.85, '/dev/null')
