#!/apollo/bazel-bin/modules/oscar/tools/calibration/.venv/bin/python3

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
import copy
import sys
import os
import matplotlib

import yaml
from tqdm.auto import tqdm
from matplotlib import pyplot as plt
import matplotlib.dates as mdates
from datetime import datetime

from scipy.spatial.transform import Rotation
from scipy.spatial.distance import cdist

import numpy as np

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import record
from modules.common_msgs.localization_msgs import localization_pb2


plt.rcParams['axes.grid'] = True

vfrom_ts = np.vectorize(datetime.fromtimestamp)


def setup_hist(axs, x, title, xlabel=None, ylabel=None):

    remove_shared_axis(axs)
    # return X ticks after sharex=True
    axs.tick_params(
        axis='x',           # changes apply to the x-axis
        which='both',       # both major and minor ticks are affected
        bottom=True,
        top=False,
        labelbottom=True)

    axs.hist(x, bins="auto")
    axs.set_title(title)
    axs.set_xlabel(xlabel)
    axs.set_ylabel(ylabel)


def setup_plot(axs, x, y, title, c=None, xlabel=None, ylabel=None):
    # return X ticks after sharex=True
    axs.tick_params(
        axis='x',           # changes apply to the x-axis
        which='both',       # both major and minor ticks are affected
        bottom=True,
        top=False,
        labelbottom=True)

    axs.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
    axs.plot(x, y, c=c)
    axs.set_title(title)
    axs.set_xlabel(xlabel)
    axs.set_ylabel(ylabel)


def setup_scatter(axs, x, y, title, c=None, xlabel=None, ylabel=None):
    # return X ticks after sharex=True
    axs.tick_params(
        axis='x',           # changes apply to the x-axis
        which='both',       # both major and minor ticks are affected
        bottom=True,
        top=False,
        labelbottom=True)

    axs.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
    axs.scatter(x, y, c=c, s=1)
    axs.set_title(title)
    axs.set_xlabel(xlabel)
    axs.set_ylabel(ylabel)


def get_localization_data(src_folder):

    cumulative_data = []

    files = sorted([f'{root}/{f}' for (root, _, files)
                   in os.walk(src_folder) for f in files])

    for rfile in tqdm(files):

        freader = record.RecordReader(rfile)

        cur_ar = []

        for channelname, msg, datatype, timestamp in freader.read_messages():
            if channelname == '/apollo/localization/pose':

                msg_struct = localization_pb2.LocalizationEstimate()
                msg_struct.ParseFromString(msg)
                cur_ar.append(np.array([msg_struct.header.timestamp_sec, msg_struct.pose.position.x, msg_struct.pose.position.y,
                                        msg_struct.pose.orientation.qx, msg_struct.pose.orientation.qy, msg_struct.pose.orientation.qz, msg_struct.pose.orientation.qw,
                                        msg_struct.pose.heading]))
        if len(cur_ar) > 0:
            cur_ar = np.stack(cur_ar)
            cumulative_data.append(cur_ar)

    cumulative_data = np.concatenate(cumulative_data)
    return cumulative_data


def quat2mat(x): return Rotation.from_quat(x).as_matrix()
def quat2euler(x): return Rotation.from_quat(x).as_euler('xyz')
def v_quat2euler(x): return np.apply_along_axis(quat2euler, 1, x)


def create_new_yaml_data(cur_offset_data, updated_primary_point, updated_secondary_point):
    updated_data = copy.deepcopy(cur_offset_data)

    for ant_key, ant_vals in zip(['primary', 'secondary'], [updated_primary_point, updated_secondary_point]):

        for key, val in zip(['x', 'y', 'z'], ant_vals):

            updated_data['leverarm'][ant_key]['offset'][key] = float(np.around(
                val, decimals=3))
    return updated_data


def save_yaml(data, save_fname):
    with open(save_fname, 'w') as f:
        yaml.dump(data, f, default_flow_style=False)


def remove_shared_axis(axs):
    axs.get_shared_x_axes().remove(axs)
    axs.xaxis.major = matplotlib.axis.Ticker()
    axs.xaxis.set_major_locator(matplotlib.ticker.AutoLocator())
    axs.xaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter())


def get_approximated_headings(coords, points_distance_threshold):

    ress = []

    MAX_DIST_SEARCH_WINDOW = 100
    for idx in tqdm(range(len(coords))):

        smallest_idx = max(0, idx - MAX_DIST_SEARCH_WINDOW)
        biggest_idx = min(len(coords), idx + MAX_DIST_SEARCH_WINDOW)

        dists = cdist(
            [coords[idx]], coords[smallest_idx:biggest_idx])[0]

        nearest_to_thresh_prev = np.argmin(
            np.abs(dists[:max(1, idx) - smallest_idx] - points_distance_threshold)) + smallest_idx
        p1 = coords[nearest_to_thresh_prev]

        nearest_to_thresh_next = np.argmin(
            np.abs(dists[idx - smallest_idx:] - points_distance_threshold)) + idx
        p2 = coords[min(nearest_to_thresh_next, len(coords) - 1)]

        vec = np.array((p2 - p1) / 2)
        angle = np.arctan2(vec[1], vec[0])
        ress.append((angle * 180 / np.pi, np.linalg.norm(vec)))

    ress = np.array(ress).T

    return ress


def moving_average(x, w):
    return np.convolve(x, np.ones(w), 'valid') / w


def main(src_folder, save_plot_dir, visualize=True):
    data = get_localization_data(src_folder)

    # filter broken localization results
    keep_inds = data[:, 1] < 1e10
    data = data[keep_inds]
    keep_inds = data[:, 1] > 1e3
    data = data[keep_inds]
    coords, headings = data[:, 1: 3], data[:, 7]

    headings *= 180 / np.pi

    points_distance_threshold = 0.5

    angles, norms = get_approximated_headings(
        coords, points_distance_threshold)

    diffs = angles - headings
    diffs[diffs < - 180] += 360
    diffs[diffs > 180] -= 360

    print(f'Median value of offset: {np.median(diffs):.3f}')

    h, _ = np.histogram(diffs, bins='fd')
    # angle = (edges[h.argmax()] + edges[h.argmax() + 1]) / 2
    angle = np.median(diffs)
    quat = Rotation.from_euler('xyz', [0, 0, -angle], degrees=True).as_quat()
    quat = {key: val for key, val in zip(
        ['qx', 'qy', 'qz', 'qw'], quat.astype(float).tolist())}

    print("heading offset of GNSS:")

    [print(f"{k}: {v}") for k, v in zip(quat.keys(), quat.values())]

    fig, axs = plt.subplots(nrows=3, ncols=2,
                            # figsize=(19.2, 10.8),
                            figsize=(38.4, 21.6),
                            constrained_layout=True,
                            sharex="all"
                            )

    ts_x = vfrom_ts(data[:, 0])

    filtered_angles = angles[norms > points_distance_threshold / 2]
    filtered_diffs = diffs[norms > points_distance_threshold / 2]
    filtered_ts_x = ts_x[norms > points_distance_threshold / 2]

    print(
        f'Filtering static points: from {len(diffs)} to {len(filtered_diffs)}')

    fig.suptitle(f"Data from folder: {src_folder}")

    setup_hist(axs[0, 0], filtered_diffs, "Diffs histogram", xlabel="degrees")

    setup_scatter(axs[1, 0], filtered_ts_x[filtered_diffs > 0], filtered_diffs[filtered_diffs > 0],
                  "Diffs scatter", c="red", ylabel="degrees")
    setup_scatter(axs[1, 0], filtered_ts_x[filtered_diffs <= 0], filtered_diffs[filtered_diffs <= 0],
                  "Diffs scatter", ylabel="degrees")

    window = min(501, int(np.ceil(len(filtered_diffs) // 2) // 2 * 2 + 1))

    def setup_moving_average_plot(axs, x, data, window, title, c=None):
        cut_x = x[window // 2: -(window // 2)]
        averages = moving_average(data, window)

        nrows = data.size - window + 1
        strided_view = np.lib.stride_tricks.as_strided(data, shape=(
            nrows, window), strides=(data.strides[0], data.strides[0]))
        lower_border = np.percentile(strided_view, 25, axis=-1)
        higher_border = np.percentile(
            strided_view, 75, axis=-1)

        setup_plot(axs, cut_x, averages, title, c=c)
        axs.fill_between(cut_x, lower_border, higher_border, alpha=0.3)

    setup_moving_average_plot(
        axs[1, 0], filtered_ts_x, filtered_diffs, window, "Filtered Diffs", c="orange")
    # setup_plot(axs[1, 0], filtered_ts_x[window // 2: -(window // 2)], moving_average(
    #     filtered_diffs, window), "filtered_diffs", c="orange")
    axs[1, 0].legend(
        [f"Moving average (window: {window / 100} sec)", "> 0", "<= 0"], loc="upper left")

    setup_plot(axs[2, 0], filtered_ts_x, filtered_angles,
               "Heading plot", ylabel="degrees")
    setup_plot(axs[2, 0], ts_x, headings,
               "Heading plot", ylabel="degrees")
    axs[2, 0].legend(["Calculated angles", "Headings"], loc="upper left")

    setup_plot(axs[0, 1], ts_x, coords[:, 0], "X", ylabel="metres")
    setup_plot(axs[1, 1], ts_x, coords[:, 1], "Y", ylabel="metres")
    setup_plot(axs[2, 1], ts_x, norms, "Norms", ylabel="metres")

    if src_folder.endswith("/"):
        src_folder = src_folder[:-1]
    base_fname = os.path.basename(
        src_folder.replace('(', '_').replace(')', '_'))

    save_path = f"{os.path.dirname(os.path.realpath(__file__))}/{save_plot_dir}/{base_fname}_plot.png"
    os.makedirs(os.path.dirname(save_path), exist_ok=True)
    print(f"saving figure: {save_path}")
    plt.savefig(save_path)

    if visualize:
        plt.show()


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str, default='/apollo/data/bag/2022/24_08_nissan_first',
                        help='path to source folder with .record files')

    parser.add_argument('-s', '--save_plot_dir', type=str, default='/apollo/modules/oscar/tools/calibration/gnss2vehicle/plots',
                        help='a folder to save plots')

    parser.add_argument("-v", "--visualize",
                        action="store_true", help="show plots")

    args = parser.parse_args()

    print(args)

    main(args.path, args.save_plot_dir, args.visualize)
