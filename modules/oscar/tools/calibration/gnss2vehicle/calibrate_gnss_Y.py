#!/apollo/bazel-bin/modules/oscar/tools/calibration/.venv/bin/python3


###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
from math import degrees
import os
import sys
import time
from copy import deepcopy as dc

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")
import numpy as np
import matplotlib

from matplotlib import pyplot as plt
from matplotlib.patches import Circle, Rectangle
from scipy.spatial.transform import Rotation
import yaml
import cv2 as cv

from cyber.python.cyber_py3 import cyber, record
from modules.common_msgs.localization_msgs import localization_pb2

from tqdm.auto import tqdm

from gnss2vehicle.utils import AngleDiffConverter, EMA


from common.common import save_transform_to_yaml


np.set_printoptions(precision=3)


COLORS_SET = np.random.uniform(0, 1, size=(50, 3))
MAX_POINTS = 2e6
def dist(p1, p2): return np.sqrt((p2[0] - p1[0])**2 + (p2[1] - p1[1])**2)


class Calibrator:
    def __init__(self, rfolder=None, visualize=True, heading_offset_yaml=None, x_offset=0, z_offset=0):

        self.visualize = visualize

        self.rfolder = rfolder

        self.input_record = rfolder is not None

        # self.poses = np.empty((0, 3), dtype=np.float32) # x, y, heading angle
        self.poses = []

        self.last_time = None
        self.message_received = False

        self.x_offset = x_offset
        self.z_offset = z_offset

        self.time_thresh = 0.1
        self.dist_thresh = 0.2

        self.car_width = 2.5
        self.car_length = 6.3

        self.angle_converter = AngleDiffConverter()

        self.radius_ema = EMA(init_val=0, alpha=0.5)

        if heading_offset_yaml is None:
            self.heading_offset_degrees = 0
        else:
            print(f'Got heading offset file: {heading_offset_yaml}')
            with open(heading_offset_yaml, 'r') as f:
                h = yaml.safe_load(f)
            self.heading_offset_degrees = Rotation.from_quat(
                [h['x'], h['y'], h['z'], h['w']]).as_euler('xyz', degrees=True)[-1]
            print(
                f'Heading correction: {self.heading_offset_degrees:.3f} degrees')
        self.last_pose = None

        self.last_circle_c = None

        self.calc_dists = []

        print('Init complete')

    def check_msg(self, pose, cur_time):

        if self.last_time is None:
            self.last_time = cur_time
        elif (cur_time - self.last_time < self.time_thresh) or (dist(self.poses[-1][:-1], pose[:-1]) < self.dist_thresh):
            return False
        return True

    def save_all_points(self):
        for idx, f in enumerate(tqdm(sorted(os.listdir(self.rfolder)))):
            rfile = self.rfolder + '/' + f
            freader = record.RecordReader(rfile)

            for channelname, msg, datatype, timestamp in freader.read_messages():
                if channelname != '/apollo/localization/pose':
                    continue

                msg_struct = localization_pb2.LocalizationEstimate()
                msg_struct.ParseFromString(msg)

                pose = [msg_struct.pose.position.x, msg_struct.pose.position.y,
                        msg_struct.pose.heading + self.heading_offset_degrees * np.pi / 180]

                # print(pose, msg_struct.measurement_time)

                if not self.check_msg(pose, msg_struct.measurement_time):
                    continue

                self.poses.append(pose)
        self.poses = np.array(self.poses[1:])
        # self.poses[0] = self.poses[1]
        # self.poses = self.poses[(self.poses[:, 0] < np.quantile(self.poses[:, 0], 0.95)) & (self.poses[:, 0] > np.quantile(self.poses[:, 0], 0.05))]

    def divideCircles(self):

        period = 0
        periods = [0]
        for h1, h2 in zip(self.poses[:, -1], self.poses[1:, -1]):
            # hs.append(h2 + 2 * np.pi if h2 - h1 < - np.pi else h2)
            cur_diff = h2 - h1
            if cur_diff > 2 * np.pi:
                cur_diff -= 2 * np.pi
            if cur_diff < - 2 * np.pi:
                cur_diff += 2 * np.pi

            if cur_diff > np.pi:
                period -= 1
            elif cur_diff < -np.pi:
                period += 1
            periods.append(period)
        periods = np.array(periods)

        all_dists = []
        all_rad_angles = []
        all_radiuses = []

        for p in list(set(periods.tolist()))[1:]:
            cur_circle_headings = self.poses[periods == p, -1]
            cur_circle_coords = self.poses[periods == p, :-1]

            (cX, cY), (w, h), _ = cv.fitEllipse(
                cur_circle_coords.astype(np.float32))
            radius = (w + h) / 4

            all_radiuses.append(np.ones(len(cur_circle_coords)) * radius)

            rad_angles = np.arctan2(
                cur_circle_coords[:, 1] - cY, cur_circle_coords[:, 0] - cX)
            all_rad_angles.append(rad_angles)

        all_rad_angles = np.concatenate(all_rad_angles)
        all_radiuses = np.concatenate(all_radiuses)

        headings = [dc(self.poses[periods != 0, -1][0])]
        period = 0
        for h in self.poses[periods != 0, -1][1:]:
            h = h + 2 * np.pi * period
            if h - headings[-1] <= -np.pi:
                period += 1
                headings.append(h + 2 * np.pi)
            elif h - headings[-1] >= np.pi:
                period -= 1
                headings.append(h - 2 * np.pi)
            else:
                headings.append(h)

        rad_angles_corr = [all_rad_angles[0]]
        period = 0
        for h in all_rad_angles[1:]:
            h = h + 2 * np.pi * period
            if h - rad_angles_corr[-1] <= -np.pi:
                period += 1
                rad_angles_corr.append(h + 2 * np.pi)
            elif h - rad_angles_corr[-1] >= np.pi:
                period -= 1
                rad_angles_corr.append(h - 2 * np.pi)
            else:
                rad_angles_corr.append(h)

        angle_diffs = np.array(headings) - np.array(rad_angles_corr)

        dists = all_radiuses * np.cos(angle_diffs)

        print(
            f'Calculated Y offset: {dists.mean():.3f} +- {dists.std():.3f} m')

        return dists.mean()


def main(path, off_save_file, x_off, z_off, save_fname):
    calib = Calibrator(rfolder=path,
                       heading_offset_yaml=off_save_file,
                       x_offset=x_off,
                       z_offset=z_off)

    calib.save_all_points()
    dist = calib.divideCircles()

    save_transform_to_yaml(
        np.array([0, 0, 0, 1, x_off, dist, z_off]), save_fname)

    return dist


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str, default='/apollo/data/bag/calib_10_circles/',
                        help='path to source folder with .record files')

    parser.add_argument('-o', '--offset', type=str, default=f'{sys.path[0]}/zero_quat.yaml',
                        help='quaternion after heading angle correction')

    parser.add_argument('-x', '--x_off', type=float, default=0,
                        help='x offset (cannot be calibrated)')

    parser.add_argument('-z', '--z_off', type=float, default=1.85,
                        help='z offset (cannot be calibrated)')

    parser.add_argument('-f', '--fname', type=str, default=f'{sys.path[0]}/res_transform.yaml',
                        help='name of a file to save extrinsics')

    args = parser.parse_args()

    try:
        main(args.path, args.offset, args.x_off, args.z_off, args.fname)
    except KeyboardInterrupt:
        exit()
