#!/apollo/bazel-bin/modules/oscar/tools/calibration/.venv/bin/python3

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
import os
import sys

import numpy as np

from matplotlib import pyplot as plt

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from modules.control.proto import calibration_table_pb2
from modules.control.proto.control_conf_pb2 import ControlConf
from modules.tools.common import proto_utils


def get_calibration_table_pb(speeds, cmds, accels):
    calibration_table_pb = calibration_table_pb2.ControlCalibrationTable()

    for speed, cmd, accel in zip(speeds, cmds, accels):
        item = calibration_table_pb.calibration.add()
        item.speed = speed
        item.acceleration = accel
        item.command = cmd
    return calibration_table_pb


def downsample_table(data):

    data = data[data[:, 3] > 0.1]
    accel = data[:, -1]
    speed = data[:, 3]
    cmd = data[:, 1] - data[:, 0]

    speed_max = np.round(speed.max())
    cmd_min = np.round(cmd.min())

    xv, yv = np.meshgrid(np.arange(0.1, speed_max, step=speed_step),
                         np.arange(cmd_min, 100.1, step=cmd_step))  # 100.1 is a workaround to include 100

    grid = np.stack([xv.flatten(), yv.flatten()]).T

    # speed_cmd_grid = np.stack([speed / speed_max * 100, cmd]).T
    speed_cmd_grid = np.stack([speed, cmd]).T

    avg_thresh = 0.5

    final_table = []

    for x, y in grid:

        is_far_from_zero = abs(y) > 20
        cur_thresh = avg_thresh if is_far_from_zero else 1
        neighbors_idxs = np.where((np.abs(speed_cmd_grid[:, 0] - x) < speed_step / cur_thresh) & (
            np.abs(speed_cmd_grid[:, 1] - y) < cmd_step / cur_thresh))[0]
        cur_accels = accel[neighbors_idxs]

        if len(cur_accels) == 0:
            if not is_far_from_zero:
                final_table.append((x, y, 0))
        else:
            final_table.append((x, y, np.mean(cur_accels)))

    final_table = np.array(final_table)
    # print(final_table.shape)
    return final_table.T


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-t', '--table', type=str, help='a path to .csv file or folder containing this files',
                        default='collected_data/16.09.2022-12:31:21.csv')
    parser.add_argument('-s', '--src_ctrl', type=str, help='a path to src control config .pb.txt file',
                        default='/apollo/modules/calibration/data/mb_actros/control_conf/control_conf.pb.txt')
    parser.add_argument('-d', '--dst_ctrl', type=str,
                        help='a path to dest control config .pb.txt file', default='control_conf_calibrated.pb.txt')

    parser.add_argument('--speed_step', type=float,
                        help='step of speed downsampling', default=1)
    parser.add_argument('--cmd_step', type=float,
                        help='step of cmd downsampling', default=5)
    parser.add_argument('--controller', type=str,
                        choices=["lon", "mpc", "both"],
                        help='choose controller to replace calibration table. Can be one of "lon", "mpc" or "both" for ',
                        default="lon")

    args = parser.parse_args()
    print(args)

    fname = args.table
    ctrl_cfg_fname = args.src_ctrl
    ctrl_save_fname = args.dst_ctrl
    speed_step = args.speed_step
    cmd_step = args.cmd_step

    if fname.endswith('csv'):
        data = np.loadtxt(fname, skiprows=0, dtype=np.float64, delimiter=',')
    else:
        data = []
        for f in os.listdir(fname):
            if not f.endswith('csv'):
                print(f"Skipping file {f}")
                continue

            data.append(np.loadtxt(
                f'{fname}/{f}', skiprows=0, dtype=np.float64, delimiter=','))
        data = np.concatenate(data)

    (speeds, cmds, accels) = downsample_table(data)
    print(f"Downsampled {len(data)} points to {len(speeds)}")

    # Read protobuf config and replace table
    ctrl_conf_pb = proto_utils.get_pb_from_text_file(
        ctrl_cfg_fname, ControlConf())
    calibration_table_pb = get_calibration_table_pb(speeds, cmds, accels)

    if args.controller == 'lon':
        ctrl_conf_pb.lon_controller_conf.calibration_table.CopyFrom(
            calibration_table_pb)
    elif args.controller == 'mpc':
        ctrl_conf_pb.mpc_controller_conf.calibration_table.CopyFrom(
            calibration_table_pb)
    elif args.controller == 'both':
        ctrl_conf_pb.lon_controller_conf.calibration_table.CopyFrom(
            calibration_table_pb)
        ctrl_conf_pb.mpc_controller_conf.calibration_table.CopyFrom(
            calibration_table_pb)
    else:
        print(f'Wrong controller: {args.controller}')

    print(f'Saving to file: {ctrl_save_fname}')
    with open(ctrl_save_fname, 'w') as f:
        f.write(str(ctrl_conf_pb))

    # show result
    figure = plt.figure(figsize=(18, 10.8))
    ax = plt.axes(projection='3d')
    ax.scatter3D(speeds, cmds, accels)
    ax.scatter3D(data[:, 3], data[:, 1] - data[:, 0], data[:, -1], alpha=0.5)
    # ax.set_xticks(speeds)
    # ax.set_yticks(cmds[::10])
    ax.set_xlabel('Speed')
    ax.set_ylabel('Cmd')
    ax.set_zlabel('Acceleration')
    ax.legend(['downsampled', 'original'])

    print('showing')
    plt.show()
