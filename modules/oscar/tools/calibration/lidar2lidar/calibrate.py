#!/apollo/bazel-bin/modules/oscar/tools/calibration/.venv/bin/python3

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
from datetime import datetime
import time
import sys
import os

from copy import deepcopy as dc
from typing import Iterable
from scipy.spatial.transform import Rotation
import numpy as np
from tqdm.auto import tqdm
import open3d as o3d

import pygicp

from matplotlib import pyplot as plt
plt.rcParams['axes.grid'] = True

from common.common import load_transform_from_yaml, quat_pos_to_tf_mat, tf_mat_to_quat_pos, save_transform_to_yaml


from kiss_icp.kiss_icp import KissICP
from kiss_icp.config import load_config

DOWNSAMPLE = False
REMOVE_GROUND_POINTS = False
# bbox cropping params
THRESH = 1500
HEIGHT = 200


def remove_ground(pc):

    _, inliers = pc.segment_plane(distance_threshold=0.1,
                                  ransac_n=3,
                                  num_iterations=2000)

    pc = pc.select_by_index(inliers, invert=True)
    return pc


cfg = load_config(None, None, None)


def register_clouds(pc1, pc2, init_transform):

    reg = KissICP(cfg)

    pc1_tr = dc(pc1).transform(np.linalg.inv(init_transform))

    reg.register_frame(np.asarray(pc1_tr.points), 0)
    reg.register_frame(np.asarray(pc2.points), 0)

    return init_transform @ reg.poses[-1]


def merge_pointclouds(pc1, pc2):
    res_pc = o3d.geometry.PointCloud()
    res_pc.points = o3d.utility.Vector3dVector(np.concatenate(
        [np.asarray(pc1.points),
         np.asarray(pc2.points)]
    ))
    return res_pc


def compare_transforms(pc1, pc2, transforms: Iterable):

    n_points = []
    for transform in transforms:
        pc = merge_pointclouds(pc1, dc(pc2).transform(transform))
        pc = pc.voxel_down_sample(0.05)
        n_points.append(len(pc.points))

    [print(f'{(l - n_points[0])/n_points[0]:.3f}, {l}') for l in n_points[1:]]


class Vis:
    def __init__(self):
        self.vis = o3d.visualization.Visualizer()
        self.vis.create_window()

    def update(self, pc1, pc2, transform):

        self.vis.clear_geometries()
        self.vis.add_geometry(pc1)
        self.vis.add_geometry(pc2.transform(transform))
        view = self.vis.get_view_control()
        view.set_zoom(0.3)
        view.set_front(np.array([0., 0, 1]))
        view.set_lookat(np.array([0., 0, 10]))
        view.set_up(np.array([0, 1., 0]))

        self.vis.poll_events()
        self.vis.update_renderer()

    def loop(self):

        tmp_pc = o3d.geometry.PointCloud()
        tmp_pc.points = o3d.utility.Vector3dVector(
            np.concatenate([pc.points for pc in self.pcs]))
        print(len(tmp_pc.points))
        tmp_pc = tmp_pc.voxel_down_sample(0.5)
        print(len(tmp_pc.points))
        try:
            while True:
                self.vis.poll_events()
                self.vis.update_renderer()
        except KeyboardInterrupt:
            return


def main(folder, init_transform, save_file, downsample=True, step=1, show_first=True, visualize=True):

    init_transformation = load_transform_from_yaml(init_transform)

    files = sorted(os.listdir(folder))
    main_pcd_files = [f for f in files if 'main' in f]
    sec_pcd_files = [f for f in files if 'sec' in f]

    main_pcd_files = main_pcd_files[::step]
    sec_pcd_files = sec_pcd_files[::step]

    if visualize:
        vis = o3d.visualization.Visualizer()
        vis.create_window()
        view = vis.get_view_control()

        view.set_zoom(0.3)
        view.set_front(np.array([0., 0, 1]))
        view.set_lookat(np.array([0., 0, 10]))
        view.set_up(np.array([0, 1., 0]))

    res_matrices = []
    for idx, (main_pcd_f, sec_pcd_f) in tqdm(enumerate(zip(main_pcd_files, sec_pcd_files)), total=len(main_pcd_files)):

        # if idx > 20:
        #     break
        pc1 = o3d.io.read_point_cloud(folder + '/' + main_pcd_f)
        pc2 = o3d.io.read_point_cloud(folder + '/' + sec_pcd_f)

        # Downsampling
        if downsample:
            ds_size = 0.1
            pc1 = pc1.voxel_down_sample(ds_size)
            pc2 = pc2.voxel_down_sample(ds_size)

        # Croppping

        min_b = np.array([-THRESH, -THRESH, -HEIGHT],
                         dtype=np.float64).reshape((3, 1))
        max_b = np.array([THRESH, THRESH, THRESH],
                         dtype=np.float64).reshape((3, 1))
        bbox = o3d.geometry.AxisAlignedBoundingBox(min_b, max_b)
        pc1 = pc1.crop(bbox)
        # pc2 = pc2.crop(bbox)

        # outlier removal
        _, inds = pc1.remove_statistical_outlier(
            nb_neighbors=20, std_ratio=2.0)
        pc1 = pc1.select_by_index(inds)
        _, inds = pc2.remove_statistical_outlier(
            nb_neighbors=20, std_ratio=2.0)
        pc2 = pc2.select_by_index(inds)

        # tr_pc1_pc2 = np.linalg.inv(init_transformation) @ np.linalg.inv(p1) @ p2 @ init_transformation

        # ground plane removal

        if REMOVE_GROUND_POINTS:

            pc1 = remove_ground(pc1)
            pc2 = remove_ground(pc2)

        # Transformation registration
        source = np.asarray(pc2.points)
        target = np.asarray(pc1.points)

        res_transform = register_clouds(pc1, pc2, init_transformation)

        # res_transform = pygicp.align_points(target, source,
        #                                     initial_guess=init_transformation,
        #                                     method='VGICP',
        #                                     max_correspondence_distance=1e3,
        #                                     # neighbor_search_method='DIRECT1',
        #                                     num_threads=24
        #                                     )

        # register_frame()

        pc1.paint_uniform_color([1, 0, 0])
        pc2.paint_uniform_color([0, 0, 1])
        if show_first and idx == 0:

            res_pc = dc(pc2).transform(res_transform)
            fake_res_pc = dc(pc2).transform((init_transformation))
            # pc2.paint_uniform_color([0, 1, 0])
            fake_res_pc.paint_uniform_color([0, 1, 0])
            res_pc.paint_uniform_color([0, 0, 1])
            # fake_res_pc.paint_uniform_color([1, 0, 1])

            compare_transforms(pc1, pc2, [init_transformation, res_transform])

            o3d.visualization.draw_geometries([pc1, res_pc])
            # o3d.visualization.draw_geometries([pc1, fake_res_pc])

        if visualize:
            vis.clear_geometries()
            vis.add_geometry(pc1)
            vis.add_geometry(dc(pc2).transform(res_transform))

            vis.poll_events()
            vis.update_renderer()

        res_matrices.append(tf_mat_to_quat_pos(res_transform))

    res_matrices = np.stack(res_matrices)

    res_pose = []
    for i in range(res_matrices.shape[-1]):
        h, edges = np.histogram(res_matrices[:, i], bins='fd')
        res_pose.append((edges[h.argmax()] + edges[h.argmax() + 1]) / 2)

    def print_extrinsics(q, t):
        # q = R.from_matrix(mat[:-1, :-1]).as_quat()
        s = f'''
transform:
  translation:
    x: {t[0]}
    y: {t[1]}
    z: {t[2]}
  rotation:
    x: {q[0]}
    y: {q[1]}
    z: {q[2]}
    w: {q[3]}

    '''

        print(s)

    print_extrinsics(res_pose[:4], res_pose[4:])

    print("XYZ:", Rotation.from_quat(
        res_pose[:4]).as_euler("XYZ", degrees=True))

    titles = ['qx', 'qy', 'qz', 'qw', 'x', 'y', 'z']
    # [print(f"{key}: {a}") for a, key in zip(res_pose, titles)]

    save_transform_to_yaml(np.array(res_pose), save_file)

    fig, axs = plt.subplots(nrows=4, ncols=2, figsize=(19.2, 10.8))
    plt.grid()

    for i in range(res_matrices.shape[1]):
        axs[i // 2, i %
            2].plot(np.arange(len(res_matrices[:, i])), res_matrices[:, i])
        axs[i // 2, i % 2].set_title(titles[i])

    plt.show()

    return


def get_parser():  # pragma: no cover
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str, default=f'{sys.path[0]}/RL_and_RR_pcds',
                        help='path to source folder with point cloud .pcd files')
    parser.add_argument('-i', '--init', type=str,
                        help='initial extrinsics file', default=f'{sys.path[0]}/init_transform_ruby_l_ruby_r.yaml')

    parser.add_argument('-r', '--res', type=str,
                        help='result extrinsics file', default=f'{sys.path[0]}/res_transform_ruby_l_ruby_r.yaml')

    return parser


if __name__ == '__main__':  # pragma: no cover
    parser = get_parser()
    args = parser.parse_args()

    main(args.path, args.init, args.res, step=1, show_first=False)
