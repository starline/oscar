#!/apollo/bazel-bin/modules/oscar/tools/calibration/.venv/bin/python3

###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import unittest
import sys
import os

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../')

import pytest

import matplotlib
from matplotlib import pyplot as plt
matplotlib.use('agg')

from lidar2lidar.calibrate import main as calibrate
from lidar2lidar.check_plane_seg import main as check_plane_seg


class TestLidar2Lidar(unittest.TestCase):

    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)

        self.src_aligned_pc_folder = '/apollo/modules/oscar/tools/test_data/lidar2lidar/pcds_aligned'
        self.init_extrinsics_file = '/apollo/modules/oscar/tools/test_data/lidar2lidar/init_transform_lidar2lidar.yaml'
        self.res_extrinsics_file = '/apollo/modules/oscar/tools/test_data/lidar2lidar/out/res_transform_lidar2lidar.yaml'

    @pytest.mark.run(order=1)
    def test_registration(self):
        calibrate(self.src_aligned_pc_folder, self.init_extrinsics_file,
                  self.res_extrinsics_file, visualize=False)

    @pytest.mark.run(order=2)
    def test_check_plane_seg(self):

        check_plane_seg(self.src_aligned_pc_folder,
                        self.init_extrinsics_file, self.res_extrinsics_file)
