#!/apollo/bazel-bin/modules/oscar/tools/calibration/.venv/bin/python3

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
import os
import sys


from scipy.spatial.transform import Rotation
import open3d as o3d
import numpy as np
from tqdm.auto import tqdm
import matplotlib
from matplotlib import pyplot as plt

from common.common import load_transform_from_yaml

plt.rcParams['axes.grid'] = True
np.set_printoptions(suppress=True, precision=3)


def preprocess(pc):
    pc = pc.voxel_down_sample(0.1)

    THRESH = 5
    HEIGHT = 10
    min_b = np.array([-THRESH, -THRESH, -HEIGHT],
                     dtype=np.float64).reshape((3, 1))
    max_b = np.array([THRESH, THRESH, HEIGHT],
                     dtype=np.float64).reshape((3, 1))
    bbox = o3d.geometry.AxisAlignedBoundingBox(min_b, max_b)
    return pc.crop(bbox)


def find_plane(pc: o3d.geometry.PointCloud, transform=np.eye(4)):
    pc = preprocess(pc)

    pc = pc.transform(transform)

    plane_model, inliers = pc.segment_plane(distance_threshold=0.05,
                                            ransac_n=3,
                                            num_iterations=2000)
    return plane_model


def main(folder, init_extrinsics_f, res_extrinsics_f):

    files = sorted(os.listdir(folder))

    files = [f for f in files if "main" in f]

    step = 1

    files = files[::step]

    main_to_sec_transform_init = load_transform_from_yaml(init_extrinsics_f)
    main_to_sec_transform_res = load_transform_from_yaml(res_extrinsics_f)

    res_diffs = []
    init_diffs = []
    pbar = tqdm(files, total=len(files))
    for f in pbar:

        main_pc = o3d.io.read_point_cloud(folder + '/' + f)
        sec_pc = o3d.io.read_point_cloud(
            folder + '/' + f.replace('main', 'sec'))

        main_plane = find_plane(main_pc)
        init_sec_plane = find_plane(
            sec_pc, transform=main_to_sec_transform_init)
        res_sec_plane = find_plane(sec_pc, transform=main_to_sec_transform_res)

        diff_init = abs(main_plane[-1]) - abs(init_sec_plane[-1])
        diff_res = abs(main_plane[-1]) - abs(res_sec_plane[-1])

        pbar.set_description(
            f'Diff: Init - {diff_init:.3f}, Res - {diff_res:.3f}')

        init_diffs.append(diff_init * 1000)
        res_diffs.append(diff_res * 1000)

    plt.hist(init_diffs, bins='fd')
    plt.hist(res_diffs, bins='fd')
    plt.legend(['Init transform', 'Result transform'])
    plt.xlabel('Отклонение, мм')
    plt.show()


if __name__ == "__main__":  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str, default=f'{sys.path[0]}/RL_and_RR_pcds',
                        help='path to source folder with .pcd files')
    parser.add_argument('-i', '--init', type=str,
                        help='initial extrinsics file', default=f'{sys.path[0]}/init_transform_ruby_l_ruby_r.yaml')

    parser.add_argument('-r', '--res', type=str,
                        help='result extrinsics file', default=f'{sys.path[0]}/res_transform_ruby_l_ruby_r.yaml')
    args = parser.parse_args()

    print(args)

    main(args.path, args.init, args.res)
