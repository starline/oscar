#!/apollo/bazel-bin/modules/oscar/tools/calibration/.venv/bin/python3

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
import os
import sys
import numpy as np


import jax.numpy as jnp
from jax.config import config
from tqdm.auto import tqdm

from lidar2gnss.opt_utils import *

from common.common import save_transform_to_yaml, load_transform_from_yaml, tf_mat_to_quat_pos

np.set_printoptions(precision=3, floatmode='fixed', suppress=True)

config.update("jax_enable_x64", True)


def main(gnss_poses, registration_transformations, init_extrinsics_file, save_file):

    init_pose = load_transform_from_yaml(init_extrinsics_file, quat=True)

    # gt_z = init_pose[-1]
    gt_z = 0
    # files = sorted(os.listdir(gnss_poses))

    # print(gnss_poses)

    # world_poses = [np.loadtxt(f'{poses}/{f}', dtype=np.float64)
    #                for f in files if 'pose' in f]

    if gnss_poses.endswith("txt"):
        world_poses = np.loadtxt(gnss_poses)[:, 2:]
        world_poses = np.apply_along_axis(
            quat_pos_to_tf_mat, 1, world_poses)
    else:
        world_poses = np.load(gnss_poses)

    reg_poses = np.load(registration_transformations) if registration_transformations.endswith(
        ".npy") else np.loadtxt(registration_transformations)

    world_transforms = np.stack([np.linalg.inv(p1) @ p2 for p1,
                                 p2 in zip(world_poses[:-1], world_poses[1:])])

    reg_transforms = np.stack([np.linalg.inv(p1) @ p2 for p1,
                               p2 in zip(reg_poses[:-1], reg_poses[1:])])

    # a = reg_poses[0]
    # for t in reg_transforms:
    #     a = a @ t
    # print(a - reg_poses[-1])

    # return

    # print(reg_poses.shape, world_poses.shape, reg_transforms.shape)
    # print(reg_poses[500])
    # print(world_poses[0])
    # print

    # return

    # trs = [jnp.linalg.inv(np.loadtxt(f'{folder}/{f}'))
    #        for f in files if 'res' in f][:-1]

    # init_pose = jnp.array([0, 0, 0, 1, 0, 0, 0.0])

    # trs = np.array(trs)
    # p12s = np.array(p12s)

    # print(outliers_inds.shape)

    # print(trs.shape)
    # print(p12s.shape)

    # filter outliers by 1.5 IRQ
    # outliers_inds = find_outliers(trs)
    # print(outliers_inds)
    # if len(outliers_inds) != 0:
    #     trs = np.delete(trs, outliers_inds, axis=0)
    #     p12s = np.delete(p12s, outliers_inds, axis=0)

    # print(f"Loaded {len(p12s)} data samples")
    # print(f"Loaded {len(p12s)} data samples after outlier removal. ")

    i1s = (np.random.rand(5, 7) - 0.5) * 2
    i1s = jnp.stack([i1 + init_pose for i1 in i1s])
    # i1s = [init_pose]
    # i1s = [np.array([0, 0, 0, 0, 0, 0.0, 1])]
    # print(i1s)

    # # residuals = (predict(x, par))

    # W = np.eye(4)

    # print(world_transforms[0])
    # print(reg_transforms[0])
    # print(world_transforms[0] - np.linalg.inv(W) @ reg_transforms[0] @ W)

    # return

    # reg_transforms = []
    # fake_tr = np.array([
    #     [1, 0, 0, 0.5],
    #     [0, 1, 0, 10],
    #     [0, 0, 1, -50],
    #     [0, 0, 0, 1],
    # ])

    # for t1, t2 in zip(world_poses[:-1], world_poses[1:]):

    #     reg_transforms.append(np.linalg.inv(
    #         t1 @ fake_tr) @ (t2 @ fake_tr))

    # reg_transforms = np.stack(reg_transforms)

    # reg_tr = np.linalg.inv(
    #     world_poses[150] @ fake_tr) @ (world_poses[170] @ fake_tr)

    # world_tr = np.linalg.inv(world_poses[150]) @ (world_poses[170])

    # print("world_tr: ", world_tr)
    # print("reg_transforms: ", reg_tr)

    # # print(np.linalg.inv(fake_tr) @ reg_tr @ fake_tr)
    # print((fake_tr) @ reg_tr @ np.linalg.inv(fake_tr))
    # print(predict(reg_tr, fake_tr))
    # print(tf_mat_to_quat_pos(
    #     world_tr) - tf_mat_to_quat_pos(predict(reg_tr, fake_tr)))
    # # print((fake_tr) @ reg_transforms[1] @ np.linalg.inv(fake_tr))

    # return

    # results = np.stack([optimize(reg_transforms, world_transforms, i1, gt_z=gt_z)
    # results = np.stack([optimize(reg_transforms[150:], world_transforms[150:], i1, gt_z=gt_z)
    # results = np.stack([optimize(reg_poses[150:], world_poses[150:], i1, gt_z=gt_z)
    #                    for i1 in tqdm(i1s)])

    
    results = np.stack([optimize(reg_poses, world_poses, i1, gt_z=gt_z)
                       for i1 in tqdm(i1s)])

    # unique_results = np.unique(results, axis=0)
    unique_results = np.unique(results.round(decimals=5), axis=0)

    # unique_results = sorted(unique_results, key=lambda x: np.linalg.norm(
    #     xy_loss(x[:-1], world_transforms, reg_transforms, gt_z)))

    print('Results with loss value:')
    # [print(f'{res}, {np.linalg.norm(xy_loss(res[:-1], world_transforms, reg_transforms, gt_z)):.4f}')
    [print(f'{res}, {np.linalg.norm(loss(res, reg_transforms, world_transforms)):.4f}')
     for res in unique_results]

    save_transform_to_yaml(
        unique_results[0], save_file, replace_file_content=init_extrinsics_file)
    print('Saving result with minimum loss value')


if __name__ == '__main__':  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # parser.add_argument('-g', '--gnss', type=str, default=f'{sys.path[0]}/../../data/m11_pointclouds_ruby_l/poses/gt_poses.txt',
    parser.add_argument('-g', '--gnss', type=str, default=f'{sys.path[0]}/../results/latest/m11_pointclouds_ruby_l_gt.npy',
                        help='path to source folder with .pcd files')

    parser.add_argument('-r', '--reg', type=str, default="/apollo/modules/oscar/tools/calibration/results/latest/m11_pointclouds_ruby_l_poses.npy",
                        help='path to registration poses')

    parser.add_argument('-i', '--init_file', type=str, default="/apollo/modules/oscar/tools/calibration/lidar2gnss/lidar_ruby_left_extrinsics.yaml",
                        help='init transform_file')

    parser.add_argument('-s', '--save_file', type=str,
                        help='file with result transforms', default=f'{sys.path[0]}/res_ruby_left_extrinsics.yaml')

    args = parser.parse_args()

    main(args.gnss, args.reg, args.init_file, args.save_file)
