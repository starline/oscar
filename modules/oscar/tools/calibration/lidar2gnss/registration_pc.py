#!/apollo/bazel-bin/modules/oscar/tools/calibration/.venv/bin/python3

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
from datetime import datetime
import shutil
import sys

import open3d as o3d
import os

# import pygicp


import numpy as np

from tqdm.auto import tqdm

from common.common import load_transform_from_yaml, Vis, register_clouds, remove_ground_points, quat_pos_to_tf_mat


REMOVE_GROUND_POINTS = False

# bbox cropping params
THRESH = 1000
HEIGHT = 1


# def find_transform(source, target, tr_pc1_pc2):

#     cur_tr = pygicp.align_points(target, source,
#                                  initial_guess=np.linalg.inv(tr_pc1_pc2),
#                                  method='VGICP',
#                                  # neighbor_search_method='DIRECT7',
#                                  # k_correspondences=100,
#                                  voxel_resolution=0.05,
#                                  num_threads=24
#                                  )

#     return cur_tr


def main(folder, init_extrinsics_f, save_folder='default_reg', downsample=False, visualize=True, step=10):

    if visualize:
        visualizer = Vis()

    if os.path.exists(save_folder):
        shutil.rmtree(save_folder, ignore_errors=True)

    files = sorted(os.listdir(f"{folder}/pcds"))

    files = [f"{folder}/pcds/{f}" for f in files]

    poses_f = f"{folder}/poses/gt_poses.txt"
    poses = np.loadtxt(poses_f)

    # print(poses[0])
    # print(poses[0, 2:-3])
    # print(quat_pos_to_tf_mat(poses[0, 2:]))
    # return

    poses = np.stack([quat_pos_to_tf_mat(p[2:]) for p in poses])

    init_transformation = load_transform_from_yaml(init_extrinsics_f)

    files = files[::step]
    poses = poses[::step]

    diffs = []

    # for idx, (pose1, pose2) in tqdm(enumerate(zip(poses[:-1], poses[1:])), total=len(poses) - 1):
    # for idx, (pose1, pose2) in tqdm(enumerate(zip(poses[:-1], poses[1:]))):

    for (pose1, pose2, f1, f2) in tqdm(zip(poses[:-1], poses[1:], files[:-1], files[1:])):

        # if idx < 60:
        #     continue

        # f1 = f'{pose1[:-1].split(" ")[0]}.pcd'
        # f2 = f'{pose2[:-1].split(" ")[0]}.pcd'

        # r1, t1 = pose1[:-1].split(' ')[1:-3], pose1[:-1].split(' ')[-3:]
        # r2, t2 = pose2[:-1].split(' ')[1:-3], pose2[:-1].split(' ')[-3:]

        # r1 = np.array(r1).reshape((3, 3)).astype(np.float64)
        # r2 = np.array(r2).reshape((3, 3)).astype(np.float64)

        # p1 = np.eye(4, dtype=np.float64)
        # p2 = np.eye(4, dtype=np.float64)
        # p1[:-1, :-1] = r1
        # p2[:-1, :-1] = r2
        # p1[:-1, -1] = np.array(t1).astype(np.float64)
        # p2[:-1, -1] = np.array(t2).astype(np.float64)

        # dt1 = datetime.strptime(
        #     pose1[:-1].split(" ")[0], "%Y-%m-%d-%H-%M-%S-%f")
        # dt2 = datetime.strptime(
        #     pose2[:-1].split(" ")[0], "%Y-%m-%d-%H-%M-%S-%f")

        # diffs.append(dt2 - dt1)

        pc1 = o3d.io.read_point_cloud(f1)
        pc2 = o3d.io.read_point_cloud(f2)

        # Downsampling
        # if downsample:
        #     ds_size = 0.1
        #     pc1 = pc1.voxel_down_sample(ds_size)
        #     pc2 = pc2.voxel_down_sample(ds_size)

        # Croppping

        # min_b = np.array([-THRESH, -THRESH, -HEIGHT],
        #                  dtype=np.float64).reshape((3, 1))
        # max_b = np.array([THRESH, THRESH, THRESH],
        #                  dtype=np.float64).reshape((3, 1))
        # bbox = o3d.geometry.AxisAlignedBoundingBox(min_b, max_b)
        # pc1 = pc1.crop(bbox)
        # pc2 = pc2.crop(bbox)

        # # outlier removal
        # _, inds = pc1.remove_statistical_outlier(
        #     nb_neighbors=20, std_ratio=2.0)
        # pc1 = pc1.select_by_index(inds)
        # _, inds = pc2.remove_statistical_outlier(
        #     nb_neighbors=20, std_ratio=2.0)
        # pc2 = pc2.select_by_index(inds)

        tr_pc1_pc2 = np.linalg.inv(
            init_transformation) @ np.linalg.inv(pose1) @ pose2 @ init_transformation

        source = np.asarray(pc1.points)
        target = np.asarray(pc2.points)

        if REMOVE_GROUND_POINTS:
            source = remove_ground_points(source)
            target = remove_ground_points(target)

        # Transformation registration

        print(pose1)
        print(pose2)
        print()

        res_transform = register_clouds(pc1, pc2, tr_pc1_pc2)

        # print(np.linalg.inv(p1) @ p2)
        # print()
        # print(np.linalg.inv(tr_pc1_pc2))

        # print()

        # print(res_transform)
        # res_pc = dc(pc1).transform(res_transform)
        # fake_res_pc = dc(pc1).transform(np.linalg.inv(tr_pc1_pc2))
        # pc1.paint_uniform_color([1, 0, 0])
        # pc2.paint_uniform_color([0, 1, 0])
        # res_pc.paint_uniform_color([0, 0, 1])
        # fake_res_pc.paint_uniform_color([1, 0, 1])

        # o3d.visualization.draw_geometries([pc1, pc2, res_pc])

        # Saving result
        # if save_folder is not None:
        #     f_prefix = pose1[:-1].split(' ')[0]

        #     os.makedirs(f'{save_folder}', exist_ok=True)
        #     np.savetxt(f'{save_folder}/{f_prefix}_pose.txt', pose1)
        #     np.savetxt(f'{save_folder}/{f_prefix}_res_tr.txt', res_transform)

        if visualize:
            visualizer.update(pc2, res_transform)

    if save_folder is not None:
        print(save_folder)

    if visualize:
        visualizer.loop()

    return


if __name__ == '__main__':  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--pc_folder', type=str, default=f'{sys.path[0]}/pcds_infinity',
                        help='path to source folder with .pcd files')

    parser.add_argument('-s', '--step', type=int, default=10,
                        help='step')

    parser.add_argument('-r', '--reg_results', type=str,
                        help='folder to save registration results', default=f'{sys.path[0]}/reg_results')

    parser.add_argument('-i', '--init_extr', type=str, default=f'{sys.path[0]}/init_transform_ruby_l.yaml',
                        help='path to initial extrinsics file')

    args = parser.parse_args()

    main(args.pc_folder,
         args.init_extr,
         save_folder=args.reg_results,
         step=args.step
         )
