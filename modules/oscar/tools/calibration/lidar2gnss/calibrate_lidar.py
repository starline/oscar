#!/apollo/bazel-bin/modules/oscar/tools/calibration/.venv/bin/python3

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
import os
import sys


from lidar2gnss.registration_pc import main as register_transformations
from lidar2gnss.optimize_tranform import main as optimize_transform
from lidar2gnss.compare_plane_seg import main as compare_plane_seg
from lidar2gnss.merge_all_pc import main as show_merged_pc

line = '*' * 50

if __name__ == "__main__":  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-p', '--pc_folder', type=str, default=f'{sys.path[0]}/pcds_infinity',
                        help='path to source folder with .pcd files')

    parser.add_argument('-s', '--step', type=int, default=10,
                        help='step')

    parser.add_argument('-r', '--reg_results', type=str,
                        help='folder to save registration results', default=f'{sys.path[0]}/default_reg')

    parser.add_argument('-i', '--init_extr', type=str, default=f'{sys.path[0]}/init_transform_ruby_l.yaml',
                        help='path to initial extrinsics file')

    parser.add_argument('-o', '--res_extr', type=str, default=f'{sys.path[0]}/res_transform_default.yaml',
                        help='path to calibrated extrinsics file that will be created')

    parser.add_argument('-v', '--vis', action='store_true',
                        help='visualise')

    args = parser.parse_args()

    step = args.step
    pc_folder = args.pc_folder
    save_folder = args.reg_results
    init_extrinsics_file = args.init_extr
    res_transform_f = args.res_extr

    print(line)
    print('1/2 Transformation registration')
    print(line)
    # register_transformations(
    #     pc_folder, init_extrinsics_file, save_folder, visualize=args.vis, step=step)

    os.system(f"pipenv run kiss_icp_pipeline --visualize /apollo/modules/oscar/tools/data/m11_pointclouds_ruby_l/ --dataloader apollo")

    print(line)
    print('2/2 Optimization of transformation')
    print(line)
    optimize_transform(save_folder, init_extrinsics_file, res_transform_f)

    if args.vis:
        print(line)
        print('Checking of transformation: merging point clouds')
        print(line)
        show_merged_pc(pc_folder, init_extrinsics_file,
                       save_folder, res_transform_f)

        print(line)
        print(
            'Checking of transformation: comparing plane segmentation wrt initial transform')
        print(line)
        compare_plane_seg(pc_folder, init_extrinsics_file,
                          f'{pc_folder}/0_poses.txt', res_transform_f, step=step)

    else:
        print('Visualization turned off (flag -v)')
