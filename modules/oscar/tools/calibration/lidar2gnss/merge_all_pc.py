#!/apollo/bazel-bin/modules/oscar/tools/calibration/.venv/bin/python3

###############################################################################
# Copyright 2022 ScPA StarLine Ltd. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
import sys

import open3d as o3d
import os
from copy import deepcopy as dc

import numpy as np
from numpy.linalg import inv
from scipy.spatial.transform import Rotation

from tqdm.auto import tqdm

from common.common import load_transform_from_yaml

np.set_printoptions(precision=3, suppress=True)


SHOW_REGISTRATION = False
SHOW_INIT_TRANSFORMATION = True
SHOW_RESULT_TRANSFORMATION = True
SHOW_LIDAR = True


def main(pc_folder, init_extrinsics_f, localization_poses_file, registration_poses_file, res_transform_file):

    init_transformation = load_transform_from_yaml(init_extrinsics_f)
    res_transformation = load_transform_from_yaml(res_transform_file)

    localization_poses = np.load(localization_poses_file)
    registration_poses = np.load(registration_poses_file)

    files = [f for f in sorted(os.listdir(pc_folder)) if 'pcd' in f]

    step = 1

    print(len(files), localization_poses.shape, registration_poses.shape)

    files = files[::step]
    localization_poses = localization_poses[::step]
    registration_poses = registration_poses[::step]

    pcs = []
    if SHOW_INIT_TRANSFORMATION or SHOW_INIT_TRANSFORMATION or SHOW_REGISTRATION:
        for f in tqdm(files):
            pc = o3d.io.read_point_cloud(
                pc_folder + '/' + f).voxel_down_sample(0.5)

            THRESH = 100
            HEIGHT = 20
            min_b = np.array([-THRESH, -THRESH, -HEIGHT],
                             dtype=np.float64).reshape((3, 1))
            max_b = np.array([THRESH, THRESH, THRESH],
                             dtype=np.float64).reshape((3, 1))
            bbox = o3d.geometry.AxisAlignedBoundingBox(min_b, max_b)
            pc = pc.crop(bbox)
            pcs.append(pc)

    print(f'loaded {len(pcs)} point clouds')

    # draw transform with poses from registration
    if SHOW_REGISTRATION:
        print('Visualizing merged points from registration')

        reg_pcs = [dc(pc).transform(pose)
                   for pc, pose in zip(pcs, registration_poses)]
        print(len(reg_pcs))
        o3d.visualization.draw_geometries(reg_pcs)

    # draw transformed point clouds with init transformation
    if SHOW_INIT_TRANSFORMATION:

        print('Init transformation:')
        print(init_transformation)
        # init_poses_list = []
        # for idx, pose in enumerate(localization_poses):
        #     # pose = np.loadtxt(res_folder + '/' + pose_f, dtype=np.float64)
        #     init_poses_list.append(pose)

        init_lidar_poses_list = [
            p @ init_transformation for p in localization_poses]

        init_pcs = [dc(pc).transform(pose)
                    for pc, pose in zip(pcs, init_lidar_poses_list)]

        points = np.concatenate([np.asarray(pc.points) for pc in init_pcs])
        merged_pc = o3d.geometry.PointCloud()
        merged_pc.points = o3d.utility.Vector3dVector(points)
        merged_pc = merged_pc.voxel_down_sample(0.1)
        o3d.visualization.draw_geometries([merged_pc])
        print(len(merged_pc.points))

    # draw transformed point clouds with res transformation
    if SHOW_RESULT_TRANSFORMATION:
        res_poses_list = []

        # res_transformation[:-1, :-1] = init_transformation[:-1, :-1]

        print("Optimized transformation:")
        print(res_transformation)

        print(Rotation.from_matrix(
            res_transformation[:-1, :-1]).as_euler('xyz', degrees=True))

        for idx, pose in enumerate(localization_poses):
            # pose = np.loadtxt(res_folder + '/' + p_file)
            res_poses_list.append(pose @ res_transformation)

        res_pcs = [dc(pc).transform(pose)
                   for pc, pose in zip(pcs, res_poses_list)]

        points = np.concatenate([np.asarray(p.points) for p in res_pcs])
        merged_pc = o3d.geometry.PointCloud()
        merged_pc.points = o3d.utility.Vector3dVector(points)
        merged_pc = merged_pc.voxel_down_sample(0.1)
        print(len(merged_pc.points))
        o3d.visualization.draw_geometries([merged_pc])

    # draw lidar positions on world frame with init transfrom and result transform
    if SHOW_LIDAR:

        def convert_points(poses, color=(0, 0, 0), transform=None):
            points = []

            if transform is not None:
                for pose in poses:
                    points.append((pose @ transform)[:-1, -1])
            else:
                points = poses[:, :-1, -1]

            pc = o3d.geometry.PointCloud()
            pc.points = o3d.utility.Vector3dVector(np.array(points))

            pc.paint_uniform_color(color)

            return pc

        print(res_transformation)

        init_pc = convert_points(
            localization_poses, transform=init_transformation, color=(1, 0, 0))
        res_pc = convert_points(
            localization_poses, transform=res_transformation, color=(1, 1, 0))

        localization_pc = convert_points(localization_poses, color=(0, 0, 1))

        registrations = []
        for pose in registration_poses:
            registrations.append(
                ((init_transformation) @ pose)[:-1, -1])
            # ((init_transformation) @ pose @ np.linalg.inv(init_transformation))[:-1, -1])

        registration_pc = o3d.geometry.PointCloud()
        registration_pc.points = o3d.utility.Vector3dVector(
            np.array(registrations))

        registration_pc.paint_uniform_color([0, 1, 0])

        print(np.min(np.asarray(init_pc.points), axis=0))
        print(np.max(np.asarray(init_pc.points), axis=0))
        print()
        print(np.min(np.asarray(res_pc.points), axis=0))
        print(np.max(np.asarray(res_pc.points), axis=0))

        o3d.visualization.draw_geometries([
            init_pc,
            res_pc,
            localization_pc,
            # registration_pc
        ])


if __name__ == '__main__':  # pragma: no cover

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-f", "--pc_folder", type=str,
                        default="/apollo/modules/oscar/tools/data/m11_pointclouds_ruby_l/pcds")
    parser.add_argument("-i", "--init_transform_file", type=str,
                        default="/apollo/modules/oscar/tools/calibration/lidar2gnss/lidar_ruby_left_extrinsics.yaml")
    parser.add_argument("-l", "--localization_poses_file", type=str,
                        default="/apollo/modules/oscar/tools/calibration/results/latest/m11_pointclouds_ruby_l_gt.npy")
    parser.add_argument("-p", "--registration_poses_file", type=str,
                        default="/apollo/modules/oscar/tools/calibration/results/latest/m11_pointclouds_ruby_l_poses.npy")
    parser.add_argument("-r", "--result_transform_file", type=str,
                        default="/apollo/modules/oscar/tools/calibration/lidar2gnss/res_ruby_left_extrinsics.yaml")

    args = parser.parse_args()

    main(args.pc_folder,
         args.init_transform_file,
         args.localization_poses_file,
         args.registration_poses_file,
         args.result_transform_file
         )
