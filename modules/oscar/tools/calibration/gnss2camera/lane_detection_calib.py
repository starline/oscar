###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import argparse
import itertools
import math
import os
import sys
from copy import deepcopy as dc
import time
from threading import Lock

import yaml
import cv2 as cv
import numpy as np
from scipy.spatial.transform import Rotation as R

sys.path.append('/apollo')  # nopep8
sys.path.append("/apollo/bazel-bin")  # nopep8

from utils import load_camera_matrix, load_dimensions, load_distortion, load_transformation, draw_line, load_transformations, timeit
from car_dimensions_projector import CarDimensionsProjector
from extraction.utils import TFBuffer
from map_projector import MapProjector
from modules.common_msgs.sensor_msgs import sensor_image_pb2
from modules.common_msgs.transform_msgs import transform_pb2
from cyber.python.cyber_py3 import cyber, cyber_time

np.set_printoptions(precision=4, suppress=True)


def polar2cartesian(rho: float, theta_rad: float, rotate90: bool = False):
    x = np.cos(theta_rad) * rho
    y = np.sin(theta_rad) * rho
    m = np.nan
    if not np.isclose(x, 0.0):
        m = y / x
    if rotate90:
        if m is np.nan:
            m = 0.0
        elif np.isclose(m, 0.0):
            m = np.nan
        else:
            m = -1.0 / m
    b = 0.0
    if m is not np.nan:
        b = y - m * x

    return m, b


def intersection(m1: float, b1: float, m2: float, b2: float):
    # Consider y to be equal and solve for x
    # Solve:
    #   m1 * x + b1 = m2 * x + b2

    if m1 == m2:
        print(m1, m2)
    x = (b2 - b1) / (m1 - m2)
    # Use the value of x to calculate y
    y = m1 * x + b1

    return int(round(x)), int(round(y))


class LaneDetector:
    def __init__(self) -> None:
        pass

    def run(self, image):

        frame_HSV = cv.cvtColor(image, cv.COLOR_BGR2HSV)

        # row_shift = frame_HSV.shape[0] // 3
        row_shift = 0
        frame_HSV = frame_HSV[row_shift:, :]

        low_H = 0
        high_H = 255

        low_S = 0
        high_S = 80

        low_V = 180
        high_V = 255

        frame_threshold = cv.inRange(
            frame_HSV, (low_H, low_S, low_V), (high_H, high_S, high_V))

        frame_threshold = cv.erode(frame_threshold, np.ones((3, 3)))
        edges = cv.Canny(frame_threshold, 50, 150, apertureSize=3)
        lines = cv.HoughLines(edges, 1, np.pi / 180, 100)

        return lines if lines is not None else [], image


class LaneCalibrator:
    def __init__(self,
                 camera_intrinsics_file="",
                 undistort=True
                 ) -> None:

        self.angles_history = []
        self.centers_history = []

        self.loc_frame_height = 1
        self.width, self.height = load_dimensions(camera_intrinsics_file)

        self.M = load_camera_matrix(
            camera_intrinsics_file)

        self.enable_undistortion = undistort

        if self.enable_undistortion:
            self.D = load_distortion(
                camera_intrinsics_file)

            self.distortion_maps = cv.initUndistortRectifyMap(
                self.M, self.D, np.eye(3), self.M, (self.width, self.height), cv.CV_32FC1)

        self.lane_detector = LaneDetector()

        print("LaneCalibrator Init complete")

    def process_image(self, image):

        if self.enable_undistortion:
            image = cv.remap(
                image, *self.distortion_maps, cv.INTER_LINEAR)

        lines, drawing_image = self.lane_detector.run(image)
        if len(lines) < 4:
            print(f"Few lines: {len(lines)}")
            return None, drawing_image

        lines_params = []

        # row_shift = image.shape[0] // 3
        row_shift = 0

        thetas = []
        THETA_THRESH = 0.15
        for line in lines:
            rho = line[0][0]
            theta = line[0][1]

            if len(thetas) > 0 and min(np.abs(np.array(thetas) - theta)) < THETA_THRESH:
                continue
            thetas.append(theta)

            lines_params.append((polar2cartesian(rho, theta, True)))
            a = math.cos(theta)
            b = math.sin(theta)
            x0 = a * rho
            y0 = b * rho
            pt1 = (int(x0 + 1000 * (-b)), int(row_shift + y0 + 1000 * (a)))
            pt2 = (int(x0 - 1000 * (-b)), int(row_shift + y0 - 1000 * (a)))

            cv.line(drawing_image, pt1, pt2, (255, 0, 0), 1, cv.LINE_AA)

        lines_params = np.array(lines_params)

        intersections = []
        for (m1, b1), (m2, b2) in itertools.combinations(lines_params, 2):

            if m1 == m2:
                continue
            try:
                intersections.append(intersection(m1, b1, m2, b2))
            except ValueError:
                continue
        intersections = np.array(intersections).reshape((-1, 2))
        intersections[:, 1] += row_shift

        if len(intersections) == 0:
            return None, drawing_image

        mean_intersection = np.mean(intersections, axis=0, dtype=int)
        for p in intersections:
            cv.circle(drawing_image, (p[0], p[1]), 3, (0, 255, 0), -1)

        cv.circle(
            drawing_image, (mean_intersection[0], mean_intersection[1]), 2, (0, 255, 0), -1)

        self.centers_history.append(
            (mean_intersection[0], mean_intersection[1]))

        mean_c = np.mean(np.array(self.centers_history), axis=0).astype(int)
        cv.circle(drawing_image, (mean_c[0], mean_c[1]), 5, (255, 255, 0), -1)

        # compute yaw and pitch
        inv_M = np.linalg.inv(self.M)
        exp_vp = np.ones((3))
        exp_vp[:-1] = mean_intersection
        r3 = inv_M @ exp_vp
        r3 /= np.linalg.norm(r3)

        pitch = math.asin(r3[1])
        yaw = -math.atan2(r3[0], r3[2])

        self.angles_history.append((yaw, pitch))

        mean_angles = np.mean(
            np.array(self.angles_history), axis=0) * 180 / np.pi

        return mean_angles, drawing_image


def save_extrinsics(transform_matrix, file, frame_id):

    quat = R.from_matrix(transform_matrix[:-1, :-1]).as_quat()
    with open(file, 'w') as f:
        yaml.safe_dump({
            "header": {
                "frame_id": "novatel"
            },
            "transform": {
                "translation": {
                    "x": float(transform_matrix[0, -1]),
                    "y": float(transform_matrix[1, -1]),
                    "z": float(transform_matrix[2, -1]),
                },
                "rotation": {
                    "x": float(quat[0]),
                    "y": float(quat[1]),
                    "z": float(quat[2]),
                    "w": float(quat[3]),
                }
            },
            "child_frame_id": frame_id
        }, f)


class OrientationCalibrationNode:

    def __init__(self, camera_intrinsics_file, channel, loc2novatel_tr_file, novatel2cam_tr_file) -> None:

        self.calibrator = LaneCalibrator(
            camera_intrinsics_file=camera_intrinsics_file,
        )

        # wheel_radius = -0.1092
        wheel_radius = 0
        # wheel_radius = -0.5092

        self.map_projector = MapProjector(
            map_file="/apollo/modules/map/data/NISSAN_WITH_SIGNALS_FOR_VANYA/base_map.txt",
            camera_matrix=camera_intrinsics_file,
            localization2novatel_tr=loc2novatel_tr_file,
            novatel2camera_tr=novatel2cam_tr_file,
            left_line_color=(0, 0, 255),
            localization_frame_road_offset=wheel_radius,
            # localization_frame_height=-1.482 - 0.55,
            distance_threshold=2500,
        )

        self.car_dimensions_projector = CarDimensionsProjector(
            novatel2camera_tr=novatel2cam_tr_file,
            camera_matrix=camera_intrinsics_file,
            color=(0, 255, 0),
            interval=[5, 100],
            width=2.5,
            height=-(1.2 + wheel_radius),
            line_width=3
        )

        self.image_lock = Lock()
        self.current_image_msg = None

        self.updated_extrinsics_file = "out_extrinsics.yaml"

        self.camera_frame_id = "camera_left"

        cyber.init()
        self.node = cyber.Node("ImageLaneCalibrator")

        self.image_is_compressed = "compressed" in channel
        if self.image_is_compressed:
            image_type = sensor_image_pb2.CompressedImage
        else:
            image_type = sensor_image_pb2.Image

        self.writer = self.node.create_writer(
            "/camera_lane_det", image_type)
        self.image_reader = self.node.create_reader(
            channel, image_type, self.image_cb)

        self.tf_buffer = TFBuffer(node_for_callback=self.node)

        self.calib_tf_writer = self.node.create_writer(
            "/camera_tf_calibrated", transform_pb2.TransformStampeds)

    def image_cb(self, msg):
        with self.image_lock:
            self.current_image_msg = dc(msg)

    def process_image(self):

        with self.image_lock:

            if self.current_image_msg is None:
                return False

            current_msg = self.current_image_msg

            self.current_image_msg = None

        image = np.frombuffer(current_msg.data, np.uint8)
        if not self.image_is_compressed:
            image = image.reshape(
                (current_msg.height, current_msg.width, 3))
        else:
            image = cv.imdecode(image, cv.IMREAD_COLOR)

        calib_result, drawing_image = self.calibrator.process_image(image)

        if calib_result is not None:

            new_matrix = self.car_dimensions_projector.init_novatel2camera_tr
            new_matrix[:-1, :-1] = R.from_euler(
                "XY", [-90 + calib_result[1], calib_result[0]], degrees=True).as_matrix()

            save_extrinsics(new_matrix, self.updated_extrinsics_file,
                            self.camera_frame_id)

            self.car_dimensions_projector.update_camera_transform(new_matrix)
            self.map_projector.update_camera_transform(new_matrix)

        TS_DIFF_THRESHOLD = 0.05
        if not self.tf_buffer.empty():

            for i in range(2):
                loc_tf, tf_ts = self.tf_buffer.get_nearest(
                    current_msg.header.timestamp_sec, return_ts=True)

                diff = current_msg.header.timestamp_sec - tf_ts
                if diff < TS_DIFF_THRESHOLD:
                    break

                if i != 0:
                    print(
                        f"Big TS diff, {diff} for ts {current_msg.header.timestamp_sec}")

                    time.sleep(0.05)

            self.map_projector.draw(drawing_image, loc_tf)

        self.car_dimensions_projector.draw_car_dimensions(drawing_image)

        if self.image_is_compressed:
            current_msg.data = cv.imencode(".jpg", drawing_image)[1].tobytes()
        else:
            current_msg.data = drawing_image.tobytes()

        self.writer.write(current_msg)

        out_transform = transform_pb2.TransformStampeds()
        tr = out_transform.transforms.add()

        tr.header.frame_id = "novatel"
        tr.child_frame_id = "camera_left_calibrated"

        tr.transform.translation.x = self.car_dimensions_projector.novatel2camera_tr[0, -1]
        tr.transform.translation.y = self.car_dimensions_projector.novatel2camera_tr[1, -1]
        tr.transform.translation.z = self.car_dimensions_projector.novatel2camera_tr[2, -1]

        quat = R.from_matrix(
            self.car_dimensions_projector.novatel2camera_tr[:-1, :-1]).as_quat()
        tr.transform.rotation.qx, tr.transform.rotation.qy, tr.transform.rotation.qz, tr.transform.rotation.qw = quat

        self.calib_tf_writer.write(out_transform)

    def spin(self):

        while cyber.ok():
            if not self.process_image():
                time.sleep(0.05)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--cfg-file', type=str, default=f'{sys.path[0]}/cfg_annotation.yaml',
                        help='path config yaml file')

    parser.add_argument('-m', '--map', type=str,
                        default=None, help='Overwrite map file')
    parser.add_argument("--loc2novatel", type=str, default=None,
                        help="Overwrite transformation file between 'localization' frame and 'novatel' frame")
    parser.add_argument("--novatel2cam", type=str, default=None,
                        help="Overwrite transformation file between 'novatel' frame and 'camera' frame")
    parser.add_argument("--intrinsics", type=str, default=None,
                        help="Overwrite camera intrinsics file")
    parser.add_argument("--channel", type=str, default=None,
                        help="Overwrite Channel name for input image")

    args = parser.parse_args()

    print(args)

    tr = OrientationCalibrationNode(
        camera_intrinsics_file="/apollo/modules/calibration/data/mb_actros/perception_conf/sensor_params/camera_near_intrinsics.yaml",
        channel="/apollo/sensor/camera/near/image/compressed",
        loc2novatel_tr_file="/apollo/modules/calibration/data/mb_actros/gnss_conf/novatel_localization_extrinsics.yaml",
        novatel2cam_tr_file="/apollo/modules/calibration/data/mb_actros/perception_conf/sensor_params/camera_near_extrinsics.yaml"
    )

    # tr = OrientationCalibrationNode(
    #     camera_intrinsics_file="/apollo/modules/calibration/data/mb_actros/perception_conf/sensor_params/camera_left_intrinsics.yaml",
    #     channel="/apollo/sensor/camera/left/image/compressed",
    #     loc2novatel_tr_file="/apollo/modules/calibration/data/mb_actros/gnss_conf/novatel_localization_extrinsics.yaml",
    #     novatel2cam_tr_file="/apollo/modules/calibration/data/mb_actros/perception_conf/sensor_params/camera_left_extrinsics.yaml"
    # )

    tr.spin()
