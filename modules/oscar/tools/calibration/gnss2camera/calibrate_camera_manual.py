import argparse
import sys
import time
import cv2 as cv
import numpy as np
from scipy.spatial.transform import Rotation as R
import yaml

sys.path.append(f"{sys.path[0]}/../../camera_drawing")  # nopep8
from annotate_camera import ImagePainter
from utils import load_transformation

np.set_printoptions(precision=4, suppress=True)

HELP_STR = '''
q   w   e
a   s   d
w/s - pitch angle
a/d - yaw angle
q/e - roll angle

u   i   o
j   k   l
i/k - forward/backward (Y axis)
j/l - left/right (X axis)
u/o - up/down (Z axis)

-/+ - divide/multiply step value by 2

f - stop/resume capturing messages
m - show current matrix and translation, quaternion
h - show help
Esc - quit
'''


class UpdateHandler:

    def __init__(self, init_transform, matrices_to_update: list, painter) -> None:

        self.painter = painter
        if isinstance(matrices_to_update, np.ndarray):
            self.target_matrices_views = [matrices_to_update.view()]
        elif isinstance(matrices_to_update, list):
            self.target_matrices_views = [m.view() for m in matrices_to_update]
        else:
            raise f"supported argument types for matrices_to_update: List[np.ndarray], np.ndarray. Got: {type(matrices_to_update)}"

        self.rotation_x, self.rotation_y, self.rotation_z = R.from_matrix(
            init_transform[:-1, :-1]).as_euler('XYZ')

        self.shift_x, self.shift_y, self.shift_z = init_transform[:-1, -1]

        self.roll = np.linalg.norm(
            [self.rotation_x, self.rotation_y, self.rotation_z])
        self.update_val = 0.01

    def on_button(self, key):

        if key == ord('w'):
            print(
                f'changing x: {self.rotation_x:.4f} -> {(self.rotation_x - self.update_val):.4f}')
            self.rotation_x -= self.update_val

        elif key == ord('s'):
            print(
                f'changing X: {self.rotation_x:.4f} -> {(self.rotation_x + self.update_val):.4f}')
            self.rotation_x += self.update_val

        elif key == ord('a'):
            print(
                f'changing Y: {self.rotation_y:.4f} -> {(self.rotation_y + self.update_val):.4f}')
            self.rotation_y += self.update_val
        elif key == ord('d'):
            print(
                f'changing y: {self.rotation_y:.4f} -> {(self.rotation_y - self.update_val):.4f}')
            self.rotation_y -= self.update_val

        elif key == ord('q'):
            print(
                f'changing z: {self.rotation_z:.4f} -> {(self.rotation_z - self.update_val):.4f}')
            self.rotation_z -= self.update_val

        elif key == ord('e'):
            print(
                f'changing Z: {self.rotation_z:.4f} -> {(self.rotation_z + self.update_val):.4f}')
            self.rotation_z += self.update_val

        elif key == ord('l'):
            print(
                f'changing x shift: {self.shift_x:.4f} -> {(self.shift_x - self.update_val):.4f}')
            self.shift_x -= self.update_val

        elif key == ord('j'):
            print(
                f'changing X shift: {self.shift_x:.4f} -> {(self.shift_x + self.update_val):.4f}')
            self.shift_x += self.update_val

        elif key == ord('u'):
            print(
                f'changing z shift: {self.shift_z:.4f} -> {(self.shift_z - self.update_val):.4f}')
            self.shift_z -= self.update_val

        elif key == ord('o'):
            print(
                f'changing Z shift: {self.shift_z:.4f} -> {(self.shift_z + self.update_val):.4f}')
            self.shift_z += self.update_val

        elif key == ord('i'):
            print(
                f'changing y shift: {self.shift_y:.4f} -> {(self.shift_y - self.update_val):.4f}')
            self.shift_y -= self.update_val

        elif key == ord('k'):
            print(
                f'changing Y shift: {self.shift_y:.4f} -> {(self.shift_y + self.update_val):.4f}')
            self.shift_y += self.update_val

        elif key == ord('m'):
            self.print_matrix()
        elif key == ord('+'):
            print(
                f'changing step: {self.update_val:.4f} -> {(self.update_val * 2):.4f}')
            self.update_val *= 2
        elif key == ord('-'):
            print(
                f'changing step: {self.update_val:.4f} -> {(self.update_val / 2):.4f}')
            self.update_val /= 2
        elif key == ord('f'):

            self.painter.toggle_callbacks()

        elif key == ord('h'):
            print(HELP_STR)

        self.update_matrices()

    def print_matrix(self):
        print(self.target_matrices_views[0])

        print("rotation quaternion:", R.from_matrix(
            self.target_matrices_views[0][:-1, :-1]).as_quat())
        print("translation vector:",
              self.target_matrices_views[0][:-1, -1])

    def update_matrices(self):

        for m in self.target_matrices_views:
            m[:-1, :-1] = R.from_euler('XYZ', [self.rotation_x,
                                               self.rotation_y, self.rotation_z]).as_matrix()
            m[0, -1] = self.shift_x
            m[1, -1] = self.shift_y
            m[2, -1] = self.shift_z


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--cfg-file', type=str, default=f'{sys.path[0]}/cfg_calibrate.yaml',
                        help='path config yaml file')

    args = parser.parse_args()

    with open(args.cfg_file, 'r', encoding='utf-8') as f:
        cfg = yaml.safe_load(f)

    painter = ImagePainter(**cfg)

    init_transform = load_transformation(cfg['novatel2cam_tr_file'])
    updater = UpdateHandler(init_transform, [
                            painter.novatel2camera_tr], painter)

    init_values = R.from_matrix(
        painter.map_projector.novatel2camera_tr[:-1, :-1]).as_rotvec()

    cv.namedWindow("gui", cv.WINDOW_GUI_EXPANDED)
    cv.resizeWindow("gui", 1280, 720)

    print(HELP_STR)

    while True:

        if painter.current_image_msg is None:
            time.sleep(0.1)
            continue

        res_image = painter.process_image()
        final_im = cv.cvtColor(res_image, cv.COLOR_BGR2RGB)
        cv.imshow("gui", final_im)

        key = cv.waitKey(10)

        if key == 27:  # == Esc
            cv.destroyAllWindows()
            print("result matrix:")
            updater.print_matrix()
            break

        if key != -1:
            updater.on_button(key)
