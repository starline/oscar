# Fake Perception

Данный модуль служит для тестирования карты, модуля Prediction. Указанные в файле ```*.pb.txt``` препятствия периодически публикуются в канал.

## fake_perception.py

Скрипт публикует статичные препятствия.

### Параметры

 - ```-p```, ```--period``` - Период публикации сообщений;
 - ```-c```, ```--channel``` - Название канала для публикации, default: **/apollo/perception/obstacles**;
 - ```-o```, ```--obstacles_file``` - Путь к Protobuf файлу со статическими препятствиями, default: **/apollo/modules/tools/oscar_tools/fake_perception/static_obstacles.pb.txt**.


## fake_local_perception.py

Скрипт публикует препятствия, движущиеся сонаправленно и с такой же скоростью, как и ВАТС (статичные относительно ВАТС). Для этого происходит чтение канала **/apollo/localization/pose** и трансформация координат в глобальную СК.

### Параметры

 - ```-p```, ```--period``` - Период публикации сообщений;
 - ```-c```, ```--channel``` - Название канала для публикации, default: **/apollo/perception/obstacles**;
 - ```-o```, ```--obstacles_file``` - Путь к Protobuf файлу со статическими препятствиями, default: **/apollo/modules/tools/oscar_tools/fake_perception/static_local_obstacles.pb.txt**.
