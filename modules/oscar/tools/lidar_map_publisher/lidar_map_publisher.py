###############################################################################
# Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
#
# Created by Ivan Nenakhov <nenakhov.id@starline.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###############################################################################

import os
import re
import sys
import time
import argparse

import numpy as np

sys.path.append('/apollo')
sys.path.append("/apollo/bazel-bin")

from cyber.python.cyber_py3 import cyber

from modules.common_msgs.sensor_msgs import pointcloud_pb2
from modules.common_msgs.transform_msgs.transform_pb2 import TransformStampeds
from extraction.utils import MsgCache


def read_pointclouds(path, offset):

    assert os.path.exists(path), f"Path does not exist: {path}"

    if isinstance(path, str) and path.endswith(".npy"):

        point_cloud = np.loadtxt(path)

        if len(point_cloud.shape) == 1:
            point_cloud = point_cloud.reshape((-1, 4))

        chunk = pointcloud_pb2.PointCloud()
        point_cloud[:, :3] -= offset
        for point in point_cloud:
            chunk.point.add(
                x=point[0], y=point[1], z=point[2], intensity=int(point[3]))
        return chunk

    raise RuntimeError(f"Path not valid: {path}")


class ChunkProvider:

    def __init__(self, src_folder, chunk_distance, offset, cache_folder, frame="world", chunk_size=200) -> None:
        assert os.path.isdir(src_folder)

        self.src_folder = src_folder

        self.chunks = {}

        self.chunk_distance = chunk_distance

        self.chunk_size = chunk_size

        self.offset = offset
        self.frame = frame

        self.chunks_cache = MsgCache(
            cache_folder)

        pattern = re.compile(r'chunk_x(?P<x>\d+)_y(?P<y>\d+).npy')

        for f in sorted(os.listdir(self.src_folder)):
            if not f.endswith("npy"):
                print("Skipping file", f)
                continue

            match = pattern.match(f)

            assert match is not None, f

            x = int(match.group('x'))
            y = int(match.group('y'))

            self.chunks[(x, y)] = f'{self.src_folder}/{f}'

        assert len(self.chunks) > 1, f"Too few chunks: {len(self.chunks)}"

        # check_chunk_size
        chunk_sizes = np.array(list(self.chunks.keys()))

        xs = np.array(sorted(set(chunk_sizes[:, 0])))
        ys = np.array(sorted(set(chunk_sizes[:, 1])))
        diffs_x = np.unique(xs[1:] - xs[:-1])
        diffs_y = np.unique(ys[1:] - ys[:-1])

        assert len(diffs_x) == 1 and len(
            diffs_y) == 1, f"Chunk origin differences are not same. x: {diffs_x}, y: {diffs_y}"
        assert diffs_x[0] == diffs_y[0], "Chunks are not squared. x: {diffs_x[0]}, y: {diffs_y[0]}"

        self.chunk_size = diffs_x[0]

        self.current_chunks = {}

        print(f"Loaded {len(self.chunks)} chunks")
        print(f"Chunk size: {self.chunk_size} m")
        print(f"Render distance: {self.chunk_distance} m")

    def update_chunks(self, loc_x, loc_y):

        position = np.array([loc_x, loc_y])

        updated = False

        for x, y in self.chunks.keys():
            chunk_center = np.array([x, y]) + self.chunk_size // 2

            if np.linalg.norm(position - chunk_center) > self.chunk_distance:
                if (x, y) in self.current_chunks:
                    self.current_chunks.pop((x, y))
                    updated = True
                continue

            if (x, y) in self.current_chunks:
                continue

            chunk = self.chunks[(x, y)]
            if isinstance(chunk, str):

                filename = chunk

                cache_chunk = self.chunks_cache.load(filename)
                if cache_chunk is None:

                    chunk = read_pointclouds(filename, self.offset)

                    print("Saving chunk:", filename)
                    self.chunks_cache.save(chunk, filename)
                else:
                    # print("Loading chunk from cache", filename)
                    chunk = pointcloud_pb2.PointCloud.FromString(cache_chunk)

            self.current_chunks[(x, y)] = chunk

            updated = True

        return updated

    def get_current_chunks(self):

        pc_msg = pointcloud_pb2.PointCloud()
        pc_msg.header.timestamp_sec = time.time()
        pc_msg.header.frame_id = self.frame

        for pc in self.current_chunks.values():
            pc_msg.point.extend(pc.point)

        return pc_msg


class LidarMapPublisher:

    def __init__(self, chunks_folder, write_channel, chunk_distance, cache_folder, offset=np.zeros(3)):

        cyber.init()
        self.node = cyber.Node("LidarMapPublisher")
        self.write_channel = write_channel
        self.chunk_distance = chunk_distance

        self.offset = offset

        self.cache_folder = cache_folder

        self.static_tf_msg = TransformStampeds()
        tf = self.static_tf_msg.transforms.add()
        tf.header.frame_id = "world"
        tf.child_frame_id = "lidar_map_local_tf"

        tf.transform.translation.x = self.offset[0]
        tf.transform.translation.y = self.offset[1]
        tf.transform.translation.z = self.offset[2]

        tf.transform.rotation.qx = 0
        tf.transform.rotation.qy = 0
        tf.transform.rotation.qz = 0
        tf.transform.rotation.qw = 0

        self.chunk_provider = ChunkProvider(
            chunks_folder, self.chunk_distance, self.offset, self.cache_folder, frame=tf.child_frame_id)

        self.writer = self.node.create_writer(
            self.write_channel, pointcloud_pb2.PointCloud)

        self.tf_writer = self.node.create_writer(
            "/lidar_map_tf", TransformStampeds)

        self.tf_reader = self.node.create_reader(
            "/tf", TransformStampeds, self.tf_callback)

    def tf_callback(self, msg):

        translation = msg.transforms[0].transform.translation

        st = time.time()
        if self.chunk_provider.update_chunks(translation.x, translation.y):

            update_dur = time.time() - st
            merged_chunks = self.chunk_provider.get_current_chunks()

            dur = time.time() - st
            print(
                f"Sending cloud with {len(merged_chunks.point)} points, update time: {update_dur:.3f}, process time: {dur:.3f} s")
            self.writer.write(merged_chunks)

            self.tf_writer.write(self.static_tf_msg)

    def spin(self):
        self.node.spin()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--path', type=str,
                        help='path to chunks folder', default="/apollo/modules/oscar/tools/extraction/extraction/m11_chunks")
    parser.add_argument('-c', '--channel', type=str, help='name of channel to publish',
                        default="/lidar_map_pointcloud")
    parser.add_argument('-d', '--distance', type=float, help='distance to chunk to render',
                        default=400)

    parser.add_argument("--cache_folder", type=str, help="Cache folder",
                        default="/apollo/modules/oscar/tools/data/msgs_cache")

    parser.add_argument("-o", "--offset", type=float, nargs=3, help="offset (x, y, z) to subtract from points in world frame. Is needed to prevent jittering in FoxGlove",
                        default=[3e5, 6e6, 0])

    args = parser.parse_args()
    print(args)

    tr = LidarMapPublisher(args.path, args.channel,
                           args.distance, args.cache_folder, offset=args.offset)
    tr.spin()
