# !/usr/bin/python3

#############################################################################
# Copyright (C) 2024 ScPA StarLine Ltd - All Rights Reserved                #
#                                                                           #
# Created by Ilya Dovgopolik <ilya.dovgopolik@mail.ru>                      #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################

from array import *
import matplotlib.pyplot as plt
import numpy as np
import sys

class LowLevelLogParser:

    def twos_complement(self,value, bits):
        if value & (1 << (bits-1)):
            value -= 1 << bits
        return value

    def __init__(self):
        # filename = sys.argv[1]
        filename = input('Enter the path to log file: ')

        angles = array('i',[])
        tic_angles = array('i',[])
        torques_info = array('i',[])
        tic_torques_info = array('i', [])
        torques_command = array('i',[])
        tic_torques_command = array('i', [])

        with open (filename, 'r') as logfile:
            for line in logfile:

                if '0x001' in line:
                    splitted_line = line.split()
                    angle = (int(splitted_line[8], 16) << 8) | int(splitted_line[7], 16)
                    angle = self.twos_complement(angle, 16)
                    angles.append(angle)
                    tic_angle = int(splitted_line[13])
                    tic_angles.append(tic_angle)

                if '0x012' in line:
                    splitted_line = line.split()
                    torque_info = (int(splitted_line[10], 16) << 8) | int(splitted_line[9], 16)
                    tic_torque_info = int(splitted_line[13])
                    if splitted_line[8] == '0x01':
                        direction_info = -1
                    else: 
                        direction_info = 1

                    torques_info.append(torque_info*direction_info)
                    tic_torques_info.append(tic_torque_info)
                
                if '0x032' in line:
                    splitted_line = line.split()
                    torque_command = (int(splitted_line[10], 16) << 8) | int(splitted_line[9], 16)
                    tic_torque_command = int(splitted_line[13])
                    if splitted_line[8] == '0x01':
                        direction_command = -1
                    else: 
                        direction_command = 1

                    torques_command.append(torque_command*direction_command)
                    tic_torques_command.append(tic_torque_command)

        fig, (subfig1, subfig2, subfig3) = plt.subplots(3, 1)
        fig.tight_layout(pad=1.0)
        subfig1.plot(tic_angles, angles, linewidth=2.0)
        subfig1.grid(True)
        subfig1.title.set_text('Angle on steering wheel')
        subfig1.xaxis.set_label_position('bottom')
        subfig1.set_xlabel('Tics')
        subfig1.set_ylabel('Angle, degrees')
        subfig1.set_xlim([0, tic_angles[-1]])

        subfig2.plot(tic_torques_command, torques_command, linewidth=2.0)
        subfig2.grid(True)
        subfig2.title.set_text('Torque command to steering wheel')
        subfig2.xaxis.set_label_position('bottom')
        subfig2.set_xlabel('Tics')
        subfig2.set_ylabel('Torques, N*m')
        subfig2.set_xlim([0, tic_torques_command[-1]])

        subfig3.plot(tic_torques_info, torques_info, linewidth=2.0)
        subfig3.grid(True)
        subfig3.title.set_text('Torque on steering wheel')
        subfig3.xaxis.set_label_position('bottom')
        subfig3.set_xlabel('Tics')
        subfig3.set_ylabel('Torques, N*m')
        subfig3.set_xlim([0, tic_torques_info[-1]])

        plt.show()

if __name__ == '__main__':

    player = LowLevelLogParser()