/******************************************************************************
 * Copyright 2017 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/drivers/lidar/velodyne/fusion/pri_sec_fusion_component.h"

#include <memory>
#include <thread>

namespace apollo {
namespace drivers {
namespace velodyne {

using apollo::cyber::Time;

bool PriSecFusionComponent::Init() {
  if (!GetProtoConfig(&conf_)) {
    AWARN << "Load config failed, config file" << ConfigFilePath();
    return false;
  }
  buffer_ptr_ = apollo::transform::Buffer::Instance();

  fusion_writer_ = node_->CreateWriter<PointCloud>(conf_.fusion_channel());

  for (const auto& channel : conf_.input_channel()) {
    auto reader = node_->CreateReader<PointCloud>(channel);
    readers_.emplace_back(reader);
  }
  return true;
}

bool PriSecFusionComponent::Proc(
    const std::shared_ptr<PointCloud>& point_cloud) {
  // поменять логику: сделать по отдельному треду, которые будут отдельно
  // трансформировать облака точек с каждого канала, потом объединять их.

  ZoneScopedN("Fusion");
  AERROR << "Fusion transport diff: "
         << Time::Now().ToSecond() - point_cloud->header().timestamp_sec();

  AERROR << "Fusion transport latency: "
         << apollo::cyber::Time::Now().ToSecond() -
                point_cloud->header().lidar_timestamp() / 1e9;

  auto start_time = Time::Now().ToSecond();
  auto target = std::make_shared<PointCloud>(*point_cloud);
  auto fusion_readers = readers_;
  do {
    ZoneScopedN("Fusion reader Cycle");
    for (auto itr = fusion_readers.begin(); itr != fusion_readers.end();) {
      (*itr)->Observe();
      if (!(*itr)->Empty()) {
        // проводить трансформацию прямо в объекте source???

        std::shared_ptr<apollo::drivers::PointCloud> source;
        // auto source = (*itr)->GetLatestObserved();
        {
          ZoneScopedN("Reading");
          source = (*itr)->GetLatestObserved();
        }
        if (conf_.drop_expired_data() && IsExpired(target, source)) {
          ++itr;
        } else {
          // распараллелить???
          ZoneScopedN("Fusion");
          Fusion(target, source);
          itr = fusion_readers.erase(itr);
        }
      } else {
        ++itr;
      }
    }

    // всегда спим 5 мсек???
    std::this_thread::sleep_for(std::chrono::milliseconds(5));

    // OSCAR changes: move while to the end of cycle to grant execution of one
    // cycle if wait_time_s == 0
  } while ((Time::Now().ToSecond() - start_time) < conf_.wait_time_s() &&
           fusion_readers.size() > 0);

  double end_time = cyber::Time::Now().ToSecond();
  double module_latency = (end_time - start_time) * 1e3;

  AINFO << std::setprecision(16) << "FRAME_STATISTICS: process time(msec)["
        << module_latency << "], cur_latency: "
        << (end_time - point_cloud->measurement_time()) * 1000;

  target->mutable_header()->set_timestamp_sec(end_time);

  {
    ZoneScopedN("Fusion write");

    std::string info = "Points: " + std::to_string(target->point().size());
    ZoneText(info.c_str(), info.size());
    fusion_writer_->Write(target);
  }

  TracyPlot("Fusion points", static_cast<int64_t>(target->point().size()));

  return true;
}

bool PriSecFusionComponent::IsExpired(
    const std::shared_ptr<PointCloud>& target,
    const std::shared_ptr<PointCloud>& source) {
  auto diff = target->measurement_time() - source->measurement_time();

  AINFO << "Diff: [" << target->header().frame_id() << " -> "
        << source->header().frame_id() << "]: " << diff;
  return diff * 1000 > conf_.max_interval_ms();
}

bool PriSecFusionComponent::QueryPoseAffine(const std::string& target_frame_id,
                                            const std::string& source_frame_id,
                                            Eigen::Affine3d* pose) {
  std::string err_string;

  // бесполезная проверка???
  if (!buffer_ptr_->canTransform(target_frame_id, source_frame_id,
                                 cyber::Time(0), 0.02f, &err_string)) {
    AERROR << "Can not find transform. "
           << "target_id:" << target_frame_id << " frame_id:" << source_frame_id
           << " Error info: " << err_string;
    return false;
  }
  apollo::transform::TransformStamped stamped_transform;
  try {
    stamped_transform = buffer_ptr_->lookupTransform(
        target_frame_id, source_frame_id, cyber::Time(0));
  } catch (tf2::TransformException& ex) {
    AERROR << ex.what();
    return false;
  }
  *pose =
      Eigen::Translation3d(stamped_transform.transform().translation().x(),
                           stamped_transform.transform().translation().y(),
                           stamped_transform.transform().translation().z()) *
      Eigen::Quaterniond(stamped_transform.transform().rotation().qw(),
                         stamped_transform.transform().rotation().qx(),
                         stamped_transform.transform().rotation().qy(),
                         stamped_transform.transform().rotation().qz());
  return true;
}

void PriSecFusionComponent::AppendPointCloud(
    std::shared_ptr<PointCloud> point_cloud,
    std::shared_ptr<PointCloud> point_cloud_add, const Eigen::Affine3d& pose) {
  // почему считаем, что трансформация может быть nan???
  if (std::isnan(pose(0, 0))) {
    // нельзя скопировать сразу все точки???
    for (auto& point : point_cloud_add->point()) {
      PointXYZIT* point_new = point_cloud->add_point();
      point_new->set_intensity(point.intensity());
      point_new->set_timestamp(point.timestamp());
      point_new->set_x(point.x());
      point_new->set_y(point.y());
      point_new->set_z(point.z());
    }
  } else {
    for (auto& point : point_cloud_add->point()) {
      // зачем копировать nan точки???
      if (std::isnan(point.x())) {
        // можно ли зарезервировать место заранее???
        PointXYZIT* point_new = point_cloud->add_point();

        // можно скопировать сразу объект точки, а не ее поля?

        // add_ponit, intensity и timestamp можно вынести за if
        point_new->set_intensity(point.intensity());
        point_new->set_timestamp(point.timestamp());
        point_new->set_x(point.x());
        point_new->set_y(point.y());
        point_new->set_z(point.z());
      } else {
        PointXYZIT* point_new = point_cloud->add_point();
        point_new->set_intensity(point.intensity());
        point_new->set_timestamp(point.timestamp());
        Eigen::Matrix<float, 3, 1> pt(point.x(), point.y(), point.z());
        point_new->set_x(static_cast<float>(
            pose(0, 0) * pt.coeffRef(0) + pose(0, 1) * pt.coeffRef(1) +
            pose(0, 2) * pt.coeffRef(2) + pose(0, 3)));
        point_new->set_y(static_cast<float>(
            pose(1, 0) * pt.coeffRef(0) + pose(1, 1) * pt.coeffRef(1) +
            pose(1, 2) * pt.coeffRef(2) + pose(1, 3)));
        point_new->set_z(static_cast<float>(
            pose(2, 0) * pt.coeffRef(0) + pose(2, 1) * pt.coeffRef(1) +
            pose(2, 2) * pt.coeffRef(2) + pose(2, 3)));
      }
    }
  }

  int new_width = point_cloud->point_size() / point_cloud->height();
  point_cloud->set_width(new_width);
}

bool PriSecFusionComponent::Fusion(std::shared_ptr<PointCloud> target,
                                   std::shared_ptr<PointCloud> source) {
  Eigen::Affine3d pose;
  if (QueryPoseAffine(target->header().frame_id(), source->header().frame_id(),
                      &pose)) {
    ZoneScopedN("Append");
    AppendPointCloud(target, source, pose);
    return true;
  }
  return false;
}

}  // namespace velodyne
}  // namespace drivers
}  // namespace apollo
