load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

load("@bazel_tools//tools/build_defs/repo:git.bzl", "new_git_repository")


def repo():
    http_archive(
    name = "com_github_mcap_cpp",
    build_file = "//third_party/mcap:mcap.BUILD",
    sha256 = "1cb2ae9f2e910eeb2e93b3ab722744d1805b9da45764e4fd88703b669413350d",
    strip_prefix = "mcap-releases-cpp-v1.1.0",
    patches =["//third_party/mcap:suppress_unused.patch"],
    patch_args = ["-p1"],
    urls = ["https://github.com/foxglove/mcap/archive/refs/tags/releases/cpp/v1.1.0.tar.gz"],
    )

    http_archive(
        name = "com_github_zstd",
        build_file = "//third_party/mcap:zstd.BUILD",
        sha256 = "7c42d56fac126929a6a85dbc73ff1db2411d04f104fae9bdea51305663a83fd0",
        strip_prefix = "zstd-1.5.2",
        urls = ["https://github.com/facebook/zstd/releases/download/v1.5.2/zstd-1.5.2.tar.gz"]
    )

    http_archive(
        name = "com_github_lz4",
        build_file = "//third_party/mcap:lz4.BUILD",
        sha256 = "0b0e3aa07c8c063ddf40b082bdf7e37a1562bda40a0ff5272957f3e987e0e54b",
        strip_prefix = "lz4-1.9.4",
        urls = ["https://github.com/lz4/lz4/archive/refs/tags/v1.9.4.tar.gz"],
    )
