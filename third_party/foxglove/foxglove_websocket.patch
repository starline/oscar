diff --git a/cpp/foxglove-websocket/include/foxglove/websocket/common.hpp b/cpp/foxglove-websocket/include/foxglove/websocket/common.hpp
index d3ef9af..dbbea24 100644
--- a/cpp/foxglove-websocket/include/foxglove/websocket/common.hpp
+++ b/cpp/foxglove-websocket/include/foxglove/websocket/common.hpp
@@ -16,10 +16,10 @@ constexpr char CAPABILITY_PARAMETERS_SUBSCRIBE[] = "parametersSubscribe";
 constexpr char CAPABILITY_SERVICES[] = "services";
 constexpr char CAPABILITY_CONNECTION_GRAPH[] = "connectionGraph";
 
-constexpr std::array DEFAULT_CAPABILITIES = {
-  CAPABILITY_CLIENT_PUBLISH, CAPABILITY_CONNECTION_GRAPH, CAPABILITY_PARAMETERS_SUBSCRIBE,
-  CAPABILITY_PARAMETERS,     CAPABILITY_SERVICES,
-};
+// constexpr std::array DEFAULT_CAPABILITIES = {
+//   CAPABILITY_CLIENT_PUBLISH, CAPABILITY_CONNECTION_GRAPH, CAPABILITY_PARAMETERS_SUBSCRIBE,
+//   CAPABILITY_PARAMETERS,     CAPABILITY_SERVICES,
+// };
 
 using ChannelId = uint32_t;
 using ClientChannelId = uint32_t;
diff --git a/cpp/foxglove-websocket/include/foxglove/websocket/websocket_logging.hpp b/cpp/foxglove-websocket/include/foxglove/websocket/websocket_logging.hpp
index 22e463b..2e6b8ef 100644
--- a/cpp/foxglove-websocket/include/foxglove/websocket/websocket_logging.hpp
+++ b/cpp/foxglove-websocket/include/foxglove/websocket/websocket_logging.hpp
@@ -1,6 +1,7 @@
 #pragma once
 
-#include <asio/ip/address.hpp>
+#include <boost/asio/ip/address.hpp>
+// #include <asio/ip/address.hpp>
 #include <websocketpp/logger/levels.hpp>
 
 #include <functional>
@@ -11,7 +12,7 @@ namespace foxglove {
 
 using LogCallback = std::function<void(WebSocketLogLevel, char const*)>;
 
-inline std::string IPAddressToString(const asio::ip::address& addr) {
+inline std::string IPAddressToString(const boost::asio::ip::address& addr) {
   if (addr.is_v6()) {
     return "[" + addr.to_string() + "]";
   }
diff --git a/cpp/foxglove-websocket/include/foxglove/websocket/websocket_server.hpp b/cpp/foxglove-websocket/include/foxglove/websocket/websocket_server.hpp
index 66660ca..e109f91 100644
--- a/cpp/foxglove-websocket/include/foxglove/websocket/websocket_server.hpp
+++ b/cpp/foxglove-websocket/include/foxglove/websocket/websocket_server.hpp
@@ -215,6 +215,7 @@ inline Server<ServerConfiguration>::Server(std::string name, LogCallback logger,
   _server.get_elog().set_callback(_logger);
 
   std::error_code ec;
+
   _server.init_asio(ec);
   if (ec) {
     throw std::runtime_error("Failed to initialize websocket server: " + ec.message());
@@ -239,7 +240,7 @@ inline Server<ServerConfiguration>::~Server() {}
 
 template <typename ServerConfiguration>
 inline void Server<ServerConfiguration>::socketInit(ConnHandle hdl) {
-  std::error_code ec;
+  boost::system::error_code ec;
   _server.get_con_from_hdl(hdl)->get_raw_socket().set_option(Tcp::no_delay(true), ec);
   if (ec) {
     _server.get_elog().write(RECOVERABLE, "Failed to set TCP_NODELAY: " + ec.message());
@@ -465,9 +466,9 @@ inline void Server<ServerConfiguration>::start(const std::string& host, uint16_t
     throw std::runtime_error("Server already started");
   }
 
-  std::error_code ec;
+  websocketpp::lib::error_code ec;
 
-  _server.listen(host, std::to_string(port), ec);
+  _server.listen(port, ec);
   if (ec) {
     throw std::runtime_error("Failed to listen on port " + std::to_string(port) + ": " +
                              ec.message());
@@ -488,9 +489,10 @@ inline void Server<ServerConfiguration>::start(const std::string& host, uint16_t
     throw std::runtime_error("WebSocket server failed to listen on port " + std::to_string(port));
   }
 
-  auto endpoint = _server.get_local_endpoint(ec);
-  if (ec) {
-    throw std::runtime_error("Failed to resolve the local endpoint: " + ec.message());
+  boost::system::error_code ec2;
+  auto endpoint = _server.get_local_endpoint(ec2);
+  if (ec2) {
+    throw std::runtime_error("Failed to resolve the local endpoint: " + ec2.message());
   }
 
   const std::string protocol = _options.useTls ? "wss" : "ws";
@@ -1138,7 +1140,7 @@ inline void Server<ServerConfiguration>::sendServiceResponse(ConnHandle clientHa
 
 template <typename ServerConfiguration>
 inline uint16_t Server<ServerConfiguration>::getPort() {
-  std::error_code ec;
+  boost::system::error_code ec;
   auto endpoint = _server.get_local_endpoint(ec);
   if (ec) {
     throw std::runtime_error("Server not listening on any port. Has it been started before?");
@@ -1196,10 +1198,10 @@ inline void Server<ServerConfiguration>::updateConnectionGraph(
   }
 
   std::vector<std::string> removedTopics, removedServices;
-  std::set_difference(knownTopicNames.begin(), knownTopicNames.end(), topicNames.begin(),
-                      topicNames.end(), std::back_inserter(removedTopics));
-  std::set_difference(knownServiceNames.begin(), knownServiceNames.end(), serviceNames.begin(),
-                      serviceNames.end(), std::back_inserter(removedServices));
+  std::copy_if(knownTopicNames.begin(), knownTopicNames.end(), std::back_inserter(removedTopics), 
+               [&topicNames](const std::string& topic) {return topicNames.find(topic) == topicNames.end();});
+  std::copy_if(knownServiceNames.begin(), knownServiceNames.end(), std::back_inserter(removedServices), 
+               [&serviceNames](const std::string& service) {return serviceNames.find(service) == serviceNames.end();});
 
   if (publisherDiff.empty() && subscriberDiff.empty() && servicesDiff.empty() &&
       removedTopics.empty() && removedServices.empty()) {
