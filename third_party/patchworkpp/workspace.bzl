load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

def repo():
    http_archive(
        name = "com_github_patchworkpp",
        build_file = "//third_party/patchworkpp:patchworkpp.BUILD",
        sha256 = "d1ed145e6ddc2e42dfebce29d18d68cadb3b781f0c34c8d85a5b07fe1429453f",
        strip_prefix = "patchwork-plusplus-dc33f599aae03b7f2388b429adc39d5403347ee6",
        urls = ["https://github.com/url-kaist/patchwork-plusplus/archive/dc33f599aae03b7f2388b429adc39d5403347ee6.zip"],
    )


