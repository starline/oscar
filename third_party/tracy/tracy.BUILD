# taken from https://github.com/wolfpld/tracy/issues/717

load("@bazel_skylib//rules:common_settings.bzl", "bool_flag")

bool_flag(
    name = "enable_tracy",
    build_setting_default = False,
)

config_setting(
    name = "do_enable_tracy",
    flag_values = {":enable_tracy": "True"},
)

TRACY_SOURCE_FILES = [
    "public/TracyClient.cpp",
    "public/client/TracyArmCpuTable.hpp",
    "public/client/TracyCallstack.hpp",
    "public/client/TracyCpuid.hpp",
    "public/client/TracyDebug.hpp",
    "public/client/TracyDxt1.hpp",
    "public/client/TracyFastVector.hpp",
    "public/client/TracyLock.hpp",
    "public/client/TracyProfiler.hpp",
    "public/client/TracyRingBuffer.hpp",
    "public/client/TracyScoped.hpp",
    "public/client/TracyStringHelpers.hpp",
    "public/client/TracySysPower.hpp",
    "public/client/TracySysTime.hpp",
    "public/client/TracySysTrace.hpp",
    "public/client/TracyThread.hpp",
    "public/client/tracy_rpmalloc.hpp",
    "public/common/TracyAlign.hpp",
    "public/common/TracyAlloc.hpp",
    "public/common/TracyColor.hpp",
    "public/common/TracyForceInline.hpp",
    "public/common/TracyMutex.hpp",
    "public/common/TracyProtocol.hpp",
    "public/common/TracyQueue.hpp",
    "public/common/TracySocket.hpp",
    "public/common/TracyStackFrames.hpp",
    "public/common/TracySystem.hpp",
    "public/common/TracyUwp.hpp",
    "public/common/TracyVersion.hpp",
    "public/common/TracyYield.hpp",
    "public/common/tracy_lz4.hpp",
    "public/common/tracy_lz4hc.hpp",
    "public/libbacktrace/backtrace.hpp",
    "public/libbacktrace/filenames.hpp",
    "public/libbacktrace/internal.hpp",
    "public/tracy/TracyD3D11.hpp",
    "public/tracy/TracyD3D12.hpp",
    "public/tracy/TracyLua.hpp",
    "public/tracy/TracyOpenCL.hpp",
    "public/tracy/TracyOpenGL.hpp",
]

# .cpp files are included, we need a local_hdrs to better hide them
TRACY_HEADER_FILES = [
    "public/client/TracyAlloc.cpp",
    "public/client/TracyCallstack.cpp",
    "public/client/TracyCallstack.h",
    "public/client/TracyDxt1.cpp",
    "public/client/TracyOverride.cpp",
    "public/client/TracyProfiler.cpp",
    "public/client/TracySysPower.cpp",
    "public/client/TracySysTime.cpp",
    "public/client/TracySysTrace.cpp",
    "public/client/tracy_SPSCQueue.h",
    "public/client/tracy_concurrentqueue.h",
    "public/client/tracy_rpmalloc.cpp",
    "public/common/TracyApi.h",
    "public/common/TracySocket.cpp",
    "public/common/TracyStackFrames.cpp",
    "public/common/TracySystem.cpp",
    "public/common/tracy_lz4.cpp",
    "public/common/tracy_lz4hc.cpp",
    "public/libbacktrace/alloc.cpp",
    "public/libbacktrace/config.h",
    "public/libbacktrace/dwarf.cpp",
    "public/libbacktrace/elf.cpp",
    "public/libbacktrace/fileline.cpp",
    "public/libbacktrace/macho.cpp",
    "public/libbacktrace/mmapio.cpp",
    "public/libbacktrace/posix.cpp",
    "public/libbacktrace/sort.cpp",
    "public/libbacktrace/state.cpp",
    "public/tracy/Tracy.hpp",  # This is the one public header.
    "public/tracy/TracyC.h",
]

# This list defines the features that Tracy will support. At a minimum,
# TRACY_ENABLE needs to be defined in order to support any instrumentation.
TRACY_ENABLED_FEATURES = [
    # Variables can only be set/unset, value doesn't matter, uncomment to
    # set to a non-default.
    #"TRACY_ON_DEMAND",
    "TRACY_CALLSTACK=10",
    #"TRACY_NO_CALLSTACK",
    #"TRACY_NO_CALLSTACK_INLINES",
    #"TRACY_ONLY_LOCALHOST",
    #"TRACY_NO_BROADCAST",
    #"TRACY_ONLY_IPV4",
    #"TRACY_NO_CODE_TRANSFER",
    #"TRACY_NO_CONTEXT_SWITCH",
    "TRACY_NO_EXIT",
    #"TRACY_NO_SAMPLING",
    #"TRACY_NO_VERIFY",
    #"TRACY_NO_VSYNC_CAPTURE",
    # "TRACY_NO_FRAME_IMAGE",
    #"TRACY_NO_SYSTEM_TRACING",
    #"TRACY_PATCHABLE_NOPSLEDS",
    "TRACY_DELAYED_INIT",
    #"TRACY_MANUAL_LIFETIME",
    #"TRACY_FIBERS",
    # "TRACY_NO_CRASH_HANDLER",
    #"TRACY_TIMER_FALLBACK",
    "TRACY_ENABLE",
]

# Include this library as a dependency if you have a target (like a unit test)
# that needs to always include support for Tracy without having to rely on a
# flag being passed. Prefer to use ":client" whenever possible; see the
# documents associated with that target for more
# details.
cc_library(
    name = "client_enabled",
    srcs = TRACY_SOURCE_FILES,
    hdrs = TRACY_HEADER_FILES,
    defines = TRACY_ENABLED_FEATURES,
    includes = ["public"],
    linkopts = ["-ldl"],
    visibility = ["//visibility:public"],
)

cc_library(
    name = "client_disabled",
    srcs = TRACY_SOURCE_FILES,
    hdrs = TRACY_HEADER_FILES,
    includes = ["public"],
    linkopts = ["-ldl"],
    visibility = ["//visibility:public"],
)

# The ":client_enabled" project will be picked as a dependency if The
# `--@com_github_tracy//:enable_tracy=True` flag is specified; if the flag is
# set to False or omitted, ":client_disabled" will be picked instead.
cc_library(
    name = "tracy",
    linkopts = ["-ldl"],
    visibility = ["//visibility:public"],
    deps = select({
        ":do_enable_tracy": [":client_enabled"],
        "//conditions:default": [":client_disabled"],
    }),
)

####################### Old BUILD file with cmake invocation #######################

# load("@rules_foreign_cc//foreign_cc:defs.bzl", "cmake")

# package(default_visibility = ["//visibility:public"])

# filegroup(
#     name = "tracy_files",
#     srcs = glob(["**/*"]),
# )

# cmake(
#     name = "tracy",
#     build_args = [
#         "-j 12",
#     ],
#     copts = [
#         "-fPIC",
#     ],
#     generate_args = [
#         "-DTRACY_NO_FRAME_IMAGE=ON",
#         # "-DTRACY_NO_SYSTEM_TRACING=ON",
#         "-DTRACY_NO_EXIT=ON",
#         # "-DTRACY_NO_CALLSTACK=ON",
#         # "-DTRACY_NO_CODE_TRANSFER=ON",
#         # "-DTRACY_NO_CONTEXT_SWITCH=ON",
#         "-DTRACY_DELAYED_INIT=ON",
#         "-DTRACY_ENABLE=OFF",  # by default we turn off profiling, overriding this value in bazel.rc
#         "-DBUILD_SHARED_LIBS=ON",
#     ],
#     lib_source = ":tracy_files",
#     out_shared_libs = [
#         "libTracyClient.so",
#         # "libTracyClient.so.0.10.0"
#     ],
# )