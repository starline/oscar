### Общая архитектура проекта

![platform](images/soft_hard_description/oscar_scheme_semi_v1.png)


<br />

### Беспилотный стек

[Cyber RT](cyber/README.md)

[FoxGlove](../../modules/oscar/foxglove/README.md)

[Dreamview](modules/dreamview/README.md)

[Драйверы аппаратного обеспечения](modules/drivers/README.md)

[Alpha - взаимодействие с автомобилем](../../modules/oscar/alpha/README.md)

[Модуль трансформаций](modules/transform/README.md)

[Модуль управления](modules/control/README.md)

[Модуль планирования движения](modules/planning/README.md)

[Модуль локализации](modules/localization/README.md)

[Модуль прогнозирования](modules/prediction/README.md)

[Модуль распознавания](modules/perception/README.md)

[HD-карты](modules/maps/README.md)

[Модуль мониторинга состояний модулей Supervisor](modules/supervisor/README.md)


<br />

Значительная часть стека базируется на наработках исходного проекта
[apollo.auto](https://github.com/ApolloAuto/apollo), но имеет множество дополнений и правок, обеспечивающих работоспособность стека и предоставляющих дополнительные инструменты (визуализация, калибровка, конвертация, ...).

Начать работу с беспилотным стеком следует с [инcтрукции по установке и сборке](docs/oscar/README.md). Использование [oscar_tools](oscar_tools.md) взаимодействия со средой разработки **обязательно**.


<br />

### Мобильная платформа

![car](images/soft_hard_description/truck_2024.png)

Мобильная платформа представляет собой транспортное средство с установленным
[ПАК Alpha](https://alpha.starline.ru/), бортовым вычислителем, бортовой системой питания, системой омывания сенсорики и самой сенсорики.

Избыточный состав сенсорики устанавливается для исследовательских целей и может
быть значительно сокращет колличественно и качественно сохранив возможность работы беспилотного стека в рамках различных приложений.


<br />

### Серверная платформа

Как и Apollo, в рамках проекта OSCAR разрабатывается ряд закрытых серверных
решений:

* **HDMapCloud** - сервис хранения и валидации HD-карт. Поддерживает [ApolloOpenDrive](https://gitlab.com/starline/maps/hdmapconverter) и [LaneLet2](https://github.com/fzi-forschungszentrum-informatik/Lanelet2). Сервис доступен публично https://maps.starline.ru/hd-maps/viewer.

* **BlueBottle** - платформа для автоматизации общего назначения. Имеет микросервисную архитектуру и позволяет запускать сценарии вычислений и обработки данных на небольших кластерах над распределенно хранящимися данными (бортовые вычислители, стационарные ПК, серверное хранилище).

* **Fleet API** - сервисы и API для телеметрии и удаленного доступа к флоту.


<br />

### Симулятор

#### CARLA

Поддержка CARLA обеспечивается через [bridge](https://gitlab.com/starline/oscar_utils/carla/carla_apollo_bridge), который устанавливается в `modules/oscar/carla` и через [oscar_tools](oscar_tools.md).

[Инструкция по использованию CARLA](carla.md)

#### LGSVL

Ранее использовался в качестве основного симулятора. [Разработчик прекратил](https://github.com/lgsvl/simulator) разработку и поддержку, что осложнило дальнейшее его использование и развитие в рамках проекта OSCAR.

 [Инструкция по использованию LGSVL ( не актуально! )](lgsvl.md)