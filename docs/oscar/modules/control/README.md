# Control module

Модуль Control представляет собой набор алгоритмов, генерирующий команды управления шасси в целях выполнения траектории, спрогнозированной в модуле Planning. Общая схема модуля:

![scheme](images/control1.png)

Сontrol component берет последние сообщения из Localization, Chassis и Planning и при выполненных условиях, описанных ниже, передает их на обработку каждому контроллеру, включенному в сontrol_conf.

Условия генерации управляющего воздействия:
1. Работающие модули Planning, Chassis и Localization;
2. Отсутствие статуса estop в Planning;
3. Наличие траектории в Planning;
4. Неотрицательная скорость при включенной DRIVE передаче.

Более подробный пайплайн основной компоненты, который можно проследить в /apollo/modules/control/control_component.cc:

![pipeline](images/control2.png)

Код всех контроллеров находится в /oscar/modules/control/controller.

[Конфигурации модуля Control](configs.md).

[Описание управления с джойстика (Teleop управление)](teleop.md).

Описание контроллеров [Longitudinal](longitudinal.md), [Lateral](lateral.md) и [MPC](mpc.md).

Описание ноды [Control multiplexer](ctrl_mux.md).