# Longitudinal controller (LON)

1. [Описание алгоритма работы Apollo Longitudinal Control](описание-алгоритма-работы-apollo-longitudinal-control)
2. [Блок-схема алгоритма](Блок-схема-алгоритма)
3. [Отладка долготного контроллера](Отладка-долготного-контроллера)
4. [Связанные конфиги](Связанные-конфиги)

**Longitudinal controller (контроллер долготы)** отвечает за регулировку движения автомобиля по оси X.

**Примечание**: в стэке Apollo система координат автомобиля развёрнута таким образом, что ось y направлена вдоль автомобиля, а ось x поперёк.

В случае беспилотных автомобилей, и конкретно в случае Apollo, реализация данного алгоритма отвечает за команды ускорения/торможения. Общая задача звучит как минимизация смещения (ошибки) координаты х автомобиля от значений, которые выдаёт модуль planning. 

## Описание алгоритма работы Apollo Longitudinal Control

На вход контроллера поступают локализация, данные от топика chassis, траектория движения и управляющее воздействие. В дальнейшем, контроллер исходя из полученных данных коректирует управляющее воздействие. 

На первом этапе происходит расчёт долгтных ошибок. Вычисление базируется на сравнении текущего положения беспилотника, ближайшей точки траектории и предыдущей точки траектории.

### Расчёт долготных ошибок

Главными переменными, с которыми происходят манипуляции в подмодуле поиска долгтных ошибок, являются следующие:


```txt
  s: долготное расстояние, пройденное вдоль исполняемой траектории (longitudinal accumulated distance along reference trajectory)
  s_dot: долготная скорость вдоль траектории (longitudinal velocity along reference trajectory)
  d: широтное расстояние в текущий момент (по сути широта в полосе(lateral distance w.r.t. reference trajectory))  
  d_dot: скорость изменения широты (lateral distance change rate, i.e. dd/dt)
```
<details>
<summary>Вычисление переменных</summary>
Сначала эти переменные вычисляются. Происходит это следующим образом:

```c++
  d = cos_ref_theta * dy - sin_ref_theta * dx;
  где dx = x - ref_point.x(), dy = y - ref_point.y();
  ref_theta - угол поворота колёс (руля) в точке траектории.
  Подразумевается, что ошибки в исполнении команды на руль нет.


  double dot_rd_nd = dx * cos_ref_theta + dy * sin_ref_theta;
  *s = ref_point.s() + dot_rd_nd;
  
  d_dot = v * sin_delta_theta;
  Здесь double sin_delta_theta = std::sin(delta_theta),
  double delta_theta = theta - ref_point.theta();

  s_dot = v * cos_delta_theta / one_minus_kappa_r_d;
  Здесь double cos_delta_theta = std::cos(delta_theta), 
  double one_minus_kappa_r_d = 1 - ref_point.kappa() * (*ptr_d), kappa - кривизна.
```

На следующем этапе происходит вычисление реальных скорости и ускорения беспилотника:
```c++
  double lon_speed = vehicle_state->linear_velocity() * std::cos(heading_error);
  double lon_acceleration = vehicle_state->linear_acceleration() * std::cos(heading_error);
```
После этого вычисляются ошибки:
```c++
station_error = reference_point.path_point().s() - s_matched;
speed_error = reference_point.v() - s_dot_matched;
acceleration_error = reference_point.a() -
                                lon_acceleration / one_minus_kappa_lat_error;
Здесь:
double one_minus_kappa_lat_error = 1 - reference_point.path_point().kappa() *
                                             vehicle_state->linear_velocity() *
                                             std::sin(heading_error);
```
</details>

### Расчёт нового управляющего сигнала

Задача сводится к формированию команд газ/тормоз. Управляющий сигнал ограничивается настройками контроллера. Ограничение накладывается на значение ошибки и работает следующим образом:
1. Выполняется расчёт текущего значения ошибки.
2. Выполняется сравнения значения ошибки с заданными в конфиге (control_conf.pb.txt) предельными значениями ошибки.
3. Если текущая ошибка принадлежит диапазону (-предельная ошибка;предельная ошибка), то её значение берётся как значение ошибки для контроллера. Если текущая ошибка не попадает в диапазон, то в качестве ошибки для контроллера выбирается либо -предельная ошибка, либо предельная ошибка, в зависимости от того, что ближе к реальной ошибке. 

Longitudinal контроллер базируется на ПИД-регуляторе. Существует 2 возможных набора коэффициентов ПИД-регулятора для longitudinal контроллера: low_speed и high_speed. Выбор настроек определяется текущей скоростью автомобиля. В конфиге модуля control (control_conf.pb.txt) прописан параметр switch_speed, который и определяет границу между low_speed и high_speed. 

После загрузки настроек, происходит регулировка управляющего воздействия(по положению):
```c++
  double speed_offset =
      station_pid_controller_.Control(station_error_limited, ts);
```

В методе Control реализован ПИД-регулятор. 

В настройках модуля Control (control_conf.pb.txt) прописывается ограничение на величину управляющего сигнала. Ограничение реализовано аналогично ограничению предельной ошибки. 

После регулировки по положению, происходит аналогичная регулировка по скорости.
```c++
  acceleration_cmd_closeloop =
      speed_pid_controller_.Control(speed_controller_input_limited, ts);
```

В регуляторе предусмотрена опция фильтрации уклона от траектории при движении (противооткат).

```c++
double slope_offset_compensation = digital_filter_pitch_angle_.Filter(
      GRA_ACC * std::sin(injector_->vehicle_state()->pitch()));
```

Затем формируется команда на ускорение:

```c++
  double acceleration_cmd =
      acceleration_cmd_closeloop + debug->preview_acceleration_reference() +
      FLAGS_enable_slope_offset * debug->slope_offset_compensation();
```

На последнем этапе происходит трансформация ускорений в команды газ/тормоз. Для этого используется калибровочная таблица в control_conf.pb.txt. Эта таблица содержит значения ускорений, соответствующих им команд газ/тормоз для различных скоростных режимов. 
Максимальное значение команд газ/тормоз может быть ограничено аналогично ограничению ошибки и управляющего сигнала в vehicle_param.pb.txt.


## Блок-схема алгоритма

![lon_control](images/lon_control.png)

## Отладка долготного контроллера

Задача настройки контроллера сводится к тому, чтобы обратная связь сигнала управления была как можно более идентична сигналу управления. Оценить эту характеристику лучше всего с использованием Foxglove. В долготном контроллере интересны следующие сигналы: положение, скорость и ускорения. В топике control в FoxGlove есть ошибки этих сигналов:

![lon_control](images/lon_st.png)

![lon_control](images/lon_s.png)

![lon_control](images/lon_a.png)

Первое, что необходимо сделать - собрать калибровочную таблицу ускорений от педалей газа и тормоза. Для этого существует инструмент, описанный здесь.

После получения акутальной калибровочной таблицы, выполняется настройка регуляторов положения и скорости. В качестве регуляторов используются ПИД-регуляторы. Коэффициенты регуляторов находятся в modules/control/conf/control_conf.pb.txt.

Коэффициент P отвечает за быстродействие и чувствительность, I за статическую ошибку, D за колебательность. Настройку лучше всего начинать с коэффициента P. Для регулятора скорости предусмотрен инструмент изменения коэффициентов регулятора при переходе на высокую скорость. Порог между низкой и высокой скоростью задаётся параметром switch_speed.

Настройка считается выполненной, когда ошибки сигналов управления близки к нулю.

## Связанные конфиги

Настройки для LON контроллера входят в общую группу настроек control_conf и также находятся в longitudinal_controller_conf, увидеть их можно в файле control_conf.pb.txt и longitudinal_controller_conf.pb.txt (раздел lon_controller_conf {})

<details>
<summary>Таблица</summary>

| <p align ="center">Параметр</p> | <p align ="center">Расшифровка</p> | <p align ="center">Значение по умолчанию</p> |
|----------|----------|-----------------------|
| ts | Временной интервал, соответствующий среднему периоду отработки алгоритма, служит входом для всех PID | 0.01 |
| brake_minimum_action | Верхняя deadband-граница (мертвая зона) воздействия на педаль тормоза | 0.0 |
| throttle_minimum_action | Верхняя deadband-граница (мертвая зона) воздействия на педаль ускорения | 0.0 |
| speed_controller_input_limit | Предельное значение ошибки по скорости | 2.0 |
| station_error_limit | Предельное значение ошибки по положению | 2.0 |
| preview_window | Используется для расчета количества шагов, на которые заранее считается ошибка положения | 20.0 |
| standstill_acceleration | Значение ускорения при входе в зону отсановки или значении скорости и ускорения меньше минимального (чтобы авто не катилось) | -0.3 |
| enable_reverse_leadlag_compensation | Включение компенсации обратного опережения | false |
| station_pid_conf | Настройки ПИД-регулятора по положению (состоянию) ||
| integrator_enable | Включить компенсацию статической ошибки  (интегральная часть) | false |
| integrator_saturation_level | Ограничение насыщения (по модулю) | 0.3 |
| kp | Пропорциональный коэффициент | 0.2 |
| ki | Интегральный коэффициент | 0.0 |
| kd | Дифференцирующий коэффициент | 0.0 |
||||
| low_speed_pid_conf | Настройки ПИД-регулятора при езде на низкой скорости ||
| integrator_enable | Включить компенсацию статической ошибки  (интегральная часть) | true |
| integrator_saturation_level | Ограничение насыщения (по модулю) | 0.3 |
| kp | Пропорциональный коэффициент | 0.4 |
| ki | Интегральный коэффициент | 0.3 |
| kd | Дифференцирующий коэффициент | 0.0 |
||||
| high_speed_pid_conf | Настройки ПИД-регулятора при езде на высокой скорости ||
| integrator_enable | Включить компенсацию статической ошибки (интегральная часть) | true |
| integrator_saturation_level | Ограничение насыщения (по модулю) | 0.3 |
| kp | Пропорциональный коэффициент | 0.8 |
| ki | Интегральный коэффициент | 0.3 |
| kd | Дифференцирующий коэффициент | 0.0 |
||||
| switch_speed | Граница скорости между low_speed и high_speed | 2.0 |
| reverse_station_pid_conf | Настройки ПИД-регулятора для заднего хода ||
| integrator_enable | Включить накопление ошибки | true |
| integrator_saturation_level | Ограничение насыщения (по модулю) | 0.5 |
| kp | Пропорциональный коэффициент | 2.0 |
| ki | Интегральный коэффициент | 0.3 |
| kd | Дифференцирующий коэффициент | 0.0 |
||||
| reverse_station_leadlag_conf | Конфигурация альфа-бета фильтра (наблюдателя, контроллера) запаздывания по положению для заднего хода ||
| innerstate_saturation_level | Ограничение насыщения (по модулю) | 1000 |
| alpha | Альфа составляющая | 1.0 |
| beta | Бета составляющая | 1.0 |
| tau | Тау (гамма) составляющая | 0.0 |
||||
| reverse_speed_leadlag_conf | Конфигурация альфа-бета фильтра (наблюдателя, контроллера) запаздывания по скорости для заднего хода ||
| innerstate_saturation_level | Ограничение насыщения (по модулю) | 1000 |
| alpha | Альфа составляющая | 1.0 |
| beta | Бета составляющая | 1.0 |
| tau | Тау (гамма) составляющая | 0.0 |
||||
| pitch_angle_filter_conf | Конфигурация фильтра ||
| cutoff_freq | Частота среза для фильтра Баттерворта | 5 |
||||
| query_relative_time | Промежуток времени между запросами | 0.8 |
| minimum_speed_protection | Если скорость авто равна или больше этой, то контроллер начнает регулировать её | 0.1 |
</details>