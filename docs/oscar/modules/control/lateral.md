# Lateral controller (LAT)

1. [Описание работы алгоритма](#описание-работы-алгоритма)
2. [LQR](#lqr)

     2.1. [Настройка LQR](#настройка-lqr)
  
3. [MRAC-контроллер](#mrac-контроллер)
4. [Формирование сигнала управления](#формирование-сигнала-управления)
5. [Отладка широтного контроллера](#отладка-широтного-контроллера)
6. [Связанные конфиги](#связанные-конфиги)

**Латеральный контроллер (контроллер широты)** - представляет из себя классический линейно-квадратичный регулятор (LQR) угла положения рулевого колеса с надстройкой в виде MRAC для борьбы с запаздыванием сигнала и внесением "мягкости" в управление.

## Описание работы алгоритма

Контроллер получает на вход локализацию, шасси, траекторию движения, сформированную модулем планирования, команду управления (в эту переменную будет записана команда управления в конце цикла работы планировщика). 

**Примечание:** Если модуль не получит актуальной информации о локализации, то он расчитает heading исходя из ориентации автомобиля.

Широтный контроллер гибок к задержкам локализации. Если сигнал стабилен, то начальное положение равно текущему положению. Это значит, что в качестве текущей точки беспилотника выбирается только что пришедшее из локализации положение. В случае, если произошла задержка (в контроллере реализовано условие, что время считывания предыдущего сигнала должно быть больше 1.0e-6), то производится вычисление текущего положения:

<details>
<summary></summary>

```c++
      auto x_diff_map = curr_vehicle_x - init_vehicle_x_;
      auto y_diff_map = curr_vehicle_y - init_vehicle_y_;
      auto theta_diff = curr_vehicle_heading - init_vehicle_heading_;

      auto cos_map_veh = std::cos(init_vehicle_heading_);
      auto sin_map_veh = std::sin(init_vehicle_heading_);

      auto x_diff_veh = cos_map_veh * x_diff_map + sin_map_veh * y_diff_map;
      auto y_diff_veh = -sin_map_veh * x_diff_map + cos_map_veh * y_diff_map;

      auto cos_theta_diff = std::cos(-theta_diff);
      auto sin_theta_diff = std::sin(-theta_diff);

      auto tx = -(cos_theta_diff * x_diff_veh - sin_theta_diff * y_diff_veh);
      auto ty = -(sin_theta_diff * x_diff_veh + cos_theta_diff * y_diff_veh);

      auto ptr_trajectory_points =
          target_tracking_trajectory.mutable_trajectory_point();
      std::for_each(
          ptr_trajectory_points->begin(), ptr_trajectory_points->end(),
          [&cos_theta_diff, &sin_theta_diff, &tx, &ty,
           &theta_diff](common::TrajectoryPoint &p) {
            auto x = p.path_point().x();
            auto y = p.path_point().y();
            auto theta = p.path_point().theta();

            auto x_new = cos_theta_diff * x - sin_theta_diff * y + tx;
            auto y_new = sin_theta_diff * x + cos_theta_diff * y + ty;
            auto theta_new = common::math::NormalizeAngle(theta - theta_diff);

            p.mutable_path_point()->set_x(x_new);
            p.mutable_path_point()->set_y(y_new);
            p.mutable_path_point()->set_theta(theta_new);
          });
```
После этого выполняется обновление данных об ориентации автомобиля:
```c++
void LatController::UpdateDrivingOrientation() {
  auto vehicle_state = injector_->vehicle_state();
  driving_orientation_ = vehicle_state->heading();
  matrix_bd_ = matrix_b_ * ts_;
  // Reverse the driving direction if the vehicle is in reverse mode
  if (FLAGS_reverse_heading_control) {
    if (vehicle_state->gear() == canbus::Chassis::GEAR_REVERSE) {
      driving_orientation_ =
          common::math::NormalizeAngle(driving_orientation_ + M_PI);
      // Update Matrix_b for reverse mode
      matrix_bd_ = -matrix_b_ * ts_;
      ADEBUG << "Matrix_b changed due to gear direction";
    }
  }
}
```
Здесь matrix_b_:
```c++
  matrix_b_(1, 0) = cf_ / mass_;
  matrix_b_(3, 0) = lf_ * cf_ / iz_;
```
</details>

На следующем этапе происходит обновление данных о состоянии автомобиля. Сначала производится пересчёт координат автомобиля из системы координат на задней оси в систему координат в центре масс:

<details>
<summary></summary>

```c++
math::Vec2d VehicleStateProvider::ComputeCOMPosition(
    const double rear_to_com_distance) const {
  // set length as distance between rear wheel and center of mass.
  Eigen::Vector3d v;
  if ((FLAGS_state_transform_to_com_reverse &&
       vehicle_state_.gear() == canbus::Chassis::GEAR_REVERSE) ||
      (FLAGS_state_transform_to_com_drive &&
       vehicle_state_.gear() == canbus::Chassis::GEAR_DRIVE)) {
    v << 0.0, rear_to_com_distance, 0.0;
  } else {
    v << 0.0, 0.0, 0.0;
  }
  Eigen::Vector3d pos_vec(vehicle_state_.x(), vehicle_state_.y(),
                          vehicle_state_.z());
  // Initialize the COM position without rotation
  Eigen::Vector3d com_pos_3d = v + pos_vec;

  // If we have rotation information, take it into consideration.
  if (vehicle_state_.pose().has_orientation()) {
    const auto &orientation = vehicle_state_.pose().orientation();
    Eigen::Quaternion<double> quaternion(orientation.qw(), orientation.qx(),
                                         orientation.qy(), orientation.qz());
    // Update the COM position with rotation
    com_pos_3d = quaternion.toRotationMatrix() * v + pos_vec;
  }
  return math::Vec2d(com_pos_3d[0], com_pos_3d[1]);
}
```
</details>

Затем вычисляются широтные ошибки. Вычисление базируется на сравнении текущего положения автомобиля и точки траектории, в которой он должен быть.

<details>
<summary></summary>

```c++
target_point = trajectory_analyzer.QueryNearestPointByPosition(x, y);
...
const double dx = x - target_point.path_point().x();
const double dy = y - target_point.path_point().y();
...
const double cos_target_heading = std::cos(target_point.path_point().theta());
const double sin_target_heading = std::sin(target_point.path_point().theta());
...
double lateral_error = cos_target_heading * dy - sin_target_heading * dx;
...
debug->set_ref_heading(target_point.path_point().theta());
double heading_error =
      common::math::NormalizeAngle(theta - debug->ref_heading());
```
</details>

Кроме того, контроллер рассчитывает переменные heading_error_feedback, lateral_error_feedback и некоторые производные.
Переменные heading_error_feedback и lateral_error_feedback по сути дела являются неким предсказанием будущей ошибки. Краткая суть следующая. Помимо ошибки по положению, которая была вычислена ранее, нужно так же учитывать то, что автомобиль может двигаться с какой-то линейной скоростью и поворачивать. Поэтому для полного учёта ошибки необходимо взять рассчитанные ошибки по положению lateral_error и heading_error и для некоторого промежутка будущего(окна) учесть движение и поворот:

<details>
<summary></summary>

```c++
  if (std::fabs(linear_v) >= low_speed_bound_) {
    lookahead_station = lookahead_station_high_speed_;
    lookback_station = lookback_station_high_speed_;
  } else if (std::fabs(linear_v) < low_speed_bound_ - low_speed_window_) {
    lookahead_station = lookahead_station_low_speed_;
    lookback_station = lookback_station_low_speed_;
  } else {
    lookahead_station = common::math::lerp(
        lookahead_station_low_speed_, low_speed_bound_ - low_speed_window_,
        lookahead_station_high_speed_, low_speed_bound_, std::fabs(linear_v));
    lookback_station = common::math::lerp(
        lookback_station_low_speed_, low_speed_bound_ - low_speed_window_,
        lookback_station_high_speed_, low_speed_bound_, std::fabs(linear_v));
  }
...
    auto lookahead_point = trajectory_analyzer.QueryNearestPointByRelativeTime(
        target_point.relative_time() +
        lookahead_station /
            (std::max(std::fabs(linear_v), 0.1) * std::cos(heading_error)));
    heading_error_feedback = common::math::NormalizeAngle(
        heading_error + target_point.path_point().theta() -
        lookahead_point.path_point().theta());
...
   lateral_error_feedback =
        lateral_error + lookahead_station * std::sin(heading_error);
...
  auto lateral_error_dot = linear_v * std::sin(heading_error);
  auto lateral_error_dot_dot = linear_a * std::sin(heading_error);
```
</details>

Следующий этап работы контроллера - обновление матрицы состояния:

<details>
<summary></summary>

```c++
void LatController::UpdateMatrix() {
  double v;
  // At reverse driving, replace the lateral translational motion dynamics with
  // the corresponding kinematic models
  if (injector_->vehicle_state()->gear() == canbus::Chassis::GEAR_REVERSE) {
    v = std::min(injector_->vehicle_state()->linear_velocity(),
                 -minimum_speed_protection_);
    matrix_a_(0, 2) = matrix_a_coeff_(0, 2) * v;
  } else {
    v = std::max(injector_->vehicle_state()->linear_velocity(),
                 minimum_speed_protection_);
    matrix_a_(0, 2) = 0.0;
  }
  matrix_a_(1, 1) = matrix_a_coeff_(1, 1) / v;
  matrix_a_(1, 3) = matrix_a_coeff_(1, 3) / v;
  matrix_a_(3, 1) = matrix_a_coeff_(3, 1) / v;
  matrix_a_(3, 3) = matrix_a_coeff_(3, 3) / v;
  Matrix matrix_i = Matrix::Identity(matrix_a_.cols(), matrix_a_.cols());
  matrix_ad_ = (matrix_i - ts_ * 0.5 * matrix_a_).inverse() *
               (matrix_i + ts_ * 0.5 * matrix_a_);
}
```
Здесь matrix_a_:
```c++
  matrix_a_(0, 1) = 1.0;
  matrix_a_(1, 2) = (cf_ + cr_) / mass_;
  matrix_a_(3, 2) = (lf_ * cf_ - lr_ * cr_) / iz_;
```
matrix_a_coeff_:
```c++
  matrix_a_coeff_(0, 2) = 0.0;
  matrix_a_coeff_(1, 1) = -(cf_ + cr_) / mass_;
  matrix_a_coeff_(1, 3) = (lr_ * cr_ - lf_ * cf_) / mass_;
  matrix_a_coeff_(3, 1) = (lr_ * cr_ - lf_ * cf_) / iz_;
  matrix_a_coeff_(3, 3) = -1.0 * (lf_ * lf_ * cf_ + lr_ * lr_ * cr_) / iz_;
```
iz - момент инерции: iz_ = lf_ * lf_ * mass_front + lr_ * lr_ * mass_rear;

После обновления матрицы вызывается метод UpdateMatrixCompound. Он создаёт составную дискретную матрицу с моделью предварительного просмотра дороги:

```c++
void LatController::UpdateMatrixCompound() {
  // Initialize preview matrix
  matrix_adc_.block(0, 0, basic_state_size_, basic_state_size_) = matrix_ad_;
  matrix_bdc_.block(0, 0, basic_state_size_, 1) = matrix_bd_;
  if (preview_window_ > 0) {
    matrix_bdc_(matrix_bdc_.rows() - 1, 0) = 1;
    // Update A matrix;
    for (int i = 0; i < preview_window_ - 1; ++i) {
      matrix_adc_(basic_state_size_ + i, basic_state_size_ + 1 + i) = 1;
    }
  }
}
```
</details>

Если установлен флаг enable_gain_scheduler, то коэффициенты матрицы регулятора q будут динамически изменяться в зависимости от текущей скорости транспортного средства. Значения коэффициентов для конкретных скоростей прописываются в control_conf.pb.txt. Также на этом этапе выполняется решение задачи линейно-квадартичного управления:

<details>
<summary></summary>

```c++
  if (FLAGS_enable_gain_scheduler) {
    matrix_q_updated_(0, 0) =
        matrix_q_(0, 0) * lat_err_interpolation_->Interpolate(
                              std::fabs(vehicle_state->linear_velocity()));
    matrix_q_updated_(2, 2) =
        matrix_q_(2, 2) * heading_err_interpolation_->Interpolate(
                              std::fabs(vehicle_state->linear_velocity()));
    common::math::SolveLQRProblem(matrix_adc_, matrix_bdc_, matrix_q_updated_,
                                  matrix_r_, lqr_eps_, lqr_max_iteration_,
                                  &matrix_k_);
  } else {
    common::math::SolveLQRProblem(matrix_adc_, matrix_bdc_, matrix_q_,
                                  matrix_r_, lqr_eps_, lqr_max_iteration_,
                                  &matrix_k_);
  }
```
</details>

Более подробное описание решения задачи линейно-квадартичного управления рассказывается в разделе [LQR](#lqr).
Дальше происходит формирование сигнала управления (см. раздел [Формирование сигнала управления](#формирование-сигнала-управления)) и опционально компенсация задержек (см. раздел [MRAC-контроллер](#mrac-контроллер))

## LQR

Линейно-квадратичный регулятор (linear-quadratic regulator (LQR)) используется для оптимизации в контроллере широты (lateral). Задача сводится к минимизации функции затрат, которая регулируется весовыми коэффициентами системы на основе математической модели. Функция стоимости определяется как отклонение от желаемого значения.  Ключевое отличие между LQR и MPC: MPC использует набор данных (чаще всего ошибок) фиксированной длины (интервал времени), а вот LQR использует все входные данные на всём интервале работы (так как оценивает всю функцию входных данных, а не выборку). Отсюда вытекают очевидные преимущества: LQR глобально стабилен, а MPC - локально. 
<details>
<summary>Реализация LQR выглядит следующим образом:</summary>

```c++
void SolveLQRProblem(const Matrix &A, const Matrix &B, const Matrix &Q,
                     const Matrix &R, const Matrix &M, const double tolerance,
                     const uint max_num_iteration, Matrix *ptr_K) {
  if (A.rows() != A.cols() || B.rows() != A.rows() || Q.rows() != Q.cols() ||
      Q.rows() != A.rows() || R.rows() != R.cols() || R.rows() != B.cols() ||
      M.rows() != Q.rows() || M.cols() != R.cols()) {
    AERROR << "LQR solver: one or more matrices have incompatible dimensions.";
    return;
  }

  Matrix AT = A.transpose();
  Matrix BT = B.transpose();
  Matrix MT = M.transpose();

  // Solves a discrete-time Algebraic Riccati equation (DARE)
  // Calculate Matrix Difference Riccati Equation, initialize P and Q
  Matrix P = Q;
  uint num_iteration = 0;
  double diff = std::numeric_limits<double>::max();
  while (num_iteration++ < max_num_iteration && diff > tolerance) {
    Matrix P_next =
        AT * P * A -
        (AT * P * B + M) * (R + BT * P * B).inverse() * (BT * P * A + MT) + Q;
    // check the difference between P and P_next
    diff = fabs((P_next - P).maxCoeff());
    P = P_next;
  }

  if (num_iteration >= max_num_iteration) {
    ADEBUG << "LQR solver cannot converge to a solution, "
              "last consecutive result diff is: "
           << diff;
  } else {
    ADEBUG << "LQR solver converged at iteration: " << num_iteration
           << ", max consecutive result diff.: " << diff;
  }
  *ptr_K = (R + BT * P * B).inverse() * (BT * P * A + MT);
}
```

Здесь Matrix A = matrix_adc_, Matrix B = matrix_bdc_, Matrix Q = matrix_q_, Matrix R = matrix_r_, Matrix M = Matrix::Zero(Q.rows(), R.cols()), tolerance и max_num_iteration берутся из control_conf.pb.txt, ptr_K - результирующая матрица K (решение проблемы). 

</details>

### Настройка LQR
Настройка регулятора происходит в 2 этапа следующим образом. В файле oscar/modules/control/conf/control_conf.pb.txt в lat_controller_conf:
```txt
matrix_q: 0.1 # ошибка курсового угла (heading error)
matrix_q: 0.0
matrix_q: 0.1 # широтная ошибка (lateral error)
matrix_q: 0.0

```
На первом этапе настройки необходимо на низкой скорости изменять 1-й и 3-й параметры до тех пор, пока heading error и lateral error не станут близкими к нулю. Второй этап заключается в настройке регулятора на разных скоростях. В файле oscar/modules/control/conf/control_conf.pb.txt в lat_controller_conf есть поля lat_err_gain_scheduler и heading_err_gain_scheduler:

<details>
<summary>Таблица</summary>

```txt
lat_err_gain_scheduler {
    scheduler {
      speed: 4.0
      ratio: 1.0
    }
    scheduler {
      speed: 8.0
      ratio: 0.6
    }
    scheduler {
      speed: 12.0
      ratio: 0.2
    }
    scheduler {
      speed: 20.0
      ratio: 0.1
    }
    scheduler {
      speed: 25.0
      ratio: 0.05
    }
  }
  heading_err_gain_scheduler {
    scheduler {
      speed: 4.0
      ratio: 1.0
    }
    scheduler {
      speed: 8.0
      ratio: 0.6
    }
    scheduler {
      speed: 12.0
      ratio: 0.4
    }
    scheduler {
      speed: 20.0
      ratio: 0.2
    }
    scheduler {
      speed: 25.0
      ratio: 0.1
    }
  }
```
</details>

## MRAC-контроллер

Model Reference Adaptive Controller (MRAC) - это инструмент адаптивного управления системой. В первую очередь он необходим для компенсации существующей задержки между отправкой командой на руль и началом её исполнения.  Работает MRAC следующим образом. Он в своём составе имеет эталонную модель, на которую приходит сигнал управления параллельно с реальной системой. Эта эталонная модель выдаёт эталонный выходной сигнал, который приходит в блок адаптации. В этот же блок приходит входной (после контроллера) и выходной сигналы реальной системы. После сравнения сигналов в контроллер отправляются скорректированные коэффициенты.

![mrac](images/MRAC.png)

Работает MRAC следующим образом. Сначала происходит обновление желаемого состояния (reference_state):
<details>
<summary></summary>

```c++
void MracController::UpdateReference() {
  Matrix matrix_i = Matrix::Identity(model_order_, model_order_);
  state_reference_.col(0) =
      (matrix_i - ts_ * 0.5 * matrix_a_reference_).inverse() *
      ((matrix_i + ts_ * 0.5 * matrix_a_reference_) * state_reference_.col(1) +
       ts_ * 0.5 * matrix_b_reference_ *
           (input_desired_(0, 0) + input_desired_(0, 1)));
}
```
Здесь matrix_a_reference_:
```c++
  if (model_order_ == 1) {
    matrix_a_reference_(0, 0) = -1.0 / tau_reference_;
    ...
  }
else if (model_order_ == 2) {
    matrix_a_reference_(0, 1) = 1.0;
    matrix_a_reference_(1, 0) = -wn_reference_ * wn_reference_;
    matrix_a_reference_(1, 1) = -2 * zeta_reference_ * wn_reference_;
    ...
}
```
matrix_b_reference:
```c++
  if (model_order_ == 1) {
    ...
    matrix_b_reference_(0, 0) = 1.0 / tau_reference_;
  } else if (model_order_ == 2) {
    ...
    matrix_b_reference_(1, 0) = wn_reference_ * wn_reference_;
  }
```
Следующие параметры подтягиваются из control_conf.pb.txt:
```c++
  tau_reference_ = mrac_conf.reference_time_constant();
  wn_reference_ = mrac_conf.reference_natural_frequency();
  zeta_reference_ = mrac_conf.reference_damping_ratio();
```
</details>

На следующем этапе формируются две матрицы адаптации gain_state_adaption_ и gain_input_adaption_:
<details>
<summary></summary>

```c++
  UpdateAdaption(&gain_state_adaption_, state_action_,
                 gamma_state_adaption_ * gamma_ratio_state_);

  UpdateAdaption(&gain_input_adaption_, input_desired_,
                 gamma_input_adaption_ * gamma_ratio_input_);
```
Здесь state_action_.col(0) = state, input_desired_(0, 0) = command, gamma_input_adaption_(0, 0) = mrac_conf.adaption_desired_gain() (прописывается в control_conf.pb.txt), gamma_state_adaption_ определяется следующим образом:
```c++
  for (int i = 0; i < model_order_; ++i) {
    gamma_state_adaption_(i, i) =
        (i <= x_size - 1) ? mrac_conf.adaption_state_gain(i)
                          : mrac_conf.adaption_state_gain(x_size - 1);
  ...
  }
```
Где x_size = mrac_conf.adaption_state_gain_size() (размер адаптационной матрицы состояния).
</details>

Процесс формирования выглядит следующим образом:
<details>
<summary></summary>

```c++
void MracController::UpdateAdaption(Matrix *law_adp, const Matrix state_adp,
                                    const Matrix gain_adp) {
  Matrix state_error = state_action_ - state_reference_;
  law_adp->col(0) =
      law_adp->col(1) -
      0.5 * ts_ * gain_adp *
          (state_adp.col(0) * (state_error.col(0).transpose() +
                               compensation_anti_windup_.col(0).transpose()) +
           state_adp.col(1) * (state_error.col(1).transpose() +
                               compensation_anti_windup_.col(1).transpose())) *
          matrix_p_adaption_ * matrix_b_adaption_;
}
```
Здесь compensation_anti_windup_ определяется следующим образом:
```c++
  compensation_anti_windup_.col(1) = compensation_anti_windup_.col(0);
  compensation_anti_windup_.col(0) = gain_anti_windup_ * offset_windup;
```
Где gain_anti_windup_:
```c++
    gain_anti_windup_(i, i) =
        (i <= aw_size - 1)
            ? mrac_conf.anti_windup_compensation_gain(i)
            : mrac_conf.anti_windup_compensation_gain(aw_size - 1);
```
offset_windup:
```c++
  offset_windup(0, 0) =
      ((control_command > bound_command_) ? bound_command_ - control_command
                                          : 0.0) +
      ((control_command < -bound_command_) ? -bound_command_ - control_command
                                           : 0.0);
  if (model_order_ > 1) {
    offset_windup(1, 0) =
        ((control_command > previous_command + bound_command_rate_ * ts_)
             ? bound_command_rate_ - (control_command - previous_command) / ts_
             : 0.0) +
        ((control_command < previous_command - bound_command_rate_ * ts_)
             ? -bound_command_rate_ - (control_command - previous_command) / ts_
             : 0.0);
  }
```
</details>

**Примечание**. Работа antiwindup коррекции завязана на коэффициент gain_anti_windup_, который задаётся в control_conf.pb.txt. Если коэффициент равен 0, то antiwindup коррекция не включена, так как compensation_anti_windup_.col(0) = 0. Кроме того, метод, формирующий матрицы offset_windup и compensation_anti_windup_ (MracController::AntiWindupCompensation) запускается после расчёта адаптационных матриц. Таким образом, на первом цикле работы mrac-контроллера независимо от прописанных настроек antiwindup коррекция не производится.

Существуют условия, при которых только что сформированные матрицы адаптации не будут использованы, а их обновление отменено. Это связано с накладываемыми ограниченями. 
<details>
<summary>Для mrac_order = 1 выглядит это следующим образом:</summary>

```c++
  if (model_order_ == 1 && adaption_clamping_enabled &&
      (gain_state_adaption_(0, 0) > 0.0 ||
       gain_state_adaption_(0, 0) < gain_state_clamping_(0, 0) ||
       gain_input_adaption_(0, 0) > gain_input_clamping_(0, 0) ||
       gain_input_adaption_(0, 0) < 1.0)) {
    gain_state_adaption_.col(0) = gain_state_adaption_.col(1);
    gain_input_adaption_.col(0) = gain_input_adaption_.col(1);
  }
```
Одно из условий adaption_clamping_enabled = (tau_clamping_ >= Epsilon), где tau_clamping_ берётся из control_conf.pb.txt (clamping_time_constant), а Epsilon захардкожен в коде со значением const double Epsilon = 0.000001.
В случае, если условие выполняется, то gain_state_clamping_ и gain_input_clamping_ принимают следующие значения:
```c++
    if (adaption_clamping_enabled) {
      ...
      gain_state_clamping_(0, 0) =
          (matrix_a_clamping(0, 0) - matrix_a_estimate(0, 0)) /
          matrix_b_estimate(0, 0);
      gain_input_clamping_(0, 0) =
          matrix_b_clamping(0, 0) / matrix_b_estimate(0, 0);
    }
```
Где:
```c++
matrix_a_clamping(0, 0) = -1.0 / tau_clamping_;
matrix_b_clamping(0, 0) = 1.0 / tau_clamping_;
```
</details>

<details>
<summary>Для mrac_order = 2 проверка и пересчёт значений выглядит по-другому:</summary>

```c++
else if (model_order_ == 2 &&
             (rise_time_estimate >= ts_ && settling_time_estimate >= ts_)) {
    const double wn_estimate = 1.8 / rise_time_estimate;
    const double zeta_estimate =
        4.6 / (rise_time_estimate * settling_time_estimate);
    matrix_a_estimate(0, 1) = 1.0;
    matrix_a_estimate(1, 0) = -wn_estimate * wn_estimate;
    matrix_a_estimate(1, 1) = -2 * zeta_estimate * wn_estimate;
    matrix_b_estimate(1, 0) = wn_estimate * wn_estimate;
    gain_state_adaption_init_.col(0) =
        (common::math::PseudoInverse<double, 2, 1>(matrix_b_estimate) *
         (matrix_a_reference_ - matrix_a_estimate))
            .transpose();
    gain_input_adaption_init_.col(0) =
        (common::math::PseudoInverse<double, 2, 1>(matrix_b_estimate) *
         matrix_b_reference_)
            .transpose();
             }
```
Здесь:
```c++
  const double rise_time_estimate =
      latency_param.dead_time() + latency_param.rise_time();
  const double settling_time_estimate =
      latency_param.dead_time() + latency_param.settling_time();
```
ts_ берётся из control_conf.pb.txt
</details>

После выполнения проверки и получения новых матриц адаптации или отката к старым, mrac контроллер формирует новый сигнал управления:

```c++
  double control_unbounded =
      gain_state_adaption_.col(0).transpose() * state_action_.col(0) +
      gain_input_adaption_(0, 0) * input_desired_(0, 0);
```
Контроллер может ограничивать сигнал управления (saturation), поэтому следующим этапом работы будет выполнение этого ограничения:

<details>
<summary></summary>

```c++
int MracController::BoundOutput(const double output_unbounded,
                                const double previous_output, double *output) {
  int status = 0;
  if (output_unbounded > bound_command_ ||
      output_unbounded > previous_output + bound_command_rate_ * ts_) {
    *output = (bound_command_ < previous_output + bound_command_rate_ * ts_)
                  ? bound_command_
                  : previous_output + bound_command_rate_ * ts_;
    // if output exceeds the upper bound, then status = 1; while if output
    // changing rate exceeds the upper rate bound, then status = 2
    status =
        (bound_command_ < previous_output + bound_command_rate_ * ts_) ? 1 : 2;
  } else if (output_unbounded < -bound_command_ ||
             output_unbounded < previous_output - bound_command_rate_ * ts_) {
    *output = (-bound_command_ > previous_output - bound_command_rate_ * ts_)
                  ? -bound_command_
                  : previous_output - bound_command_rate_ * ts_;
    // if output exceeds the lower bound, then status = -1; while if output
    // changing rate exceeds the lower rate bound, then status = -2
    status = (-bound_command_ > previous_output - bound_command_rate_ * ts_)
                 ? -1
                 : -2;
  } else {
    *output = output_unbounded;
    // if output does not violate neither bound nor rate bound, then status = 0
    status = 0;
  }
  return status;
}
```
</details>

После этого вызывается antiwindup коррекция:

<details>
<summary></summary>

```c++
AntiWindupCompensation(control_unbounded, control_previous_);
...
void MracController::AntiWindupCompensation(const double control_command,
                                            const double previous_command) {
  Matrix offset_windup = Matrix::Zero(model_order_, 1);
  offset_windup(0, 0) =
      ((control_command > bound_command_) ? bound_command_ - control_command
                                          : 0.0) +
      ((control_command < -bound_command_) ? -bound_command_ - control_command
                                           : 0.0);
  if (model_order_ > 1) {
    offset_windup(1, 0) =
        ((control_command > previous_command + bound_command_rate_ * ts_)
             ? bound_command_rate_ - (control_command - previous_command) / ts_
             : 0.0) +
        ((control_command < previous_command - bound_command_rate_ * ts_)
             ? -bound_command_rate_ - (control_command - previous_command) / ts_
             : 0.0);
  }
  compensation_anti_windup_.col(1) = compensation_anti_windup_.col(0);
  compensation_anti_windup_.col(0) = gain_anti_windup_ * offset_windup;
}
```

</details>

Последнее, что делает контроллер перед окончанием цикла - обновляет переменные, содержащие предыдущие значения:
```c++
  gain_state_adaption_.col(1) = gain_state_adaption_.col(0);
  gain_input_adaption_.col(1) = gain_input_adaption_.col(0);
  state_reference_.col(1) = state_reference_.col(0);
  state_action_.col(1) = state_action_.col(0);
  input_desired_.col(1) = input_desired_.col(0);
  control_previous_ = control;
```
## Формирование сигнала управления

На вход в контроллер приходят координаты, ориентация, heading, траектория  автомобиля и control_command в точке траектории.

Если модуль не получает heading из локализации, то рассчитывает его самостоятельно из ориентации.

Команда на руль рассчитывается следующим образом:

```c++
steer_angle = steer_angle_feedback + steer_angle_feedforward +
                steer_angle_feedback_augment;
```

Здесь steer_angle_feedback - обратная связь, steer_angle_feedforward - прямая связь, steer_angle_feedback_augment- запаздывание. 

steer_angle_feedback = это элемент (0;0) произведения матрицы K на матрицу состояний.
```c++
const double steer_angle_feedback = -(matrix_k_ * matrix_state_)(0, 0) * 180 /
                                      M_PI * steer_ratio_ /
                                      steer_single_direction_max_degree_ * 100;
```

Матрица K - матрица, содержащая усиливающие коэффициенты (прописывается в config файле модуля control (control_conf.pb.txt)). При расчёте сразу происходит перевод в проценты от максимальной команды. steer_ratio_(передаточное число рулевого управления) и steer_single_direction_max_degree_ задаются в config файле с параметрами автомобиля (vehicle_param.pb.txt).

Расчёт steer_angle_feedforward происходит в функции LatController::ComputeFeedForward:

<details>
<summary></summary>

```c++
double LatController::ComputeFeedForward(double ref_curvature) const {
  const double kv =
      lr_ * mass_ / 2 / cf_ / wheelbase_ - lf_ * mass_ / 2 / cr_ / wheelbase_;

  // Calculate the feedforward term of the lateral controller; then change it
  // from rad to %
  const double v = injector_->vehicle_state()->linear_velocity();
  double steer_angle_feedforwardterm;
  if (injector_->vehicle_state()->gear() == canbus::Chassis::GEAR_REVERSE) {
    steer_angle_feedforwardterm = wheelbase_ * ref_curvature * 180 / M_PI *
                                  steer_ratio_ /
                                  steer_single_direction_max_degree_ * 100;
  } else {
    steer_angle_feedforwardterm =
        (wheelbase_ * ref_curvature + kv * v * v * ref_curvature -
         matrix_k_(0, 2) *
             (lr_ * ref_curvature -
              lf_ * mass_ * v * v * ref_curvature / 2 / cr_ / wheelbase_)) *
        180 / M_PI * steer_ratio_ / steer_single_direction_max_degree_ * 100;
  }

  return steer_angle_feedforwardterm;
}
```

</details>

Расчёт для передней и задней передач отличается. Сразу производится пересчёт в проценты от максимально возможной команды на поворот. Здесь:
```c++
lr_ = wheelbase_ * (1.0 - mass_rear / mass_);
lf_ = wheelbase_ * (1.0 - mass_front / mass_);
mass_rear = mass_rl + mass_rr;
mass_front = mass_fl + mass_fr;
mass_fl, mass_fr, mass_rl, mass_rr - распределение масс по колёсам автомобиля (берутся из config файла (vehicle_param.pb.txt)).
cf, cr - жесткость автомобиля на поворотах (vehicle corning stiffness, ,берутся из config файла (vehicle_param.pb.txt)).
mass_ = mass_front + mass_rear;
v - линейная скорость автомобиля.
```

steer_angle_feedback_augment - запаздывание. Расчёт этого параметра производится контроллером запаздывания (LeadlagController) на основе сигнала ошибки и дельты t.

В контроллере существует возможность ограничить максимальный угол поворота и максимальную скорость поворота. Ограничение угла задаётся параметром set_steer_limit в config файле модуля control (control.conf). Ограничения скорости задаётся двумя параметрами: флагом enable_maximum_steer_rate_limit в config файле модуля control (control.conf) и фактическим значением ограничения max_steer_angle_rate в vehicle_param.pb.txt.

Дополнительно команда на поворот руля может быть обработана MRAC-контроллером, если в config файле модуля control(control.conf) выставлен соответствующий флаг и в control_conf.pb.txt заданы настройки контроллера (внутри lateral_controller). Этот контроллер может в значительной степени компенсировать запаздывание команды на руль. Наложенные раннее ограничения на максимальные угол и скорость поворота учитываются в MRAC-контроллере.

Существует также постфильтрация полученных значений. За это отвечает digital_filter_. Он фильтрует lateral и heading ошибки.

## Отладка широтного контроллера

Задача настройки контроллера сводится к тому, чтобы обратная связь сигнала управления была как можно более идентична сигналу управления. Оценить эту характеристику лучше всего с использованием Foxglove. В широтном контроллере интересны следующие сигналы: широта, курсовой угол, команды на руль. Начинать настройку лучше всего с команд на руль, так как если система управления руля не отрабатывает сигнал управления как нужно, то оценить, где причина больших широтной ошибки и ошибки курсового угла, в соответстсвующих контроллерах или в руле достаточно сложно.

### Отладка контроллера руля

Для оценки качества управления рулём наиболее подходят графики из топика модуля control, показывающие steer_angle и feedback по нему:

![mrac](images/otladka_1.png)

Результат хорошей настройки должен выглядеть примерно так:

![mrac](images/otladka_2.png)

Здесь на начальном этапе происходят переходные процессы, но по их окончании сигнал управления и обратная связь по сигналу управления очень близки. Влиять на качество управления рулём можно так:

1. Настройка ПИД-контроллера непосредственно руля в альфе. 

Коэффициенты ПИД-регулятора руля находятся в modules/calibration/datd/mb_actros/alpha_conf/alpha_param.json.

Коэффициент P в первую очередь влияет на быстродействие и чувствительность (насколько маленький угол поворота руля контроллер сможет отработать), коэффициент I компенсирует статическую ошибку, коэффициент D влияет на колебательность и дребезжание руля. Saturation'ы в контроллере предотвращают насыщение.

2. Настройка некоторых параметров в конфигурационном файле модуля control modules/control/conf/control_conf.pb.txt. 

Параметр max_steering_percentage_allowed может ограничить максимальное вращение руля, если есть такая необходимость.

Параметр max_lateral_acceleration влияет на быстродействие, чувствительность и реакцию руля.

Значительное влияние может оказать MRAC-контроллер. Его основная задача - минимизация задержки между отправкой и получением команды на руль. MRAC-контроллер включается через параметр enable_steer_mrac_control. Параметры mrac_saturation_level и anti_windup_compensation_gain влияют на реакцию и колебательность. Общая рекомендация mrac_saturation_level делать небольшим по величине, anti_windup_compensation_gain без необходимости windup компенсации деражть нулевым.

3. Последующая настройка широтного регулирования и регулирования курсового угла. 

Настройка широтной составляющей и составляющей, отвечающей за курсовой угол могут повлиять на настройку руля. В этом случае необходимо провести перенастройку параметров из пунктов 1 и 2.

### Отладка широтной составляющей контроллера

Широтная составляющая контроллера регулируется параметрами в modules/control/conf/control_conf.pb.txt. Изменение 3-го matrix_q влияет на величину широтной ошибки и на чувствительность, изменение 4-го matrix_q влияет на быстродействие широтного регулирования.

**Комбинация высокой чувствительности и высокой реакции приводит к колебаниями!**

Изменение параметра max_lateral_acceleration влияет на широтную ошибку.

Кроме того, в контроллере реализовано изменение коэффициентов широтной составляющей в зависимости от скорости движения в виде lat_err_gain_scheduler. Общая рекомендация по настройке следующая: сначала настраивается движение на небольшой скорости со значением коэффициента умножения для этой скорости ratio: 1.0. Затем происходит увеличение скорости и, если контроллер не справляется, то изменять лучше именно коэффициент умножения ratio (обычно для больших скоростей его нужно уменьшать).

Качество управления можно оценивать по графику широтной ошибки из топика control:

![mrac](images/otladka_l.png)

На приведённом графике максимальное значение широтной ошибки около 0.1 м, что является качественным регулированием.

### Отладка составляющей, отвечающей за курсовой угол

Настройка аналогична настройке широтной составляющей контроллера, но настраивать нужно 1-й и 2-й matrix_q. Изменение 1-го отвечает за величину ошибки курсового угла и чувствительность, изменение 2-го за быстродействие. Также реализовано изменение коэффициентов в зависимости от скорости.

Качество управления можно оценивать по графику ошибки курсового угла из топика control:

![mrac](images/otladka_h.png)

На приведённом графике максимальная ошибка равна 0.017 радиан или 0.969 градусов, что является качественным регулированием.

## Связанные конфиги

<details>
<summary>Таблица</summary>

| <p align ="center">Переменная</p> | <p align ="center">Расшифровка</p> | <p align ="center">Значение по умолчанию</p> |
|----------|----------|-----------------------|
| ts | Временной интервал, соответствующий среднему периоду отработки алгоритма, служит входом для всех PID | 0.01 |
| preview_window | Используется для расчета количества шагов, на которые заранее считается ошибка положения | 0 |
| cf | Угловая жесткость обобщенного переднего колеса | 155494.663 |
| cr | Угловая жесткость обобщенного заднего колеса | 155494.663 |
| mass_fl | Полная масса автомобиля, приходящаяся на переднее левое колесо | 520 |
| mass_fr | Полная масса автомобиля, приходящаяся на переднее правое колесо | 520 |
| mass_rl | Полная масса автомобиля, приходящаяся на заднее левое колесо | 520 |
| mass_rr | Полная масса автомобиля, приходящаяся на заднее правое колесо | 520 |
| eps | Точность приближения для решения задачи оптимизации | 0.01 |
| matrix_q | Значения весовой матрицы состояния в формате diag[Q1, Q2, ..., Qn]. Значение Q1 | 0.05 |
| matrix_q | Значения весовой матрицы состояния в формате diag[Q1, Q2, ..., Qn]. Значение Q2 | 0.0 |
| matrix_q | Значения весовой матрицы состояния в формате diag[Q1, Q2, ..., Qn]. Значение Q3 | 1.0 |
| matrix_q | Значения весовой матрицы состояния в формате diag[Q1, Q2, ..., Qn]. Значение Q4 | 0.0 |
| reverse_matrix_q | Значения весовой матрицы состояния в формате diag[Q1, Q2, ..., Qn] для движения назад (задний ход). Значение Q1 | 0.05 |
| reverse_matrix_q | Значения весовой матрицы состояния в формате diag[Q1, Q2, ..., Qn] для движения назад (задний ход). Значение Q2 | 0.0 |
| reverse_matrix_q | Значения весовой матрицы состояния в формате diag[Q1, Q2, ..., Qn] для движения назад (задний ход). Значение Q3 | 1.0 |
| reverse_matrix_q | Значения весовой матрицы состояния в формате diag[Q1, Q2, ..., Qn] для движения назад (задний ход). Значение Q4 | 0.0 |
| cutoff_freq | Значение частоты среза для фильтра Баттерворта | 10 |
| mean_filter_window_size | Интервал работы фильтра средних частот | 10 |
| max_iteration | Максимальная длина горизонта предсказания динамики и решения задачи оптимизации в итерациях | 150 |
| max_lateral_acceleration | Максимальное значение поперечного ускорения | 5.0 |
| enable_reverse_leadlag_compensation | Включить компенсатор запаздывания | true |
| enable_steer_mrac_control | Включить MRAC-контроллер | false |
||||
| enable_look_ahead_back_control || true |
| lookahead_station || 1.4224 |
| lookback_station || 2.8448 |
| lat_err_gain_scheduler |||
| speed | Скорость | 4.0; 8.0; 12.0; 20.0; 25.0 |
| ratio | Передаточное число | 1.0; 0.6; 0.2; 0.1; 0.05 |
| heading_err_gain_scheduler |||
| speed | Скорость | 4.0; 8.0; 12.0; 20.0; 25.0 |
| ratio | Передаточное число | 1.0; 0.6; 0.2; 0.1; 0.05 |
| reverse_leadlag_conf | Конфигурация альфа-бета фильтра (наблюдателя, контроллера) запаздывания по для заднего хода ||
| innerstate_saturation_level | Ограничение насыщения (по модулю) | 3000 |
| alpha | Альфа составляющая | 1.0 |
| beta | Бета составляющая | 1.0 |
| tau | Тау (гамма) составляющая | 0.0 |
| steer_mrac_conf | Настройки MRAC-контроллера ||
| mrac_model_order | Порядок матрицы эталонной модели (1 - только положение руля, 2 - положение и скорость) | 1 |
| reference_time_constant | Временной интервал, соответствующий среднему периоду отработки алгоритма для эталонной модели. ДОЛЖЕН быть равен ts | 0.01 |
| reference_natural_frequency | Частота для эталонной модели | 10 |
| reference_damping_ratio | Коэффициент демпфирования (для гашения колебаний сигналов) | 0.9 |
| adaption_state_gain | Коэффициент адаптации входного сигнала | 1.0 |
| adaption_desired_gain | Коэффициент адаптации желаемого сигнала | 1.0 |
| adaption_nonlinear_gain | Коэффициент нелинейной адаптации | 1.0 |
| adaption_matrix_p | Коэффициент адаптации матрицы p | 1.0 |
| mrac_saturation_level | Уровень насыщения контроллера | 1.0 |
| anti_windup_compensation_gain | Компенсатор возбуждения для интегратора | 0.0 |

</details>