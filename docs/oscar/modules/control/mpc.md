# MPC controller (MPC)

1. [Описание работы алгоритма](#описание-работы-алгоритма)
2. [Уравнение динамики](#уравнение-динамики)
3. [Матрицы и выражения описания системы](#матрицы-и-выражения-описания-системы)
4. [Формирование курсового угла](#формирование-курсового-угла)
4. [Краткое руководство по взаимодействию](#краткое-руководство-по-взаимодействию)
5. [Связанные конфиги](#связанные-конфиги)

**Model Predictive Control (MPC)** - вид управления, основанный на предсказании поведения системы. Он основывается на реакциях системы на стандартные воздействия (неких эмпирических данных, полученных ранее). Задача сводится к предсказанию поведения зависимых переменных при изменении независимых. Зависимые переменные - те, что мы можем каким-либо образом изменять через контроллер (скорость, ускорение и др.), на независимые же никак повлиять нельзя (расход топлива, окружающие объекты (реакция на них)). В MPC независимые переменные представляются как помехи, которые необходимо компенсировать.  В Apollo MPC используется в **контроллере долготы (longitudinal)**. Очень популярен для систем, построенных на **репере Френе (Frenet frame)**.

![mpc](images/MPC.png) 

## Описание работы алгоритма

Контроллер получает на вход актуальные сообщения каналов планировщика, локализации и шасси. Первый этап - расчёт долгтных ошибок. Процесс аналогичен описанному в [Longitudinal controller](longitudinal.md). 
Затем происходит обновление матрицы состояний. Суть процесса заключается в вычислении широтных ошибок и внесении их в матрицу.
<details>
<summary>Основные моменты:</summary>

```c++
  const auto matched_point =
      trajectory_analyzer.QueryNearestPointByPosition(x, y);

  const double dx = x - matched_point.path_point().x();
  const double dy = y - matched_point.path_point().y();

  const double cos_matched_theta = std::cos(matched_point.path_point().theta());
  const double sin_matched_theta = std::sin(matched_point.path_point().theta());

  debug->set_lateral_error(cos_matched_theta * dy - sin_matched_theta * dx);


  debug->set_ref_heading(matched_point.path_point().theta());
  const double delta_theta =
      common::math::NormalizeAngle(theta - debug->ref_heading());
  debug->set_heading_error(delta_theta);

  const double sin_delta_theta = std::sin(delta_theta);

  double lateral_error_dot = linear_v * sin_delta_theta;
  double lateral_error_dot_dot = linear_a * sin_delta_theta;

  debug->set_lateral_error_rate(lateral_error_dot);
  debug->set_lateral_acceleration(lateral_error_dot_dot);
  debug->set_lateral_jerk(
      (debug->lateral_acceleration() - previous_lateral_acceleration_) / ts_);
  previous_lateral_acceleration_ = debug->lateral_acceleration();

  debug->set_curvature(matched_point.path_point().kappa());

  debug->set_heading_error(delta_theta);
  debug->set_heading_rate(angular_v);
  debug->set_ref_heading_rate(debug->curvature() * matched_point.v());
  debug->set_heading_error_rate(debug->heading_rate() -
                                debug->ref_heading_rate());

  debug->set_heading_acceleration(
      (debug->heading_rate() - previous_heading_rate_) / ts_);
  debug->set_ref_heading_acceleration(
      (debug->ref_heading_rate() - previous_ref_heading_rate_) / ts_);
  debug->set_heading_error_acceleration(debug->heading_acceleration() -
                                        debug->ref_heading_acceleration());
  previous_heading_rate_ = debug->heading_rate();
  previous_ref_heading_rate_ = debug->ref_heading_rate();

  debug->set_heading_jerk(
      (debug->heading_acceleration() - previous_heading_acceleration_) / ts_);
  debug->set_ref_heading_jerk(
      (debug->ref_heading_acceleration() - previous_ref_heading_acceleration_) /
      ts_);
  debug->set_heading_error_jerk(debug->heading_jerk() -
                                debug->ref_heading_jerk());
  previous_heading_acceleration_ = debug->heading_acceleration();
  previous_ref_heading_acceleration_ = debug->ref_heading_acceleration();
...
  matrix_state_(0, 0) = debug->lateral_error();
  matrix_state_(1, 0) = debug->lateral_error_rate();
  matrix_state_(2, 0) = debug->heading_error();
  matrix_state_(3, 0) = debug->heading_error_rate();
  matrix_state_(4, 0) = debug->station_error();
  matrix_state_(5, 0) = debug->speed_error();
```

</details>

<details>
<summary>Следующий этап - обновление матрицы:</summary>

```c++
  matrix_a_(1, 1) = matrix_a_coeff_(1, 1) / v;
  matrix_a_(1, 3) = matrix_a_coeff_(1, 3) / v;
  matrix_a_(3, 1) = matrix_a_coeff_(3, 1) / v;
  matrix_a_(3, 3) = matrix_a_coeff_(3, 3) / v;

  Matrix matrix_i = Matrix::Identity(matrix_a_.cols(), matrix_a_.cols());
  matrix_ad_ = (matrix_i - ts_ * 0.5 * matrix_a_).inverse() *
               (matrix_i + ts_ * 0.5 * matrix_a_);

  matrix_c_(1, 0) = (lr_ * cr_ - lf_ * cf_) / mass_ / v - v;
  matrix_c_(3, 0) = -(lf_ * lf_ * cf_ + lr_ * lr_ * cr_) / iz_ / v;
  matrix_cd_ = matrix_c_ * debug->ref_heading_rate() * ts_;
```
Здесь matrix_a_ - матрица состояния беспилотника:
```c++
  matrix_a_(0, 1) = 1.0;
  matrix_a_(1, 2) = (cf_ + cr_) / mass_;
  matrix_a_(2, 3) = 1.0;
  matrix_a_(3, 2) = (lf_ * cf_ - lr_ * cr_) / iz_;
  matrix_a_(4, 5) = 1.0;
  matrix_a_(5, 5) = 0.0;
```
где cf, cr - коэффициенты жесткости на поворотах, берутся из vehicle_params.pb.txt. lr_ = wheelbase_ * (1.0 - mass_rear / mass_), lf_ = wheelbase_ * (1.0 - mass_front / mass_), iz_ = lf_ * lf_ * mass_front + lr_ * lr_ * mass_rear;
matrix_a_coeff_ - матрица коэффициентов состояния беспилотника:
```c++
  matrix_a_coeff_(1, 1) = -(cf_ + cr_) / mass_;
  matrix_a_coeff_(1, 3) = (lr_ * cr_ - lf_ * cf_) / mass_;
  matrix_a_coeff_(2, 3) = 1.0;
  matrix_a_coeff_(3, 1) = (lr_ * cr_ - lf_ * cf_) / iz_;
  matrix_a_coeff_(3, 3) = -1.0 * (lf_ * lf_ * cf_ + lr_ * lr_ * cr_) / iz_;
```
matrix_ad_ - это дискретизированная matrix_a_:
```c++
  matrix_ad_ = (matrix_i - ts_ * 0.5 * matrix_a_).inverse() *
               (matrix_i + ts_ * 0.5 * matrix_a_);
```
matrix_c_ - матрица смещений.
matrix_cd_ - дискретизированная matrix_c_. 

</details>

<details>
<summary>Следующий этап - вычисление сигнала feedforward (прямая связь):</summary>

```c++
  const double v = injector_->vehicle_state()->linear_velocity();
  const double kv =
      lr_ * mass_ / 2 / cf_ / wheelbase_ - lf_ * mass_ / 2 / cr_ / wheelbase_;
  steer_angle_feedforwardterm_ = Wheel2SteerPct(
      wheelbase_ * debug->curvature() + kv * v * v * debug->curvature());
```
Здесь wheelbase - колёсная база из vehicle_params.pb.txt. Функция Wheel2SteerPct возвращает wheel_angle / wheel_single_direction_max_degree_ * 100.
Если в config был прописан флаг enable_gain_scheduler, то в зависимости от текущей скорости коэффициенты матриц регулятора (matrix_q и matrix_r) будут умножены на значение из control_conf.pb.txt. Если флаг не был установлен, то матрица останется неизменной:
```c++
  if (FLAGS_enable_gain_scheduler) {
    matrix_q_updated_(0, 0) =
        matrix_q_(0, 0) *
        lat_err_interpolation_->Interpolate(vehicle_state->linear_velocity());
    matrix_q_updated_(2, 2) =
        matrix_q_(2, 2) * heading_err_interpolation_->Interpolate(
                              vehicle_state->linear_velocity());
    steer_angle_feedforwardterm_updated_ =
        steer_angle_feedforwardterm_ *
        feedforwardterm_interpolation_->Interpolate(
            vehicle_state->linear_velocity());
    matrix_r_updated_(0, 0) =
        matrix_r_(0, 0) * steer_weight_interpolation_->Interpolate(
                              vehicle_state->linear_velocity());
  } else {
    matrix_q_updated_ = matrix_q_;
    matrix_r_updated_ = matrix_r_;
    steer_angle_feedforwardterm_updated_ = steer_angle_feedforwardterm_;
  }
```
Значения матрицы q по умолчанию берутся из control_conf.pb.txt.
</details>

На следующем этапе происходит определение необходимой компенсации (иcходя из ошибки полученной по обратной связи (Feedback)). Для решения задачи используется солвер OSQP:

<details>
<summary></summary>

```c++
  apollo::common::math::MpcOsqp mpc_osqp(
      matrix_ad_, matrix_bd_, matrix_q_updated_, matrix_r_updated_,
      matrix_state_, lower_bound, upper_bound, lower_state_bound,
      upper_state_bound, reference_state, mpc_max_iteration_, horizon_,
      mpc_eps_);
  if (!mpc_osqp.Solve(&control_cmd)) {
    AERROR << "MPC OSQP solver failed";
  } else {
    ADEBUG << "MPC OSQP problem solved! ";
    control[0](0, 0) = control_cmd.at(0);
    control[0](1, 0) = control_cmd.at(1);
  }
```
На выходе формируются два значения ошибок:
control[0](0, 0) = control_cmd.at(0) - значение ошибки угла поворота (steering angle).
control[0](1, 0) = control_cmd.at(1) - значение долготной ошибки (ошибка ускорения).
</details>

Если включён флаг enable_feedforward_compensation, то происходит вычисление значения сигнала управления, которое необходимо компенсировать:

<details>
<summary></summary>

```c++
  for (int i = 0; i < basic_state_size_; ++i) {
    unconstrained_control += control_gain[0](0, i) * matrix_state_(i, 0);
  }
  unconstrained_control += addition_gain[0](0, 0) * v * debug->curvature();
  if (enable_mpc_feedforward_compensation_) {
    unconstrained_control_diff =
        Wheel2SteerPct(control[0](0, 0) - unconstrained_control);
    if (fabs(unconstrained_control_diff) <= unconstrained_control_diff_limit_) {
      steer_angle_ff_compensation =
          Wheel2SteerPct(debug->curvature() *
                         (control_gain[0](0, 2) *
                              (lr_ - lf_ / cr_ * mass_ * v * v / wheelbase_) -
                          addition_gain[0](0, 0) * v));
    } else {
      control_gain_truncation_ratio = control[0](0, 0) / unconstrained_control;
      steer_angle_ff_compensation =
          Wheel2SteerPct(debug->curvature() *
                         (control_gain[0](0, 2) *
                              (lr_ - lf_ / cr_ * mass_ * v * v / wheelbase_) -
                          addition_gain[0](0, 0) * v) *
                         control_gain_truncation_ratio);
    }
  } else {
    steer_angle_ff_compensation = 0.0;
  }

```
</details>

На следующем этапе происходит вычисление результирующего угла поворота беспилотника:

<details>
<summary></summary>

```c++
  double steer_angle = steer_angle_feedback +
                       steer_angle_feedforwardterm_updated_ +
                       steer_angle_ff_compensation;
```
</details>

Если установлен соответствующий флаг, то максимальный поворот руля может быть ограничен:

<details>
<summary></summary>

```c++
  if (FLAGS_set_steer_limit) {
    const double steer_limit = std::atan(max_lat_acc_ * wheelbase_ /
                                         (vehicle_state->linear_velocity() *
                                          vehicle_state->linear_velocity())) *
                               steer_ratio_ * 180 / M_PI /
                               steer_single_direction_max_degree_ * 100;

    // Clamp the steer angle with steer limitations at current speed
    double steer_angle_limited =
        common::math::Clamp(steer_angle, -steer_limit, steer_limit);
    steer_angle_limited = digital_filter_.Filter(steer_angle_limited);
    steer_angle = steer_angle_limited;
    debug->set_steer_angle_limited(steer_angle_limited);
  }
```
</details>

Таким образом формирование команды на поворот руля завершилось:

<details>
<summary></summary>

```c++
  steer_angle = common::math::Clamp(steer_angle, -100.0, 100.0);
  cmd->set_steering_target(steer_angle);
```
</details>

Параллельно формируется команда ускорения:

<details>
<summary></summary>

```c++
  if ((planning_published_trajectory->trajectory_type() ==
       apollo::planning::ADCTrajectory::NORMAL) &&
      (std::fabs(debug->acceleration_reference()) <=
           max_acceleration_when_stopped_ &&
       std::fabs(debug->speed_reference()) <= max_abs_speed_when_stopped_)) {
    acceleration_cmd =
        (chassis->gear_location() == canbus::Chassis::GEAR_REVERSE)
            ? std::max(acceleration_cmd, -standstill_acceleration_)
            : std::min(acceleration_cmd, standstill_acceleration_);
    ADEBUG << "Stop location reached";
    debug->set_is_full_stop(true);
  }
```
</details>

Для полученного ускорения из калибровочной таблицы подбираются соответствующие значения газа и тормоза:

<details>
<summary></summary>

```c++
  double calibration_value = 0.0;
  if (FLAGS_use_preview_speed_for_table) {
    calibration_value = control_interpolation_->Interpolate(
        std::make_pair(debug->speed_reference(), acceleration_cmd));
  } else {
    calibration_value = control_interpolation_->Interpolate(std::make_pair(
        injector_->vehicle_state()->linear_velocity(), acceleration_cmd));
  }

  debug->set_calibration_value(calibration_value);

  double throttle_cmd = 0.0;
  double brake_cmd = 0.0;
  if (calibration_value >= 0) {
    throttle_cmd = std::max(calibration_value, throttle_lowerbound_);
    brake_cmd = 0.0;
  } else {
    throttle_cmd = 0.0;
    brake_cmd = std::max(-calibration_value, brake_lowerbound_);
  }
```
</details>

Если установлен флаг, ограничивающий скорость вращения руля, то ограничение будет выставлено:

<details>
<summary></summary>

```c++
  cmd->set_steering_rate(FLAGS_steer_angle_rate);
```
</details>
На этом этапе команды для долготного управления сформированы:

<details>
<summary></summary>

```c++
  cmd->set_throttle(throttle_cmd);
  cmd->set_brake(brake_cmd);
  cmd->set_acceleration(acceleration_cmd);
```
</details>

## Уравнения динамики

![mdl](images/model.jpg)

![eq1](images/equation1.png)

## Матрицы и выражения описания системы

![eq2](images/equation2.png)

Состояние приводится в пространстве ошибок. Весовые матрицы:

![eq3](images/equation3.png)

Q<sub>11</sub> - вес ошибки поперечного положения;
Q<sub>22</sub> - вес ошибки поперечной скорости;
Q<sub>33</sub> - вес ошибки направления (угла рысканья);
Q<sub>44</sub> - вес ошибки скорости изменения направления;
Q<sub>55</sub> - вес ошибки продольного положения;
Q<sub>66</sub> - вес ошибки продольной скорости;
R<sub>11</sub> - вес изменения угла поворота;
R<sub>22</sub> - вес изменения ускорения.

Критерий оптимальности:

![eq4](images/equation4.png)

## Формирование курсового угла

На вход в контроллер приходят координаты, ориентация, heading, траектория  автомобиля и control_command в точке траектории.

Если модуль не получает heading из локализации, то рассчитывает его самостоятельно из ориентации.

Команда на руль рассчитывается следующим образом:

```c++
  double steer_angle = steer_angle_feedback +
                       steer_angle_feedforwardterm_updated_ +
                       steer_angle_ff_compensation;
```
Здесь steer_angle_feedback - текущий угол поворота шасси, steer_angle_feedforwardterm_updated_ - запаздывание, steer_angle_ff_compensation - ошибка.

<details>
<summary>Определение steer_angle_feedback:</summary>

```c++
steer_angle_feedback = Wheel2SteerPct(control[0](0, 0));
...
double MPCController::Wheel2SteerPct(const double wheel_angle) {
  return wheel_angle / wheel_single_direction_max_degree_ * 100;
}
```
control - команды на управление, полученые после решения задачи солвером osqp:
```c++
  apollo::common::math::MpcOsqp mpc_osqp(
      matrix_ad_, matrix_bd_, matrix_q_updated_, matrix_r_updated_,
      matrix_state_, lower_bound, upper_bound, lower_state_bound,
      upper_state_bound, reference_state, mpc_max_iteration_, horizon_,
      mpc_eps_);
  if (!mpc_osqp.Solve(&control_cmd)) {
    AERROR << "MPC OSQP solver failed";
  } else {
    ADEBUG << "MPC OSQP problem solved! ";
    control[0](0, 0) = control_cmd.at(0);
    control[0](1, 0) = control_cmd.at(1);
  }
```

</details>

<details>
<summary>Определение steer_angle_feedforwardterm_updated_:</summary>

```c++
  if (FLAGS_enable_gain_scheduler) {
    matrix_q_updated_(0, 0) =
        matrix_q_(0, 0) *
        lat_err_interpolation_->Interpolate(vehicle_state->linear_velocity());
    matrix_q_updated_(2, 2) =
        matrix_q_(2, 2) * heading_err_interpolation_->Interpolate(
                              vehicle_state->linear_velocity());
    steer_angle_feedforwardterm_updated_ =
        steer_angle_feedforwardterm_ *
        feedforwardterm_interpolation_->Interpolate(
            vehicle_state->linear_velocity());
    matrix_r_updated_(0, 0) =
        matrix_r_(0, 0) * steer_weight_interpolation_->Interpolate(
                              vehicle_state->linear_velocity());
  } else {
    matrix_q_updated_ = matrix_q_;
    matrix_r_updated_ = matrix_r_;
    steer_angle_feedforwardterm_updated_ = steer_angle_feedforwardterm_;
  }
```
Флаг FLAGS_enable_gain_scheduler отвечает за изменение коэффициентов регулятора для разных скоростей (из табличек в control_conf.pb.txt)
Здесь matrix_q_ и matrix_r_ - матрицы коэффициентов регулятора, которые прописаны в config файле(control_conf.pb.txt).

</details>

<details>
<summary>Определение steer_angle_ff_compensation:</summary>

```c++
  for (int i = 0; i < basic_state_size_; ++i) {
    unconstrained_control += control_gain[0](0, i) * matrix_state_(i, 0);
  }
  unconstrained_control += addition_gain[0](0, 0) * v * debug->curvature();
  if (enable_mpc_feedforward_compensation_) {
    unconstrained_control_diff =
        Wheel2SteerPct(control[0](0, 0) - unconstrained_control);
    if (fabs(unconstrained_control_diff) <= unconstrained_control_diff_limit_) {
      steer_angle_ff_compensation =
          Wheel2SteerPct(debug->curvature() *
                         (control_gain[0](0, 2) *
                              (lr_ - lf_ / cr_ * mass_ * v * v / wheelbase_) -
                          addition_gain[0](0, 0) * v));
    } else {
      control_gain_truncation_ratio = control[0](0, 0) / unconstrained_control;
      steer_angle_ff_compensation =
          Wheel2SteerPct(debug->curvature() *
                         (control_gain[0](0, 2) *
                              (lr_ - lf_ / cr_ * mass_ * v * v / wheelbase_) -
                          addition_gain[0](0, 0) * v) *
                         control_gain_truncation_ratio);
    }
  } else {
    steer_angle_ff_compensation = 0.0;
  }
```
Здесь: 

control_gain - главная матрица команд на управление (сформирована из controls_ под размер матрицы состояний)

```c++
  Matrix control_gain_matrix = Matrix::Zero(controls_, basic_state_size_);
  std::vector<Matrix> control_gain(horizon_, control_gain_matrix);
```
addition_gain - вспомогательная матрица команд на управление (сформирована из controls_ под вектор (для линейной скорости))
```c++
  Matrix addition_gain_matrix = Matrix::Zero(controls_, 1);
  std::vector<Matrix> addition_gain(horizon_, addition_gain_matrix);
```
lr_ = wheelbase_ * (1.0 - mass_rear / mass_);

lf_ = wheelbase_ * (1.0 - mass_front / mass_);

mass_rear = mass_rl + mass_rr;

mass_front = mass_fl + mass_fr;

mass_fl, mass_fr, mass_rl, mass_rr - распределение масс по колёсам автомобиля (берутся из config файла (vehicle_param.pb.txt)).

cf, cr - жесткость автомобиля на поворотах (vehicle corning stiffness,берутся из config файла (vehicle_param.pb.txt)).

mass_ = mass_front + mass_rear;

unconstrained_control - ошибка feedforward;

control_gain_truncation_ratio - усечение.

</details>

В контроллере существует возможность ограничить максимальный угол поворота и максимальную скорость поворота. Ограничение угла задаётся параметром set_steer_limit в config файле модуля control (control.conf). Ограничения скорости задаётся двумя параметрами: флагом enable_maximum_steer_rate_limit в config файле модуля control (control.conf) и фактическим значением ограничения max_steer_angle_rate в vehicle_param.pb.txt.

В отличии от Lateral контроллера, использование MRAC-надстройки не предусмотрено, но у MPC-контроллера встроен свой алгоритм компенсации задержек по рулю, поэтому после корректной настройки контроллер должен справиться с широтной регуляцией и без MRAC-надстройки.

Существует также постфильтрация полученных значений. За это отвечает digital_filter_. Он фильтрует lateral и heading ошибки.
## Недостатки

1. При составлении закона управления игнорируется угол скольжения системы. Управление должно производится по проекции ускорения, но в реальности просисходит по полному вектору линейного ускорения;
2. Отсутствует вес, учитывающий скорость вращения руля;
3. В модели не учитываются рывки (производные выше второго порядка);
4. Из-за отсутствия учета поворота руля в калибровочной таблице а также недоработки 1 при тестировании часто наблюдается эффект "срезания углов" - эффект, при котором во время поворота руля автомобиль замедляется и накапливает ошибку продольного положения из-за неточностей табличного и модельного представления, в связи с этим происходит реплан (генерация новой траектории из-за большой ошибки) во время поворота, и, как следствие - рывок рулем.

## Краткое руководство по взаимодействию

Для работы с контроллером без желания вникать в математику можно прочитать данный абзац.

При настройке "с нуля" рекомендуется сначала внести кинематические параметры автомобиля в соответствующий конфиг. В Apollo умножения значений жесткостей cf и cr на два для учета слияния нет, как в большинстве других подобных алгоритмов, поэтому умножение происходит еще при конфигурации модели, то есть жесткости cf и cr обычно берутся равными 155494.663 (экспериментальное среднее значение). Распределение массы на колеса для первого приближения можно взять равное , но, если знаете примерный процент распределения нагрузки в автомобиле - можно пересчитать нагрузки на переднюю и заднюю ось.

Перед настройкой нужно убедится в рабочей частоте отработки, она должна быть не меньше ~60 Гц. В случае невыполнения данного условия можно уменьшить горизонт управления, но меньше 30-40 ставить смысла нет.

При настройке весов рекомендуется обнулить все веса кроме одного (например, продольного положения), и начинать настройку с него, ориентируясь на ошибку. Как только получится приемлемая реакция со стороны автомобиля, можно добавлять по одной остальные составляющие. После получения матрицы Q можно подстроить матрицу R для "штрафования" за резкие действия - управление должно стать мягче. Самые важные значения - 1, 3, 5, 6 в матрице Q. Поперечная ошибка (Lateral error), ошибка направления (Heading error), ошибка продольного положения (Station error) и ошибка скорости (Speed error). Набор минимум для успешного выполнения траекторий.

## Связанные конфиги

<details>
<summary>Таблица</summary>

| <p align ="center">Переменная</p> | <p align ="center">Расшифровка</p> | <p align ="center">Значение по умолчанию</p> |
|----------|----------|-----------------------|
| ts | Временной интервал, соответствующий среднему периоду отработки алгоритма, служит входом для всех PID | 0.01 |
| cf | Угловая жесткость обобщенного переднего колеса | 155494.663 |
| cr | Угловая жесткость обобщенного заднего колеса | 155494.663 |
| mass_fl | Полная масса автомобиля, приходящаяся на переднее левое колесо | 520 |
| mass_fr | Полная масса автомобиля, приходящаяся на переднее правое колесо | 520 |
| mass_rl | Полная масса автомобиля, приходящаяся на заднее левое колесо | 520 |
| mass_rr | Полная масса автомобиля, приходящаяся на заднее правое колесо | 520 |
| eps | Точность приближения для решения задачи оптимизации | 0.01 |
| matrix_q | Значения весовой матрицы состояния в формате diag[Q1, Q2, ..., Qn]. Значение Q1 | 3.0 |
| matrix_q | Значения весовой матрицы состояния в формате diag[Q1, Q2, ..., Qn]. Значение Q2 | 0.0 |
| matrix_q | Значения весовой матрицы состояния в формате diag[Q1, Q2, ..., Qn]. Значение Q3 | 35 |
| matrix_q | Значения весовой матрицы состояния в формате diag[Q1, Q2, ..., Qn]. Значение Q4 | 0.0 |
| matrix_q | Значения весовой матрицы состояния в формате diag[Q1, Q2, ..., Qn]. Значение Q5 | 50.0 |
| matrix_q | Значения весовой матрицы состояния в формате diag[Q1, Q2, ..., Qn]. Значение Q6 | 10.0 |
| matrix_r | Значения весовой матрицы управляющего воздействия в формате diag[R1, R2, ..., Rn]. Значение R1 | 3.25 |
| matrix_r | Значения весовой матрицы управляющего воздействия в формате diag[R1, R2, ..., Rn]. Значение R2 | 1.0 |
| cutoff_freq | Значение частоты среза для фильтра Баттерворта | 10 |
| mean_filter_window_size | Интервал фильтра средних частот | 10 |
| max_iteration | Максимальная длина горизонта предсказания динамики и решения задачи оптимизации в итерациях | 150 |
| max_lateral_acceleration | Максимальное значение поперечного ускорения  | 5.0 |
| standstill_acceleration | Значение ускорения после остановки | -0.3 |
| brake_minimum_action | Верхняя deadband-граница (мертвая зона) воздействия на педаль тормоза | 0.0 |
| throttle_minimum_action | Верхняя deadband-граница (мертвая зона) воздействия на педаль ускорения | 0.0 |
| enable_mpc_feedforward_compensation | Использование компенсации запаздывания в законе прямого управления | false |

</details>