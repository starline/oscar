# Модуль планирования (planning)

## Содержание

1. [Архитектура](#архитектура)
2. [Состав модуля](#состав-модуля)
3. [Теоретические основы](#теоретические-основы)
4. [Методы планирования модуля](#методы-планирования-модуля)
5. [Схема взаимодействий элементов модуля](#схема-взаимодействий-элементов-модуля)
6. [Алгоритмы парковки](#алгоритмы-парковки)
7. [Отладка модуля planning](#отладка-модуля-planning)
8. [Частота работы модуля Planning](#частота-работы-модуля-planning)
9. [Обработка данных карты в модуле](#обработка-данных-карты-в-модуле)
10. [Настройки, определяющие поведение автомобиля в движении и его взаимодействие с окружающими объектами, конфигурации модулей](#настройки-определяющие-поведение-автомобиля-в-движении-и-его-взаимодействие-с-окружающими-объектами-конфигурации-модулей)

Модуль **Planning** предназначен для создания траектории движения и отправки запросов на её исполнение модулю **Сontrol**. Один из основных модулей. Взаимодействует с модулями **Control**, **Localization**, **Routing** и **Perception**.
На вход модуля поступают данные из топиков _/routing/response_, _/traffic\_light_, _/planning\_pad_ и, опционально, _/relative\_map_. Модуль отправляет данные в топики _/planning\_trajectory_ и _/routing/request_. 
Через топик _/routing/response_ в систему поступают данные о маршруте. Для получения этих данных необходимо отправить запрос в топик _/routing/request_ (отправить начальную и конечную точки пути). Путь формируется в модуле маршрутизации **Routing** (вход модуля - карта и запрос). 
Из топика _/traffic\_light_ модуль получает разрешение или запрет на проезд перекрёстка (в зависимости от цвета).
Топик _/relative\_map_ подключается, если установлен специальный флаг и используется для работы в режиме навигации по карте.
_/planning\_pad_ - топик для ручного управления беспилотником. Инициализируется подмодулем _/pad\_terminal_. Возможны 7 вариантов:

0 - Движение вперёд;

1 - Влево;

2 - Вправо;

3 - Остановка у обочины;

4 - Стоп;

5 - Возобновить Круиз;

10 - Выход из ручного режима.

Данные для одного цикла планирования содержатся в виде *репера Френе(-Серре)* (*фрейм Френе(-Серре)*, *трёхгранник Френе(-Серре)* (*Frenet(-Serret) frame*)).

Подробная схема взаимодействия основных модулей **OSCAR**:

![Interactions](images/planning/Interaction.jpeg)

## Архитектура

Модуль планирования имеет модульную архитектуру. Каждый вариант сценария вождения разбит на модули, каждый модуль можно конфигурировать и менять так, чтобы это не влияло на работу других модулей. Архитектура модуля планирования представлена на схеме:

![Architecture](images/planning/architecture5.0.png)

## Состав модуля:

1. **Apollo FSM** - конечный автомат, который определяет состояние транспортного средства с помощью карты высокой четкости, учитывая его местоположение и маршрут.
2. **Planning dispatcher** - в зависимости от состояния объекта и другой связанной информации вызывает соответствующий планировщик
3. **Planner** - получает контекстные данные и всю необходимую информацию, определяет текущую цель ТС, выполянет планирование и генерирует траекторию, после чего обновляет Planning Context для последующих запусков.
4. **Deciders & Optimizers** - набор библиотек не имеющих строгой привязки к какому-либо модулю, которые осуществляют принятие решений (задачи) и различные оптимизации. Оптимизируется траектория (положения, скорости, ускорения). Решатели формируют советы когда перестроиться, когда остановиться, подползти или когда подползание выполнено. 
5. **Planning Context** - набор информации о уже выполненом планировании и движении. Необходим для формирования начальных условий (например, начальной скорости, ускорения и др. в случае, если новое планирования было запущено до окончания предыдущего) планирования.

## Теоретические основы

Для описания состояния автомобиля в любой момент времения используются 4 переменные: стандартные для всех схожих систем x, y, θ и дополнительный параметр κ. x и у - координаты беспилотника в мировой системе координат(СК), θ - курсовой угол, κ - кривизна в точке. 

Беспилотник представляется в виде **репера (рамки) Френе**. При заданной гладкой направляющей линии репер Френе разделяет движения транспортного средства в декартовом пространстве на два независимых одномерных движения: продольное движение вдоль направляющей линии, и поперечное движение ортогонально направляющей линии. Таким образом, задача планирования траектории в декартовом пространстве преобразуется в две менее размерные и независимые задачи планирования в системе Френе и это упрощает задачу планирования за счёт уменьшения размерности. 

Формально задача планирования траектории заключается в нахождении функции τ(t), которая для каждого момента времени t отображает соответствующую конфигурацию (x, y, θ, κ). Более приземлённая формулировка - генерация траектории вдоль направляющей линии. 

Значения скорости, ускорения, курсового угла и команды управления рулём неявно заложены в направляющей линии. Направляющая линия определяется на основе карты, как совокупность координат в СК карты. На первом этапе планирования происходит расчёт всех необходимых геометрических величин для планирования (кривизна, производная от кривизны, курсовой угол и т.д.) и сглаживание направляющей линии. Для сглаживания используются полиномиальные спиральные кривые пятого порядка. 

Расчёт курсового угла:
```txt
heading = atan2(x1 - х0, у1 - у0);
```
Расчёт происходит в файле reference_line_info.cc (версия apollo 6.0) в строчке:
```c++
    const double tracking_heading =
        std::atan2(cur_point_y - start_point_y, cur_point_x - start_point_x);
```
Расчёт параметра θ:
```txt
θ = ref_point.heading + atan2(frenet_point.dl, 1 - frenet_point.l * ref_point.kappa)
```
kappa, она же κ, она же кривизна. l - ширина (расстояние) (то же самое, что s, но для широты). dl - производная по l.
Расчёт происходит в файле path_data.cc (версия apollo 6.0) в строчке:
```c++
    const double theta = CartesianFrenetConverter::CalculateTheta(
        ref_point.heading(), ref_point.kappa(), frenet_point.l(),
        frenet_point.dl());
```

После сглаживания направляющей линии на втором этапе происходит генерация траектории. Траектория должна отвечать трём критериям (указаны в порядке приоритетности): достижимость цели, безопасность и комфорт. Генерация траектории происходит путём дискретизации сглаженной направляющей линии до производной второго порядка с постоянным членом третьего порядка и апроксимации дискретизированных точек с учётом ограничений. В модуле используется дискретизация по времени.

Есть три варианта сглаживания, которые могут быть использованы в модуле:

1. Discrete Points Smoother (Finite elements position deviation smoother).
2. QP-spline smoother
3. Spiral smoother

Настройки методов лежат в соответствующих конфигах в /modules/planning/conf/. Параметр **curb_shift** отвечает за кривизну сглаженной линии.


## Методы планирования модуля:

1. **Lattice**. Планировщик на основе наложения направленной сетки. Подробнее в: [Lattice_planner](lattice_planner.md)
2. **Navi**. Планировщик по навигационной карте. Подробнее в: [Navi_planner](navi_planner.md)
3. **Open_space**. Планировщик для парковки задним ходом. Подробнее в: [Open_space_planner](open_space_planner.md)
4. **Public_road**. Основной планировщик системы. Именно он выполняет планирования исходя из текущего сценария. Подробнее в: [Public_road_planner](public_road_planner.md)
5. **RTK**. Планировщик, который создаёт траекторию через несколько опорных точек на карте. Подробнее в: [RTK_planner](rtk_planner.md)

Взаимодействие происходит следующим образом. При запуске модуля инициализируется _planning\_component_, который, в зависимости от того, что требуется, запускает режим _OnLane\_planning_ или _navi\_planning_. Первый может инициализировать один из двух планировщиков: _lattice_ или _public\_road_ (по умолчанию _public\_road_). Второй инициализирует _navi\_planner_ или _RTK_. _Open\_space\_planner_ может вызываться в любом из режимов. _Scenario\_manager_ взаимодействует только с _public\_road planner_.

## Схема взаимодействий элементов модуля:

![flow_chart](images/planning/Diagram1.jpeg)

### Блок-схема элемента _OnLane\_planning_ представлена ниже:

![OnLane_planning](images/planning/OnLane_planning.jpeg)

_estop_ - emergency stop (экстренная остановка). Метод _Init_ подтягивается из планировщика, который инициализирован диспетчером.

Отдельной блок-схемой представлен метод _Plan_ для _OnLane\_planning_:

![Submodule_plan](images/planning/Plan_OnLane_.jpeg)

Метод _Plan_ подтягивается из планировщика, который инициализирован диспетчером.

### Блок-схема элемента _Navi\_planning_ представлена ниже:

![Navi_planning](images/planning/Navi_planning.jpeg)

Метод _Init_ подтягивается из планировщика, который инициализирован диспетчером.

Отдельной блок-схемой представлен метод _Plan_ для _Navi\_planning_:

![Submodule_plan2](images/planning/Plan_Navi.jpeg)

Метод _Plan_ подтягивается из планировщика, который инициализирован диспетчером.

Стоит отметить, запуск планировщика, происходящий вызовом метода _Plan_, в четырёх методах планирования прописан одинаково (имеется ввиду одинаковое название и входные данные). За определение конкретного планировщика отвечает подмодуль _planner\_dispatcher_. Поэтому теоретически _OnLane\_planning_ тоже может вызвать _navi\_planner_ и _RTK\_planner_, а _Navi\_planning_, соответственно, может вызвать _lattice\_planner_ и _public\_road\_planner_. В начале выполнения метода _Plan_ программы _navi\_planner_ стоит защита, которая проверяет что находится во флагах, и не разрешает выполнение, если флаг _use\_navi\_planning_ не установлен. У остальных планировщиков защиты на входе нет.

Подробнее о правилах дорожного движения и о том, как их изменять написано в [Rules](rules.md).

## Алгоритмы парковки

OSCAR имеет три алгоритма парковки: парковка у обочины, парковка на парковочное место, парковка и выезд со специальной полосы. Подробное описание доступно [здесь](parking.md).

## Отладка модуля planning

Модуль отвечает за создание траектории движения с допущением, что модуль control может отработать эту траекторию около идеально (определённая робастность в модуле planning сохраняется). Основная задача настройки - обеспечить построение безопасной траектории, если физически возможен безопасный проезд беспилотника.

![good_plan](images/planning/plan.png)

Отладка происходит путём изменения параметров, отвечающих за различные допуски и параметров, определяющих работу решателей дифференциальных уравнений.

Абсолютно все настройки со значениями по умолчанию перечислены в modules/planning/common/planning_gflags.cc. Чтобы не пересобирать проект, лучше всего прописывать новые настройки в modules/planning/conf/planning.conf (для режима navi_planning в planning_navi.conf). В модуле есть три сглаживателя траектории: spiral, qp_spline и discrete_points. Каждый из них имеет отдальный файл с настройками в директории modules/planning/conf/. Наиболее оптимальным для грузовика является discrete_points. 

В случае, если planning не создаёт траекторию и проблема локализована в нём (проблема может быть связана с control в общем случае), то в первую очередь имеет смысл отредактировать параметры допусков:

change_lane_min_length

longitudinal_acceleration_lower_bound

longitudinal_acceleration_upper_bound

lateral_acceleration_bound

static_obstacle_nudge_l_buffer

nonstatic_obstacle_nudge_l_buffer

lane_change_obstacle_nudge_l_buffer

lateral_ignore_buffer

max_stop_distance_obstacle

min_stop_distance_obstacle

obstacle_lat_buffer

obstacle_lon_start_buffer

obstacle_lon_end_buffer

Второй вариант решения проблемы - изменение параметров настройки оптимизаторов модуля. Настройки лежат в modules/planning/conf/planning_config.pb.txt.

**По умолчанию оптимизаторы настроены корректно. Изменять их стоит аккуратно и с бэкапами.**

## Частота работы модуля planning

По умолчанию частота публикации сообщений модулей задаётся в dag файле, через который происходит запуск модуля. Но часть модулей работают с частотой публикации сообщений других модулей. Planning является одним из таких модулей. [Подробнее про dag файлы](https://git.starline.ru/oscar/oscar-8/-/blob/master/docs/04_CyberRT/CyberRT_API_for_Developers.md?ref_type=heads#3-dag-file-format).
Модуль planning работает с частотой, равной частоте работы модуля prediction. Это реализовано в вызове основного метода модуля planning PlanningComponent::Proc. В свою очередь модуль prediction работает с частотой, равной частоте работы perception, частота работы которого зависит от частоты получения данных от сенсоров (в случае, если от разных сенсоров данные летят с разной частотой, выбирается опорная частота). Общая зависимость следующая: 

Опорный сенсор -> Perception -> Prediction -> Planning.

**Но есть исключение!** Если используется Fake_Prediction, то в его dag файле частота публикации сообщений задаётся явным образом:

```
interval: 100
```

Кроме этого, в модуле planning есть частота обновления траектории: planning_cycle_time. Она обратно пропорциональна параметру planning_loop_rate, который задаётся в файле настроек модуля planning modules/planning/conf/planning.conf. 

В случае, если разница между частотой работы модуля planning и частотой обновления траектории становится значительной (больше 90 fps), скорость движения беспилотника будет значительно превосходить (более чем на 2 м/с) заданную скорость движения в модуле planning, что будет вызывать колебания на педалях газа и тормоза, которые физически ощущаются как короткие рывки вперёд-назад. Также в ситуациях, когда данные о скорости по внешним причинам подскакивают (например, при наезде на кочку), случается резкое торможение. Допускать большой разницы между названными частотами не стоит. Изменение любой из частот требует подстройки регулятора руля и/или регуляторов модуля control. 

Также желательно, чтобы частота публикации сообщений модулем planning и planning_cycle_time были кратны одному числу, тогда наблюдается наибольшее соответствие reference от planning при исполнении на беспилотнике.
 
## Обработка данных карты в модуле

Обработка данных карты происходит в reference_line_provider.cc. Есть два варианта обработки, один в режиме navi_planning, другой в on-lane planning. 
Рассмотрим режим on-lane planning. На первом этапе из карты выделяются данные о номере полосы, её приоритетности и соседних полосах:

<details>
<summary>Первый этап</summary>

```c++
  for (const auto &path_pair : relative_map_->navigation_path()) {
    const auto lane_id = path_pair.first;
    navigation_lane_ids.insert(lane_id);
  }
  if (navigation_lane_ids.empty()) {
    AERROR << "navigation path ids is empty";
    return false;
  }
  // get current adc lane info by vehicle state
  common::VehicleState vehicle_state = vehicle_state_provider_->vehicle_state();
  hdmap::LaneWaypoint adc_lane_way_point;
  if (!GetNearestWayPointFromNavigationPath(vehicle_state, navigation_lane_ids,
                                            &adc_lane_way_point)) {
    return false;
  }
  const std::string adc_lane_id = adc_lane_way_point.lane->id().id();
  auto *adc_navigation_path = apollo::common::util::FindOrNull(
      relative_map_->navigation_path(), adc_lane_id);
  if (adc_navigation_path == nullptr) {
    AERROR << "adc lane cannot be found in relative_map_->navigation_path";
    return false;
  }
  const uint32_t adc_lane_priority = adc_navigation_path->path_priority();
  // get adc left neighbor lanes
  std::vector<std::string> left_neighbor_lane_ids;
  auto left_lane_ptr = adc_lane_way_point.lane;
  while (left_lane_ptr != nullptr &&
         left_lane_ptr->lane().left_neighbor_forward_lane_id_size() > 0) {
    auto neighbor_lane_id =
        left_lane_ptr->lane().left_neighbor_forward_lane_id(0);
    left_neighbor_lane_ids.emplace_back(neighbor_lane_id.id());
    left_lane_ptr = hdmap->GetLaneById(neighbor_lane_id);
  }
  ADEBUG << adc_lane_id
         << " left neighbor size : " << left_neighbor_lane_ids.size();
  for (const auto &neighbor : left_neighbor_lane_ids) {
    ADEBUG << adc_lane_id << " left neighbor : " << neighbor;
  }
  // get adc right neighbor lanes
  std::vector<std::string> right_neighbor_lane_ids;
  auto right_lane_ptr = adc_lane_way_point.lane;
  while (right_lane_ptr != nullptr &&
         right_lane_ptr->lane().right_neighbor_forward_lane_id_size() > 0) {
    auto neighbor_lane_id =
        right_lane_ptr->lane().right_neighbor_forward_lane_id(0);
    right_neighbor_lane_ids.emplace_back(neighbor_lane_id.id());
    right_lane_ptr = hdmap->GetLaneById(neighbor_lane_id);
  }
  ADEBUG << adc_lane_id
         << " right neighbor size : " << right_neighbor_lane_ids.size();
  for (const auto &neighbor : right_neighbor_lane_ids) {
    ADEBUG << adc_lane_id << " right neighbor : " << neighbor;
  }

```

</details>

На втором этапе выполняется проверка, нет ли более приоритетной полосы и, если есть, то определяет её как цель для перестроения.

<details>
<summary>Второй этап</summary>

```c++
  using LaneIdPair = std::pair<std::string, uint32_t>;
  std::vector<LaneIdPair> high_priority_lane_pairs;
  ADEBUG << "relative_map_->navigation_path_size = "
         << relative_map_->navigation_path_size();
  for (const auto &path_pair : relative_map_->navigation_path()) {
    const auto lane_id = path_pair.first;
    const uint32_t priority = path_pair.second.path_priority();
    ADEBUG << "lane_id = " << lane_id << " priority = " << priority
           << " adc_lane_id = " << adc_lane_id
           << " adc_lane_priority = " << adc_lane_priority;
    // the smaller the number, the higher the priority
    if (adc_lane_id != lane_id && priority < adc_lane_priority) {
      high_priority_lane_pairs.emplace_back(lane_id, priority);
    }
  }
  // get the target lane
  bool is_lane_change_needed = false;
  LaneIdPair target_lane_pair;
  if (!high_priority_lane_pairs.empty()) {
    std::sort(high_priority_lane_pairs.begin(), high_priority_lane_pairs.end(),
              [](const LaneIdPair &left, const LaneIdPair &right) {
                return left.second < right.second;
              });
    ADEBUG << "need to change lane";
    // the highest priority lane as the target navigation lane
    target_lane_pair = high_priority_lane_pairs.front();
    is_lane_change_needed = true;
  }
```

</details>

На следующем этапе определяется, справа или слева находится целевая полоса:

<details>
<summary>Третий этап</summary>

```c++
  routing::ChangeLaneType lane_change_type = routing::FORWARD;
  std::string nearest_neighbor_lane_id;
  if (is_lane_change_needed) {
    // target on the left of adc
    if (left_neighbor_lane_ids.end() !=
        std::find(left_neighbor_lane_ids.begin(), left_neighbor_lane_ids.end(),
                  target_lane_pair.first)) {
      // take the id of the first adjacent lane on the left of adc as
      // the nearest_neighbor_lane_id
      lane_change_type = routing::LEFT;
      nearest_neighbor_lane_id =
          adc_lane_way_point.lane->lane().left_neighbor_forward_lane_id(0).id();
    } else if (right_neighbor_lane_ids.end() !=
               std::find(right_neighbor_lane_ids.begin(),
                         right_neighbor_lane_ids.end(),
                         target_lane_pair.first)) {
      // target lane on the right of adc
      // take the id  of the first adjacent lane on the right of adc as
      // the nearest_neighbor_lane_id
      lane_change_type = routing::RIGHT;
      nearest_neighbor_lane_id = adc_lane_way_point.lane->lane()
                                     .right_neighbor_forward_lane_id(0)
                                     .id();
    }
  }
```

</details>

После получения необходимых данных, запускается процесс принятия решений.
По умолчанию, выбирается вариант движения по текущей полосе вперёд.

<details>
<summary>Первичное формирование</summary>

```c++
  for (const auto &path_pair : relative_map_->navigation_path()) {
    const auto &lane_id = path_pair.first;
    const auto &path_points = path_pair.second.path().path_point();
    auto lane_ptr = hdmap->GetLaneById(hdmap::MakeMapId(lane_id));
    RouteSegments segment;
    segment.emplace_back(lane_ptr, 0.0, lane_ptr->total_length());
    segment.SetCanExit(true);
    segment.SetId(lane_id);
    segment.SetNextAction(routing::FORWARD);
    segment.SetStopForDestination(false);
    segment.SetPreviousAction(routing::FORWARD);
```

</details>

Затем выполняется проверка, необходимо ли изменение полосы. Если необходимо, то в качестве действия выбирается смена полосы.
Если мы уже в нужном месте, то выбирается движение вперёд. Варианты определяются переменной lane_change_type (FORWARD, RIGHT, LEFT).

<details>
<summary>Проверка изменения полосы</summary>

```c++
    if (is_lane_change_needed) {
      if (lane_id == nearest_neighbor_lane_id) {
        ADEBUG << "adc lane_id = " << adc_lane_id
               << " nearest_neighbor_lane_id = " << lane_id;
        segment.SetIsNeighborSegment(true);
        segment.SetPreviousAction(lane_change_type);
      } else if (lane_id == adc_lane_id) {
        segment.SetIsOnSegment(true);
        segment.SetNextAction(lane_change_type);
      }
    }
```

</details>

На последнем этапе формируются reference_lines:

<details>
<summary>Формирование reference_lines</summary>

```c++
    segments->emplace_back(segment);
    std::vector<ReferencePoint> ref_points;
    for (const auto &path_point : path_points) {
      ref_points.emplace_back(
          MapPathPoint{Vec2d{path_point.x(), path_point.y()},
                       path_point.theta(),
                       LaneWaypoint(lane_ptr, path_point.s())},
          path_point.kappa(), path_point.dkappa());
    }
    reference_lines->emplace_back(ref_points.begin(), ref_points.end());
    reference_lines->back().SetPriority(path_pair.second.path_priority());
  }
  return !segments->empty();
```

</details>

## Настройки, определяющие поведение автомобиля в движении и его взаимодействие с окружающими объектами, конфигурации модулей

Эти настройки прописаны в oscar/modules/planning/common/planning_gflags.cc.

Здесь включаются и выключаются сценарии, подключаются config файлы для каждого сценария и задаётся ряд настроек для планировщкиов. Параметры приведены в таблице ниже.

<details>
<summary>Таблица</summary>

| <p align ="center">Параметр</p> | <p align ="center">Описание</p> | <p align ="center">Значение по умолчанию</p> |
|----------|----------|-----------------------|
|| <p align ="center">**RTK planner**</p> ||
| rtk_trajectory_forward | Количество точек, включённых в RTK траекторию | 800 |
| rtk_trajectory_resolution | Разрешение выходной траектории из RTK планнера | 0.1 |
| |
| look_forward_extend_distance | Размер шага при расширении контрольной линии | 50 |
| reference_line_stitch_overlap_distance | Расстояние до пересечения с существующей контрольной линией когда происходит сшивание контрольных линий | 20 |
| enable_smooth_reference_line | Сглаживание траектории | true |
| prioritize_change_lane | Предпочитать операцию по перестроению полосы другим операциям | false |
| change_lane_min_length | Дистанция маневра не должна превышать это значение | 30 |
| replan_lateral_distance_threshold | Порог значения широты для перепланирования | 0.5 |
| replan_longtitudinal_distance_threshold | Порог значения долготы для перепланирования | 2.5 |
| enable_reference_line_provider_thread | Включить поток для контрольной линии | true |
| default_reference_line_width | Начальная ширина контрольной линии | 4 |
| smoothed_reference_line_max_diff | Максимальное отклонение сглаженной траектории от несглаженной | 5 |
| planning_upper_speed_limit | Максимальная скорость (м/с) в планировании | 4.5 |
|| <p align ="center">**Контроль временной заполняемости траектории**</p> ||
| trajectory_time_length | Длительность траектории | 8 |
| trajectory_time_min_interval | Минимальное время публикования траектории (с) | 0.02 |
| trajectory_time_max_interval | Максимальное время публикования траектории (с) | 0.1 |
| trajectory_time_high_density_period | Сохранение высокой плотности в течение следующих нескольких секунд | 1 |
| |
| enable_trajectory_check | Проверка адекватности траектории | false |
| speed_lower_bound | Минимальная допустимая скорость | -0.1 |
| speed_upper_bound | Максимальная допустимая скорость | 40 |
| longtitudinal_acceleration_lower_bound | Минимальное допустимое ускорение по долготе | -4.5 |
| longtitudinal_acceleration_upper_bound | Максимальное допустимое ускорение по долготе | 4.0 |
| latteral_acceleration_bound | Граница ускорения по широте (симметричная вправо и влево) | 2 |
| longtitudinal_jerk_lower_bound | Минимальный рывок по долготе | -4 |
| longtitudinal_jerk_upper_bound | Максимальный рывок по долготе | 2 |
| bound_of_latteral_jerk | Границы рывка по широте (симметричная вправо и влево) | 2 |
| kappa_bound | Граница кривизны траектории | 0.1979 |
| st_max_s |  | 100 |
| st_max_t |  | 8 |
|| <p align ="center">**Принятие решений**</p> ||
| enable_nudge_slowdown | Включение замедления перед препятствием | true |
| static_obstacle_nudge_l_buffer | Минимальное расстояние до статического объекта | 0.6 |
| nonstatic_obstacle_nudge_l_buffer | Минимальное расстояние до динамического объекта | 0.8 |
| lane_change_obstacle_nudge_l_buffer | Минимальное расстояние при изменении полосы | 0.6 |
| latteral_ignore_buffer | Если препятствие за этой областью (по широте), то оно игнорируется | 3 |
| max_stop_distance_obstacle | Максимальная дистанция торможения перед объектом в полосе | 12 |
| min_stop_distance_obstacle | Минимальная дистанция торможения перед объектом в полосе | 10 |
| follow_min_distance | Минимальное расстояние до следующего авто/велосипеда/двигающегося объекта | 2.5 |
| yield_distance | Минимальное расстояние пропуска движущегося объекта ( исключая велосипедиста и пешехода ) | 15 |
| follow_time_buffer | Буффер в секундах для вычисления следующего расстояния | 2.5 |
| follow_min_time_sec | Минимальное время слежения в st области перед принятием приемлемого движения. это необходимо для того, чтобы различать движущееся препятствие, пересекающее текущую полосу беспилотника и перемещающееся в другом направлении | 2 |
| signal_expire_time_sec | Время считывания информации о сигнале светофора в секундах | 5 |
| destination_obstacle_id | Идентификатор препятствия для преобразования пункта назначения в препятствие | DEST |
| destination_check_distance | Если расстояние между беспилотником и местом назначения меньше этого, то считается, что мы достигли цели | 5 |
| virtual_stop_wall_length | Длина (метры) виртуальной стены остановки | 0.1 |
| virtual_stop_wall_height | Высота (метры) виртуальной стены остановки | 2 |
|| <p align ="center">**Определители пути**</p> ||
| enable_skip_path_tasks | Пропустить все задачи пути и использовать обрезанный предыдущий путь | false |
| obstacle_lat_buffer | Боковой буфер препятствий (в метрах) для определения границ трассы | 0.4 |
| obstacle_lon_start_buffer | Продольный стартовый буфер препятствия (в метрах) для для определения границ траектории | 3 |
| obstacle_lon_end_buffer | Продольный конечный буфер препятствия (в метрах) для для определения границ траектории | 2 |
| static_obstacle_speed_threshold | Порог скорости, позволяющий определить, является ли препятствие статичным или нет | 0.5 |
| lane_borrow_max_speed | Порог скорости для смены полосы движения | 5 |
| long_term_blocking_obstacle_cycle_threshold	| Порог цикла для длительно блокирующего препятствия | 3 |
|| <p align ="center"> **Предсказание (Prediction part)** </p> ||
| prediction_total_time | Время предсказания | 5 |
| align_prediction_time | Включить согласование времени планирования на основе данных прогноза | false |
|| <p align ="center">**Траектория**</p> ||
| turn_signal_distance | Расстояние до поворота, начиная с которого включается сигнал поворота (расстояние регламентируется ПДД) | 50 |
| trajectory_point_num_for_debug | Количество точек выходной траектории для отладки | 10 |
| lane_change_prepare_length | Минимальное расстояние, необходимое для смены полосы движения | 50 |
| allowed_lane_change_failure_time | Время, отведенное на сбой при смене полосы движения до обновления расстояния подготовки | 2 |
| enable_smarter_lane_change | Позволяет более разумно менять полосу движения с увеличенным расстоянием подготовки | false |
|| <p align ="center">**QpSt оптимизатор**</p> ||
| slowdown_profile_deceleration | Замедление для генерации профиля торможения. м/с^2 | -4 |
|| <p align ="center">**SQP решатель**</p> ||
| enable_sqp_solver | Вкл/выкл SQP решателя | true |
|| <p align ="center">**Потоки**</p> ||
| use_multi_thread_to_add_obstacles | Использовать несколько потоков для расчёта стоимости кривой в dp_st_graph | false |
|| <p align ="center">**Lattice planner**</p> ||
| numerical_epsilon | Epsilon | 1e-6 |
| default_cruise_speed | Скорость движения в круизе | 4 |
| trajectory_time_resolution | Разрешение времени планирования (шаг) | 0.1 |
| trajectory_space_resolution | Разрешение пространства планирования (шаг) | 1 |
| speed_lon_decision_horizon | Продольный горизонт для принятия решения о скорости (метры) | 100 |
| num_velocity_sample | Количество выборок скорости в "выбирателе" конечных условий | 6 |
| enable_backup_trajectory | Генерировать резервную траекторию когда планирование не удается | true |
| backup_trajectory_cost | Стоимость резервной траектории по умолчанию | 1000 |
| min_velocity_sample_gap | Минимальный интервал выборки для скорости | 1 |
| lon_collision_buffer | Буфер для сохранения продольных расстояний до других транспортных средств | 2.5 |
| lat_collision_buffer | Буфер для сохранения боковых расстояний до других транспортных средств | 1 |
| num_sample_follow_per_timestamp | Количество точек выборки для каждой последующей временной метки | 3 |
|| <p align ="center">**Lattice оценочные параметры**</p> ||
| weight_lon_objective | Значение стоимости продольного хода | 10 |
| weight_lon_jerk | Значение стоимости продольного рывка | 1 |
| weight_lon_collision | Значение стоимости продольного столкновения | 5 |
| weight_lat_offset | Значение стоимости бокового смещения | 2 |
| weight_lat_comfort | Значение стоимости бокового комфорта | 10 |
| weight_centripetal_acceleration | Значение центростремительного ускорения | 1.5 |
| cost_non_priority_reference_line | Стоимость планирования по неприоритетной контрольной линии | 5 |
| weight_same_side_offset | Значение стоимости бокового смещения в том же направлении | 1 |
| weight_opposite_side_offset | Значение стоимости бокового смещения в противоположном направлении | 10 |
| weight_dist_travelled | Значение стоимости пройденного пути | 10 |
| weight_target_speed | Значение стоимости целевой скорости | 1 |
| lat_offset_bound | Граница бокового смещения | 3 |
| lon_collision_yield_buffer | Продольный буфер для уступки | 1 |
| lon_collision_overtake_buffer | Продольный буфер для обгона | 5 |
| lon_collision_cost_std | Стандартное отклонение функции стоимости продольного столкновения | 0.5 |
| default_lon_buffer | Продольный буфер по умолчанию для выборки временных точек пути | 5 |
| time_min_density | Минимальная временная плотность для поиска точек выборки | 1 |
| comfort_acceleration_factor | Фактор комфортного разгона | 0.5 |
| polynomial_minimal_param | Минимальный параметр времени в полиномах | 0.01 |
| lattice_stop_buffer | Буфер перед остановкой для проверки траекторий | 0.02 |
| lateral_optimization | Использование оптимизации для планировщика | true |
| weight_lateral_offset | Значение смещения в оптимизации | 1 |
| weight_lateral_derivative | Значение производной | 500 |
| weight_lateral_second_order_derivative | Значение производной второго порядка | 1000 |
| weight_lateral_third_order_derivative | Значение производной третьего порядка | 1000 |
| weight_lateral_obstacle_distance | Значение расстояния до препятствия при оптимизации траектории | 0 |
| lateral_third_order_derivative_max | Максимальное разрешение (значение) для производной третьего порядка | 0.1 |
| lateral_derivative_bound_default | значение по умолчанию для границы производной | 2 |
| max_s_lateral_optimization | Максимальное s для оптимизации | 60 |
| default_delta_s_lateral_optimization | Дельта s по умолчанию для оптимизации | 1 |
| bound_buffer | Буфер до границе для боковой оптимизации | 0.1 |
| nudge_buffer | Буфер до подталкивания (столкновения) для оптимизации | 0.3 |
| fallback_total_time | Общее время траектории отката | 3 |
| fallback_time_unit | Время отката единицы траектории в секундах | 0.1 |
| speed_bump_speed_limit | Ограничение скорости при проезде лежачего полицейского, м/с. По умолчанию 10 миль/час | 4.4704 |
|| <p align ="center">**Navigation mode**</p> ||
| enable_planning_pad_msg | Управление включением сообщений панели планирования | false |
|| <p align ="center">**Open space planner**</p> ||
| open_space_planning_period | Расчетное время для периода планирования open space planner | 4 |
| open_space_prediction_time_horizon | Время в секундах, которое мы используем из траектории препятствий, полученной из prediction | 2 |
| enable_perception_obstacles | Учёт обнаруженных препятствий | true |
| enable_open_space_planner_thread | Включить поток в open space planner для публикации траектории | false |
| use_dual_variable_warm_start | Использовать двойной регулируемый теплый старт | true |
| use_gear_shift_trajectory | Предоставить автомобилю время для переключения передачи | false |
| open_space_trajectory_stitching_preserved_length | Количество сохраненных точек в траекторной сшивке для open space траектории | infinity |
| enable_smoother_failsafe | Следует ли использовать результат горячего старта в качестве окончательного вывода, когда плавный запуск не удается | false |
| use_s_curve_speed_smooth | Использование s-образной кривой для сглаживания Hybrid A* скорости/ускорения | false |
| use_iterative_anchoring_smoother | Использовать более плавное итеративное закрепление для планирования в open space | false |
| enable_parallel_trajectory_smoothing | Следует ли сначала разбивать траекторию и параллельно выполнять сглаживание | false |
| use_osqp_optimizer_for_reference_line | Использовать ли OSQP оптимизацию для контрольной линии | true |
| enable_osqp_debug | Включить подробный вывод отладки OSQP в журнале | false |
| |
| export_chart | Экспортировать chart в planning | false |
| enable_record_debug | Запись оталдочной информации в формате chart | false |
| default_front_clear_distance | Свободное пространство спереди в случае, если нет препятствий вокруг | 300 |
| max_trajectory_len | Максимально возможная длина траектории (метры) | 1000 |
| enable_rss_fallback | Откат rss | false |
| enable_rss_info | Включить rss_info в trajectory_pb | true |
| rss_max_front_obstacle_distance | Максимальное расстояние спереди до препятствия (метры) | 3000 |
| enable_planning_smoother | Включить более плавное планирование между различными циклами планирования | true |
| smoother_stop_distance | Если такое расстояние до точки остановки, то начинаем остановку (обеспечивает плавную остановку) | 10 |
| enable_parallel_hybrid_a | Включить Hybrid A* параллельную имплементацию | false |
| open_space_standstill_acceleration | Двигаться ли в месте остановки, м/с^2 | 0 |
| enable_dp_reference_speed | Снизить результат dp до крейсерской скорости по умолчанию | true |
| message_latency_threshold | Порог задержки сообщения | 0.02 |
| enable_lane_change_urgency_checking | Проверить срочность смены полосы движения | false |
| short_path_length_threshold | Порог для слишком короткого пути | 20 |
| trajectory_stitching_preserved_length | Количество сохраненных точек при сшивании траектории | 20 |
| side_pass_driving_width_l_buffer | Ширина бокового прохода l буфера | 0.1 |
| use_st_drivable_boundary | Использовать st_drivable границу при планировании скорости | true |
| enable_reuse_path_in_lane_follow | Включить повторное использование пути в дорожке следования  | false |
| use_smoothed_dp_guide_line | Штрафовать результат оптимизации скорости, чтобы он был близок к направляющей линии dp | false |
| use_soft_bound_in_nonlinear_speed_opt | Запретить мягкую границу в нелинейной скорости opt | true |
| use_front_axe_center_in_path_planning | Если при планировании траектории использовать центр передней оси, траектория может быть более быстрой | false |

</details>
