# Модуль мониторинга Supervisor

[Инструкция по созданию подмодулей](submodules.md)

[Визуальное оповещение](visualization.md)

[Описание Supervisor Python API](python_api.md)

[Мониторинг GNSS - IMU](monitor.md)

## Описание

Для наблюдения и визуализации работы узлов проекта OSCAR был разработан модуль **Supervisor**, целью которого являтся сбор информации о работоспособности программных компонент **Apollo**, *hardware* модулей и своевременное информирование оператора беспилотного автомобиля по нескольким каналам.

## Запуск

```bash
mainboard -d modules/supervisor/dag/sv_launch.dag
```
## Устройство модуля

![Устройство модуля](images/OSCAR_SV_UI.png)

**Supervisor** опрашивает все подмодули (**Submodule**), информирует оператора о состоянии модулей через специальный интерфейс.

**Guardian** - модуль, следящий за решениями **Supervisor** и реализующий действия, предписанные в канале */supervisor/decision* и связанные непосредственно с автомобилем и его безопасной остановкой.

**Supervisor API** - **API**, реализованное на *Python 3* и позволяющее взаимодействовать с модулем Supervisor в обход **Cyber API** по упрощенному сценарию. Использование:

```python
import supervisor_api
 
sv_test = supervisor_api.SupervisorPreferences()
```
**Web UI** - принцип взаимодействия с оператором через *Web-интерфейс*.

## Конфигурация

Для конфигурации модуля Supervisor перед запуском в директории */apollo/modules/supervisor/conf/* находится файл **preferences.yaml** со следующими структурами внутри:

```yaml
sv:
  sound_on: true
  debug_mode: false
gnss:
  debug_mode: false
  sound_on: false
imu:
  debug_mode: false
  sound_on: true
canbus:
  debug_mode: false
  sound_on: true
perception:
  debug_mode: false
  sound_on: true
```
Здесь:

**debug_mode** - режим отладки для всех модулей сразу (звукового сообщения и решений о торможении приниматься не будет);

**sound_on** - звуковое оповещение для всех модулей (При данном включенном параметре можно выключать звук у конкретных модулей, но при выключенном, звук будет выключен у всех модулей в обязательном порядке).

**Примечание!** При переезде на другую версию часто меняются названия библиотек и в соответствующих билдах их нужно обновить, чтобы **supervisor** заработал.