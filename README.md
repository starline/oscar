![logo](docs/oscar/images/main_page/truck_main_2024.png)

# OSCAR

Проект OSCAR (Open-Source CAR) - это открытая программно-аппаратная платформа для создания беспилотного автомобиля [4-го уровня автономности](https://en.wikipedia.org/wiki/Autonomous_car#Levels_of_driving_automation) на базе проекта [apollo.auto](https://apollo.auto) от [Baidu](https://www.baidu.com/).


## Документация

[Описание программно-аппаратной платформы](docs/oscar/soft_hard_description.md)

[Инcтрукция по установке и сборке проекта](docs/oscar/README.md)

<!-- [Работа с симулятором](docs/oscar/lgsvl.md) -->

[Исходная документация Apollo](README_apollo_en.md)


## Сообщество

Амбициозным студентам и активным специалистам из Open Source сообщетсва, ищущим возможность участия в проекте, можно ознакомиться с [публичным списоком задач проекта](docs/oscar/task_for_students.md).

Для технических вопросов и предложений по проекту используйте [issues](https://gitlab.com/groups/starline/-/issues) и [MR](https://gitlab.com/groups/starline/-/merge_requests) на соответствующий *gitlab* проект или группу.

В рамках проекта OSCAR ежегодно проводится **Хакатон  "StarLine Беспилотный"**, где участникам предоставлятся возможность разработать и представить решения задач, стоящих перед исследователями в области робототехники и автономного транспорта. Больше подробностей о регламенте можно найти на [официальной странице](https://robofinist.ru/event/1190) и на [официальном репозитории](https://gitlab.com/starline/hackathon/hackathon_2022) хакатона.

![hsl](docs/oscar/images/main_page/hsl_20.png)


## Alpha

[Alpha](https://alpha.starline.ru/) - универсальный программно-аппаратный комплекс, разработанный в рамках проекта OSCAR для управления транспортными средствами. Комплекс обеспечивает возможность цифрового управления (ускорение, торможение, руление, переключение коробки передач, световая индикация и т.д.), сбор телеметрии и данных сенсорики, а также контроль доступа и мониторинг состояния автомобиля.

ПАК Alpha может быть интегрирован на широкий спектр транспортных средств и имеет открытый протокол для взаимодействия.

Больше информации можно узнать:

* На сайте [alpha.starline.ru](https://alpha.starline.ru/)

* В публичном [репозитории проекта](https://gitlab.com/starline/alpha/alpha_docs)


## Контакты

С нами можно связаться по почте **smartcar@starline.ru**.

Или через группу в [Телеграм](https://t.me/oscar_community).

<!-- А еще у нас есть [Twitter](https://twitter.com/starline_oscar) и [YouTube](https://www.youtube.com/channel/UC1ZPtOXu7cr4HsQKRpElDnA). -->
