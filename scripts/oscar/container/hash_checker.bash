#!/usr/bin/env bash

#############################################################################
# Copyright 2024 ScPA StarLine Ltd. All Rights Reserved.                    #
#                                                                           #
# Created by Ivan Nenakhov <nenakhov.id@starline.com>                       #
#                                                                           #
# Licensed under the Apache License, Version 2.0 (the "License");           #
# you may not use this file except in compliance with the License.          #
# You may obtain a copy of the License at                                   #
#                                                                           #
# http://www.apache.org/licenses/LICENSE-2.0                                #
#                                                                           #
# Unless required by applicable law or agreed to in writing, software       #
# distributed under the License is distributed on an "AS IS" BASIS,         #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
# See the License for the specific language governing permissions and       #
# limitations under the License.                                            #
#############################################################################


OSCAR_ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../" && pwd -P )"
TARGET_FILE=$OSCAR_ROOT_DIR/docker/scripts/oscar_dev_setup.sh
HASH_FOLDER=$OSCAR_ROOT_DIR/.oscar_env
HASH_FILE=$HASH_FOLDER/oscar_dev_setup_hash

source $OSCAR_ROOT_DIR/scripts/apollo.bashrc

YELLOW="\033[1;33m"
NC='\033[0m'

function calculate_checksum() {

    local hash=($(sha256sum $TARGET_FILE))
    echo "$hash"
}

function check() {
    CURRENT_HASH=$(calculate_checksum)
    [ -f $HASH_FILE ] && SAVED_HASH=$(cat $HASH_FILE) || SAVED_HASH=""

    if [ "$CURRENT_HASH" != "$SAVED_HASH" ]; then
        echo -e ${YELLOW}[Warning] oscar_dev_setup.sh content is newer than was used at container creation. Rebuild container.
    fi
}

function update() {

    HASH=$(calculate_checksum)

    ! [ -d $HASH_FOLDER ] && mkdir -p $HASH_FOLDER

    echo $HASH > $HASH_FILE

    if [ -f "$HASH_FILE" ]; then
        ok Hash of docker/scripts/oscar_dev_estup.sh was written to $HASH_FILE
    else
        error $HASH_FILE was not created
    fi
}

function main() {
    [ $# -eq 0 ] && echo "No argument (check/update) provided. Exiting" && exit 1

    if [ "$1" = "check" ]; then
        check
    elif [ "$1" = "update" ]; then
        update
    else
        echo "Invalid argument: $1. Possible values are check/update"
        exit 1
    fi
}

main $@
