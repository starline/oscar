EXEC_PATH=$PWD
APOLLO_ROOT_DIR="/apollo/"

function print_help() {
    printf "bash generate_additional_map_files.sh MAP_DIR \n"
}

function generate_bin_map(){
    $APOLLO_ROOT_DIR/bazel-bin/modules/map/tools/bin_map_generator --map_dir=$1 --output_dir=$1
}

function generate_routing_map() {
    cd $APOLLO_ROOT_DIR
    bash $APOLLO_ROOT_DIR/scripts/generate_routing_topo_graph.sh --map_dir "$1"
    cd $EXEC_PATH
}

function generate_sim_map(){
    cd "$1"
    cp base_map.txt sim_map.txt
    cd $EXEC_PATH
}

if [ "$#" == 0 ] || [ "$1" == "--help" ] || [ "$1" == "-h" ]; then
    print_help

elif [ -d "$1" ]; then
    dir=$1

    if [[ "${dir}" == */ ]]; then
            dir="${dir: : -1}"
    fi

    generate_sim_map "$dir"
    generate_bin_map "$dir"
    generate_routing_map "$dir"

else
    echo "Folder "$1" do not exists."
fi
