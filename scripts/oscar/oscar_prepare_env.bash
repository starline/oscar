#!/usr/bin/env bash

OSCAR_ROOT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../../" && pwd -P)"
SETUP_SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/oscar_tools" && pwd -P)"

source $OSCAR_ROOT_DIR/scripts/apollo.bashrc

function add_line_to_file() {
    grep -qxF "$1" $2 && return 1 || echo $1 >>$2
    return 0
}

function add_oscar_commands() {
    add_line_to_file "source $SETUP_SCRIPT_DIR/setup.sh" ~/.bashrc
    if [ "$?" -eq 0 ]; then
        ok "OSCAR commands were added to ~/.bashrc"
    else
        info "OSCAR commands already sourced to ~/.bashrc"
    fi

    source ~/.bashrc
}

function sudo_add_line_to_file() {
    grep -qxF "$1" $2 && return 1 || sudo bash -c "echo $1 >> $2"
    return 0
}

function find_disk_in_fstab() {
    grep -q "$1" "$2"
    return $?
}

function create_symlink_to_disk() {
    # creates symlink in /apollo/data/bag

    SSD_SRC_DIR=$1
    LABEL=$(basename $1)

    SYMLINK_DIR=data/bag

    mkdir -p $OSCAR_ROOT_DIR/$SYMLINK_DIR
    cd $OSCAR_ROOT_DIR/$SYMLINK_DIR

    if ! [ -e "$LABEL" ]; then
        ln -s $SSD_SRC_DIR $LABEL
        ok "$SYMLINK_DIR/$LABEL: Symlink to external SSD created"
    else
        info "$SYMLINK_DIR/$LABEL: Symlink to external SSD already exists"
    fi
}

function main() {
    add_oscar_commands
    create_symlink_to_disk /media/data_ssd_8tb

}

main $@
