#!/usr/bin/env bash


OSCAR_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../../../" && pwd -P )"


# License of SDK permits distribution. The script below tries to pull it from StarLine inner server.
# In case of not finding a folder with name $CAMERA_DRIVER_DIR (see below for details) Lucid driver directory
# is added to bazelignore list to prevent building it.
# Arena SDK is availabe to download manually from https://thinklucid.com/arena-software-development-kit/
# You should unpack the archive ArenaSDK_Linux_x64 to $OSCAR_ROOT/modules/oscar/drivers/camera/lucid in order to use Lucid Camera


CAMERA_DRIVER_DIR=modules/oscar/drivers/camera
ARENA_SDK_DIR_ABS=$OSCAR_ROOT/$CAMERA_DRIVER_DIR/lucid/ArenaSDK_Linux_x64

WEBSITE=git.starline.ru
PROJECT=oscar/drivers/arena_sdk

SSH_REPO=git@$WEBSITE:$PROJECT.git
HTTPS_REPO=https://$WEBSITE/$PROJECT

echo -e "\nTrying to pull Arena SDK with SSH, then with HTTPS."
git clone $SSH_REPO $ARENA_SDK_DIR_ABS  2>/dev/null || git clone $HTTPS_REPO $ARENA_SDK_DIR_ABS 2>/dev/null

if ! [ -d $ARENA_SDK_DIR_ABS ] ; then
    echo "Download failed. Adding Lucid camera driver to .bazelignore list"

    if ! grep -Fxq $CAMERA_DRIVER_DIR $OSCAR_ROOT/.bazelignore; then
        echo -e "\n$CAMERA_DRIVER_DIR\n" >> $OSCAR_ROOT/.bazelignore
        truncate -s -1 $OSCAR_ROOT/.bazelignore
    fi
else
    echo "Arena SDK is installed"
fi
