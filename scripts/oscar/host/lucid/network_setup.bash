#!/usr/bin/env bash

set -e

OSCAR_ROOT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../../../../" && pwd -P)"

source $OSCAR_ROOT_DIR/scripts/apollo.bashrc

running_containers="$(docker container list --format '{{.Names}}' | grep apollo_dev_$USER)"

[ "$running_containers" == apollo_dev_$USER ] && DOCKER_RUNNING=true || DOCKER_RUNNING=false

[ $DOCKER_RUNNING = false ] && oscar docker start

set +e

LIB_INSTALLED=$(docker exec -u $USER apollo_dev_$USER \
    ldconfig -p | grep libarena.so)
set -e

[[ "$LIB_INSTALLED" == "" ]] && info "Camera driver not built. Skipping network setup" && exit

MATCHED_IPS=$(docker exec -u $USER apollo_dev_$USER \
    /apollo/modules/oscar/drivers/camera/lucid/ArenaSDK_Linux_x64/Utilities/IpConfigUtility /list |
    grep -o -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' || echo "")

[ "$MATCHED_IPS" = "" ] && exit
[ $DOCKER_RUNNING = false ] && oscar docker stop

CAMERA_CONNECTED=false

for IP in $MATCHED_IPS; do
    case $IP in
    255.255.*.*)
        # echo mask
        ;;
    *.*.0.1)
        # echo Gateway
        ;;
    *)
        CAMERA_CONNECTED=true
        INTERFACE=$(ip route get $IP | grep -Po '(?<=(dev ))(\S+)')
        CURRENT_MTU=$(cat /sys/class/net/$INTERFACE/mtu)
        [ $CURRENT_MTU != 9000 ] || sudo ifconfig $INTERFACE mtu 9000
        info "Set mtu 9000 for $INTERFACE connected to Lucid camera at $IP"
        ;;
    esac
done

grep -qxF 'net.core.rmem_default=1048576' /etc/sysctl.conf || sudo echo 'net.core.rmem_default=1048576' >>/etc/sysctl.conf && sudo sysctl -p -q &&
    grep -qxF 'net.core.rmem_max=1048576' /etc/sysctl.conf || sudo echo 'net.core.rmem_max=1048576' >>/etc/sysctl.conf && sudo sysctl -p -q
