#!/usr/bin/env bash

# set -e

# This script is used to automatically check
# if current local branch is up to date with
# remote default branch (develop/master) and notify about new commits.

OSCAR_ROOT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../../../" && pwd -P)"

TIMEOUT=5s
TIMEOUT_ERROR_CODE=124
FETCH_ERROR_CODE=128

source $OSCAR_ROOT_DIR/scripts/apollo.bashrc

function check_folder() {

    cd $1

    REPO=$(basename $1)

    ! [ -d "$1/.git" ] && error "$1 is not a git repository" && return 1

    FETCH_OUTPUT=$(GIT_ASKPASS=true timeout $TIMEOUT git fetch origin 2>&1)
    ec=$?

    if [ $ec -eq $TIMEOUT_ERROR_CODE ]; then
        error "git fetch timeout ($TIMEOUT) exceeded: $1. $FETCH_OUTPUT"
        return 0
    elif [ $ec -eq $FETCH_ERROR_CODE ]; then
        warning "Cannot automatically fetch updates: $1. Ensure using SSH access or HTTP public repo. Used remote url: $(git remote get-url origin)"
        return 0

    elif [ $ec -ne 0 ]; then
        error $ec 
        error "Could not fetch remote source for: $1. $FETCH_OUTPUT"
        return 0
    fi

    # if develop branch exists on remote, track it. Otherwise, track master
    git rev-parse --verify origin/develop >/dev/null 2>&1 && MAIN_BRANCH=develop || MAIN_BRANCH=master

    COMMITS_TO_PULL=$(git rev-list --left-only --count origin/$MAIN_BRANCH...HEAD)

    if [ $COMMITS_TO_PULL -gt 0 ]; then
        warning "$REPO: There are $COMMITS_TO_PULL new commits in branch: origin/$MAIN_BRANCH. Consider merging changes with current branch."
    else
        ok "$REPO: Current branch is up to date with branch: origin/$MAIN_BRANCH."
    fi

}

function check_recursively() {

    find $1 -name .git -type d -prune -print0 | while read -d $'\0' file; do
        directory=$(dirname $file)
        check_folder $directory
    done

}

check_folder $OSCAR_ROOT_DIR
check_recursively $OSCAR_ROOT_DIR/modules/oscar/drivers
check_recursively $OSCAR_ROOT_DIR/modules/oscar/alpha
check_recursively $OSCAR_ROOT_DIR/modules/calibration/data
